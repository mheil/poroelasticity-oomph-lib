Instructions:
-------------

-- Build:

      make chris

-- To find out what options are available

      ./chris --help

-- The bash script 

     chris_quick_run.bash

   contains sample command line arguments. If the code is run
   as shown there, it will write the results into directory RESLT 
   (and will moan if that doesn't exist).

-- The bash script

     run_chris.bash

   is more comprehensive (and therefore complex) and allows 
   automatic parameter studies. Currently runs all the executables
   simutaneously so it may thrash machines with little memory.
   Outputs comparison into compare.pdf in specified run directory.
   This can all be changed BUT was mainly developed by/for MH. 
   Lesser mortals may prefer alternative approaches.

-- The bash script

      plot_chris.bash

   converts "all" *.dat files into paraviewable vtu/vtp/pvd files.
   May run a long time and ultimately use lots of diskspace.

   To run, cd into the directory that contains the results first!
   This script is run automatically by run_chris.bash to do the
   post-processing.

-- Once plot_chris.bash is run, the following pvsm files can be
   loaded into paraview to visualise the results (they all
   assume that paraview is launched in the directory that contains
   the results).

     -- chris.pvsm

        Default visualisation, used by run_chris.bash, say,
        with scaling factors etc. set for a 100Pa (Re=6000)
        periodic forcing.
 
    -- porous_flux_test.pvsm

        Shows Navier-Stokes and pore pressures (scaled to the
        same "height") to show their continuity and the variation
        across the porous region (the syrinx cover). Plot in bottom
        right shows SSS Navier-Stokes pressure which flies out of the
        window most of the time. Scaling again appropriate for 100Pa
        (Re=6000).

    -- regions.pvsm

        Plots the different regions in the two solid meshes.
        Note: Switching to "Surface With Edges" display shows elements too.

    -- regularly_spaced.pvsm

        Not totally successful attempt at plotting flow fields at
        equally spaced points within the fluid and solid domains.
        Main problem is simply the aspect ratio of the domain...
        (Also, locate_zeta(...) misses out a few plot points so they're
        not totally regularly spaced; MH can fix this if we really want
        to go for this way of representing the data (doubt it).

    -- sss_fsi.pvsm

        Test/sanity check of fsi coupling on SSS FSI interfaces
        (continuity of velocity (BJS) and fluid traction acting 
        onto solid).

    -- syrinx_fsi.pvsm

        Ditto for syrinx.

    -- meshes.pvsm

        Shows the meshes used in the various regions.

-- Comparison against ADINA:

    -- Wall displacement on syrinx fsi boundary:

       Run 

            compare_solid_with_adina.bash 

       Currently this produces:

         r_poro_fsi_syrinx.dat
         z_poro_fsi_syrinx.dat
         ur_poro_fsi_syrinx.dat
         uz_poro_fsi_syrinx.dat

       which should be matlab-able. Internal sanity check done
       by re-processing this data yet again to turn it into paraview-able
       form which can be viewed with

            adina_visualisation_solid.pvsm

       whic animates the wall displacement field.

    -- Navier-Stokes flow field on internal sss fsi boundaries:

       Run 

            compare_fluid_with_adina.bash 

       Currently this produces:
        
         r_bjs_fsi_inner_sss.dat
         z_bjs_fsi_inner_sss.dat
         vr_bjs_fsi_inner_sss.dat
         vz_bjs_fsi_inner_sss.dat
         p_bjs_fsi__inner_sss.dat

       which should be matlab-able. Internal sanity check done
       by re-processing this data yet again to turn it into paraview-able
       form which can be viewed with

            adina_visualisation_fluid.pvsm

       which animates the velocity vectors, coloured by pressure
       on the sss fsi boundaries.



