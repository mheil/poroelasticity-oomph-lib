#! /bin/bash

if [ $# -eq 0 ]; then
    echo "Usage: Specify --clean_up to clean up or specify stems"
    exit
fi
if [ $1 == "--clean_up" ]; then
    echo " "
    echo "------------------------------------------------------------ "
    echo " "
    echo "Cleaning up: removing all *.dat, *.vtu, *vtp, *.pvd files"
    echo "in directory"
    echo " " 
    
    echo "      "`pwd`
    echo " " 
    nfiles=`ls *.dat *.vtu *.vtp *.pvd | wc -w `
    echo " " 
    echo "About to remove a total of : "$nfiles" files"
    echo " " 
    echo "Enter anything to continue..."
    read junk
    rm *.dat *.vtu *.vtp *.pvd
    exit
fi
stem_list=$@
echo "Interpreting the "$#" command line args:"
echo " " 
echo "   " $stem_list
echo " "
echo "as stems. "
echo " "
echo "Enter anything to continue..."
read junk


# Multiplier for all lengths/displacements (in Meters). Inner diameter
# of dura at "inlet": 20mm
lengthscale=20.0e-3

# Timestep [in sec]
dt=0.005

# Reynolds number
#re=6000.0 # about 100 Pa
re=14140.0 # 500 Pa


# Velocity scale factor (in meters per second), to be multiplied
# by Reynolds number (only depends on material properties; shouldn't
# need updating)
F_u=5.0e-5

# Pressure scale factor (in Pascals), to be multiplied
# by Reynolds number (only depends on material properties; shouldn't
# need updating)
F_p=2.5e-6


#-------------------------------------------------------------------


#stem_list="cord11h_f.sh.hi.allSAS" #cord11h_f.sh.hi.allSyr" #cord11h_f.sh.hi.allSAS" # "cord11h_f.allSAS" # "cord11h_f.allSy"
for stem in `echo $stem_list`; do

r_adina_file=$stem".coordY.1st"
z_adina_file=$stem".coordZ.1st"
vr_adina_file=$stem".velY"
vz_adina_file=$stem".velZ"
p_adina_file=$stem".press"
ur_adina_file=$stem".dispY"
uz_adina_file=$stem".dispZ"
strip_control_m.bash $r_adina_file
strip_control_m.bash $z_adina_file
strip_control_m.bash $vr_adina_file
strip_control_m.bash $vz_adina_file
strip_control_m.bash $p_adina_file
strip_control_m.bash $ur_adina_file
strip_control_m.bash $uz_adina_file

mesh_motion_list="1"
for mesh_motion_flag in `echo $mesh_motion_list`; do

mesh_motion_suffix=""
if [ $mesh_motion_flag == 1 ]; then
    mesh_motion_suffix="with_mesh_motion"
else
    mesh_motion_suffix="without_mesh_motion"
fi

# is_num trick is from
# http://rosettacode.org/wiki/Determine_if_a_string_is_numeric:
# "The following function uses the fact that non-numeric strings in 
# AWK are treated as having the value 0 when used in arithmetics, but 
# not in comparison"
awk  -v lengthscale=$lengthscale 'function is_num(x){return(x==x+0)}{if ((NF!=0)&&(is_num($1)!=0)){printf(" %e\n",$2/lengthscale)}}' $r_adina_file > r_adina.dat
awk  -v lengthscale=$lengthscale 'function is_num(x){return(x==x+0)}{if ((NF!=0)&&(is_num($1)!=0)){printf(" %e\n",$2/lengthscale)}}' $z_adina_file > z_adina.dat


if [ $mesh_motion_flag == 1 ]; then
    awk -v lengthscale=$lengthscale 'function is_num(x){return(x==x+0)}BEGIN{count=0; filename=""}
{
if ($1=="Time")
{
filename="junk_ur"count".dat"; count++
}
else 
{
if ((NF!=0)&&(is_num($1)!=0)){printf(" %e\n",$2/lengthscale) > filename}
}
}
' $ur_adina_file


    awk -v lengthscale=$lengthscale 'function is_num(x){return(x==x+0)}BEGIN{count=0; filename=""}
{
if ($1=="Time")
{
filename="junk_uz"count".dat"; count++
}
else 
{
if ((NF!=0)&&(is_num($1)!=0)){printf(" %e\n",$2/lengthscale) > filename}
}
}
' $uz_adina_file



fi



awk  -v re=$re -v f_u=$F_u 'function is_num(x){return(x==x+0)}BEGIN{count=0; filename=""}
{
if ($1=="Time")
{
filename="junk_vr"count".dat"; count++
}
else 
{
if ((NF!=0)&&(is_num($1)!=0)){printf(" %e\n",$2/(re*f_u)) > filename}
}
}
' $vr_adina_file



awk -v re=$re -v f_u=$F_u  'function is_num(x){return(x==x+0)}BEGIN{count=0; filename=""}
{
if ($1=="Time")
{
filename="junk_vz"count".dat"; count++
}
else 
{
if ((NF!=0)&&(is_num($1)!=0)){printf(" %e\n",$2/(re*f_u)) > filename}
}
}
' $vz_adina_file


awk  -v re=$re -v f_p=$F_p 'function is_num(x){return(x==x+0)}BEGIN{count=0; filename=""}
{
if ($1=="Time")
{
filename="junk_p"count".dat"; count++
}
else 
{
if ((NF!=0)&&(is_num($1)!=0)){printf(" %e\n",$2/(re*f_p)) > filename}
}
}
' $p_adina_file


file_list1=""
file_list2=""
file_list3=""
file_list1=`ls junk_p?.dat ` 
file_list2=`ls junk_p??.dat ` 
file_list3=`ls junk_p???.dat ` 
file_list=$file_list1" "$file_list2" "$file_list3


count=0
for file in `echo $file_list`; do 
    echo "Doing file junk_p"`echo $count`".dat"
    if [ $mesh_motion_flag == 1 ]; then
        paste -d "" r_adina.dat junk_ur`echo $count`.dat > tmp_r_adina.dat
        paste -d "" z_adina.dat junk_uz`echo $count`.dat > tmp_z_adina.dat
        awk '{print $1+$2" "}' tmp_r_adina.dat > tmp_r_adina2.dat
        awk '{print $1+$2}' tmp_z_adina.dat > tmp_z_adina2.dat
        paste -d "" tmp_r_adina2.dat tmp_z_adina2.dat junk_vr`echo $count`.dat junk_vz`echo $count`.dat junk_p`echo $count`.dat > adina_nst_`echo $stem`_`echo $mesh_motion_suffix``echo $count`.dat
    else
        paste -d "" r_adina.dat z_adina.dat junk_vr`echo $count`.dat junk_vz`echo $count`.dat junk_p`echo $count`.dat > adina_nst_`echo $stem`_`echo $mesh_motion_suffix``echo $count`.dat
    fi
    let count=count+1
done


oomph-convert -o -z -p2 adina_nst_`echo $stem`_`echo $mesh_motion_suffix`*.dat
makePvd adina_nst_`echo $stem`_`echo $mesh_motion_suffix` adina_nst_`echo $stem`_`echo $mesh_motion_suffix`.pvd


done
done
rm junk*.dat
rm tmp*.dat
