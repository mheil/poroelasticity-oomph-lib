#! /bin/bash

#! /bin/bash

if [ $# -eq 0 ]; then
    echo "Usage: Specify --clean_up to clean up or specify stems"
    exit
fi
if [ $1 == "--clean_up" ]; then
    echo " "
    echo "------------------------------------------------------------ "
    echo " "
    echo "Cleaning up: removing all *.dat, *.vtu, *vtp, *.pvd files"
    echo "in directory"
    echo " " 
    
    echo "      "`pwd`
    echo " " 
    nfiles=`ls *.dat *.vtu *.vtp *.pvd | wc -w `
    echo " " 
    echo "About to remove a total of : "$nfiles" files"
    echo " " 
    echo "Enter anything to continue..."
    read junk
    rm *.dat *.vtu *.vtp *.pvd
    exit
fi
stem_list=$@
echo "Interpreting the "$#" command line args:"
echo " " 
echo "   " $stem_list
echo " "
echo "as stems. "
echo " "
echo "Enter anything to continue..."
read junk


# Multiplier for all lengths/displacements (in Meters). Inner diameter
# of dura at "inlet": 20mm
lengthscale=20.0e-3

# Timestep [in sec]
dt=0.005

#-------------------------------------------------------------------

for stem in `echo $stem_list`; do

r_adina_file=$stem".coordY.1st"
z_adina_file=$stem".coordZ.1st"
ur_adina_file=$stem".dispY"
uz_adina_file=$stem".dispZ"
strip_control_m.bash $r_adina_file
strip_control_m.bash $z_adina_file
strip_control_m.bash $ur_adina_file
strip_control_m.bash $uz_adina_file


# is_num trick is from
# http://rosettacode.org/wiki/Determine_if_a_string_is_numeric:
# "The following function uses the fact that non-numeric strings in 
# AWK are treated as having the value 0 when used in arithmetics, but 
# not in comparison"
awk  -v lengthscale=$lengthscale 'function is_num(x){return(x==x+0)}{if ((NF!=0)&&(is_num($1)!=0)){printf(" %e\n",$2/lengthscale)}}' $r_adina_file > r_adina.dat
awk  -v lengthscale=$lengthscale 'function is_num(x){return(x==x+0)}{if ((NF!=0)&&(is_num($1)!=0)){printf(" %e\n",$2/lengthscale)}}' $z_adina_file > z_adina.dat


awk -v lengthscale=$lengthscale 'function is_num(x){return(x==x+0)}BEGIN{count=0; filename=""}
{
if ($1=="Time")
{
filename="junk_ur"count".dat"; count++
}
else 
{
if ((NF!=0)&&(is_num($1)!=0)){printf(" %e\n",$2/lengthscale) > filename}
}
}
' $ur_adina_file


awk -v lengthscale=$lengthscale 'function is_num(x){return(x==x+0)}BEGIN{count=0; filename=""}
{
if ($1=="Time")
{
filename="junk_uz"count".dat"; count++
}
else 
{
if ((NF!=0)&&(is_num($1)!=0)){printf(" %e\n",$2/lengthscale) > filename}
}
}
' $uz_adina_file


file_list1=""
file_list2=""
file_list3=""
file_list1=`ls junk_ur?.dat ` 
file_list2=`ls junk_ur??.dat ` 
file_list3=`ls junk_ur???.dat ` 
file_list=$file_list1" "$file_list2" "$file_list3


count=0
for file in `echo $file_list`; do 
    echo "Doing file junk_ur"`echo $count`".dat"
    paste -d "" r_adina.dat junk_ur`echo $count`.dat > tmp_r_adina.dat
    paste -d "" z_adina.dat junk_uz`echo $count`.dat > tmp_z_adina.dat
    awk '{print $1+$2" "}' tmp_r_adina.dat > tmp_r_adina2.dat
    awk '{print $1+$2}' tmp_z_adina.dat > tmp_z_adina2.dat
    paste -d "" r_adina.dat z_adina.dat junk_ur`echo $count`.dat junk_uz`echo $count`.dat  > adina_displ_`echo $stem`_`echo $count`.dat
    let count=count+1
done


oomph-convert -o -z -p2 adina_displ_`echo $stem`_*.dat
makePvd adina_displ_`echo $stem`_ adina_displ_`echo $stem`.pvd


done

rm junk*.dat
rm tmp*.dat



exit

#####################################################################
#####################################################################


for stem in `echo $stem_list`; do

r_adina_file=$stem".coordY.1st"
z_adina_file=$stem".coordZ.1st"
ur_adina_file=$stem".dispY.out"
uz_adina_file=$stem".dispZ.out"

strip_control_m.bash $r_adina_file
strip_control_m.bash $z_adina_file
strip_control_m.bash $ur_adina_file
strip_control_m.bash $uz_adina_file

awk '{if ((NF!=0)&&($1!="%")){printf(" %e\n",$2)}}' $r_adina_file > r_adina.dat
awk '{if ((NF!=0)&&($1!="%")){printf(" %e\n",$2)}}' $z_adina_file > z_adina.dat
 

ncols=`awk 'BEGIN{done=0}{if (done==0){if ((NF!=0)&&($1!="%")){print NF; done=1}}}' $ur_adina_file`

count=1;
while [ $count -lt $ncols ]; do
  filename=`echo "adina_displ_"$stem"_"$count".dat"`
  echo "filename = "$filename
  let count=$count+1
  echo "doing step $count ur"
  awk -v col=$count 'BEGIN{first=1}{if (first==1){first=0} else {if ((NF!=0)&&($1!="%")){printf(" %e\n",$col)}}}' $ur_adina_file > ur_col.dat
  echo "doing step $count uz"
  awk -v col=$count 'BEGIN{first=1}{if (first==1){first=0} else {if ((NF!=0)&&($1!="%")){printf(" %e\n",$col)}}}' $uz_adina_file > uz_col.dat
  paste -d "" r_adina.dat z_adina.dat ur_col.dat uz_col.dat > $filename
done

oomph-convert -z -p2 adina_displ_`echo $stem`*.dat 
makePvd adina_displ_$stem adina_displ_$stem.pvd
rm r_adina.dat
rm z_adina.dat
rm ur_col.dat
rm uz_col.dat

done
 


