makePvd soln-syrinx-fluid soln-syrinx-fluid.pvd
makePvd soln-sss-fluid soln-sss-fluid.pvd
makePvd soln-outer-poro soln-outer-poro.pvd
makePvd soln-inner-poro soln-inner-poro.pvd

oomph-convert -z -p2 poro_fsi_syrinx*.dat
makePvd poro_fsi_syrinx poro_fsi_syrinx.pvd
oomph-convert -z -p2 bjs_fsi_syrinx*.dat
makePvd bjs_fsi_syrinx bjs_fsi_syrinx.pvd

oomph-convert -z -p2 poro_fsi_sss*.dat
makePvd poro_fsi_sss poro_fsi_sss.pvd
oomph-convert -z -p2 bjs_fsi_sss*.dat
makePvd bjs_fsi_sss bjs_fsi_sss.pvd
oomph-convert -z -p2 bjs_fsi_inner_sss*.dat
makePvd bjs_fsi_inner_sss bjs_fsi_inner_sss.pvd

oomph-convert -z -p2 regular_syrinx_fluid*.dat 
makePvd regular_syrinx_fluid regular_syrinx_fluid.pvd
oomph-convert -z -p2 regular_sss_fluid*.dat 
makePvd regular_sss_fluid regular_sss_fluid.pvd
oomph-convert -z -p2 regular_inner_poro*.dat
makePvd regular_inner_poro regular_inner_poro.pvd
oomph-convert -z -p2 regular_outer_poro*.dat
makePvd regular_outer_poro regular_outer_poro.pvd
oomph-convert -z soln-inner-poro-syrinx-cover*.dat
makePvd soln-inner-poro-syrinx-cover soln-inner-poro-syrinx-cover.pvd

oomph-convert -z *region.dat
oomph-convert *mesh.dat
