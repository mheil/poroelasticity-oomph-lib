#! /bin/bash

fake_prefix=""
#fake_prefix="fake_"

first_step=341
nsteps_per_period=80


data_dir=`pwd`
echo "Data directory: "$data_dir

# Master run directory; make and move into it
#--------------------------------------------
run_dir=CYCLE_AVERAGE_FIRST_`echo $first_step`_N_`echo $nsteps_per_period`
if [ -e "$run_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $run_dir and try again"
    echo " "
    exit
fi
mkdir $run_dir
cd $run_dir


stem_list="stripped_poro_output_on_fsi_syrinx_boundary stripped_poro_output_on_fsi_inner_sss_boundary"
for stem in `echo $stem_list`; do

    step=$first_step
    count=0
    while [ $count -lt $nsteps_per_period ]; do
        file=$stem`printf "%05d" $step`".dat"
        if [ "$fake_prefix" != "" ]; then
            echo "validation currently broken!"
            exit
            cp $data_dir/$file `echo $fake_prefix``echo $file`
            file=`echo $fake_prefix``echo $file`
            echo "FILE: "$file
            pseudo_sin=`date | awk -v i=$count -v n=$nsteps_per_period '{print sin(i/n*2.0*4.0*atan2(1,1))}'`
            echo "pseudo sin: $count $pseudo_sin"
            awk -v i=$count -v n=$nsteps_per_period '{if ($1=="ZONE"){print $0}else{print $1 " " $2 " " $3 " " $4 " " cos($2)*(30+14000*sin(i/n*2.0*4.0*atan2(1,1)))}}' $file > .junk
            mv .junk $file
        fi
        echo "file: " $file
        #oomph-convert -z -p2 $file
        #---------------------------------------------------------------
        # qr and qz are columnx 8 and 9; nr and nz are columns 14 and 15
        #---------------------------------------------------------------
        awk '{if ($1!="ZONE"){print $1 " " $2 " " ($8*$14+$9*$15)*$14 " " ($8*$14+$9*$15)*$15 }}' $data_dir/$file > extracted_r_z_qn_r_qn_z_`echo $file`
        if [ $count -eq 0 ]; then
            cp extracted_r_z_qn_r_qn_z_`echo $file` .tmp_sum
        else
            paste extracted_r_z_qn_r_qn_z_`echo $file` .tmp_sum > .both.dat
            awk '{{print $1 " " $2 " " $3+$7 " "$4+$8 }}' .both.dat > .tmp_sum
        fi
        oomph-convert -o -p2 -z extracted_r_z_qn_r_qn_z_`echo $file`
        let step=$step+1
        let count=$count+1
    done
    
    awk -v n=$nsteps_per_period '{print $1 " " $2 " " $3/n " " $4/n}' .tmp_sum | sort -g -k 2 > cycle_average_`echo $stem`.dat

    makePvd extracted_r_z_qn_r_qn_z_`echo $stem` extracted_r_z_qn_r_qn_z_`echo $stem`.pvd
    oomph-convert -o -p2 cycle_average_`echo $stem`.dat
done

cd $data_dir
