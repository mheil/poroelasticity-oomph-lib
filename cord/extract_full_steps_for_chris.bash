stem_list="poro_output_on_fsi_syrinx_boundary poro_output_on_fsi_inner_sss_boundary"

# Chris directory
#----------------
chris_dir="FULL_STEPS"
if [ -e "$chris_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $chris_dir and try again"
    echo " "
    exit
fi
mkdir $chris_dir

# Initial condition
cp trace_header.dat `echo $chris_dir`/trace_full_steps_only.dat
awk '{if (NR==1){print $0}}' trace.dat >> `echo $chris_dir`/trace_full_steps_only.dat
for stem in `echo $stem_list`; do
    full_step=0
    old_file=`echo $stem``echo $full_step`.dat
    count=0
    new_file=stripped_`echo $stem``printf "%05d" $count`.dat
    echo "FILES: " $old_file " " $new_file
    awk '{if ($1!="ZONE"){print $0}}' `echo $old_file` | sort -n -k 2 > `echo $chris_dir/$new_file`
done

# Remaining steps
full_step_file_list=`ls full_step*.dat`
count=0
for full_step_file in `echo $full_step_file_list`; do
    full_step=`cat full_step$count.dat`
    echo $count" : "$full_step
    awk -v full_step=$full_step '{if ($NF==full_step){print $0}}' trace.dat >> `echo $chris_dir/trace_full_steps_only.dat`

    let count=$count+1

    for stem in `echo $stem_list`; do
        old_file=`echo $stem``echo $full_step`.dat
        new_file=stripped_`echo $stem``printf "%05d" $count`.dat
        echo "FILES: " $old_file " " $new_file
        awk '{if ($1!="ZONE"){print $0}}' `echo $old_file` | sort -n -k 2 > `echo $chris_dir/$new_file`
    done



done


exit
