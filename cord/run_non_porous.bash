#! /bin/bash

# Important files to be moved
important_files_list="chris chris.cc $0"


# Permanent storage directory
#----------------------------
#root_permanent_storage_dir="/home/ds413/matthias/chris_scratch"
#if [ ! -e "$root_permanent_storage_dir" ];  then
#    echo " "
#    echo "ERROR: Root permanent storage directory $root_permanent_storage_dir doesn't exist. Please create it."
#    echo " "
#    exit
#else
#    echo " "
#    echo "Root permanent storage directory $root_permanent_storage_dir exists. Good!"
#    echo " "
#fi



# Master run directory
#---------------------
run_dir="NEW_RUN_non_porous_orig" 
#run_dir="/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/NEW_RUNS_k1em14_BL_factor0.25" 
if [ -e "$run_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $run_dir and try again"
    echo " "
    exit
fi


# Permemant storage directory; make it
#-------------------------------------
#if [ -e "$root_permanent_storage_dir/$run_dir" ];  then
#    echo " "
#    echo "ERROR: Please delete directory $root_permanent_storage_dir/$run_dir and try again"
#    echo " "
#    exit
#fi
#mkdir $root_permanent_storage_dir/$run_dir
#echo "Permanent storage dir: $root_permanent_storage_dir/$run_dir"


# Make master run dir and move into it
#-------------------------------------
mkdir $run_dir
echo " "
echo "Run dir: "`pwd`"/$run_dir"

cp $important_files_list "$run_dir"
cd $run_dir


# Generic element area for fluid
#-------------------------------
el_area_fluid=0.01

# Generic element area for solid
#-------------------------------
el_area_solid=0.01

# Default for Biot parameter alpha
#---------------------------------
alpha=0.0

# Drained Poisson's ratio (usually 0.35!)
#----------------------------------------
drained_nu=0.35

# All inner solid porous
#-----------------------
do_all_inner_porous=0 # hierher: 0 or 1 for all porous or only syrinx cover
all_porous_dir_label=""
if [ $do_all_inner_porous -eq 1 ]; then
    all_porous_dir_label="_all_inner_solid_porous"
    # 0.4 is a good value for discretisation of edge length for standard resolution in syrinx cover
    #----------------------------------------------------------------------------------------------
    ds_syrinx_poro_fsi_boundary_raw=0.4
    ds_syrinx_poro_fsi_boundary_near_sing_raw=0.04
    delta_syrinx_poro_fsi_boundary_near_sing_raw=0.4
    bl_poro_scaling_factor=0.25 # 1.0    # acts on bl thickness and target element area in bl

    all_inner_porous_flags="--cord_pia_and_filum_are_porous --ds_syrinx_poro_fsi_boundary_raw $ds_syrinx_poro_fsi_boundary_raw --ds_syrinx_poro_fsi_boundary_near_sing_raw $ds_syrinx_poro_fsi_boundary_near_sing_raw --delta_syrinx_poro_fsi_boundary_near_sing_raw $delta_syrinx_poro_fsi_boundary_near_sing_raw --bl_poro_scaling_factor $bl_poro_scaling_factor "
fi


# Do proper poro-elasticity or Darcy only
#----------------------------------------
proper_poro_elasticity_flag_list="1" # "0 1" # hierher: 0 or 1 for darcy only or proper poro elasticity
for proper_poro_elasticity_flag in `echo $proper_poro_elasticity_flag_list`; do

if [ $proper_poro_elasticity_flag == 0 ]; then
    alpha=0.0
    drained_nu=0.49
else
    if [ $proper_poro_elasticity_flag == 1 ]; then
        alpha=1.0
        drained_nu=0.35
    else
        echo "Wrong proper_poro_elasticity_flag"
        exit
    fi
fi 

# Flag for drained Poisson's ratio (only used for porous runs; switched off
# if Darcy is pinned below
drained_nu_flag=" --drained_nu $drained_nu "

# Mesh motion
#------------
mesh_motion_list_list="0" # "0" # "1" #"-1 0 1"
for mesh_motion_flag in `echo $mesh_motion_list_list`; do

if [ $mesh_motion_flag  == -1 ]; then
    mesh_motion_dir_label="_fixed_fluid_mesh"
fi 
if [ $mesh_motion_flag  == 0 ]; then
    mesh_motion_dir_label="_implicit_fluid_mesh_update"
fi 
if [ $mesh_motion_flag  == 1 ]; then
    mesh_motion_dir_label="_lagged_fluid_mesh_update"
fi 

# Stress divergence form of equations?
#-------------------------------------
non_stress_div_list="1"
for non_stress_div in `echo $non_stress_div_list`; do


non_stress_div_flag=" "
non_stress_div_dir_label="_stress_div_form"
if [ $non_stress_div  == 1 ]; then
    non_stress_div_flag=" --use_non_stress_divergence_form_for_navier_stokes "
    non_stress_div_dir_label="_non_stress_div_form"
fi 


# Timestep in seconds
#--------------------
dt_in_seconds_list="0.005" # 0.001 0.0005"
for dt_in_seconds in `echo $dt_in_seconds_list`; do


# Permeability ratio
#-------------------
perm_ratio_list="0.0" # "0.1" # "1.0" # "0.1 1.0 10.0" # hierher: Ratio relative to default of k=1e-13
for perm_ratio in `echo $perm_ratio_list`; do


# Element area in syrinx cover
#-----------------------------
el_area_syrinx_cover_list="0.0001" 
for el_area_syrinx_cover in `echo $el_area_syrinx_cover_list`; do


# Restart file
#-------------
restart_file=""
#restart_file=" /media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/NEW_RUNS_k1em14_all_porous_restarted2/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio0.1_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous/restart1482.dat "


# Switch off darcy altogether?
#-----------------------------
drained_nu_dir_label="_drained_nu"`echo $drained_nu`
no_darcy_flag=" "
if [ $perm_ratio == 0.0 ]; then
    no_darcy_flag=" --pin_darcy "
    drained_nu_flag=" "
    drained_nu_dir_label=""
fi


# Create result directory and copy stuff across
#----------------------------------------------
dir=RESLT_dt`echo $dt_in_seconds`_el_syrinx_cover`echo $el_area_syrinx_cover`_el_area_fluid`echo $el_area_fluid`_el_area_solid`echo $el_area_solid`_alpha`echo $alpha``echo $drained_nu_dir_label`_k_ratio`echo $perm_ratio``echo $non_stress_div_dir_label``echo $mesh_motion_dir_label``echo $all_porous_dir_label`
if [ -e "$dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $dir and try again"
    echo " "
    exit
fi
mkdir $dir
cp $important_files_list "$dir"

# Prepare restart flag
#---------------------
restart_flag=" "
if [ "$restart_file" != "" ]; then
    restart_flag=" --restart_file $restart_file "
fi

# Assemble command line flag
#---------------------------
command_line_flag=" --dt_in_seconds $dt_in_seconds "
command_line_flag=`echo $command_line_flag --el_area_fluid $el_area_fluid `
command_line_flag=`echo $command_line_flag --el_area_poro $el_area_solid `
command_line_flag=`echo $command_line_flag --el_area_sss_near_block 0.0001 ` 
command_line_flag=`echo $command_line_flag --el_area_sss_under_block 1e-05 ` 
command_line_flag=`echo $command_line_flag --el_area_syrinx 0.0001 ` 
command_line_flag=`echo $command_line_flag --el_area_syrinx_cover $el_area_syrinx_cover `
command_line_flag=`echo $command_line_flag --alpha $alpha `
command_line_flag=`echo $command_line_flag $drained_nu_flag `
command_line_flag=`echo $command_line_flag --permeability_ratio $perm_ratio `
command_line_flag=`echo $command_line_flag $no_darcy_flag `
command_line_flag=`echo $command_line_flag --re 14142.135 `
#command_line_flag=`echo $command_line_flag --z_shrink_factor 2 --undo_z_scaling_by_element_stretching ` # hierher
command_line_flag=`echo $command_line_flag --z_shrink_factor 1 ` # hierher
command_line_flag=`echo $command_line_flag --nstep 100 ` # 500 ` # hierher
#command_line_flag=`echo $command_line_flag --suppress_all_output_apart_from_trace_and_restart ` 
command_line_flag=`echo $command_line_flag $restart_flag `
command_line_flag=`echo $command_line_flag --dir $dir `
command_line_flag=`echo $command_line_flag $non_stress_div_flag `
command_line_flag=`echo $command_line_flag --history_level_for_fluid_mesh_node_update $mesh_motion_flag `
command_line_flag=`echo $command_line_flag --suppress_regularly_spaced_output `
command_line_flag=`echo $command_line_flag $all_inner_porous_flags `
echo $command_line_flag > `echo $dir`/command_line_flag.txt

# Run it
#-------
#./chris `echo $command_line_flag` > $dir/OUTPUT & 
mpirun -np 2 ./chris `echo $command_line_flag` > $dir/OUTPUT &  # hierher: use mpi if you have it!


done
done
done
done
done
done

exit

##################################################################################

