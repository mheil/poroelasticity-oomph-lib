
#include <iostream>
#include <string>
#include <math.h>

int main ()
{

 double Fraction_of_pressure_peak=0.4;
 double Excitation_period=35.0;
 unsigned nperiod=3;
 unsigned nstep_per_period=80;
 double offset=0.5*Excitation_period*Fraction_of_pressure_peak;
 double dt=Excitation_period/double(nstep_per_period);
 double time=0.0;
 double av=0.0;
 for (unsigned i=0;i<nperiod;i++)
  {
   for (unsigned j=0;j<nstep_per_period;j++)
    {
     double amplitude=0.0;
     double period_time=fmod(time,Excitation_period);
     if (period_time<Excitation_period*Fraction_of_pressure_peak)
      {
       amplitude=pow(sin(3.14159*period_time/
                         (Excitation_period*Fraction_of_pressure_peak)),2);
       amplitude-=offset;
      }
     else
      {
       amplitude=-offset;
      }
     av+=amplitude;
     std::cout << time << " " << period_time << " " 
               << amplitude << " " << av << std::endl;
     
     // Bump
     time+=dt;
    }
  }
}
