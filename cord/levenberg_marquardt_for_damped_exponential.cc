// Generic oomph-lib sources
#include "generic.h"
#include "num_rec_utilities.cc"

using namespace std;

using namespace oomph;


/// Some function that we're going to sample and fit to. 
double function(const double& x)
{ 
 return sin(2.0*x+3.0);
}




// //=====================================================================
// /// Damped exponential whose 
// /// parameters can be fitted with Levenberg Marquardt.
// // hierher update
// //=====================================================================
// class DampedExponentialFittingFunctionObject : 
//  virtual public LevenbergMarquardtFittingFunctionObject
// {
 
// public:
 
//  /// Constructor
//  DampedExponentialFittingFunctionObject() :  
//   LevenbergMarquardtFittingFunctionObject(7) //(3)
//   {}


//  /// \short Evaluate the fitting function for the current set
//  /// of parameters
//  double fitting_function(const double& x)
//   {
//    return Parameter[0]*(1.0+Parameter[1]*exp(-Parameter[2]*Parameter[2]*x))
//    +Parameter[3]*cos(Parameter[4]*x+Parameter[5])*exp(-Parameter[6]*Parameter[6]*x);
//   }
                                 
//  /// \short Overload all interfaces of the fitting function, call the default
//  /// finite difference version
//  double fitting_function(const double &x,
//                          Vector<double> &dfit_dparam)
//   {
//    return 
//     LevenbergMarquardtFittingFunctionObject::fitting_function(x,dfit_dparam);
//   }

//  /// Number of parameters in fitting function
//  virtual unsigned nparameter()
//   {
//    return 7; //3;
//   }


// };




//=====================================================================
/// Damped exponential whose 
/// parameters can be fitted with Levenberg Marquardt.
//=====================================================================
class DampedExponentialFittingFunctionObject : 
 virtual public LevenbergMarquardtFittingFunctionObject
{
 
public:
 
 /// Constructor
 DampedExponentialFittingFunctionObject() :  
  LevenbergMarquardtFittingFunctionObject(3)
  {}


 /// \short Evaluate the fitting function for the current set
 /// of parameters
 double fitting_function(const double& x)
  {
   return Parameter[0]*(1.0+Parameter[1]*exp(-Parameter[2]*Parameter[2]*x));
  }
                                 
 /// \short Overload all interfaces of the fitting function, call the default
 /// finite difference version
 double fitting_function(const double &x,
                         Vector<double> &dfit_dparam)
  {
   return 
    LevenbergMarquardtFittingFunctionObject::fitting_function(x,dfit_dparam);
  }

 /// Number of parameters in fitting function
 virtual unsigned nparameter()
  {
   return 3;
  }


};



//====================================================================
/// Demo driver code for Levenberg Marquardt fitter
//====================================================================
int main(int argc, char **argv)
{
 
 
 // Store command line arguments
 CommandLineArgs::setup(argc,argv);
 
 // Define possible command line arguments and parse the ones that
 // were actually specified
 
 std::string trace_file_name;
 CommandLineArgs::specify_command_line_flag("--trace_file",
                                            &trace_file_name);

  std::string fit_test_file_name;
 CommandLineArgs::specify_command_line_flag("--fit_test_file",
                                            &fit_test_file_name);

 std::string asymptote_file_name;
 CommandLineArgs::specify_command_line_flag("--asymptote_file",
                                            &asymptote_file_name);

 std::string final_value_file_name;
 CommandLineArgs::specify_command_line_flag("--final_value_file",
                                            &final_value_file_name);

 double t_start_fit=-DBL_MAX;
 CommandLineArgs::specify_command_line_flag("--t_start_fit",
                                            &t_start_fit);

 // Parse command line
 CommandLineArgs::parse_and_assign(); 
 
 // Doc what has actually been specified on the command line
 CommandLineArgs::doc_specified_flags();


 // Fitting range:
 double x_min=DBL_MAX;
 double x_max=-DBL_MAX;
 Vector<std::pair<double,double> > fitting_data;

 ifstream trace_file;
 trace_file.open(trace_file_name.c_str());
 double t=0.0;
 double p=0.0;
 double final_value=0.0;
 std::string line;
 while (std::getline(trace_file, line))
  {
   if (line.empty()) continue;     // skips empty lines
   std::istringstream is(line);    // construct temporary istringstream
   is >> t >> p;
   if (t>t_start_fit)
    {
     fitting_data.push_back(std::make_pair(t,p));
     final_value=p;
     if (t>x_max) x_max=t;
     if (t<x_min) x_min=t;
    }
  }


 
 // Number of fitting points
 unsigned n_fit=fitting_data.size();


 // // Analyse
 // double slope_prev=0.0;
 // double slope_prev_prev=0.0;
 // for (unsigned i=1;i<n_fit-1;i++)
 //  {
 //   double slope=fitting_data[i].second-fitting_data[i-1].second;
 //   oomph_info << fitting_data[i].first << " " << slope << std::endl;
 //   // Previous slope is minimum
 //   if ((slope<slope_prev)&&(slope_prev_prev<slope_prev))
 //    {
 //     oomph_info << "                         Slope minimum at: "<<  fitting_data[i-1].first << std::endl;
 //    }
 //   slope_prev_prev=slope_prev;
 //   slope_prev=slope;
 //  }
       

 // Here's the function whose parameters we want to fit to the data
 DampedExponentialFittingFunctionObject fitting_fct_object;
  
 // Some initial guess for the parameters in the fitting function
 fitting_fct_object.parameter(0)=final_value; //1.5e-5; // asympt limit
 fitting_fct_object.parameter(1)=-0.435; // amplitude of decaying exp
 fitting_fct_object.parameter(2)=sqrt(0.005); // decay rate

// //-------------------------------
// // hierher this is for cover only
// //---------------------------------
//  // Some initial guess for the parameters in the fitting function
 oomph_info << "warning -- coeffs set for cover-only\n";
 fitting_fct_object.parameter(0)=3.74e-5; //1.5e-5; // asympt limit
 fitting_fct_object.parameter(1)=0.1225; // amplitude of decaying exp
 fitting_fct_object.parameter(2)=sqrt(0.0262); // decay rate

// // //-------------------------------
// // // hierher this is for elliiptical
// // //---------------------------------
//  // Some initial guess for the parameters in the fitting function
//  fitting_fct_object.parameter(0)=2.91e-5; //1.5e-5; // asympt limit
//  fitting_fct_object.parameter(1)=-0.4; // amplitude of decaying exp
//  fitting_fct_object.parameter(2)=sqrt(0.018); // decay rate


 // Build the fitter
 LevenbergMarquardtFitter fitter;

 // Set fitting function
 fitter.fitting_function_object_pt()=&fitting_fct_object;


 ofstream ic_file;
 ic_file.open("fit_ic.dat");
 for (unsigned i=0;i<n_fit;i++)
  {
   double x=fitting_data[i].first;
   double y_fit=fitting_fct_object.fitting_function(x);
   //if (CommandLineArgs::command_line_flag_has_been_set("--fit_test_file"))
    {
     ic_file << x << " " << y_fit << " " << fitting_data[i].second << std::endl;
    }
  }
 ic_file.close();



 // Fit it!
 unsigned max_iter=100;
 fitter.fit_it(fitting_data,max_iter);


 // Now check the accuracy:
 ofstream outfile;
 if (CommandLineArgs::command_line_flag_has_been_set("--fit_test_file"))
  {
   outfile.open(fit_test_file_name.c_str());
  }
 ofstream outfile2;
 if (CommandLineArgs::command_line_flag_has_been_set("--asymptote_file"))
  {
   outfile2.open(asymptote_file_name.c_str());
  }
 ofstream outfile3;
 if (CommandLineArgs::command_line_flag_has_been_set("--final_value_file"))
  {
   outfile3.open(final_value_file_name.c_str());
  }


 double error=0.0;
 for (unsigned i=0;i<n_fit;i++)
  {
   double x=fitting_data[i].first;
   double y_fit=fitting_fct_object.fitting_function(x);
   error+=pow((fitting_data[i].second-y_fit),2);
   if (CommandLineArgs::command_line_flag_has_been_set("--fit_test_file"))
    {
     outfile << x << " " << y_fit << " " << fitting_data[i].second << std::endl;
    }
   if (CommandLineArgs::command_line_flag_has_been_set("--asymptote_file"))
    {
     outfile2 << x << " " << fitting_fct_object.parameter(0) << " " << fitting_data[i].second << std::endl;
    }
   if (CommandLineArgs::command_line_flag_has_been_set("--asymptote_file"))
    {
     outfile3 << x << " " << fitting_data[n_fit-1].second << " " << fitting_data[i].second << std::endl;
    }
  }
 std::cout << "RMS fitting error: " 
           << sqrt(error/double(n_fit))
           << std::endl;
 if (CommandLineArgs::command_line_flag_has_been_set("--fit_test_file"))
  {
   outfile.close();
  }
 if (CommandLineArgs::command_line_flag_has_been_set("--asymptote_file"))
  {
   outfile2.close();
  }
 if (CommandLineArgs::command_line_flag_has_been_set("--final_value_file"))
  {
   outfile3.close();
  }

 // Output with fixed width
 std::cout.setf(ios_base::scientific,ios_base::floatfield);
 std::cout.width(15);
 std::cout << std::endl;
 std::cout << "Fitted parameters: " << std::endl;
 std::cout << "===================" << std::endl;
 std::cout << "Asymptotic limit                            : " 
           << fitting_fct_object.parameter(0)<< std::endl;

 std::cout << "Amplitude of decaying exponential           : " 
           << fitting_fct_object.parameter(1)  << std::endl;

 std::cout << "Decay rate of exponential                   : " 
           << pow(fitting_fct_object.parameter(2),2)  << std::endl;

 std::cout << "\nFinal value                                 : " 
           << fitting_data[n_fit-1].second<< std::endl;
 // Reset
 std::cout.setf(std::_Ios_Fmtflags(0), ios_base::floatfield);
 std::cout.width(0);




 // // Output coefficients:
 // std::cout << "\n\nCoefficients: " << std::endl;
 // unsigned n_param=damped_fitting_fct_object.nparameter();
 // for (unsigned i=0;i<n_param;i++)
 //  {
 //   std::cout << i << " ";
   
 //   // Output with fixed width
 //   std::cout.setf(ios_base::scientific,ios_base::floatfield);
 //   std::cout.width(15);

 //   std::cout << damped_fitting_fct_object.parameter(i) 
 //             << std::endl;

 //   // Reset
 //   std::cout.setf(std::_Ios_Fmtflags(0), ios_base::floatfield);
 //   std::cout.width(0);
 //  }


};
