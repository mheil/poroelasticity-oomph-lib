#! /bin/bash

fake_prefix=""
#fake_prefix="fake_"

first_step=415
nsteps_per_period=80


data_dir=`pwd`
echo "Data directory: "$data_dir

# Master run directory; make and move into it
#--------------------------------------------
run_dir=CYCLE_AVERAGE_DISPL_FIRST_`echo $first_step`_N_`echo $nsteps_per_period`
if [ -e "$run_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $run_dir and try again"
    echo " "
    exit
fi
mkdir $run_dir
cd $run_dir


min_ur=1000000
max_ur=-1000000
min_uz=1000000
max_uz=-1000000
min_z=1000000
max_z=-1000000

stem_list=" poro_fsi_inner_sss poro_fsi_outer_sss "
for stem in `echo $stem_list`; do

    step=$first_step
    count=0
    while [ $count -lt $nsteps_per_period ]; do
        file=$stem`echo $step`".dat"
        if [ "$fake_prefix" != "" ]; then
            cp $data_dir/$file `echo $fake_prefix``echo $file`
            file=`echo $fake_prefix``echo $file`
            echo "FILE: "$file
            pseudo_sin=`date | awk -v i=$count -v n=$nsteps_per_period '{print sin(i/n*2.0*4.0*atan2(1,1))}'`
            echo "pseudo sin: $count $pseudo_sin"
            awk -v i=$count -v n=$nsteps_per_period '{if ($1=="ZONE"){print $0}else{print $1 " " $2 " " $3 " " $4 " " cos($2)*(30+14000*sin(i/n*2.0*4.0*atan2(1,1)))}}' $file > .junk
            mv .junk $file
        fi
        echo $file
        #oomph-convert -z -p2 $file
        awk '{if ($1!="ZONE"){print $2 " " $3}}' $data_dir/$file > extracted_z_ur_`echo $file`
        awk '{if ($1!="ZONE"){print $2 " " $4}}' $data_dir/$file > extracted_z_uz_`echo $file`
        if [ $count -eq 0 ]; then
            cp extracted_z_ur_`echo $file` .tmp_sum_r
            cp extracted_z_uz_`echo $file` .tmp_sum_z
        else
            paste extracted_z_ur_`echo $file` .tmp_sum_r > .both_r.dat
            paste extracted_z_uz_`echo $file` .tmp_sum_z > .both_z.dat

            # This test doesn't make sense if the domain moves!
            #awk '{if ($1!=$3){print "ERROR"}else{print $1 " " $2+$4}}' .both.dat > .tmp_sum
            awk '{print $1 " " $2+$4}' .both_r.dat > .tmp_sum_r
            awk '{print $1 " " $2+$4}' .both_z.dat > .tmp_sum_z
            awk 'BEGIN{max_z=-100000; min_z=100000;max_p=-100000; min_p=100000} {if ($1>max_z){max_z=$1}if ($2>max_p){max_p=$2}if ($1<min_z){min_z=$1}if ($2<min_p){min_p=$2}}END{print min_z " " max_z " " min_p " " max_p}' .both_r.dat > .min_max_r
            awk 'BEGIN{max_z=-100000; min_z=100000;max_p=-100000; min_p=100000} {if ($1>max_z){max_z=$1}if ($2>max_p){max_p=$2}if ($1<min_z){min_z=$1}if ($2<min_p){min_p=$2}}END{print min_z " " max_z " " min_p " " max_p}' .both_z.dat > .min_max_z
            min_z=`awk -v current_min=$min_z '{if ($1<current_min){print$1}else{print current_min}}' .min_max_r` 
            max_z=`awk -v current_max=$max_z '{if ($2>current_max){print$2}else{print current_max}}' .min_max_r` 
            min_ur=`awk -v current_min=$min_ur '{if ($3<current_min){print$3}else{print current_min}}' .min_max_r` 
            max_ur=`awk -v current_max=$max_ur '{if ($4>current_max){print$4}else{print current_max}}' .min_max_r` 
            min_uz=`awk -v current_min=$min_uz '{if ($3<current_min){print$3}else{print current_min}}' .min_max_z` 
            max_uz=`awk -v current_max=$max_uz '{if ($4>current_max){print$4}else{print current_max}}' .min_max_z` 
        fi
        awk '{print "0.0 "$1" "$2}' extracted_z_ur_`echo $file` > .junk_r
        awk '{print "0.0 "$1" "$2}' extracted_z_uz_`echo $file` > .junk_z
        mv .junk_r extracted_z_ur_`echo $file`
        mv .junk_z extracted_z_uz_`echo $file`
        oomph-convert -o -p2 -z extracted_z_ur_`echo $file`
        oomph-convert -o -p2 -z extracted_z_uz_`echo $file`
        let step=$step+1
        let count=$count+1
    done
    
    awk -v n=$nsteps_per_period '{print "0.0 " $1 " " $2/n}' .tmp_sum_r | sort -g -k 2 > cycle_average_ur_`echo $stem`.dat
    awk -v n=$nsteps_per_period '{print "0.0 " $1 " " $2/n}' .tmp_sum_z | sort -g -k 2 > cycle_average_uz_`echo $stem`.dat

    makePvd extracted_z_ur_$fake_prefix$stem extracted_z_ur_`echo $stem`.pvd
    makePvd extracted_z_uz_$fake_prefix$stem extracted_z_uz_`echo $stem`.pvd
    oomph-convert -o -p2 cycle_average_ur_`echo $stem`.dat
    oomph-convert -o -p2 cycle_average_uz_`echo $stem`.dat
done


echo "0.0" $min_z $min_ur >  cycle_average_bounding_box_ur.dat
echo "0.0" $max_z $min_ur >> cycle_average_bounding_box_ur.dat
echo "0.0" $max_z $max_ur >> cycle_average_bounding_box_ur.dat
echo "0.0" $min_z $max_ur >> cycle_average_bounding_box_ur.dat
echo "0.0" $min_z $min_ur >> cycle_average_bounding_box_ur.dat
oomph-convert -p2 -o cycle_average_bounding_box_ur.dat

echo "0.0" $min_z $min_uz >  cycle_average_bounding_box_uz.dat
echo "0.0" $max_z $min_uz >> cycle_average_bounding_box_uz.dat
echo "0.0" $max_z $max_uz >> cycle_average_bounding_box_uz.dat
echo "0.0" $min_z $max_uz >> cycle_average_bounding_box_uz.dat
echo "0.0" $min_z $min_uz >> cycle_average_bounding_box_uz.dat
oomph-convert -p2 -o cycle_average_bounding_box_uz.dat

cd $data_dir
