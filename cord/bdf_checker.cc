//Generic routines
#include "generic.h"

using namespace std;

using namespace oomph;


//=====================================================================
/// Element that integrates single ode with bdf scheme
//=====================================================================
class BDFElement : public virtual GeneralisedElement
{
 
public:
 
 
 /// Constructor: Pass timestepper
 BDFElement(TimeStepper* timestepper_pt)
  {
   add_internal_data(new Data(timestepper_pt,1));
   
   // Long term mean
   A=1.0e-6;
   
   // Amplitude of deviation from long term mean
   B=0.01;
   
   // Decay rate towards long term mean
   Delta=0.001;

   // Amplitude of super-imposed oscillation
   C=0.1;
   
   // Period
   double period=14.14;
   Omega=2.0*MathematicalConstants::Pi/period;
  }
 
 /// Get residuals
 void fill_in_contribution_to_residuals(Vector<double>& residuals)
  {
   // Dummy
   DenseMatrix<double> jacobian;
   bool flag=false;
   
   // Get generic function
   get_residuals_generic(residuals,jacobian,flag);
  }
 
 
 /// Get residuals
 void fill_in_contribution_to_jacobian(Vector<double>& residuals,
                                       DenseMatrix<double>& jacobian)
  {
   // Get generic function
   bool flag=true;
   get_residuals_generic(residuals,jacobian,flag);
  }
 
 
 /// Exact solution
 double exact_solution(const double& t)
  {
   double fct=A*(1.0-B*exp(-Delta*t))+C*cos(Omega*t);
   return fct;
  }
 
private:
 
 /// Get residuals and/or Jacobian
 void get_residuals_generic(Vector<double> &residuals,
                            DenseMatrix<double> &jacobian,
                            const bool& flag)
  {   
   
   // Get pointer to one-and-only internal data object
   Data* dat_pt=internal_data_pt(0);
   
   // Get timestepper
   TimeStepper* timestepper_pt=dat_pt->time_stepper_pt();
   
   // Get continuous time
   double t=timestepper_pt->time_pt()->time();

   // Get dudt approximation from timestepper: 1st deriv of 0th value
   double dudt=timestepper_pt->time_derivative(1,dat_pt,0);

   // Residual
   residuals[0]=dudt-(A*B*Delta*exp(Delta*t)-C*Omega*sin(Omega*t));

   if (flag)
    {
     // Deriv w.r.t. to current (0th history) value extracts weight
     jacobian(0,0)=timestepper_pt->weight(1,0);
    }
   
  }

 // Long term mean
 double A;
 
 // Amplitude of deviation from long term mean
 double B;
 
 // Decay rate towards long term mean
 double Delta;
 
 // Amplitude of super-imposed oscillation
 double C;
 
 /// Frequency
 double Omega;
 
};

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

//=====================================================================
/// Dummy problem
//=====================================================================
 class BDFProblem : public virtual Problem
 {


 public:

  /// constructor
  BDFProblem()
   {

    // Create timestepper
    add_time_stepper_pt(new BDF<2>);

    // Make mesh
    mesh_pt()=new Mesh;

    // Create/add element
    mesh_pt()->add_element_pt(new BDFElement(time_stepper_pt(0)));
    
    // Assign eqn numbers
    oomph_info << "Number of unknowns: " << assign_eqn_numbers() << std::endl;
   }


 };


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////


//==start_of_main======================================================
/// bdf checker
//=====================================================================
int main(int argc, char* argv[])
{

 // Create problem
 BDFProblem* problem_pt=new BDFProblem;

 unsigned n_per_period=80;
 double dt=14.14/double(n_per_period);
 problem_pt->initialise_dt(dt);
 
 // Assign consistent history values
 Data* data_pt=
  problem_pt->mesh_pt()->element_pt(0)->internal_data_pt(0);
 BDFElement* el_pt=
  dynamic_cast<BDFElement*>(problem_pt->mesh_pt()->element_pt(0));
 double t=0.0;
 data_pt->set_value(0,0,el_pt->exact_solution(t));
 data_pt->set_value(1,0,el_pt->exact_solution(t-dt));
 data_pt->set_value(2,0,el_pt->exact_solution(t-2.0*dt));
 

 ofstream trace_file;
 trace_file.open("bdf_trace.dat");
 unsigned nperiod=10;
 double t_max=14.14*double(nperiod);
 t=problem_pt->time_pt()->time();
 while (t<t_max)
  {

   problem_pt->unsteady_newton_solve(dt);

   // Doc solution
   t=problem_pt->time_pt()->time();
   trace_file 
    << t << " "
    << data_pt->value(0) << " "
    << el_pt->exact_solution(t) << " "
    << abs(data_pt->value(0)-el_pt->exact_solution(t)) << " "
    << std::endl;
  }
 trace_file.close();

 
} // end_of_main











