#! /bin/bash

# Important files to be moved
important_files_list="chris chris.cc $0"


# Permanent storage directory
#----------------------------
#root_permament_storage_dir="/home/ds413/matthias/chris_scratch"
#if [ ! -e "$root_permament_storage_dir" ];  then
#    echo " "
#    echo "ERROR: Root permanent storage directory $root_permament_storage_dir doesn't exist. Please create it."
#    echo " "
#    exit
#else
#    echo " "
#    echo "Root permanent storage directory $root_permament_storage_dir exists. Good!"
#    echo " "
#fi


# Master run directory; make and move into it
#--------------------------------------------
run_dir="/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/NEW_RUNS_k1em14_all_porous_restarted3"
if [ -e "$run_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $run_dir and try again"
    echo " "
    exit
fi

# Permemant storage directory; make it
#-------------------------------------
if [ -e "$root_permament_storage_dir/$run_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $root_permament_storage_dir/$run_dir and try again"
    echo " "
    exit
fi
mkdir $root_permament_storage_dir/$run_dir
mkdir $run_dir
echo " "
echo "Run dir: "`pwd`"/$run_dir"
echo "Permanent storage dir: $root_permament_storage_dir/$run_dir"
cp $important_files_list "$run_dir"
cd $run_dir


#Element area for fluid
#----------------------
el_area_fluid=0.01 # 0.01

#Element area for solid
#----------------------
el_area_solid=0.01 # 0.01

# Biot parameter alpha
#---------------------
alpha=0.0

# Drained Poisson's ratio (usually 0.35!)
#----------------------------------------
drained_nu=0.35


# All inner solid porous
#-----------------------
do_all_inner_porous=1 # hierher
all_porous_dir_label=""
if [ $do_all_inner_porous -eq 1 ]; then
    all_porous_dir_label="_all_inner_solid_porous"
    # 0.4 is a good value for discretisation of edge length for standard resolution in syrinx cover
    #----------------------------------------------------------------------------------------------
    ds_syrinx_poro_fsi_boundary_raw=0.4
    ds_syrinx_poro_fsi_boundary_near_sing_raw=0.04
    delta_syrinx_poro_fsi_boundary_near_sing_raw=0.4
    all_inner_porous_flags="--cord_pia_and_filum_are_porous --ds_syrinx_poro_fsi_boundary_raw $ds_syrinx_poro_fsi_boundary_raw --ds_syrinx_poro_fsi_boundary_near_sing_raw $ds_syrinx_poro_fsi_boundary_near_sing_raw --delta_syrinx_poro_fsi_boundary_near_sing_raw $delta_syrinx_poro_fsi_boundary_near_sing_raw  "
fi


# Do proper poro-elasticity or Darcy only
#----------------------------------------
proper_poro_elasticity_flag_list="1" # "0 1" # hierher
for proper_poro_elasticity_flag in `echo $proper_poro_elasticity_flag_list`; do

if [ $proper_poro_elasticity_flag == 0 ]; then
    alpha=0.0
    drained_nu=0.49
else
    if [ $proper_poro_elasticity_flag == 1 ]; then
        alpha=1.0
        drained_nu=0.35
    else
        echo "Wrong proper_poro_elasticity_flag"
        exit
    fi
fi 

# Flag for drained Poisson's ratio (only used for porous runs; switched off
# if Darcy is pinned below
drained_nu_flag=" --drained_nu $drained_nu "

# Mesh motion
#------------
mesh_motion_list_list="0" # "0" # "1" #"-1 0 1"
for mesh_motion_flag in `echo $mesh_motion_list_list`; do

if [ $mesh_motion_flag  == -1 ]; then
    mesh_motion_dir_label="_fixed_fluid_mesh"
fi 
if [ $mesh_motion_flag  == 0 ]; then
    mesh_motion_dir_label="_implicit_fluid_mesh_update"
fi 
if [ $mesh_motion_flag  == 1 ]; then
    mesh_motion_dir_label="_lagged_fluid_mesh_update"
fi 

# Stress divergence form of equations?
#-------------------------------------
non_stress_div_list="1"
for non_stress_div in `echo $non_stress_div_list`; do


non_stress_div_flag=" "
non_stress_div_dir_label="_stress_div_form"
if [ $non_stress_div  == 1 ]; then
    non_stress_div_flag=" --use_non_stress_divergence_form_for_navier_stokes "
    non_stress_div_dir_label="_non_stress_div_form"
fi 


# Timestep in seconds
#--------------------
dt_in_seconds_list="0.005" # 0.001 0.0005"
for dt_in_seconds in `echo $dt_in_seconds_list`; do


# Permeability ratio
#-------------------
perm_ratio_list="0.1" # 1.0" # "0.1 1.0 10.0" # hierher
for perm_ratio in `echo $perm_ratio_list`; do


# Element area in syrinx cover
#-----------------------------
el_area_syrinx_cover_list="0.0001" # "0.00001" # hierher #  0.001" # "0.01" # "1.0e-5" # "0.01" # "1.0e-5"
for el_area_syrinx_cover in `echo $el_area_syrinx_cover_list`; do


# Restart file
#-------------
restart_file=""
restart_file=" /media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/NEW_RUNS_k1em14_all_porous_restarted2/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio0.1_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous/restart1482.dat "



#              /media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/NEW_RUNS_k1em14_all_porous/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio0.1_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous/restart490.dat "


# Switch off darcy altogether?
#-----------------------------
drained_nu_dir_label="_drained_nu"`echo $drained_nu`
no_darcy_flag=" "
if [ $perm_ratio == 0.0 ]; then
    no_darcy_flag=" --pin_darcy "
    drained_nu_flag=" "
    drained_nu_dir_label=""
fi


# Create result directory and copy stuff across
#----------------------------------------------
dir=RESLT_dt`echo $dt_in_seconds`_el_syrinx_cover`echo $el_area_syrinx_cover`_el_area_fluid`echo $el_area_fluid`_el_area_solid`echo $el_area_solid`_alpha`echo $alpha``echo $drained_nu_dir_label`_k_ratio`echo $perm_ratio``echo $non_stress_div_dir_label``echo $mesh_motion_dir_label``echo $all_porous_dir_label`
if [ -e "$dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $dir and try again"
    echo " "
    exit
fi
mkdir $dir
cp $important_files_list "$dir"

# Prepare restart flag
#---------------------
restart_flag=" "
if [ "$restart_file" != "" ]; then
    restart_flag=" --restart_file $restart_file "
fi

# Assemble command line flag
#---------------------------
command_line_flag=" --dt_in_seconds $dt_in_seconds "
command_line_flag=`echo $command_line_flag --el_area_fluid $el_area_fluid `
command_line_flag=`echo $command_line_flag --el_area_poro $el_area_solid `
command_line_flag=`echo $command_line_flag --el_area_sss_near_block 0.0001 ` #hierher
command_line_flag=`echo $command_line_flag --el_area_sss_under_block 1e-05 ` #hierher
command_line_flag=`echo $command_line_flag --el_area_syrinx 0.0001 ` # hierher
command_line_flag=`echo $command_line_flag --el_area_syrinx_cover $el_area_syrinx_cover ` # hierher
command_line_flag=`echo $command_line_flag --alpha $alpha `
command_line_flag=`echo $command_line_flag $drained_nu_flag `
command_line_flag=`echo $command_line_flag --permeability_ratio $perm_ratio `
command_line_flag=`echo $command_line_flag $no_darcy_flag `
command_line_flag=`echo $command_line_flag --re 14142.135 `
command_line_flag=`echo $command_line_flag --z_shrink_factor 1 `
command_line_flag=`echo $command_line_flag --nstep 500 `
#command_line_flag=`echo $command_line_flag --suppress_all_output_apart_from_trace_and_restart ` 
command_line_flag=`echo $command_line_flag $restart_flag `
command_line_flag=`echo $command_line_flag --dir $dir `
command_line_flag=`echo $command_line_flag $non_stress_div_flag `
command_line_flag=`echo $command_line_flag --history_level_for_fluid_mesh_node_update $mesh_motion_flag `
command_line_flag=`echo $command_line_flag --suppress_regularly_spaced_output `
command_line_flag=`echo $command_line_flag $all_inner_porous_flags `
echo $command_line_flag > `echo $dir`/command_line_flag.txt

# Run it
#-------
mpirun -np 2 ./chris `echo $command_line_flag` > $dir/OUTPUT & 


done
done
done
done
done
done

exit

##################################################################################



--restart_file RESLT_el_syrinx_cover0.01_alpha1.0_dt0.001_restarted/restart313.dat \
--restart_file RESLT_el_syrinx_cover0.01_alpha1.0_dt0.001/restart250.dat \

--drained_nu 0.49 \
--el_area_syrinx_cover 0.00001 \

--undo_z_scaling_by_element_stretching \
--pin_darcy \



--dt_in_seconds 0.005 \
--el_area_fluid 0.01 \
--el_area_poro 0.01 \
--el_area_sss_near_block 0.0001 \
--el_area_sss_under_block 1e-05 \
--el_area_syrinx 0.0001 \
--el_area_syrinx_cover 0.01 \
--permeability_multiplier 1 \
--re 14140 \
--z_shrink_factor 5 \
--undo_z_scaling_by_element_stretching \
--nstep 3 \
--dir RESLT_serial > RESLT_serial/OUTPUT &


exit





#mpirun -np 4 
./chris --pin_darcy \
--dt_in_seconds 0.005 \
--re 14140 \
--nstep 80 \
--z_shrink_factor 5 \
--undo_z_scaling_by_element_stretching \
--dir RESLT > RESLT/OUTPUT &


exit


./chris --permeability_multiplier 100000.0 --el_area_fluid 0.01 --el_area_poro 0.01 --dt_in_seconds 5.0e-3 --re 6000 --z_shrink_factor 10 --nstep 160 --dir RESLT > RESLT/OUTPUT &


exit


nohup ./chris --pin_darcy \
--dt_in_seconds 0.005 \
--el_area_fluid 0.01 \
--el_area_poro 0.01 \
--el_area_sss_near_block 0.0001 \
--el_area_sss_under_block 1e-05 \
--el_area_syrinx 0.0001 \
--el_area_syrinx_cover 0.01 \
--permeability_multiplier 1 \
--re 14140 \
--z_shrink_factor 5 \
--undo_z_scaling_by_element_stretching \
--nstep 80 \
--dir RESLT_stretch5 > RESLT_stretch5/OUTPUT &


exit

./chris --permeability_multiplier 100000.0 --el_area_fluid 0.01 --el_area_poro 0.01 --dt_in_seconds 5.0e-3 --re 6000 --z_shrink_factor 10 --nstep 160 --dir RESLT > RESLT/OUTPUT &
