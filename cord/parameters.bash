#! /bin/bash

#====================================================================
# A few helper functions
#====================================================================

# A little function 'borrowed' from the tecplot installation script...
OptionPrompt() 
{ 
 printf "%s " "$1" 
}

# Another little function 'borrowed' from the tecplot installation script...
OptionRead()
{
 read Opt
 echo $Opt
}

########################################################################
########################################################################
# Input variable parameters
########################################################################
########################################################################


#---------------------------------------------
#Input driving pressure
#---------------------------------------------
OptionPrompt "Please enter the preak driving pressure in Pa "
P_hat=`OptionRead`

#---------------------------------------------
#Input permeability
#---------------------------------------------
echo "Please enter the permeability in m^2 in the form a x 10^b: "
OptionPrompt "a ="
a=`OptionRead`
OptionPrompt "b ="
b=`OptionRead`
k_tilde=$(echo "scale=20; $a*10^$b" | bc -l)


################################################################
################################################################
#  FIXED PARAMETERS: all in SI units! (cannot check consistency
#                    internally because this ain't maple...)
################################################################
################################################################



#--------------------------------------------------------------------
# Reference value for permeability used in non-dimensionalisation
#--------------------------------------------------------------------
#k_generic:=1.0e-13*Meter^2/mu_nst;
k_tilde_reference=0.0000000000001


#--------------------------------------------------------------------
# Chris timestep:0.005 sec
#--------------------------------------------------------------------
# Timestep:=0.005*Sec;
Timestep=0.005


#----------------------------------------------------------------------------
# Drained elastic moduli 
#----------------------------------------------------------------------------
# E_cord:=5e4*dyn/cm^2;
E_cord=5000

# E_filum_and_block:=6.25e5*dyn/cm^2; 
E_filum_and_block=62500

# E_dura_and_pia:=1.25e7*dyn/cm^2;
E_dura_and_pia=1250000
           

#--------------------------------------------------
# Density of Navier-Stokes fluid (I know this one!)
# Also used for all other materials.
#--------------------------------------------------
# rho:=1000*kg/Meter^3;
rho=1000

#---------------------------------------------
# Viscosity of Navier-Stokes fluid.
#---------------------------------------------
# mu_nst:=1.0e-3*Pascal*Sec;
mu_nst=0.001

#--------------------------
# Drained Poisson's ratio 
# Chris email: 03/12/12 
#--------------------------
# nu:=0.35;
nu=0.35


#--------------------------------------------------------------------
# Length-scale: inner diameter of dura: 20mm
#--------------------------------------------------------------------
# Lengthscale:=20e-3*Meter;
Lengthscale=0.02

#--------------------------------------------------------------------
# Mean diameter of dura (for wavespeed): 21mm
#--------------------------------------------------------------------
# Mean_diameter:=21.0e-3*Meter;
Mean_diameter=0.0210

#--------------------------------------------------------------------
# Mean wall thickness of dura (for wavespeed): 1mm
#--------------------------------------------------------------------
# Mean_dura_wall_thickness:=1.0e-3*Meter;
Mean_dura_wall_thickness=0.0010


#--------------------------------------------------------------------
# Period of periodic forcing: 0.4 sec
#--------------------------------------------------------------------
# Period:=0.4*Sec;
Period=0.4




#############################################################
#############################################################
#############################################################

#---------------------------------------------
# Velocity scale based on inertial scaling with
# peak pressure at inlet 
#---------------------------------------------
# Velocscale:=sqrt(P_hat/rho);
Velocscale=$(echo "scale=20; sqrt($P_hat/$rho)" | bc -l)


#-------------------------------------------------
# Timescale -- corresponds to unit Strouhal number
#-------------------------------------------------
# Timescale:=evalf(Lengthscale/Velocscale);
Timescale=$(echo "scale=20; $Lengthscale/$Velocscale" | bc -l)

#----------------
# Reynolds number
#----------------
# Reynolds:=simplify(Velocscale*Lengthscale*rho/mu_nst);
Reynolds=$(echo "scale=20; $Velocscale*$Lengthscale*$rho/$mu_nst" | bc -l)



#==============================================
# Material parameters that express physical
# quantities in terms of Reynolds number
#==============================================
# E:=E_dura_and_pia;
E=$E_dura_and_pia


#--------------------------------------------------------------------
# Dimensional permeability -- the permeability quoted by Smillie etc. 
# and prefered by Chris isn't what I call the permeability; I 
# (and many others!) include the visocsity and
# k_tilde/mu = k_dim. Generic value currently for all media.
#--------------------------------------------------------------------
k_dim=$(echo "scale=20; $k_tilde/$mu_nst" | bc -l)


#--------------------
# Permeability ratio
#--------------------
permeability_ratio=$(echo "scale=20; $k_tilde/$k_tilde_reference" | bc -l)


#----------------------------------------------
# Factor multiplying FSI parameter: Q = F_Q Re
#----------------------------------------------
#q_factor:=simplify(mu_nst^2/(Lengthscale^2*E*rho));
q_factor=$(echo "scale=20; $mu_nst*$mu_nst/($Lengthscale*$Lengthscale*$E*$rho)" | bc -l)


#------------------------------------------------------
# Factor multiplying non-dim permeability: k = F_k 1/Re
#------------------------------------------------------
#permeability_factor:=simplify(k*E*rho/mu_nst);
permeability_factor=$(echo "scale=20; $k_dim*$E*$rho/$mu_nst" | bc -l);



#------------------------------------------------------------
# Factor for nondim inertia parameter: \Lambda = F_\Lambda Re
#------------------------------------------------------------
#lambda_factor:=simplify(mu_nst/(rho*Lengthscale)*sqrt(rho/E));
lambda_factor=$(echo "scale=20; $mu_nst/($rho*$Lengthscale)*sqrt($rho/$E)" | bc -l)



#------------------------------------------------------------
# Factor for time translation: t^* = F_T 1/Re t
#------------------------------------------------------------
T_factor=$(echo "scale=20; $rho*$Lengthscale*$Lengthscale/$mu_nst" | bc -l)
echo "Time factor F_T = " $T_factor " sec "

Timescale_again=$(echo "scale=20; $T_factor/$Reynolds" | bc -l)

#------------------------------------------------------------
# Factor for Navier-Stokes veloc translation: v^* = F_v Re v
#------------------------------------------------------------
v_factor=$(echo "scale=20; $mu_nst/($rho*$Lengthscale)" | bc -l)

Velocscale_again=$(echo "scale=20; $v_factor*$Reynolds" | bc -l)


#--------------------------------------------------------------
# Factor for Navier-Stokes pressure translation: p^* = F_p Re p
#--------------------------------------------------------------
p_factor=$(echo "scale=20; $mu_nst*$mu_nst/($rho*$Lengthscale*$Lengthscale)" | bc -l)


Pressure_scale=$(echo "scale=20; $p_factor*$Reynolds" | bc -l)
Pressure_scale_again=$(echo "scale=20; $mu_nst*$Velocscale/$Lengthscale" | bc -l)


#--------------------------------------------------------------
# Conversion factor for seepage flux to dim version
#--------------------------------------------------------------
Seepage_flux_scale=$(echo "scale=20; $k_dim*$E/$Lengthscale" | bc -l)



#==============================================
# Ratios of material property for cord
#==============================================
#R_E_cord:=E_cord/E_dura_and_pia;
R_E_cord=$(echo "scale=20; $E_cord/$E_dura_and_pia" | bc -l)


#==============================================
# Ratios of material properties for filum and block
#==============================================
#R_E_filum_and_block:=E_filum_and_block/E_dura_and_pia;
R_E_filum_and_block=$(echo "scale=20; $E_filum_and_block/$E_dura_and_pia" | bc -l)


#==============================================
# Non-dim period of periodic forcing
#==============================================
Period=$(echo "scale=20; $Period/$Timescale" | bc -l)

 
#==============================================
# Chris timestep
#==============================================
Non_dim_timestep=$(echo "scale=20; $Timestep/$Timescale" | bc -l)


#==============================================
# Raw, dimensional wavespeed based on 
# dura properties
#==============================================
# wave_speed:=sqrt(E_dura_and_pia*Mean_dura_wall_thickness/
#                       (Mean_diameter*rho));
wave_speed=$(echo "scale=20; sqrt($E_dura_and_pia*$Mean_dura_wall_thickness/($Mean_diameter*$rho))" | bc -l)


#==============================================
# How far does it travel per timestep?
#==============================================
distance_travelled_per_dt=$(echo "scale=20; $wave_speed*$Timestep"  | bc -l)


#=============================================================================
# How far does it travel per timestep (in non-dim terms (relative to diameter)
#=============================================================================
 distance_travelled_per_dt_divided_by_length_scale=$(echo "scale=20; $distance_travelled_per_dt/$Lengthscale"  | bc -l)


########################################################


echo " " 
echo "INPUT:" 
echo "======" 
echo "Peak driving pressure   : P^*_hat   = $P_hat Pa"
echo "(Kinematic) permeability: k^*_tilde = $k_tilde m^2 " 
echo "(Dynamic permeability   : k^*       = $k_dim m^3 sec/kg)" 



echo " " 
echo "oomph-lib input parameters:" 
echo "===========================" 
echo "Reynolds number     = "$Reynolds 
echo "Permeability ratio  = "$permeability_ratio
echo "[Non-dim timestep   = "$Non_dim_timestep"]"

echo " " 
echo "Parameters hard-coded in oomph-lib:" 
echo "===================================" 
echo "Non-dim period of periodic forcing = " $Period


echo " " 
echo "Dimless material parameters (hard-coded in oomph-lib):" 
echo "======================================================" 
echo "FSI factor:                                F_Q       = " $q_factor
echo "Permeability factor:                       F_k       = " $permeability_factor
echo "Inertia factor:                            F_\Lambda = " $lambda_factor 
echo "Young's modulus ratio for cord:            R_E_cord  = " $R_E_cord
echo "Young's modulus ratio for filum and block: R_E_block = " $R_E_filum_and_block 



echo " " 
echo "Dimensional material parameters:" 
echo "================================" 
echo "Navier-Stokes velocity factor:             F_v       = " $v_factor " m/sec "
echo "Navier-Stokes pressure factor:             F_p       = " $p_factor " Pa "
echo "Time factor:                               F_T       = " $T_factor " sec "


echo " " 
echo "Conversion factors from oomph-lib back to SI:"
echo "=============================================" 
echo "Lengthscale                       = $Lengthscale m " 
echo "Navier-Stokes veloc scale         = $Velocscale m/sec " 
echo "Navier Stokes veloc scale (again) = $Velocscale_again m/sec "
echo "Navier Stokes press scale         = $Pressure_scale Pa "
echo "Navier Stokes press scale (again) = $Pressure_scale_again Pa "
echo "Porous flux veloc scale           = $Seepage_flux_scale m/sec " 
echo "Pore pressure scale               = $E Pa " 
echo "Timescale                         = $Timescale sec " 
echo "Timescale (again)                 = $Timescale_again sec "



echo " "
echo "Other stuff:"
echo "============" 
echo "Wavespeed (based on dura)               = " $wave_speed "m/sec"
echo "Distance travelled per timestep         = " $distance_travelled_per_dt "m"
echo "Non-dim distance travelled per timestep = " $distance_travelled_per_dt_divided_by_length_scale
echo " "
echo " "
