// Generic oomph-lib sources
#include "generic.h"
#include "num_rec_utilities.cc"

using namespace std;

using namespace oomph;


/// Some function that we're going to sample and fit to. 
double function(const double& x)
{ 
 return 1.0+exp(-3.0*x)*3.0*sin(2.0*x+3.0);
}


//=====================================================================
/// Harmonic oscillation with phase shift and drifting mean whose 
/// parameters can be fitted with Levenberg Marquardt.
//=====================================================================
class OscillatoryFittingFunctionObject : 
 virtual public LevenbergMarquardtFittingFunctionObject
{
 
public:
 
 /// Constructor
 OscillatoryFittingFunctionObject() :  
  LevenbergMarquardtFittingFunctionObject(6)
  {}


 /// \short Evaluate the fitting function for the current set
 /// of parameters
 double fitting_function(const double& x)
  {
   return Parameter[0]*(1.0+Parameter[4]*exp(-Parameter[5]*x))+
    Parameter[1]*cos(Parameter[2]*x+Parameter[3]);
  }
                                 
 /// \short Overload all interfaces of the fitting function, call the default
 /// finite difference version
 double fitting_function(const double &x,
                         Vector<double> &dfit_dparam)
  {
   return 
    LevenbergMarquardtFittingFunctionObject::fitting_function(x,dfit_dparam);
  }

 /// Number of parameters in fitting function
 virtual unsigned nparameter()
  {
   return 6;
  }


};



//====================================================================
/// Demo driver code for Levenberg Marquardt fitter
//====================================================================
int main(int argc, char **argv)
{
 
 
 // Store command line arguments
 CommandLineArgs::setup(argc,argv);
 
 // Define possible command line arguments and parse the ones that
 // were actually specified
 
 std::string trace_file_name;
 CommandLineArgs::specify_command_line_flag("--trace_file",
                                            &trace_file_name);

  std::string fit_test_file_name;
 CommandLineArgs::specify_command_line_flag("--fit_test_file",
                                            &fit_test_file_name);

 double t_start_fit=-DBL_MAX;
 CommandLineArgs::specify_command_line_flag("--t_start_fit",
                                            &t_start_fit);

 // Parse command line
 CommandLineArgs::parse_and_assign(); 
 
 // Doc what has actually been specified on the command line
 CommandLineArgs::doc_specified_flags();


 // Fitting range:
 double x_min=DBL_MAX;
 double x_max=-DBL_MAX;
 Vector<std::pair<double,double> > fitting_data;

 ifstream trace_file;
 trace_file.open(trace_file_name.c_str());
 double t=0.0;
 double p=0.0;
 std::string line;
 while (std::getline(trace_file, line))
  {
   if (line.empty()) continue;     // skips empty lines
   std::istringstream is(line);    // construct temporary istringstream
   is >> t >> p;
   if (t>t_start_fit)
    {
     fitting_data.push_back(std::make_pair(t,p));
     if (t>x_max) x_max=t;
     if (t<x_min) x_min=t;
    }
  }

 // Number of fitting points
 unsigned n_fit=fitting_data.size();


 // Do some crude pre-sampling
 double average=fitting_data[0].second+fitting_data[1].second;
 double most_recent_max=0.0;
 double x_most_recent_max=0.0;
 double most_recent_min=0.0;
 double x_most_recent_min=0.0;
 unsigned n_max=0;
 unsigned n_min=0;
 double period=14.14;
 double phase=0.0;
 double amplitude=0.0;
 double old_slope=(fitting_data[1].second-fitting_data[0].second)/
  (fitting_data[1].first-fitting_data[0].first);
 for (unsigned i=2;i<n_fit;i++)
  {
   average+=fitting_data[i].second;
   double new_slope=
    (fitting_data[i-1].second-fitting_data[i].second)/
    (fitting_data[i-1].first-fitting_data[i].first);
   if (new_slope*old_slope<0.0)
    {
     oomph_info << "found extremum at : " 
                << fitting_data[i].first
                << std::endl;
     // It's a min:
     if (new_slope>0.0)
      {
       if (n_min>0)
        {
         period=fitting_data[i].first-x_most_recent_min;
         oomph_info << "UPDATED GUESS FOR PERIOD: " 
                    << period << std::endl;
        }
       n_min++;
       most_recent_min=fitting_data[i].second;
       x_most_recent_min=fitting_data[i].first;
      }
     // It's a max:
     else
      {
       if (n_max>0)
        {
         period=fitting_data[i].first-x_most_recent_max;
         oomph_info << "UPDATED GUESS FOR PERIOD: " 
                    << period << std::endl;
        }
       n_max++;
       most_recent_max=fitting_data[i].second;
       x_most_recent_max=fitting_data[i].first;
      }
     
     if ((n_max>0)&&(n_min>0))
      {
       amplitude=0.5*(most_recent_max-most_recent_min);
       oomph_info << "UPDATED GUESS FOR AMPLITUDE: " 
                  << amplitude << std::endl;
      }
    }
   old_slope=new_slope;
  }
 average/=double(n_fit);
 oomph_info << "Average: " << average << std::endl;

 oomph_info << "x_most_recent_max: " << x_most_recent_max << std::endl;

 phase=2.0*MathematicalConstants::Pi/period*x_most_recent_max;

 oomph_info << "Phase: " << phase << std::endl;

 // Here's the function whose parameters we want to fit to the data
 OscillatoryFittingFunctionObject fitting_fct_object;
 
 
 // Some initial guess for the parameters in the fitting function
 fitting_fct_object.parameter(0)=average; // mean
 fitting_fct_object.parameter(1)=amplitude; // amplitude
 fitting_fct_object.parameter(2)=2.0*MathematicalConstants::Pi/period; // freq
 fitting_fct_object.parameter(3)=phase; // phase shift
 fitting_fct_object.parameter(4)=-1.0e-5; // amplitude of deviation from long term mean 
 fitting_fct_object.parameter(5)=1.0e-5; // decay rate of deviation from long term mean

 // Build the fitter
 LevenbergMarquardtFitter fitter;

 // Set fitting function
 fitter.fitting_function_object_pt()=&fitting_fct_object;

 // Fit it!
 unsigned max_iter=100;
 fitter.fit_it(fitting_data,max_iter);


 double candidate_phase=fitting_fct_object.parameter(3);
 if (candidate_phase<0.0)
  {
   oomph_info << "Changing phase [1] from: " << candidate_phase << " to ";
   while (candidate_phase<0.0)
    {
     candidate_phase+=2.0*MathematicalConstants::Pi;
    }
   oomph_info << candidate_phase << std::endl;
   fitting_fct_object.parameter(3)=candidate_phase;
  }

 if (candidate_phase>2.0*MathematicalConstants::Pi)
  {
   oomph_info << "Changing phase [2] from: " << candidate_phase << " to ";
   while (candidate_phase>2.0*MathematicalConstants::Pi)
    {
     candidate_phase-=2.0*MathematicalConstants::Pi;
    }
   oomph_info << candidate_phase << std::endl;
   fitting_fct_object.parameter(3)=candidate_phase;
  }


 double candidate_freq=fitting_fct_object.parameter(2);
 double candidate_ampl=fitting_fct_object.parameter(1);
 if (candidate_freq<0.0)
  {
   oomph_info << "Changing amplitude, period and phase from: " 
              << candidate_ampl << " , "  << candidate_freq 
              << " , "  << candidate_phase 
              << " to ";

   fitting_fct_object.parameter(1)=-candidate_ampl;
   fitting_fct_object.parameter(2)=-candidate_freq;
   fitting_fct_object.parameter(3)=
    -candidate_phase+MathematicalConstants::Pi;
  }




 // Now check the accuracy:
 ofstream outfile;
 if (CommandLineArgs::command_line_flag_has_been_set("--fit_test_file"))
  {
   outfile.open(fit_test_file_name.c_str());
  }
 double error=0.0;
 for (unsigned i=0;i<n_fit;i++)
  {
   double x=fitting_data[i].first;
   double y_fit=fitting_fct_object.fitting_function(x);
   error+=pow((fitting_data[i].second-y_fit),2);
   if (CommandLineArgs::command_line_flag_has_been_set("--fit_test_file"))
    {
     outfile << x << " " << y_fit << " " << fitting_data[i].second << std::endl;
    }
  }
 std::cout << "RMS fitting error: " 
           << sqrt(error/double(n_fit))
           << std::endl;
 if (CommandLineArgs::command_line_flag_has_been_set("--fit_test_file"))
  {
   outfile.close();
  }

 
 // Output with fixed width
 std::cout.setf(ios_base::scientific,ios_base::floatfield);
 std::cout.width(15);
 std::cout << std::endl;
 std::cout << "Fitted parameters: " << std::endl;
 std::cout << "===================" << std::endl;
 std::cout << "Mean                            : " 
           << fitting_fct_object.parameter(0)<< std::endl;

 std::cout << "Amplitude                                  : " 
           << fitting_fct_object.parameter(1) << std::endl;

 std::cout << "Frequency                                   : " 
           << fitting_fct_object.parameter(2) << std::endl;

 std::cout << "Phase shift                                 : " 
           << fitting_fct_object.parameter(3)  << std::endl;

 std::cout << "Amplitude of deviation from long term mean  : " 
           << fitting_fct_object.parameter(4)  << std::endl;

 std::cout << "Decay rate of deviation from long term mean : " 
           << fitting_fct_object.parameter(5)  << std::endl;


 // Reset
 std::cout.setf(std::_Ios_Fmtflags(0), ios_base::floatfield);
 std::cout.width(0);




 // // Output coefficients:
 // std::cout << "\n\nCoefficients: " << std::endl;
 // unsigned n_param=damped_fitting_fct_object.nparameter();
 // for (unsigned i=0;i<n_param;i++)
 //  {
 //   std::cout << i << " ";
   
 //   // Output with fixed width
 //   std::cout.setf(ios_base::scientific,ios_base::floatfield);
 //   std::cout.width(15);

 //   std::cout << damped_fitting_fct_object.parameter(i) 
 //             << std::endl;

 //   // Reset
 //   std::cout.setf(std::_Ios_Fmtflags(0), ios_base::floatfield);
 //   std::cout.width(0);
 //  }


};
