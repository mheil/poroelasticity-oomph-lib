//LIC// ====================================================================
//LIC// This file forms part of oomph-lib, the object-oriented,
//LIC// multi-physics finite-element library, available
//LIC// at http://www.oomph-lib.org.
//LIC//
//LIC//           Version 0.90. August 3, 2009.
//LIC//
//LIC// Copyright (C) 2006-2009 Matthias Heil and Andrew Hazel
//LIC//
//LIC// This library is free software; you can redistribute it and/or
//LIC// modify it under the terms of the GNU Lesser General Public
//LIC// License as published by the Free Software Foundation; either
//LIC// version 2.1 of the License, or (at your option) any later version.
//LIC//
//LIC// This library is distributed in the hope that it will be useful,
//LIC// but WITHOUT ANY WARRANTY; without even the implied warranty of
//LIC// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//LIC// Lesser General Public License for more details.
//LIC//
//LIC// You should have received a copy of the GNU Lesser General Public
//LIC// License along with this library; if not, write to the Free Software
//LIC// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//LIC// 02110-1301  USA.
//LIC//
//LIC// The authors may be contacted at oomph-lib@maths.man.ac.uk.
//LIC//
//LIC//====================================================================
// Driver
#include <fenv.h>

#include "generic.h"
#include "axisym_poroelasticity.h"
#include "axisym_navier_stokes.h"
#include "meshes/triangle_mesh.h"

using namespace oomph;
using namespace std;


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////


//============================================================
/// Spline class -- padded to remain constant outside range
//============================================================
class Spline
{
 
public:
 
 /// \short Constructor: Pass x and y coordinates of function to be
 /// fitted (natural spline -- extension to prescribed slope is easy...)
 Spline(const Vector<double> &x_sample, const Vector<double> &y_sample) :
  X_sample(x_sample), Y_sample(y_sample)
  {

   Y_at_x_max=Y_sample[0];
   Y_at_x_min=Y_sample[0];
   X_min=X_sample[0];
   X_max=X_sample[0];


   int i=0;
   int k=0;
   double p=0.0;
   double qn=0.0;
   double sig=0.0;
   double un=0.0;;
   
   int n=x_sample.size();
   Spline_coeff.resize(n);
   Vector<double> u(n-1);
   
   // Natural spline
   // if (yp1 > 0.99e30)
   Spline_coeff[0]=u[0]=0.0;
   // else {
   //Spline_coeff[0] = -0.5;
   //u[0]=(3.0/(X_sample[1]-X_sample[0]))*
   //((Y_sample[1]-Y_sample[0])/(X_sample[1]-X_sample[0])-yp1);
   //}
   for (i=1;i<n-1;i++) 
    {

     if (X_sample[i]<X_min)
      {
       X_min=X_sample[i];
       Y_at_x_min=Y_sample[i];
      }
     if (X_sample[i]>X_max) 
      {
       X_max=X_sample[i];
       Y_at_x_max=Y_sample[i];
      }

     if ((X_sample[i+1]-X_sample[i])==0.0)
      {
       std::ostringstream error_stream;
       error_stream<< "X_sample[i+1] = X_sample[i] for i = " 
                   << i << std::endl;
       throw OomphLibError(
        error_stream.str(),
        OOMPH_CURRENT_FUNCTION,
        OOMPH_EXCEPTION_LOCATION);
      }
     if ((X_sample[i+1]-X_sample[i-1])==0.0)
      {
       std::ostringstream error_stream;
       error_stream<< "X_sample[i+1] = X_sample[i-1] for i = " 
                   << i << std::endl;
       throw OomphLibError(
        error_stream.str(),
        OOMPH_CURRENT_FUNCTION,
        OOMPH_EXCEPTION_LOCATION);
      }
     sig=(X_sample[i]-X_sample[i-1])/(X_sample[i+1]-X_sample[i-1]);
     p=sig*Spline_coeff[i-1]+2.0;
     Spline_coeff[i]=(sig-1.0)/p;
     u[i]=(Y_sample[i+1]-Y_sample[i])/(X_sample[i+1]-X_sample[i]) - 
      (Y_sample[i]-Y_sample[i-1])/(X_sample[i]-X_sample[i-1]);
     u[i]=(6.0*u[i]/(X_sample[i+1]-X_sample[i-1])-sig*u[i-1])/p;
    }
   // Natural spline
   //if (ypn > 0.99e30)
   qn=un=0.0;
   //else {
   //qn=0.5;
   //un=(3.0/(X_sample[n-1]-X_sample[n-2]))*
   //(ypn-(Y_sample[n-1]-Y_sample[n-2])/(X_sample[n-1]-X_sample[n-2]));
   // }

   {
    unsigned i=n-1;
    if (X_sample[i]<X_min)
     {
      X_min=X_sample[i];
      Y_at_x_min=Y_sample[i];
     }
    if (X_sample[i]>X_max) 
     {
      X_max=X_sample[i];
      Y_at_x_max=Y_sample[i];
     }
   }

   Spline_coeff[n-1]=(un-qn*u[n-2])/(qn*Spline_coeff[n-2]+1.0);
   for (k=n-2;k>=0;k--)
    {
     Spline_coeff[k]=Spline_coeff[k]*Spline_coeff[k+1]+u[k];
    }
  }
 
 
 /// Spline evalution -- padded to remain constant outside range
 double spline(const double& x)
  {

   if (x>X_max) return Y_at_x_max;
   if (x<X_min) return Y_at_x_min;

   double y=0.0;
   int k=0;
   double h=0.0;
   double b=0.0;
   double a=0.0;
   
   int n=X_sample.size();
   int klo=0;
   int khi=n-1;
   while (khi-klo > 1)
    {
     k=(khi+klo) >> 1;
     if (X_sample[k] > x) 
      {
       khi=k;
      }
     else
      {
       klo=k;
      }
    }
   h=X_sample[khi]-X_sample[klo];
   // never get here... now caught in setup (constructor)
   //if (h == 0.0)
   // {
   //  oomph_info << "Bad X_sample input to routine splint\n";
   //  abort();
   // }
   a=(X_sample[khi]-x)/h;
   b=(x-X_sample[klo])/h;
   y=a*Y_sample[klo]+b*Y_sample[khi]+((a*a*a-a)*Spline_coeff[klo]
                                      +(b*b*b-b)*Spline_coeff[khi])*(h*h)/6.0;
   
   return y;
  }
 
 
private:
 
 /// x samples
 Vector<double> X_sample;
 
 /// y samples
 Vector<double> Y_sample;
 
 /// Spline coefficients
 Vector<double> Spline_coeff;
 
 /// Value for padding to the right of data
 double Y_at_x_max;

 /// Value for padding to the left of data
 double Y_at_x_min;

 /// Min sample value
 double X_min;

 /// Max sample value
 double X_max;

};





///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

//======================================================================
/// Face element that allows "locate zeta" operations based on 
/// matching coordinate (here the axial one, i.e. x(1).
//======================================================================
template <class ELEMENT>
class EulerianBasedFaceElement : public virtual FaceGeometry<ELEMENT>, 
public virtual FaceElement
{

public:
 
 /// \short Constructor, which takes a "bulk" element and the 
 /// face index
 EulerianBasedFaceElement(FiniteElement* const &element_pt, 
                  const int &face_index) : 
  FaceGeometry<ELEMENT>(), FaceElement()
  {  
   //Attach the geometrical information to the element. N.B. This function
   //also assigns nbulk_value from the required_nvalue of the bulk element
   element_pt->build_face_element(face_index,this);
  }


 /// \short Constructor 
 EulerianBasedFaceElement() : 
  FaceGeometry<ELEMENT>(), FaceElement()
  {  
  }

 
 /// \short Intrinsic coordinate is the second coordinate (for now -- hierher;
 /// generalise) 
 double zeta_nodal(const unsigned &n, const unsigned &k,           
                          const unsigned &i) const 
  {
   return node_pt(n)->x(1); 
  } 

 /// Output nodal coordinates
 void output(std::ostream &outfile)
  {
   outfile << "ZONE" << std::endl;
   unsigned nnod=nnode();
   for (unsigned j=0;j<nnod;j++)
    {
     Node* nod_pt=node_pt(j);
     unsigned dim=nod_pt->ndim();
     for (unsigned i=0;i<dim;i++)
      {
       outfile << nod_pt->x(i) << " ";
      }
     outfile << std::endl;
    }     
  }

 /// Output at n_plot points
 void output(std::ostream &outfile, const unsigned &n_plot)
  {FiniteElement::output(outfile,n_plot);}

 /// C-style output 
 void output(FILE* file_pt)
  {FiniteElement::output(file_pt);}

 /// C_style output at n_plot points
 void output(FILE* file_pt, const unsigned &n_plot)
  {FiniteElement::output(file_pt,n_plot);}

}; 

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

//=======================================================================
/// FaceElement to output Navier-Stokes velocities and compute flux
//=======================================================================
template<class ELEMENT>
class AxisymmetricNavierStokesLinePostProcessingElement : 
 public virtual FaceGeometry<ELEMENT>,
 public virtual FaceElement
 {

 public:

  /// \short Contructor: Specify bulk element and index of face to which
  /// this face element is to be attached 
  AxisymmetricNavierStokesLinePostProcessingElement(
   FiniteElement* const &element_pt, const int &face_index) : 
   FaceGeometry<ELEMENT>(), FaceElement()
   {
    //Attach the geometrical information to the element, by
    //making the face element from the bulk element
    element_pt->build_face_element(face_index,this);
   }
  
  
  /// \short Output function: 
  /// r,z,u,v,w,p
  /// in tecplot format. Specified number of plot points in each
  /// coordinate direction.
  void output(std::ostream &outfile, const unsigned &nplot)
   {
    //Vector of local coordinates
    Vector<double> s(1);
    Vector<double> s_bulk(2);
    
    // Get bulk element
    ELEMENT* bulk_el_pt=dynamic_cast<ELEMENT*>(this->bulk_element_pt());
    
    // Tecplot header info
    outfile << tecplot_zone_string(nplot);
    
    // Loop over plot points
    unsigned num_plot_points=nplot_points(nplot);
    for (unsigned iplot=0;iplot<num_plot_points;iplot++)
     {
      
      // Get local coordinates of plot point
      get_s_plot(iplot,nplot,s);
      
      // Get coordinate in bulk
      s_bulk=local_coordinate_in_bulk(s);
      
      // Coordinates
      for(unsigned i=0;i<2;i++)
       {
        outfile << bulk_el_pt->interpolated_x(s_bulk,i) << " ";
       }
      
      // Velocities
      for(unsigned i=0;i<2;i++) 
       {
        outfile << bulk_el_pt->interpolated_u_axi_nst(s_bulk,i) << " ";
       }
      
      // Pressure
      outfile << bulk_el_pt->interpolated_p_axi_nst(s_bulk)  << " ";
      
      outfile << std::endl;   
     }
    
    // Write tecplot footer (e.g. FE connectivity lists)
    write_tecplot_zone_footer(outfile,nplot);
 
   }
  
  
  /// \short Return this element's contribution to the total volume enclosed
  /// by collection of these elements
  double contribution_to_enclosed_volume()
   {
    // Initialise
    double vol=0.0;
    
    //Find out how many nodes there are
    const unsigned n_node = this->nnode();
    
    //Set up memeory for the shape functions
    Shape psi(n_node);
    DShape dpsids(n_node,1);
    
    //Set the value of n_intpt
    const unsigned n_intpt = this->integral_pt()->nweight();
    
    //Storage for the local coordinate
    Vector<double> s(1);
    
    //Loop over the integration points
    for(unsigned ipt=0;ipt<n_intpt;ipt++)
     {
      //Get the local coordinate at the integration point
      s[0] = this->integral_pt()->knot(ipt,0);
      
      //Get the integral weight
      double W = this->integral_pt()->weight(ipt);
      
      //Call the derivatives of the shape function at the knot point
      this->dshape_local_at_knot(ipt,psi,dpsids);
      
      // Get position and tangent vector
      Vector<double> interpolated_t1(2,0.0);
      Vector<double> interpolated_x(2,0.0);
      for(unsigned l=0;l<n_node;l++)
       {
        //Loop over directional components
        for(unsigned i=0;i<2;i++)
         {
          interpolated_x[i]  += this->nodal_position(l,i)*psi(l);
          interpolated_t1[i] += this->nodal_position(l,i)*dpsids(l,0);
         }
       }
      
      //Calculate the length of the tangent Vector
      double tlength = interpolated_t1[0]*interpolated_t1[0] + 
       interpolated_t1[1]*interpolated_t1[1];
      
      //Set the Jacobian of the line element
      double J = sqrt(tlength)*interpolated_x[0];
      
      //Now calculate the normal Vector
      Vector<double> interpolated_n(2);
      this->outer_unit_normal(ipt,interpolated_n);
      
      // Assemble dot product
      double dot = 0.0;
      for(unsigned k=0;k<2;k++) 
       {
        dot += interpolated_x[k]*interpolated_n[k];
       }
      
      // Add to volume with sign chosen so that...
      
      // Factor of 1/3 comes from div trick
      vol  += 2.0*MathematicalConstants::Pi*dot*W*J/3.0;
      
     }
    
    return vol;
   }
  
  /// \short Compute contribution to integrated volume flux over this element
  /// NOTE: This evaluates the actual 3D flux so if a unit normal velocity
  /// crosses a radial line between r_min and r_max, the volume
  /// flux computed by this procedure is \pi (r_max^2-r_min^2).
  double contribution_to_total_volume_flux()
   {
    // Get pointer to bulk element
    ELEMENT* bulk_el_pt=dynamic_cast<ELEMENT*>(bulk_element_pt());
    
    //Find out how many nodes there are
    const unsigned n_node = this->nnode();
    
    //Set up memeory for the shape functions
    Shape psi(n_node);
    DShape dpsids(n_node,1);
    
    //Get the value of Nintpt
    const unsigned n_intpt = integral_pt()->nweight();
    
    //Set the Vector to hold local coordinates
    Vector<double> s(1);
    Vector<double> s_bulk(2);
    
    //Loop over the integration points
    double flux_contrib=0.0;
    for(unsigned ipt=0;ipt<n_intpt;ipt++)
     {
      //Assign values of s
      s[0] = integral_pt()->knot(ipt,0);
      
      //Get the integral weight
      double W = this->integral_pt()->weight(ipt);
      
      //Call the derivatives of the shape function at the knot point
      this->dshape_local_at_knot(ipt,psi,dpsids);
      
      // Get position and tangent vector
      Vector<double> interpolated_t1(2,0.0);
      Vector<double> interpolated_x(2,0.0);
      for(unsigned l=0;l<n_node;l++)
       {
        //Loop over directional components
        for(unsigned i=0;i<2;i++)
         {
          interpolated_x[i]  += this->nodal_position(l,i)*psi(l);
          interpolated_t1[i] += this->nodal_position(l,i)*dpsids(l,0);
         }
       }
      
      //Calculate the length of the tangent Vector
      double tlength = interpolated_t1[0]*interpolated_t1[0] + 
       interpolated_t1[1]*interpolated_t1[1];
      
      //Set the Jacobian of the line element
      double J = sqrt(tlength)*interpolated_x[0];
      
      // Get the outer unit normal
      Vector<double> interpolated_normal(2);
      outer_unit_normal(s,interpolated_normal);
      
      // Get coordinate in bulk element
      s_bulk=this->local_coordinate_in_bulk(s);
      
      /// Calculate the FE representation of u
      Vector<double> u(2);
      u[0]=bulk_el_pt->interpolated_u_axi_nst(s_bulk,0);
      u[1]=bulk_el_pt->interpolated_u_axi_nst(s_bulk,1);
      
#ifdef PARANOID    
      Vector<double> x_bulk(2);
      x_bulk[0]=bulk_el_pt->interpolated_x(s_bulk,0);
      x_bulk[1]=bulk_el_pt->interpolated_x(s_bulk,1);
      double error=sqrt((interpolated_x[0]-x_bulk[0])*
                        (interpolated_x[0]-x_bulk[0])+
                        (interpolated_x[1]-x_bulk[1])*
                        (interpolated_x[1]-x_bulk[1]));
      double tol=1.0e-10;
      if (error>tol)
       {
        std::stringstream junk;
        junk 
         << "Gap between bulk and face element coordinate\n"
         << "is suspiciously large: "
         << error << "\nBulk at: " 
         << x_bulk[0] << " " << x_bulk[1] << "\n" 
         << "Face at: " << interpolated_x[0] << " " 
         << interpolated_x[1] << "\n";
        OomphLibWarning(junk.str(),
                        OOMPH_CURRENT_FUNCTION,
                        OOMPH_EXCEPTION_LOCATION);
       }
#endif
      
    // Get net volume flux
    double flux=0.0;
    for(unsigned i=0;i<2;i++)
     {
      flux+=u[i]*interpolated_normal[i];
     }
    
    // Add 
    flux_contrib += 2.0*MathematicalConstants::Pi*flux*W*J;
   }
  return flux_contrib;
}
 




};


///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

//=======================================================================
/// FaceElement to compute volume enclosed by collection of elements
/// (attached to bulk elements of type ELEMENT).
//=======================================================================
template<class ELEMENT>
class AxisymmetricPoroelasticityLinePostProcessingElement : 
 public virtual FaceGeometry<ELEMENT>,
 public virtual FaceElement
 {

 public:

  /// \short Contructor: Specify bulk element and index of face to which
  /// this face element is to be attached 
  AxisymmetricPoroelasticityLinePostProcessingElement(
   FiniteElement* const &element_pt, const int &face_index) : 
   FaceGeometry<ELEMENT>(), FaceElement()
   {
    //Attach the geometrical information to the element, by
    //making the face element from the bulk element
    element_pt->build_face_element(face_index,this);


    /// Sign of normal
    Normal_sign=1.0;
   }

  
  /// \short Change direction of normal compared to outer unit normal
  /// of bulk element that this face element is attached to
  void set_negative_normal()
   {
    Normal_sign=-1.0;
   }

  /// \short (Re-)set direction of normal to be the same as  outer unit normal
  /// of bulk element that this face element is attached to
  void set_positive_normal()
   {
    Normal_sign=1.0;
   }

  
  /// \short Return this element's contribution to the total volume enclosed
  /// and (via arg list) increment
  double contribution_to_enclosed_volume(double& 
                                         contribution_to_volume_increment)
   {
    bool bulk_is_solid_element=true;
    if (dynamic_cast<ELEMENT*>(bulk_element_pt())==0)
     {
      bulk_is_solid_element=false;
     }

    // Initialise
    double vol=0.0;
    double dvol=0.0;

    //Find out how many nodes there are
    const unsigned n_node = this->nnode();
    
   //Set up memeory for the shape functions
    Shape psi(n_node);
    DShape dpsids(n_node,1);
    
    //Set the value of n_intpt
    const unsigned n_intpt = this->integral_pt()->nweight();
    
    //Storage for the local coordinate
    Vector<double> s(1);
    
    //Loop over the integration points
    for(unsigned ipt=0;ipt<n_intpt;ipt++)
     {
      //Get the local coordinate at the integration point
      s[0] = this->integral_pt()->knot(ipt,0);
      
      //Get the integral weight
      double W = this->integral_pt()->weight(ipt);
      
      //Call the derivatives of the shape function at the knot point
      this->dshape_local_at_knot(ipt,psi,dpsids);
      
      // Get position and tangent vector
      Vector<double> interpolated_t1(2,0.0);
      Vector<double> interpolated_x(2,0.0);
      Vector<double> interpolated_u(2,0.0);
      for(unsigned l=0;l<n_node;l++)
       {
        //Loop over directional components
        for(unsigned i=0;i<2;i++)
         {
          interpolated_x[i]  += this->nodal_position(l,i)*psi(l);
          if (bulk_is_solid_element)
           {
            interpolated_u[i]  += this->nodal_value(l,i)*psi(l);
           }
          interpolated_t1[i] += this->nodal_position(l,i)*dpsids(l,0);
         }
       }
      
      //Calculate the length of the tangent Vector
      double tlength = interpolated_t1[0]*interpolated_t1[0] + 
       interpolated_t1[1]*interpolated_t1[1];
      
      //Set the Jacobian of the line element
      double J = sqrt(tlength)*interpolated_x[0];
      
      //Now calculate the normal Vector
      Vector<double> interpolated_n(2);
      this->outer_unit_normal(ipt,interpolated_n);
      
      // Assemble dot product
      double dot = 0.0;
      double ddot = 0.0;
      for(unsigned k=0;k<2;k++) 
       {
        dot += Normal_sign*interpolated_x[k]*interpolated_n[k];
        ddot += Normal_sign*interpolated_u[k]*interpolated_n[k];
       }
      
      // Add to volume with sign chosen so that the volume is
      // positive when the elements bound the solid

      // Factor of 1/3 comes from div trick
      vol  += 2.0*MathematicalConstants::Pi*dot*W*J/3.0;

      // No factor three here because this is actually the
      // genuine expression for the increase in volume (equivalent of
      // div trick would require incorporation of u and its derivs into
      // outer unit normal, Jacobian etc.)
      dvol += 2.0*MathematicalConstants::Pi*ddot*W*J;
     }
    
    contribution_to_volume_increment=dvol;
    return vol;
   }
  

  /// \short Output function
  void output(std::ostream &outfile)
   {
    unsigned n_plot=5;
    output(outfile,n_plot);
   }
  
  
 /// \short Output function
  void output(std::ostream &outfile, const unsigned &n_plot)
   {
    unsigned n_dim = this->nodal_dimension();
    Vector<double> x(n_dim);
    Vector<double> s(n_dim-1);

    // Tecplot header info
    outfile << this->tecplot_zone_string(n_plot);
    
    // Loop over plot points
    unsigned num_plot_points=this->nplot_points(n_plot);
    for (unsigned iplot=0;iplot<num_plot_points;iplot++)
     {
      // Get local coordinates of plot point
      this->get_s_plot(iplot,n_plot,s);
      
      // Get Eulerian and Lagrangian coordinates
      this->interpolated_x(s,x);
      
      // Outer unit normal
      Vector<double> unit_normal(n_dim);
      outer_unit_normal(s,unit_normal);
      
      //Output the x,y,..
      for(unsigned i=0;i<n_dim;i++) 
       {outfile << x[i] << " ";} // column 1,2
      
      // Output outer unit normal
      for(unsigned i=0;i<n_dim;i++) 
       { 
        outfile << Normal_sign*unit_normal[i] << " "; // column 3,4
       } 
      
      outfile << std::endl;
     }
    
    // Write tecplot footer (e.g. FE connectivity lists)
    this->write_tecplot_zone_footer(outfile,n_plot);
   }

 private:

  /// Sign of normal
  double Normal_sign;

};


///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

//===Start_of_Global_Physical_Variables_namespace======================
/// Namespace for global parameters
//======================================================================
namespace Global_Physical_Variables
{

 // Use straight syrinx boundaries (true; default) or round off with ellipses
 bool Use_straight_syrinx_boundaries=true; 

 /// Match Nst and poro discretisation along syrinx fsi interface?
 bool Match_syrinx_nst_and_poro_discretisation=true; 

 /// \short History level at which wall position is evaluated for fluid node
 /// update. Should be -1 (none; default); 0 (fully implicit) 1 (lagged)\n";
 int History_level_for_fluid_mesh_node_update=-1;

 /// Output directory
 std::string Directory = "RESLT";

 /// Name of restart file
 std::string Restart_file="";




 /// Global FSI parameter -- dependent parameter; assigned below
 double Q = 1.0e-9;

 /// Period of forcing in seconds
 double Excitation_period_in_seconds=0.4;

 /// Period of forcing -- dependent parameter; compute
 double Excitation_period=0.0;

 /// Fraction of pressure peak for heat-wave-like forcing
 double Fraction_of_pressure_peak=0.4;



 // Fluid parameters
 // ----------------

 /// The Reynolds number
 double Re = 10.0;

 /// The Strouhal number
 double St = 1.0;

 /// \short The Womersley number -- dependent parameter; compute
 /// from Re and St
 double Wo = 0.0;

 /// Conversion factor for time
 double T_factor_in_inverse_seconds=2.5e-3; 
 
 /// Factor for wall inertia parameter (based on dura properties)
 double Lambda_factor=1.414213562e-6; 
 
 /// Factor for FSI parameter (based on dura properties)
 double Q_factor=2.0e-12;
 
 /// Factor for non-dim permeability (based on dura properties)
 double K_factor=125.0; 

 /// Poisson ratio for drained poro-elastic media (0.35 results in
 /// desired effective nu when used with porosity of n=0.3)
 double Nu_drained=0.35;

 // Dura properties
 //----------------

 /// \short Stiffness ratio for dura and pia: ratio of actual Young's modulus
 /// to Young's modulus used in the non-dim of the equations (unity because 
 /// we've scaled things on their properties)
 double Stiffness_ratio_dura=1.0;

 /// \short Permeability ratio; ratio of actual permeability
 /// to permeability used in the non-dim of the equations (unity because 
 /// we've scaled things on their properties)
 double Permeability_ratio_dura=1.0;

 /// Dura and Pia Poisson's ratio for drained poro-elastic medium
 double Nu_dura = 0.35;

 /// Porosity for dura and pia
 double Porosity_dura = 0.3;


 // Pia properties
 //---------------

 /// \short Stiffness ratio for dura and pia: ratio of actual Young's modulus
 /// to Young's modulus used in the non-dim of the equations (unity because 
 /// we've scaled things on their properties)
 double Stiffness_ratio_pia=1.0;

 /// \short Permeability ratio; ratio of actual permeability
 /// to permeability used in the non-dim of the equations (unity because 
 /// we've scaled things on their properties)
 double Permeability_ratio_pia=1.0;

 /// Dura and Pia Poisson's ratio for drained poro-elastic medium
 double Nu_pia = 0.35;

 /// Porosity for dura and pia
 double Porosity_pia = 0.3;


 // Cord properties
 //----------------

 /// \short Stiffness ratio for cord: ratio of actual Young's modulus
 /// to Young's modulus used in the non-dim of the equations
 double Stiffness_ratio_cord=0.004;

 /// \short Permeability ratio for cord; ratio of actual permeability
 /// to permeability used in the non-dim of the equations
 double Permeability_ratio_cord=1.0;

 /// Cord Poisson's ratio for drained poro-elastic medium
 double Nu_cord = 0.35;

 /// Porosity for cord
 double Porosity_cord = 0.3;

 // Filum properties
 //-----------------

 /// \short Stiffness ratio for filum and block: ratio of actual Young's modulus
 /// to Young's modulus used in the non-dim of the equations
 double Stiffness_ratio_filum=0.050;

 /// \short Permeability ratio for filum and block; ratio of actual 
 /// permeability to permeability used in the non-dim of the equations
 double Permeability_ratio_filum=1.0;

 /// Filum and block Poisson's ratio for drained poro-elastic medium
 double Nu_filum = 0.35;

 /// Porosity for filum and block
 double Porosity_filum= 0.3;

 // Block properties
 //-----------------

 /// \short Stiffness ratio for filum and block: ratio of actual Young's modulus
 /// to Young's modulus used in the non-dim of the equations
 double Stiffness_ratio_block=0.050;

 /// \short Permeability ratio for filum and block; ratio of actual 
 /// permeability to permeability used in the non-dim of the equations
 double Permeability_ratio_block=1.0;

 /// Filum and block Poisson's ratio for drained poro-elastic medium
 double Nu_block = 0.35;

 /// Porosity for filum and block
 double Porosity_block= 0.3;


 // Generic poroelasticity parameters
 // ---------------------------------

 /// \short The poroelasticity inertia parameter -- dependent parameter;
 /// compute from Re lambda factor
 double Lambda_sq = 0;

 /// \short Non-dim permeability -- ratio of typical porous flux to fluid veloc
 /// scale -- dependent parameter; compute from Re and k factor
 double Permeability = 0.0;

 // Alpha, the Biot parameter (same for all; incompressible fluid and solid)
 double Alpha = 1.0;

 /// \short Ratio of the densities of the fluid and solid phases of
 /// the poroelastic material (same for all)
 double Density_ratio = 1.0;

 /// \short Permeability ratio (actual permeability to reference value);
 ///  currently assumed to be the same for all; can fine grain this if needed by
 /// making direct assignments to corresponding properties of the
 /// various (poro-)elastic regions
 double Permeability_ratio=1.0;

 /// Inverse slip rate coefficient
 double Inverse_slip_rate_coefficient = 0.0;

 /// A zero!
 const double Zero = 0.0;

 // Coords of syrinx in reference config.
 //--------------------------------------

 /// Upper z coord of syrinx in ref position
 double Z_syrinx_top_centreline=-80.0;

 /// Lower z coord of syrinx in ref position
 double Z_syrinx_bottom_centreline=-220.0;

 /// z coord of upper syrinx vertex in ref position
 double Z_syrinx_top_fsi=-90.0;

 /// z coord of lower syrinx vertex in ref position
 double Z_syrinx_bottom_fsi=-210.0;

 /// r coord of upper syrinx vertex in ref position
 double R_syrinx_top_syrinx_fsi=4.512;

 /// r coord of lower syrinx vertex in ref position
 double R_syrinx_bottom_syrinx_fsi=4.128;

 /// \short r coord of point on sss cord interface at the same level
 /// as upper syrinx vertex in ref position
 double R_syrinx_top_sss_fsi=5.64;

 /// \short r coord of point on sss cord interface at the same level
 /// as lower syrinx vertex in ref position
 double R_syrinx_bottom_sss_fsi=5.16;


 /// raw radius of cord/sss fsi interface (straight bit starting at inlet)
 double raw_cord_sss_fsi_radius(const double& z_raw)
 {
  return 6.0-(6.0-4.24)*(-z_raw)/440.0;
 }

 /// raw radius of cord/syrinx fsi interface (syrinx cover)
 double raw_cord_syrinx_fsi_radius(const double& z_raw)
 {
  return 4.8-(4.8-3.392)*(-z_raw)/440.0;
 }

 /// raw radius of pia/coord boundary (straight bit starting at inlet)
 double raw_cord_pia_boundary_radius(const double& z_raw)
 {
  return 5.8-(5.8-4.04)*(-z_raw)/440.0;
 }


 /// Raw z shift of syrinx
 double Raw_syrinx_z_shift=0.0;

 /// \short Interpret shift as shortening (if shift is positive) by setting
 /// this to -1.0. This moves the upper end of the syririnx downwards
 /// while genuinely shifting the lower end upwards.
 double Shorten_syrinx_by_shift_factor=1.0;

 /// \short Update syrinx-associated coordinates and return
 /// change of radius of syrinx cover on syrinx and sss fsi boundary
 void update_syrinx_associated_coordinates(
  double& dr_syrinx_fsi, double& dr_sss_fsi)
 {

  // slope of syrinx cover on syrinx side
  double syrinx_cover_slope_on_syrinx_side=
   (R_syrinx_top_syrinx_fsi-R_syrinx_bottom_syrinx_fsi)/
   (Z_syrinx_top_fsi-Z_syrinx_bottom_fsi);

  // slope of syrinx cover on sss side
  double syrinx_cover_slope_on_sss_side=
   (R_syrinx_top_sss_fsi-R_syrinx_bottom_sss_fsi)/
   (Z_syrinx_top_fsi-Z_syrinx_bottom_fsi);

  oomph_info << "Slopes of syrinx cover (syrinx, sss):  " 
             << syrinx_cover_slope_on_syrinx_side << " " 
             << syrinx_cover_slope_on_sss_side << std::endl; 
  
  dr_syrinx_fsi=syrinx_cover_slope_on_syrinx_side*
   Raw_syrinx_z_shift;

  dr_sss_fsi=syrinx_cover_slope_on_sss_side*
   Raw_syrinx_z_shift;

  oomph_info << "Change in radius of syrinx cover (syrinx, sss):  " 
             << dr_syrinx_fsi << " " 
             << dr_sss_fsi << std::endl; 
  
  // Upper z coord of syrinx in ref position
  Z_syrinx_top_centreline+=Shorten_syrinx_by_shift_factor*Raw_syrinx_z_shift;
  
  // Lower z coord of syrinx in ref position
  Z_syrinx_bottom_centreline+=Raw_syrinx_z_shift;
  
  // z coord of upper syrinx vertex in ref position
  Z_syrinx_top_fsi+=Shorten_syrinx_by_shift_factor*Raw_syrinx_z_shift;
  
  // z coord of lower syrinx vertex in ref position
  Z_syrinx_bottom_fsi+=Raw_syrinx_z_shift;

  // r coord of upper syrinx vertex in ref position
  R_syrinx_top_syrinx_fsi+=Shorten_syrinx_by_shift_factor*dr_syrinx_fsi;

 // r coord of lower syrinx vertex in ref position
  R_syrinx_bottom_syrinx_fsi+=dr_syrinx_fsi;

  // r coord of point on sss cord interface at the same level
  // as upper syrinx vertex in ref position
  R_syrinx_top_sss_fsi+=Shorten_syrinx_by_shift_factor*dr_sss_fsi;
  
  // r coord of point on sss cord interface at the same level
  // as lower syrinx vertex in ref position
  R_syrinx_bottom_sss_fsi+=dr_sss_fsi;
 }


 // Mesh/problem parameters
 // -----------------------

 /// Z coordinate of upstream end of fine region in sss
  double Z_upstream_fine_sss_region=-105.5;

 /// Z coordinate of upstream end of fine region in sss
 double Z_downstream_fine_sss_region=-195.0;


 /// Target element area for fluid meshes
 double Element_area_fluid = 0.01;

 /// Target element area for poro-elastic meshes
 double Element_area_poro = 0.01;

 /// Target element area for syrinx cover
 double Element_area_syrinx_cover = 0.01;

 /// Target element area in SSS in the upstream/downstream vicinity of block
 double Element_area_sss_near_block = 0.01;

 /// Target element area in SSS under block and surrounding bl
 double Element_area_sss_under_block_and_bl = 0.01;

 /// Target element area for fuid elements in syrinx
 double Element_area_syrinx = 0.01;

 /// Raw boundary layer thickness (in Chris' original coordinates)
 double BL_thick=0.1;

 /// Raw poro-elastic boundary layer thickness (in Chris' original coordinates)
 double BL_thick_poro=1.0;

 /// Scaling factor for poro-elastic boundary layer. Scales 
 /// BL_thick_poro linearly and element size in layer with square of that
 /// factor (relative to Element_area_syrinx_cover.
 double BL_poro_scaling_factor=1.0;

 /// \short Element edge length on poro syrinx fsi boundary (in Chris' 
 /// original coordinates). Only used if specified via command line
 double Ds_syrinx_poro_fsi_boundary_raw=0.0;

 /// \short Length over which  poro syrinx fsi boundary is resolved more
 /// finely near corner singularities (in Chris' 
 /// original coordinates).  Only used if specified via command line.
 double Delta_syrinx_poro_fsi_boundary_near_sing_raw=0.0; 

 /// \short Element edge length in more finely resolved poro syrinx 
 /// fsi boundary  near corner singularities (in Chris' 
 /// original coordinates). Only used if specified via command line.
 double Ds_syrinx_poro_fsi_boundary_near_sing_raw=0.0;

 /// \short Moens Korteweg wavespeed based on dura properties -- dependent
 /// parameter
 double Dura_wavespeed=0.0;

 /// \short Overall scaling factor make inner diameter of outer porous
 /// region at inlet equal to one so Re etc. mean what they're
 /// supposed to mean
 double Length_scale = 1.0/20.0;

 /// Radial scaling -- vanilla length scale
 double R_scale = Length_scale;
 
 /// Aspect ratio factor to shrink domain in z direction
 double Z_shrink_factor=1.0;

 /// Axial scaling -- dependent parameter; update
 double Z_scale = Length_scale/Z_shrink_factor;

 /// Suppress regularly spaced output (can change via command line)
 bool Suppress_regularly_spaced_output=false;
 
 /// Regular output with approx 3 per thickness of dura
 unsigned Nplot_r_regular=33; 
 
 /// Regular output with equivalent axial spacing
 unsigned Nplot_z_regular=
  unsigned(double(Global_Physical_Variables::Nplot_r_regular)*600.0/11.0
           /Global_Physical_Variables::Z_shrink_factor);
 

 //-----------------------------------------------------------------------------



 /// Loads on inner poro for steady solves (volume change computations)
 //--------------------------------------------------------------------

 /// \short Spline representing SSS pressure (on fluid scale if adjusted to achieve
 /// zero net flux), read in from file
 Spline* SSS_pressure_spline_pt=0;

 /// \short Filename of file containing cycle average sss pressure
 /// whose "lower" end is being adjusted to get zero net flux into
 /// syrinx
 std::string SSS_cycle_average_pressure_filename="";

 /// \short Spline representing syrinx pressure (on solid scale if adjusted), read in
 /// from file
 Spline* Syrinx_pressure_spline_pt=0;

 /// \short Filename of file containing cycle average syrinx pressure
 std::string Syrinx_cycle_average_pressure_filename="";


 //----

 /// \short Spline representing syrinx radial traction, read in
 /// from file
 Spline* Syrinx_radial_traction_spline_pt=0;

 /// \short Filename of file containing cycle average syrinx radial traction
 std::string Syrinx_cycle_average_radial_traction_filename="";


 /// \short Spline representing syrinx axial traction, read in
 /// from file
 Spline* Syrinx_axial_traction_spline_pt=0;

 /// \short Filename of file containing cycle average syrinx axial traction
 std::string Syrinx_cycle_average_axial_traction_filename="";


 /// \short Spline representing SSS radial traction, read in
 /// from file
 Spline* SSS_radial_traction_spline_pt=0;

 /// \short Filename of file containing cycle average SSS radial traction
 std::string SSS_cycle_average_radial_traction_filename="";

 /// \short Spline representing SSS axial traction, read in
 /// from file
 Spline* SSS_axial_traction_spline_pt=0;

 /// \short Filename of file containing cycle average SSS axial traction
 std::string SSS_cycle_average_axial_traction_filename="";



 /// Setup spline 
 void setup_spline(const std::string& filename, Spline*& spline_pt)
 {
  // Storage for sample points
  Vector<double> x_sample;
  Vector<double> y_sample;
  
  ifstream sample_file;
  sample_file.open(filename.c_str());
  double x=0.0;
  double y=0.0;
  double x_prev=0.0;
  std::string line;
  while (std::getline(sample_file, line))
   {
    if (line.empty()) continue;     // skips empty lines
    std::istringstream is(line);    // construct temporary istringstream
    is >> x >> y;
    bool no_duplicate=true;
    if (x_sample.size()>0)
     {
      if (x==x_prev) no_duplicate=false;
     }
    if (no_duplicate)
     {
      x_sample.push_back(x);
      y_sample.push_back(y);
     }
    x_prev=x;
   }
  
  // Build spline
  spline_pt=new Spline(x_sample,y_sample);
 }

 /// Steady state syrinx pressure (on fluid scale!)
 double P_steady_state_syrinx=350.0;
 
 /// Steady state syrinx pressure
 void steady_state_syrinx_pressure(const double &time,
                                   const Vector<double> &x,
                                   const Vector<double> &n,
                                   double &result)
 {
  result=Q*P_steady_state_syrinx;
 }
 
 
 /// Traction exerted by syrinx
 void steady_state_syrinx_traction(const double &time,
                                   const Vector<double> &x,
                                   const Vector<double> &n,
                                   Vector<double> &traction)
 {
  traction[0]=-Q*P_steady_state_syrinx*n[0];
  traction[1]=-Q*P_steady_state_syrinx*n[1];
 }


 /// Steady state sss pressure upper end (on fluid scale)
 double P_steady_state_sss_upper=0.0;

 /// Steady state sss pressure lower end (in fluid scale)
 double P_steady_state_sss_lower=1100.0;;

 /// Width of region over which prescribed steady pressure drops (raw coordinates)
 double Width_steady_state_sss_pressure_transition_raw=20.0;

 /// Adjustment point for biased read-in sss pressure distribution
 double Z_at_p_sss_change_start_raw=-150.0; 

 /// Shift for sss pressure profile (in raw coordinates)
 double Z_sss_p_shift_raw=0.0;

 /// Steady state sss pressure
 void steady_state_sss_pressure(const double &time,
                                const Vector<double> &x,
                                const Vector<double> &n,
                                double &result)
 {
  // Shift profile
  double z_sss_p_shift=Z_sss_p_shift_raw*Z_scale*Z_shrink_factor;

  // Mean pressure read in, adjusted for variable lower pressure
  if (SSS_pressure_spline_pt!=0)
   {
    // Bias pressure
    double z_at_p_sss_change_start=
     Z_at_p_sss_change_start_raw*Z_scale*Z_shrink_factor;

    double p_at_p_change_start=SSS_pressure_spline_pt->spline(z_at_p_sss_change_start);
    double z_min=-600.0*Z_scale*Z_shrink_factor;
    double p_at_left_end=SSS_pressure_spline_pt->spline(z_min);
    double new_p_at_left_end=P_steady_state_sss_lower;
    double scaling_factor=(new_p_at_left_end-p_at_p_change_start)/
     (p_at_left_end-p_at_p_change_start);
    
    double delta_p=SSS_pressure_spline_pt->spline(x[1]-z_sss_p_shift)-p_at_p_change_start;
    double factor=1.0;
    if (x[1]-z_sss_p_shift<z_at_p_sss_change_start)
     {
      factor=scaling_factor;
     }
    result=Q*(p_at_p_change_start+factor*delta_p);   
   }
  // Made-up, piecewise linear pressure variation (without minimum 
  // under block)
  else
   {
    double z_steady_state_sss_pressure_transition_upper_raw=
     Z_at_p_sss_change_start_raw+0.5*Width_steady_state_sss_pressure_transition_raw;

    double z_steady_state_sss_pressure_transition_lower_raw=
     Z_at_p_sss_change_start_raw-0.5*Width_steady_state_sss_pressure_transition_raw;

    if (x[1]-z_sss_p_shift>z_steady_state_sss_pressure_transition_upper_raw*
        Z_scale*Z_shrink_factor)
     {
      result=Q*P_steady_state_sss_upper;
     }
    else if (x[1]-z_sss_p_shift<z_steady_state_sss_pressure_transition_lower_raw*
             Z_scale*Z_shrink_factor)
     {
      result=Q*P_steady_state_sss_lower;
     }
    else
     {
      double z_upper=z_sss_p_shift+z_steady_state_sss_pressure_transition_upper_raw*
       Z_scale*Z_shrink_factor;
      
      double z_lower=z_sss_p_shift+z_steady_state_sss_pressure_transition_lower_raw*
       Z_scale*Z_shrink_factor;
      
      result=Q*(P_steady_state_sss_lower+
                (P_steady_state_sss_upper-P_steady_state_sss_lower)
                *(x[1]-z_lower)/(z_upper-z_lower));
     }
   }
 }
 
 
 /// Traction exerted by sss
 void steady_state_sss_traction(const double &time,
                                const Vector<double> &x,
                                const Vector<double> &n,
                                Vector<double> &traction)
 {
  // Get pressure
  double p=0.0;
  steady_state_sss_pressure(time,x,n,p);
  traction[0]=-p*n[0];
  traction[1]=-p*n[1];
 }



//------------------------------------------------------------------------------


 /// Read in sss pressure (on wall scale!)
 void read_in_sss_pressure(const double &time,
                           const Vector<double> &x,
                           const Vector<double> &n,
                           double &result)
 {

  // Pressure read in
  if (SSS_pressure_spline_pt!=0)
   {    
    result=SSS_pressure_spline_pt->spline(x[1]);
   }
  else
   {
    abort();
   }
 }
 
 
 /// Traction exerted by sss (on wall scale!)
 void read_in_sss_traction(const double &time,
                           const Vector<double> &x,
                           const Vector<double> &n,
                           Vector<double> &traction)
 {
  // Get pressure
  double p=0.0;
  read_in_sss_pressure(time,x,n,p);

  if (CommandLineArgs::command_line_flag_has_been_set(
       "--sss_cycle_average_radial_traction_filename"))
   {
    traction[0]=SSS_radial_traction_spline_pt->spline(x[1]);
   }
  else
   {
    traction[0]=-p*n[0];
   }
  if (CommandLineArgs::command_line_flag_has_been_set(
       "--sss_cycle_average_axial_traction_filename"))
   {
    traction[1]=SSS_axial_traction_spline_pt->spline(x[1]);
   }
  else
   {
    traction[1]=-p*n[1];
   }
 }




//------------------------------------------------------------------------------


 /// Read in syrinx pressure (on wall scale!)
 void read_in_syrinx_pressure(const double &time,
                              const Vector<double> &x,
                              const Vector<double> &n,
                              double &result)
 {
  
  // Pressure read in
  if (Syrinx_pressure_spline_pt!=0)
   {    
    result=Syrinx_pressure_spline_pt->spline(x[1]);
   }
  else
   {
    abort();
   }
 }
 
 
 /// Traction exerted by syrinx (on wall scale!)
 void read_in_syrinx_traction(const double &time,
                              const Vector<double> &x,
                              const Vector<double> &n,
                              Vector<double> &traction)
 {
  // Get pressure
  double p=0.0;
  read_in_syrinx_pressure(time,x,n,p);

  if (CommandLineArgs::command_line_flag_has_been_set(
       "--syrinx_cycle_average_radial_traction_filename"))
   {
    traction[0]=Syrinx_radial_traction_spline_pt->spline(x[1]);
   }
  else
   {
    traction[0]=-p*n[0];
   }
  if (CommandLineArgs::command_line_flag_has_been_set(
       "--syrinx_cycle_average_axial_traction_filename"))
   {
    traction[1]=Syrinx_axial_traction_spline_pt->spline(x[1]);
   }
  else
   {
    traction[1]=-p*n[1];
   }
 }


 //-----------------------------------------------------------------------------

 /// \short Helper function to update dependent parameters (based on the
 /// linearised_poroelastic_fsi_pulsewave version)
 void update_dependent_parameters()
  {

   // changed permeability ratio? 
   if (CommandLineArgs::command_line_flag_has_been_set("--permeability_ratio"))
    {
     Global_Physical_Variables::Permeability_ratio_dura=
      Global_Physical_Variables::Permeability_ratio;
     Global_Physical_Variables::Permeability_ratio_pia=
      Global_Physical_Variables::Permeability_ratio;
     Global_Physical_Variables::Permeability_ratio_cord=
      Global_Physical_Variables::Permeability_ratio;
     Global_Physical_Variables::Permeability_ratio_filum=
      Global_Physical_Variables::Permeability_ratio;
     Global_Physical_Variables::Permeability_ratio_block=
      Global_Physical_Variables::Permeability_ratio;
     oomph_info << "\n\nNOTE: Set all permeability ratios to to: "
                << Global_Physical_Variables::Permeability_ratio 
                << "\n\n";
    }
   
   if (CommandLineArgs::command_line_flag_has_been_set
       ("--suppress_wall_inertia"))
    {
     Lambda_factor=0.0;
     oomph_info <<"\n\nNOTE: Set wall inertial to zero!\n\n";
    }

   if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
    {
     oomph_info << "Darcy is switched off everywhere. Setting Poisson's \n"
                << "ratio to 0.49\n";
     Nu_dura = 0.49;
     Nu_pia = 0.49;
     Nu_cord = 0.49;
     Nu_filum = 0.49;
     Nu_block = 0.49;
    }
   else
    {
     if (CommandLineArgs::command_line_flag_has_been_set
         ("--everything_is_porous"))
      {
       oomph_info << "Everything is porous. Setting Poisson's \n"
                  << "ratio everywhere to " << Nu_drained << "\n";
       Nu_dura = Nu_drained;
       Nu_pia = Nu_drained;
       Nu_cord = Nu_drained;
       Nu_filum = Nu_drained;
       Nu_block = Nu_drained;
      }
     else if (CommandLineArgs::command_line_flag_has_been_set
              ("--cord_pia_and_filum_are_porous"))
      {
       
       oomph_info << "Cord, pia and filum are porous. Setting Poisson's \n"
                  << "ratio elsewhere (dura and block) to 0.49\n";
       Nu_dura = 0.49;
       Nu_pia =  Nu_drained;
       Nu_cord = Nu_drained;
       Nu_filum = Nu_drained;
       Nu_block = 0.49;
      }
     else
      {
       oomph_info << "Only 'syrinx cover' is porous. Setting Poisson's \n"
                  << "ratio elsewhere to 0.49\n";
       // Note: separate treatment of syrinx cover is handled elsewhere
       Nu_dura = 0.49;
       Nu_pia = 0.49;
       Nu_cord = 0.49;
       Nu_filum = 0.49;
       Nu_block = 0.49;
      }
    }
   // The Womersley number
   Wo = Re*St;

   // (Square of) inertia parameter
   Lambda_sq = pow(Lambda_factor*Re,2);
   
   // FSI parameter
   Q = Q_factor*Re;
   
   // Non-dim permeability
   Permeability=K_factor/Re;

   // Non-dimensional period of excitation
   Excitation_period=Excitation_period_in_seconds*
    T_factor_in_inverse_seconds*Re;

   // Wavespeed in dura (constant thickness and radius as at inlet)
   double non_dim_thickness=1.0/20.0; // from Chris' sketch -- inner diameter
                                      // 20mm; wall thickness 1mm
   //Wavespeed
   if ((Q*Re)!=0.0)
    {
     Dura_wavespeed=sqrt(non_dim_thickness/(Q*Re));
    }
   else
    {
     Dura_wavespeed=999.9e99;
    }
  }

 /// Doc dependent parameters
 void doc_dependent_parameters()
  {
   oomph_info << std::endl;
   oomph_info << "Global problem parameters" << std::endl;
   oomph_info << "=========================" << std::endl;
   oomph_info << "Reynolds number (Re)                           : "
              << Re << std::endl;
   oomph_info << "Strouhal number (St)                           : "
              << St << std::endl;
   oomph_info << "Womersley number (ReSt)                        : "
              << Wo << std::endl;
   oomph_info << "FSI parameter (Q)                              : "
              << Q << std::endl << std::endl;

   oomph_info << "Factor for wall inertia parameter              : "
              << Lambda_factor << std::endl;
   oomph_info << "Factor for FSI parameter                       : "
              << Q_factor << std::endl;
   oomph_info << "Factor for non-dim permeability                : " 
              << K_factor << std::endl << std::endl;


   oomph_info << "Factor for time conversion (in sec^-1)         : " 
              << T_factor_in_inverse_seconds 
              << std::endl;
   oomph_info << "Timescale (in sec)                             : " 
              << 1.0/(T_factor_in_inverse_seconds*Re) 
              << std::endl << std::endl;
  

   oomph_info << "Dura wavespeed                                 : " 
              << Dura_wavespeed << std::endl << std::endl;


   oomph_info << "Forcing: ";
   if (CommandLineArgs::command_line_flag_has_been_set("--impulsive_pressure"))
    {
     oomph_info << "Impulsively applied pressure.\n\n";
    }
   else
    {
     oomph_info << "Time-harmonic pressure with period: "
                << Excitation_period << "\n\n";
    }

   oomph_info << "\n\nCommon poro-elastic properties:\n";
   oomph_info <<     "===============================\n\n";

   oomph_info << "(Square of) wall inertia parameter (Lambda^2)  : "
              << Lambda_sq << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability << std::endl;
   oomph_info << "Biot parameter (alpha) in all porous media     : "
              << Alpha << std::endl;
   oomph_info << "Density ratio (rho_f/rho_s) in all porous media: "
              << Density_ratio << std::endl;
   oomph_info << "Inverse slip rate coefficient                  : "
              << Inverse_slip_rate_coefficient << std::endl;

   oomph_info << "\n\nDura properties:\n";
   oomph_info <<     "================\n\n";
   oomph_info << "Stiffness ratio (non-dim Young's modulus)      : "
              << Stiffness_ratio_dura << std::endl;
   oomph_info << "Permeability ratio                             : "
              << Permeability_ratio_dura << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability*Permeability_ratio_dura << std::endl;
   if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
    {
     oomph_info << "Poisson's ratio                              : "
                << Nu_dura << std::endl;
    }
   else
    {
     oomph_info << "Poisson's ratio (non-porous bits)              : "
                << Nu_dura << std::endl;
     oomph_info << "Poisson's ratio for porous bits                : "
                << Nu_drained << std::endl;
    }
   oomph_info << "Porosity                                       : "
              << Porosity_dura <<std::endl;


   oomph_info << "\n\nPia properties:\n";
   oomph_info <<     "===============\n\n";
   oomph_info << "Stiffness ratio (non-dim Young's modulus)      : "
              << Stiffness_ratio_pia << std::endl;
   oomph_info << "Permeability ratio                             : "
              << Permeability_ratio_pia << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability*Permeability_ratio_pia << std::endl;
   if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
    {
     oomph_info << "Poisson's ratio                              : "
                << Nu_pia << std::endl;
    }
   else
    {
     oomph_info << "Poisson's ratio (non-porous bits)              : "
                << Nu_pia << std::endl;
     oomph_info << "Poisson's ratio for porous bits                : "
                << Nu_drained << std::endl;
    }
   oomph_info << "Porosity                                       : "
              << Porosity_pia <<std::endl;




   oomph_info << "\n\nCord properties:\n";
   oomph_info <<     "================\n\n";
   oomph_info << "Stiffness ratio (non-dim Young's modulus)      : "
              << Stiffness_ratio_cord << std::endl;
   oomph_info << "Permeability ratio                             : "
              << Permeability_ratio_cord << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability*Permeability_ratio_cord << std::endl;
   if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
    {
     oomph_info << "Poisson's ratio                              : "
                << Nu_cord << std::endl;
    }
   else
    {
     oomph_info << "Poisson's ratio (non-porous bits)              : "
                << Nu_cord << std::endl;
     oomph_info << "Poisson's ratio for porous bits                : "
                << Nu_drained << std::endl;
    }
   oomph_info << "Porosity                                       : "
              << Porosity_cord <<std::endl;

   oomph_info << "\n\nFilum properties:\n";
   oomph_info <<     "=================\n\n";
   oomph_info << "Stiffness ratio (non-dim Young's modulus)      : "
              << Stiffness_ratio_filum << std::endl;
   oomph_info << "Permeability ratio                             : "
              << Permeability_ratio_filum << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability*Permeability_ratio_filum << std::endl;
   oomph_info << "Poisson's ratio for drained material           : "
              << Nu_filum << std::endl;
   oomph_info << "Porosity                                       : "
              << Porosity_filum <<std::endl;
   oomph_info << std::endl << std::endl;


   oomph_info << "\n\nBlock properties:\n";
   oomph_info <<     "=================\n\n";
   oomph_info << "Stiffness ratio (non-dim Young's modulus)      : "
              << Stiffness_ratio_block << std::endl;
   oomph_info << "Permeability ratio                             : "
              << Permeability_ratio_block << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability*Permeability_ratio_block << std::endl;
   oomph_info << "Poisson's ratio for drained material           : "
              << Nu_block << std::endl;
   oomph_info << "Porosity                                       : "
              << Porosity_block <<std::endl;
   oomph_info << std::endl << std::endl;
  }

 //-----------------------------------------------------------------------------



 /// Time-dependent magnitude of driving pressure
 double time_dependent_magnitude_of_driving_pressure(const double &time)
 {
  double magnitude=1.0;
  if (!CommandLineArgs::command_line_flag_has_been_set("--impulsive_pressure"))
   {
    // Do heart-beat-like excitation with specified period?
    if (CommandLineArgs::command_line_flag_has_been_set(
         "--heart_beat_like_forcing"))
     {

      double offset=0.5*Fraction_of_pressure_peak;
      double period_time=fmod(time,Excitation_period);
      if (period_time<Excitation_period*Fraction_of_pressure_peak)
       {
        magnitude=pow(sin(MathematicalConstants::Pi*period_time/
                          (Excitation_period*Fraction_of_pressure_peak)),2);
        magnitude-=offset;
       }
      else
       {
        magnitude=-offset;
       }
      

      // // hierher: see if reinterpretation of excitation period has
      // // any side effect -- change to actual pulse wave profile
      // magnitude=sin(2.0*MathematicalConstants::Pi*time/(0.5*Excitation_period));
      
      // // Smooth start-up by using sin^2 for first quarter period
      // if (time<0.125*Excitation_period)
      //  {
      //   magnitude*=magnitude;
      //  }

     }
    // Time-periodic variation
    else
     {
      magnitude=sin(2.0*MathematicalConstants::Pi*time/Excitation_period);
      
      // Smooth start-up by using sin^2 for first quarter period
      if (time<0.25*Excitation_period)
       {
        magnitude*=magnitude;
       }
     }
   }
  return magnitude;
 }


 /// Inflow traction applied to the fluid mesh
 void fluid_inflow_boundary_traction(const double &time,
                                     const Vector<double> &x,
                                     const Vector<double> &n,
                                     Vector<double> &result)
  {
   double magnitude=time_dependent_magnitude_of_driving_pressure(time);
   result[0]=magnitude*Re*n[0];
   result[1]=magnitude*Re*n[1];
   result[2]=0.0;
  }


 //-----------------------------------------------------------------------------
 
 /// Helper function to create equally spaced vertices between
 /// between final existing vertex and specified end point. 
 /// Vertices are pushed back into Vector of vertices.
 void push_back_vertices(const Vector<double>& end,
                         const double& edge_length,
                         Vector<Vector<double> >& vertex)
 {
  Vector<double> start(2);
  unsigned n_existing=vertex.size();
  start[0]=vertex[n_existing-1][0];
  start[1]=vertex[n_existing-1][1];
  double total_length=sqrt(pow(end[0]-start[0],2)+
                           pow(end[1]-start[1],2));
  unsigned n_seg=unsigned(total_length/edge_length);
  Vector<double> x(2);
  // Skip first one!
  for (unsigned j=1;j<n_seg;j++)
   {
    x[0]=start[0]+(end[0]-start[0])*double(j)/double(n_seg-1);
    x[1]=start[1]+(end[1]-start[1])*double(j)/double(n_seg-1);
    vertex.push_back(x);
   }
 }

 /// Helper function to create equally spaced vertices between
 /// between start and end points. Vertices are pushed back
 /// into Vector of vertices.
 void push_back_vertices(const Vector<double>& start,
                         const Vector<double>& end,
                         const double& edge_length,
                         Vector<Vector<double> >& vertex)
 {
  double total_length=sqrt(pow(end[0]-start[0],2)+
                           pow(end[1]-start[1],2));
  unsigned n_seg=unsigned(total_length/edge_length);
  Vector<double> x(2);
  for (unsigned j=0;j<n_seg;j++)
   {
    x[0]=start[0]+(end[0]-start[0])*double(j)/double(n_seg-1);
    x[1]=start[1]+(end[1]-start[1])*double(j)/double(n_seg-1);
    vertex.push_back(x);
   }
 }




 /// Helper function to create non-uniformly spaced vertices
 /// along straight line between start and end
 void create_non_uniform_vertices(const Vector<double>& start,
                                  const Vector<double>& end,
                                  const double& edge_length_central,
                                  const double& edge_length_start,
                                  const double& edge_length_end,
                                  const double& delta_start,
                                  const double& delta_end,
                                  Vector<Vector<double> >& vertex)
 {
  vertex.clear();
  double total_length=sqrt(pow(end[0]-start[0],2)+
                           pow(end[1]-start[1],2));
  double length_central=total_length-delta_start-delta_end;
  if (length_central<0.0)
   {
    std::ostringstream error_stream;
    error_stream
     << "total length: " << total_length 
     << " is less than delta_start + delta_end "
     << " since delta_start = " << delta_start 
     << " delta_end = "  << delta_end << std::endl;
     throw OomphLibError(
      error_stream.str(),
       OOMPH_CURRENT_FUNCTION,
      OOMPH_EXCEPTION_LOCATION);
   }

  unsigned n_seg_central=unsigned(length_central/edge_length_central);
  unsigned n_seg_start=unsigned(delta_start/edge_length_start);
  unsigned n_seg_end=unsigned(delta_end/edge_length_end);

  Vector<double> x(2);
  x[0]=start[0];
  x[1]=start[1];
  vertex.push_back(x);
  for (unsigned j=1;j<n_seg_start;j++)
   {
    double fract=double(j)/double(n_seg_start-1)*delta_start/total_length;
    // oomph_info << "start: j: " << j 
    //            << " j_vertex: " << vertex.size() <<  " "
    //            << " fract: " << fract << " ";
    x[0]=start[0]+(end[0]-start[0])*fract;
    x[1]=start[1]+(end[1]-start[1])*fract;
    //oomph_info << x[0] << " " << x[1] << std::endl;
    vertex.push_back(x);
   }
  unsigned n_hi=n_seg_central;
  if (n_seg_end==0) n_hi=n_seg_central-1;
  for (unsigned j=1;j<n_hi;j++)
   {
    double fract=delta_start/total_length+
     double(j)/double(n_seg_central-1)*length_central/total_length;
    // oomph_info << "central: j: " << j 
    //            << " j_vertex: " << vertex.size() <<  " "
    //            << " fract: " << fract << " ";
    x[0]=start[0]+(end[0]-start[0])*fract;
    x[1]=start[1]+(end[1]-start[1])*fract;
    //oomph_info << x[0] << " " << x[1] << std::endl;
    vertex.push_back(x);
   }
  if (n_seg_end>0)
   {
    for (unsigned j=1;j<n_seg_end-1;j++)
     {
      double fract=delta_start/total_length+length_central/total_length+
       double(j)/double(n_seg_end-1)*delta_end/total_length;
      // oomph_info << "end: j: " << j 
      //            << " j_vertex: " << vertex.size() <<  " "
      //            << " fract: " << fract << " " ;
      x[0]=start[0]+(end[0]-start[0])*fract;
      x[1]=start[1]+(end[1]-start[1])*fract;
      //oomph_info << x[0] << " " << x[1] << std::endl;
      vertex.push_back(x);
     }
   }
  x[0]=end[0];
  x[1]=end[1];
  vertex.push_back(x);
 }


 //-----------------------------------------------------------------------------
 
 /// Helper function to get intersection between two lines
 Vector<double> intersection(const Vector<Vector<double> >& line1,
                             const Vector<Vector<double> >& line2)
 {
  Vector<double> intersect(2);
  double a2 =  line2[1][1] - line2[0][1];
  double b2 = -line2[1][0] + line2[0][0];
  double c2 = a2*line2[0][0]+b2*line2[0][1];
  
  double a1 =  line1[1][1] - line1[0][1];
  double b1 = -line1[1][0] + line1[0][0];
  double c1 = a1*line1[0][0]+b1*line1[0][1];
  
  double det = a1*b2 - a2*b1;
  if (abs(det)<1.0e-16)
   {
    oomph_info << "Trouble";
    abort();
   }
  intersect[0]= (b2*c1-b1*c2)/det;
  intersect[1]= (a1*c2-a2*c1)/det;
  return intersect;
  
 }


 /// \short Helper function to ellipsify nodes on specified
 /// boundary of specified mesh
 void ellipsify_boundary_nodes(Mesh* mesh_pt, const unsigned& boundary_id,
                               const bool& upward)
 {
  // Get extreme coords
  double r_min=DBL_MAX;
  double r_max=-DBL_MAX;
  double z_min=DBL_MAX;
  double z_max=-DBL_MAX;
  unsigned num_nod=mesh_pt->nboundary_node(boundary_id);
  for(unsigned inod=0;inod<num_nod;inod++)
   {
    // Get pointer to node
    Node* nod_pt=mesh_pt->boundary_node_pt(boundary_id,inod);

    if (nod_pt->x(0)>r_max) r_max=nod_pt->x(0);
    if (nod_pt->x(0)<r_min) r_min=nod_pt->x(0);
    if (nod_pt->x(1)>z_max) z_max=nod_pt->x(1);
    if (nod_pt->x(1)<z_min) z_min=nod_pt->x(1);
   }

  double a=z_max-z_min;
  double b=r_max-r_min;
  double r_centre=0.0;
  double z_centre=z_max;
  double sign=-1.0;
  if (upward)
   {
    sign=1.0;
    z_centre=z_min;
   }

  for(unsigned inod=0;inod<num_nod;inod++)
   {
    // Get pointer to node
    Node* nod_pt=mesh_pt->boundary_node_pt(boundary_id,inod);
    
    double r=nod_pt->x(0);
    double z=nod_pt->x(1);

    double phi=0.5*MathematicalConstants::Pi;
    if ((z-z_centre)!=0.0)
     {
      phi=atan(sign*a/b*(r-r_centre)/(z-z_centre));
     }

    double z_new=z_centre+sign*a*cos(phi);
    double r_new=r_centre+b*sin(phi);

    oomph_info << "ELLIPSIFIED NODES: "
               << phi << " " 
               << nod_pt->x(0) << " " 
               << nod_pt->x(1) << " " 
               << r_new << " " 
               << z_new << " " 
               << std::endl;

    nod_pt->x(0)=r_new;
    nod_pt->x(1)=z_new;
   }
 }


 /// \short Helper function to ellipsify a set of points currently on
 /// a straight line
 void ellipsify(Vector<Vector<double> >& vertex_coords, 
                const bool& upward, const bool& reverse=false)
 {


  Vector<Vector<double> > aux_vertex_coords;
  if (reverse)
   {
    unsigned n=vertex_coords.size();
    aux_vertex_coords.resize(n);
     for (unsigned j=0;j<n;j++)
      {
       aux_vertex_coords[n-1-j]=vertex_coords[j];
      }
     for (unsigned j=0;j<n;j++)
      {
       vertex_coords[j]=aux_vertex_coords[j];
      }
   }

  unsigned npt=vertex_coords.size();
  double r_start=vertex_coords[0][0];
  double z_start=vertex_coords[0][1];
  
  double r_end=vertex_coords[npt-1][0];
  double z_end=vertex_coords[npt-1][1];

  oomph_info << "before adjust: " 
             << "r_start z_start r_end z_end "
             << r_start << " " << z_start << " " << r_end << " " << z_end 
             << std::endl;


  double r_center=0.0;
  double z_center=z_start;
  if (r_end>1.0e-10)
   {
    z_center=z_end;
   }

  double sign=1.0;
  if (!upward) 
   {
    sign=-1.0;
    // if (r_end<r_start)
    //  {
    //   double tmp=r_start;
    //   r_start=r_end;
    //   r_end=tmp;
    //  }

    // if (z_end>z_start)
    //  {
    //   double tmp=z_start;
    //   z_start=z_end;
    //   z_end=tmp;
    //  }
   }

  // Half axes:
  double a=abs(z_end-z_start);
  double b=abs(r_end-r_start);

  // Total arclength
  double S_total=sqrt((r_end-r_start)*(r_end-r_start)+
                      (z_end-z_start)*(z_end-z_start));
  
  oomph_info << "a, b, S_total " 
             << a << " " 
             << b << " " 
             << S_total << " " 
             << std::endl;
  
  for (unsigned j=0;j<npt;j++)
   {
    // Current arclength
    double S=sqrt((vertex_coords[j][0]-r_start)*(vertex_coords[j][0]-r_start)+
                  (vertex_coords[j][1]-z_start)*(vertex_coords[j][1]-z_start));
    
    // Angle parameter
    double phi=0.5*MathematicalConstants::Pi*S/S_total;
    
    oomph_info << "S, phi, r, z "
               << S << " " 
               << phi << " " 
               << vertex_coords[j][0] << " " 
               << vertex_coords[j][1] << " ";

    vertex_coords[j][0]=r_center+b*sin(phi);
    vertex_coords[j][1]=z_center+sign*a*cos(phi);

    oomph_info 
               << vertex_coords[j][0] << " " 
               << vertex_coords[j][1] << " "
               << std::endl;
   }

  if (reverse)
   {
    unsigned n=vertex_coords.size();
    aux_vertex_coords.resize(n);
     for (unsigned j=0;j<n;j++)
      {
       aux_vertex_coords[n-1-j]=vertex_coords[j];
      }
     for (unsigned j=0;j<n;j++)
      {
       vertex_coords[j]=aux_vertex_coords[j];
      }
   }

 }

 /// Helper function to get intersection between boundary layer
 /// to the right of the two lines
 Vector<double> bl_intersection(const Vector<Vector<double> >& line1,
                                const Vector<Vector<double> >& line2,
                                const double& bl_thick)
 {
  // Get normal to right for first line
  Vector<double> normal(2);
  normal[0]=line1[1][1]-line1[0][1];
  normal[1]=-(line1[1][0]-line1[0][0]);
  double norm=normal[0]*normal[0]+normal[1]*normal[1];
  normal[0]/=sqrt(norm);
  normal[1]/=sqrt(norm);

  // Get boundary layer line
  Vector<Vector<double> >bl_line1(2,Vector<double>(2));
  bl_line1[0][0]=line1[0][0]+bl_thick*normal[0];
  bl_line1[0][1]=line1[0][1]+bl_thick*normal[1]*
   Global_Physical_Variables::Z_shrink_factor;
  bl_line1[1][0]=line1[1][0]+bl_thick*normal[0];
  bl_line1[1][1]=line1[1][1]+bl_thick*normal[1]*
   Global_Physical_Variables::Z_shrink_factor;

  // Get normal to right for second line
  normal[0]=line2[1][1]-line2[0][1];
  normal[1]=-(line2[1][0]-line2[0][0]);
  norm=normal[0]*normal[0]+normal[1]*normal[1];
  normal[0]/=sqrt(norm);
  normal[1]/=sqrt(norm);

  // Get boundary layer line
  Vector<Vector<double> >bl_line2(2,Vector<double>(2));
  bl_line2[0][0]=line2[0][0]+bl_thick*normal[0];
  bl_line2[0][1]=line2[0][1]+bl_thick*normal[1]*
   Global_Physical_Variables::Z_shrink_factor;
  bl_line2[1][0]=line2[1][0]+bl_thick*normal[0];
  bl_line2[1][1]=line2[1][1]+bl_thick*normal[1]*
   Global_Physical_Variables::Z_shrink_factor;

  Vector<double> intersect=intersection(bl_line1,bl_line2);

  return intersect;
 }



//============================================================================
 /// \short A class to do comparison of vectors based on their z coordinate
 //============================================================================
 class CompareVector
 {
 public:
  
  ///The actual comparison operator
  int operator() (const Vector<double>& a, const Vector<double> b)
   {
    return (a[1]<=b[1]);
   }

  };
 

 //-----------------------------------------------------------------------------
 
 /// \short Helper function to compute the volume of the sequence of
 /// "truncated cones".
 double volume(Vector<Vector<double> >& vertex)
 {
  
  // Sort based on z coordinate
  std::sort(vertex.begin(), vertex.end(),CompareVector());
  
  double vol=0.0;
  unsigned nv=vertex.size();
  for (unsigned j=1;j<nv;j++)
   {
    double h=std::fabs(vertex[j][1]-vertex[j-1][1]);
    double r1=vertex[j-1][0];
    double r2=vertex[j][0];
    vol+=MathematicalConstants::Pi/3.0*(r1*r1+r1*r2+r2*r2)*h;
   }
  return vol;
 }

 //-----------------------------------------------------------------------------

 /// \short Global function that completes the edge sign setup
 template<class ELEMENT>
 void edge_sign_setup(Mesh* mesh_pt)
  {
   // The dictionary keeping track of edge signs
   std::map<Edge,unsigned> assignments;

   // Loop over all elements
   unsigned n_element = mesh_pt->nelement();
   for(unsigned e=0;e<n_element;e++)
    {
     ELEMENT* el_pt = dynamic_cast<ELEMENT*>(mesh_pt->element_pt(e));

     // Assign edge signs: Loop over the vertex nodes (always
     // first 3 nodes for triangles)
     for(unsigned i=0;i<3;i++)
      {
       Node *nod_pt1, *nod_pt2;
       nod_pt1 = el_pt->node_pt(i);
       nod_pt2 = el_pt->node_pt((i+1)%3);
       Edge edge(nod_pt1,nod_pt2);
       unsigned status = assignments[edge];

       // This relies on the default value for an int being 0 in a map
       switch(status)
        {
         // If not assigned on either side, give it a + on current side
        case 0:
         assignments[edge]=1;
         break;
         // If assigned + on other side, give it a - on current side
        case 1:
         assignments[edge]=2;
         el_pt->sign_edge(i)=-1;
         break;
         // If assigned - on other side, give it a + on current side
        case 2:
         assignments[edge]=1;
         break;
        }
      } // end of loop over vertex nodes

    } // end of loop over elements
  }

} // end_of_Global_Physical_Variables_namespace

//===start_of_problem_class=============================================
/// Problem class
//======================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
class AxisymmetricSpineProblem : public Problem
{
public:

 /// Constructor
 AxisymmetricSpineProblem();

 /// Complete problem setup 
 void complete_problem_setup();

 /// Update before solve is empty
 void actions_before_newton_solve() {}

 /// Update after solve is empty
 void actions_after_newton_solve() {}


 /// Update fluid nodal positions (enslaved) if required
 void actions_before_newton_convergence_check()
  {
   // Update SSS fluid mesh?
   if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update==0)
    {
     oomph_info << "Updating fluid mesh in actions before newton conv check\n";
     move_fluid_nodes(
      Global_Physical_Variables::History_level_for_fluid_mesh_node_update);
    }
   else
    {
     oomph_info
      << "Not doing fluid node update in actions before newton conv check\n";
    }
  }

 /// \short Actions before read: Mesh contains face elements on unstructured
 /// meshes. Need to strip them out because the unstructured meshes are 
 /// completely re-built (with new elements) on restart.
 void actions_before_read_unstructured_meshes()
  {
   delete_face_elements();
   rebuild_global_mesh();
  }     
 
 /// Re-attach face elements after reading in (bulk) unstructured meshes
 void actions_after_read_unstructured_meshes()
  {
   create_fluid_face_elements();
   create_poro_face_elements();
   complete_problem_setup();
   rebuild_global_mesh();
  }

 /// Actions before implicit timestep
 void actions_before_implicit_timestep()
  {
   oomph_info 
    << "Time: " << time_pt()->time() 
    << " pressure magnitude: " 
    << Global_Physical_Variables::
    time_dependent_magnitude_of_driving_pressure(time_pt()->time())
    << std::endl;

   // Update fluid mesh?
   if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update==1)
    {
     oomph_info << "Updating fluid mesh in actions before implicit timestep\n";
     move_fluid_nodes(
      Global_Physical_Variables::History_level_for_fluid_mesh_node_update);
    }
   else
    {
     oomph_info 
      << "Not doing fluid node update in actions before implicit timestep\n";
    }


  }

 /// Restart
 void restart(DocInfo& doc_info)
  {
   // Pointer to restart file
   ifstream* restart_file_pt=0;
   
   // Open restart file from stem
   restart_file_pt=new ifstream(Global_Physical_Variables::Restart_file.c_str(),
                                ios_base::in);
   if (restart_file_pt!=0)
    {
     oomph_info << "Have opened "
                << Global_Physical_Variables::Restart_file.c_str() 
                << " for restart. " << std::endl;
    }
   else
    {
     std::ostringstream error_stream;
     error_stream
      << "ERROR while trying to open " 
      << Global_Physical_Variables::Restart_file.c_str()
      << " for restart." << std::endl;
     
     throw OomphLibError(
      error_stream.str(),
      "restart()",
      OOMPH_EXCEPTION_LOCATION);
    }
   
   
   // Read restart data:
   //-------------------
   if (restart_file_pt!=0)
    { 

     // Doc number
     //----------
     // Read line up to termination sign
     string input_string;
     getline(*restart_file_pt,input_string,'#');
     
     // Ignore rest of line
     restart_file_pt->ignore(80,'\n');
     
     // Doc number
     doc_info.number()=unsigned(atoi(input_string.c_str()));
     
     // Refine the mesh and read in the generic problem data
     Problem::read(*restart_file_pt);

     // Spine node update info 
     //-----------------------    
     if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update!=-1)
      {
       // SSS mesh
       //---------
       
       // Read line up to termination sign
       getline(*restart_file_pt,input_string,'#');
       
       // Ignore rest of line
       restart_file_pt->ignore(80,'\n');
       
       // Number of dumped nodes
       unsigned nnod_file=unsigned(atoi(input_string.c_str()));
       
       // Check
       unsigned nnod=SSS_fluid_mesh_pt->nnode();
       if (nnod!=nnod_file)
        {
         oomph_info << "Number of sss nodes doesn't match! File: "
                    << nnod_file << " ; Code: " << nnod << std::endl;
         abort(); // hierher
        }
       
       // Loop over SSS fluid nodes
       for (unsigned j=0;j<nnod;j++)
        {
         // Number of outer poro reference element
         getline(*restart_file_pt,input_string);
         unsigned e=unsigned(atoi(input_string.c_str()));
         SSS_fluid_mesh_outer_reference_point[j].first=
          dynamic_cast<PORO_ELEMENT*>(Outer_poro_mesh_pt->element_pt(e));
         
         // Coordinates
         getline(*restart_file_pt,input_string);
         double s0=double(atof(input_string.c_str()));
         (SSS_fluid_mesh_outer_reference_point[j].second)[0]=s0;
         getline(*restart_file_pt,input_string);
         double s1=double(atof(input_string.c_str()));
         (SSS_fluid_mesh_outer_reference_point[j].second)[1]=s1;
         
         // Number of inner poro reference element
         getline(*restart_file_pt,input_string);
         e=unsigned(atoi(input_string.c_str()));
         SSS_fluid_mesh_inner_reference_point[j].first=
          dynamic_cast<PORO_ELEMENT*>(Inner_poro_mesh_pt->element_pt(e));
         
         // Coordinates
         getline(*restart_file_pt,input_string);
         s0=double(atof(input_string.c_str()));
         (SSS_fluid_mesh_inner_reference_point[j].second)[0]=s0;
         getline(*restart_file_pt,input_string);
         s1=double(atof(input_string.c_str()));
         (SSS_fluid_mesh_inner_reference_point[j].second)[1]=s1;
         
         // Spine fraction
         getline(*restart_file_pt,input_string);
         double omega=double(atof(input_string.c_str()));
         SSS_fluid_mesh_omega[j]=omega;
        }
       
       
       //Syrinx mesh
       //-----------
       
       // Read line up to termination sign
       getline(*restart_file_pt,input_string,'#');
       
       // Ignore rest of line
       restart_file_pt->ignore(80,'\n');
       
       // Number of dumped nodes
       nnod_file=unsigned(atoi(input_string.c_str()));
       
       // Check
       nnod=Syrinx_fluid_mesh_pt->nnode();
       if (nnod!=nnod_file)
        {
         oomph_info << "Number of sss nodes doesn't match! File: "
                    << nnod_file << " ; Code: " << nnod << std::endl;
         abort(); // hierher
        }
       
       // Loop over Syrinx fluid nodes
       for (unsigned j=0;j<nnod;j++)
        {
         // Number of outer poro reference element
         getline(*restart_file_pt,input_string);
         unsigned e=unsigned(atoi(input_string.c_str()));
         Syrinx_fluid_mesh_reference_point[j].first=
          dynamic_cast<PORO_ELEMENT*>(Inner_poro_mesh_pt->element_pt(e));
         
         // Coordinates
         getline(*restart_file_pt,input_string);
         double s0=double(atof(input_string.c_str()));
         (Syrinx_fluid_mesh_reference_point[j].second)[0]=s0;
         getline(*restart_file_pt,input_string);
         double s1=double(atof(input_string.c_str()));
         (Syrinx_fluid_mesh_reference_point[j].second)[1]=s1;
         
         // Spine fraction
         getline(*restart_file_pt,input_string);
         double omega=double(atof(input_string.c_str()));
         Syrinx_fluid_mesh_omega[j]=omega;
        }
       
      }
    }
   
   // Clean up
   delete restart_file_pt;
  }

 /// Dump problem data to allow for later restart
 void dump_it(const DocInfo& doc_info, ofstream& dump_file)
  {
   // Write doc number
   dump_file << doc_info.number() << " # current doc number" << std::endl;

   // Dump the refinement pattern and the generic problem data
   Problem::dump(dump_file);

   // Spine node update info 
   //-----------------------
   if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update!=-1)
    {
     PORO_ELEMENT* inner_el_pt=0;
     Vector<double> inner_s(2);
     PORO_ELEMENT* outer_el_pt=0;
     Vector<double> outer_s(2);
     double omega=0.0;
     
     // Setup map from pointer to element number in poro meshes
     std::map<PORO_ELEMENT*,unsigned> outer_poro_element_number;
     unsigned nel = Outer_poro_mesh_pt->nelement();
     for(unsigned e=0;e<nel;e++)
      {
       outer_poro_element_number[
        dynamic_cast<PORO_ELEMENT*>(Outer_poro_mesh_pt->element_pt(e))]=e;
      }
     std::map<PORO_ELEMENT*,unsigned> inner_poro_element_number;
     nel = Inner_poro_mesh_pt->nelement();
     for(unsigned e=0;e<nel;e++)
      {
       inner_poro_element_number[
        dynamic_cast<PORO_ELEMENT*>(Inner_poro_mesh_pt->element_pt(e))]=e;
      }
     
     // Loop over all nodes in SSS mesh
     unsigned nnod=SSS_fluid_mesh_pt->nnode();
     dump_file << nnod << " # Number of SSS fluid nodes\n";
     for (unsigned j=0;j<nnod;j++)
      {
       outer_el_pt=SSS_fluid_mesh_outer_reference_point[j].first;
       outer_s=SSS_fluid_mesh_outer_reference_point[j].second;
       inner_el_pt=SSS_fluid_mesh_inner_reference_point[j].first;
       inner_s=SSS_fluid_mesh_inner_reference_point[j].second;
       omega=SSS_fluid_mesh_omega[j];
       dump_file << outer_poro_element_number[outer_el_pt] << "\n"
                 << outer_s[0] << "\n" 
                 << outer_s[1] << "\n"
                 << inner_poro_element_number[inner_el_pt] << "\n"
                 << inner_s[0] << "\n" 
                 << inner_s[1] << "\n"
                 << omega << "\n";
      }
     
     
     // Loop over all nodes in Syrinx mesh
     nnod=Syrinx_fluid_mesh_pt->nnode();
     dump_file << nnod << " # Number of syrinx fluid nodes\n";
     for (unsigned j=0;j<nnod;j++)
      {
       outer_el_pt=Syrinx_fluid_mesh_reference_point[j].first;
       outer_s=Syrinx_fluid_mesh_reference_point[j].second;
       omega=Syrinx_fluid_mesh_omega[j];     
       dump_file << inner_poro_element_number[outer_el_pt] << "\n"
                 << outer_s[0] << "\n" 
                 << outer_s[1] << "\n"
                 << omega << "\n";
      }
    }
  
  }


 /// Do steady inner poro only run to find zero net flux
 void steady_inner_poro_only_for_zero_flux_run(DocInfo& doc_info);

 /// Do steady inner poro only run with read-in pressures
 void steady_inner_poro_only_run(DocInfo& doc_info);

 /// Doc the solution
 void doc_solution(DocInfo& doc_info,unsigned npts=5);

 /// Setup mesh motion
 void setup_mesh_motion();
 

 ///\short Compute upstream and downstream syrinx volumes via line integrals  
 /// on syrinx mesh
 void compute_syrinx_volumes_from_boundary_integral(
  double& upper_syrinx_volume_from_syrinx_boundary_integral,
  double& lower_syrinx_volume_from_syrinx_boundary_integral,
  double& total_syrinx_volume_from_syrinx_boundary_integral);
  

 /// \short Impose fake cord motion for step i out of n (in a time periodic 
 /// axial displacement of cord FSI surface) -- assignment made to
 /// history value that's used for node update.
 void impose_fake_cord_motion(const unsigned& i, const unsigned& n);
 
 /// Move fluid nodes -- perform node update based on history
 /// value t_hist; default 0 means we're using the current value
 /// (= fully implicit); otherwise we use history values. Defaults to 0,
 /// i.e. fully implicit.
 void move_fluid_nodes(const unsigned& t=0);

 /// Doc "spines"
 void doc_spines(ofstream& spine_file);
      
 /// Check computation of fluid volumes (for moving fluid mesh)
 void check_computation_of_fluid_volumes();
  
 /// Doc imminent fluid node update
 void doc_node_update(const unsigned& t_hist, ofstream& doc_file);
   
 /// Dummy Global error norm for adaptive time-stepping
 double global_temporal_error_norm(){return 0.0;}

 /// Set change in syrinx volume
 void set_change_in_syrinx_volume(const double& dvol)
  {
   Volume_change_data_pt->set_value(Syrinx_volume_index,dvol);
  }
 
 /// Set change in syrinx volume
 double rate_of_change_in_syrinx_volume()
  {
   return Volume_change_data_pt->time_stepper_pt()->
    time_derivative(1,Volume_change_data_pt,Syrinx_volume_index);
  }
 
 
 /// Set change in upstream sss volume
 void set_change_in_upstream_sss_volume(const double& dvol)
  {
   Volume_change_data_pt->set_value(Upstream_sss_volume_index,dvol);
  }
 
 /// Set change in upstream sss volume
 double rate_of_change_in_upstream_sss_volume()
  {
   return Volume_change_data_pt->time_stepper_pt()->
    time_derivative(1,Volume_change_data_pt,Upstream_sss_volume_index);
  }
 
 
 /// Set change in downstream sss volume
 void set_change_in_downstream_sss_volume(const double& dvol)
  {
   Volume_change_data_pt->set_value(Downstream_sss_volume_index,dvol);
  }
 
 /// Set change in downstream sss volume
 double rate_of_change_in_downstream_sss_volume()
  {
   return Volume_change_data_pt->time_stepper_pt()->
    time_derivative(1,Volume_change_data_pt,Downstream_sss_volume_index);
  }


private:

 /// Create the poroelasticity traction/pressure elements
 void create_poro_face_elements();

 /// Create the fluid traction elements
 void create_fluid_face_elements();

 /// Delete all face elements
 void delete_face_elements();

 /// Setup FSI
 void setup_fsi();


 /// Initial "upper" syrinx volume
 double Initial_upper_syrinx_volume;

 /// Initial "lower" syrinx volume
 double Initial_lower_syrinx_volume;

 /// Pointers to the inner poro mesh
 Mesh* Inner_poro_mesh_pt;

 /// Pointers to the outer poro mesh
 Mesh* Outer_poro_mesh_pt;

 /// Pointer to the syrinx fluid mesh
 Mesh* Syrinx_fluid_mesh_pt;

 /// Pointer to the SSS fluid mesh
 Mesh* SSS_fluid_mesh_pt;

 /// Inflow fluid surface mesh
 Mesh* Inflow_fluid_surface_mesh_pt;

 /// \short Mesh of elements that can be used to analyse the
 /// syrinx volume. Attaches to syrinx mesh.
 Mesh* Syrinx_volume_from_upper_syrinx_mesh_pt;

 /// \short Mesh of elements that can be used to analyse the
 /// syrinx volume. Attaches to syrinx mesh.
 Mesh* Syrinx_volume_from_lower_syrinx_mesh_pt;

 /// \short Mesh of elements that can be used to analyse the
 /// syrinx volume. Attaches to syrinx mesh.
 Mesh* Syrinx_volume_from_line_through_syrinx_mesh_pt;

 /// \short Mesh of elements that can be used to analyse the
 /// syrinx volume. Attaches to inner poro mesh.
 Mesh* Syrinx_volume_analysis_mesh_pt;

 /// \short Mesh of elements that can be used to analyse the
 /// downstream SSS volume. Attaches to mixture of meshes
 Mesh* Downstream_SSS_volume_analysis_mesh_pt;

 /// \short Mesh of elements that can be used to analyse the
 /// upstream SSS volume. Attaches to mixture of meshes
 Mesh* Upstream_SSS_volume_analysis_mesh_pt;

 /// \short Mesh of line elements to visualise NSt veloc etc on 
 /// centreline in syrinx (mainly for time-averaged pressure)
 Mesh* Syrinx_centreline_veloc_line_mesh_pt;

 /// \short Mesh of line elements to visualise NSt veloc etc on 
 /// centreline in syrinx (mainly for time-averaged pressure)
 Mesh* SSS_inner_fsi_boundary_veloc_line_mesh_pt;

 /// Mesh of line elements to visualise NSt veloc in syrinx
 Mesh* Syrinx_veloc_line_mesh_pt;

 /// Mesh of line elements to visualise NSt veloc in gap under block
 Mesh* SSS_gap_under_block_veloc_line_mesh_pt;

 /// \short Mesh of line elements to work out flux through radial line through
 /// syrinx cover
 Mesh* Inner_poro_radial_line_mesh_pt;

 /// Mesh of line elements to visualise NSt veloc at inlet of SSS
 Mesh* SSS_veloc_line_mesh_pt;

 /// \short Mesh of line elements around entire boundary of SSS mesh
 /// (used to check volume flux computation)
 Mesh* Test_volume_flux_out_of_sss_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to inner poro mesh from syrinx
 /// fluid. Attaches to inner poro mesh.
 Mesh* Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to inner poro mesh from syrinx
 /// fluid. Attaches to inner poro mesh.
 Mesh* Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to inner poro mesh from syrinx
 /// fluid. Attaches to inner poro mesh.
 Mesh* Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to syrinx mesh from inner
 /// poro. Attaches to syrinx fluid mesh.
 Mesh* Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to syrinx mesh from inner
 /// poro. Attaches to syrinx fluid mesh.
 Mesh* Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to syrinx mesh from inner
 /// poro. Attaches to syrinx fluid mesh.
 Mesh* Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to inner poro mesh from SSS
 /// fluid. Attaches to inner poro mesh.
 Mesh* Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to inner poro mesh from SSS
 /// fluid. Attaches to inner poro mesh.
 Mesh* Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to inner poro mesh from SSS
 /// fluid. Attaches to inner poro mesh.
 Mesh* Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to SSS mesh from inner
 /// poro. Attaches to SSS fluid mesh.
 Mesh* SSS_fluid_upper_inner_poro_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to SSS mesh from inner
 /// poro. Attaches to SSS fluid mesh.
 Mesh* SSS_fluid_central_inner_poro_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to SSS mesh from inner
 /// poro. Attaches to SSS fluid mesh.
 Mesh* SSS_fluid_lower_inner_poro_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to outer poro mesh from SSS
 /// fluid near inlet. Attaches to outer poro mesh 
 Mesh* Outer_poro_SSS_fluid_near_inlet_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to SSS mesh from outer
 /// poro near inlet. Attaches to SSS fluid mesh.
 Mesh* SSS_fluid_outer_poro_near_inlet_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to outer poro mesh from SSS
 /// fluid near outlet. Attaches to outer poro mesh 
 Mesh* Outer_poro_SSS_fluid_near_outlet_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to SSS mesh from outer
 /// poro near outlet. Attaches to SSS fluid mesh.
 Mesh* SSS_fluid_outer_poro_near_outlet_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to outer poro mesh from SSS
 /// fluid. Attaches to outer poro mesh [only block!]
 Mesh* Outer_poro_SSS_fluid_block_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to SSS mesh from outer
 /// poro. Attaches to SSS fluid mesh. [only block!]
 Mesh* SSS_fluid_outer_poro_block_FSI_surface_mesh_pt;



 /// \short Mesh of solid traction elements for syrinx fsi surface of
 /// inner poro mesh -- applies non-fsi load
 Mesh* Inner_poro_syrinx_non_fsi_traction_mesh_pt;

 /// \short Mesh of solid traction elements for sss fsi surface of
 /// inner poro mesh -- applies non-fsi load
 Mesh* Inner_poro_sss_non_fsi_traction_mesh_pt;



 /// \short Mesh of EulerianBasedFaceElements that allow location of point
 /// on outer SSS FSI boundary of poro mesh based on its z coordinate.
 Mesh* Outer_SSS_FSI_geometry_mesh_pt;

 /// \short Mesh of EulerianBasedFaceElements that allow location of point
 /// on inner SSS FSI boundary of poro mesh based on its z coordinate.
 Mesh* Inner_SSS_FSI_geometry_mesh_pt;

 /// \short Mesh of EulerianBasedFaceElements that allow location of point
 /// on syrinx FSI boundary of poro mesh based on its z coordinate.
 Mesh* Syrinx_FSI_geometry_mesh_pt;

 /// Wall reference points for all SSS fluid nodes
 Vector<std::pair<PORO_ELEMENT*, Vector<double> > > 
 SSS_fluid_mesh_outer_reference_point;

 /// Wall reference points for all SSS fluid nodes
 Vector<std::pair<PORO_ELEMENT*, Vector<double> > > 
 SSS_fluid_mesh_inner_reference_point;

 /// Spine fraction for all SSS fluid nodes
 Vector<double> SSS_fluid_mesh_omega;

 /// Wall reference points for all syrinx fluid nodes
 Vector<std::pair<PORO_ELEMENT*, Vector<double> > > 
 Syrinx_fluid_mesh_reference_point;

 /// Spine fraction for all syrinx fluid nodes
 Vector<double> Syrinx_fluid_mesh_omega;

 /// \short Mesh as geom object representation of SSS fluid mesh
 MeshAsGeomObject* SSS_fluid_mesh_geom_obj_pt;
 
 /// \short Vector of pairs containing pointers to elements and
 /// local coordinates within them for regularly spaced plot points
 Vector<std::pair<FLUID_ELEMENT*,Vector<double> > > 
 SSS_fluid_regularly_spaced_plot_point;

 /// \short Mesh as geom object representation of syrinx fluid mesh
 MeshAsGeomObject* Syrinx_fluid_mesh_geom_obj_pt;
 
 /// \short Vector of pairs containing pointers to elements and
 /// local coordinates within them for regularly spaced plot points
 Vector<std::pair<FLUID_ELEMENT*,Vector<double> > > 
 Syrinx_fluid_regularly_spaced_plot_point;

 /// \short Mesh as geom object representation of outer poro mesh
 MeshAsGeomObject* Outer_poro_mesh_geom_obj_pt;
 
 /// \short Vector of pairs containing pointers to elements and
 /// local coordinates within them for regularly spaced plot points
 Vector<std::pair<PORO_ELEMENT*,Vector<double> > > 
 Outer_poro_regularly_spaced_plot_point;

 /// \short Mesh as geom object representation of inner poro mesh
 MeshAsGeomObject* Inner_poro_mesh_geom_obj_pt;
 
 /// \short Vector of pairs containing pointers to elements and
 /// local coordinates within them for regularly spaced plot points
 Vector<std::pair<PORO_ELEMENT*,Vector<double> > > 
 Inner_poro_regularly_spaced_plot_point;

 /// Line visualiser for radial line through syrinx cover (upper)
 LineVisualiser* Upper_radial_line_through_syrinx_cover_visualiser_pt;

 /// Line visualiser for radial line through syrinx cover (lower)
 LineVisualiser* Lower_radial_line_through_syrinx_cover_visualiser_pt;

 /// Pointer to the poroelasticity timestepper
 TimeStepper* Poro_time_stepper_pt;

 /// Pointer to the fluid timestepper
 TimeStepper* Fluid_time_stepper_pt;

 /// Trace file
 std::ofstream Trace_file;

 /// \short Enumeration for Lagrange multiplier IDs for Lagrange multipliers 
 /// that enforce BJS boundary condition on fluid from poro meshes
 enum
 {
  Lagrange_id_syrinx_fluid_inner_poro,
  Lagrange_id_SSS_fluid_inner_poro,
  Lagrange_id_SSS_fluid_outer_poro
 };


 /// Enumeration of the inner poro boundaries
 enum
  {
   Inner_poro_inlet_boundary_id,
   Inner_poro_upper_SSS_interface_boundary_id,
   Inner_poro_central_SSS_interface_boundary_id,
   Inner_poro_lower_SSS_interface_boundary_id,
   Inner_poro_outlet_boundary_id,
   Inner_poro_symmetry_near_outlet_boundary_id,
   Inner_poro_lower_syrinx_interface_boundary_id,
   Inner_poro_central_syrinx_interface_boundary_id,
   Inner_poro_upper_syrinx_interface_boundary_id,
   Inner_poro_symmetry_near_inlet_boundary_id,
   Inner_poro_pia_internal_boundary_id,
   Inner_poro_filum_internal_boundary_id,
   Inner_poro_lower_internal_boundary_of_poroelastic_region_boundary_id,
   Inner_poro_upper_internal_boundary_of_poroelastic_region_boundary_id,
   Inner_poro_radial_line_boundary_id,
   Upper_poro_elastic_bl_line_boundary_id,
   Lower_poro_elastic_bl_line_boundary_id,
   Syrinx_cover_poro_elastic_bl_line_boundary_id,
   Pia_boundary_poro_elastic_bl_line_boundary_id
  };


 /// Enumeration of the outer poro boundaries
 enum
  {
   Outer_poro_inlet_boundary_id,
   Outer_poro_outer_boundary_id,
   Outer_poro_outlet_boundary_id,
   Outer_poro_SSS_interface_near_outlet_boundary_id,
   Outer_poro_SSS_interface_block_boundary_id,
   Outer_poro_SSS_interface_near_inlet_boundary_id,
   Outer_poro_inner_block_boundary_id
  };

 /// Enumeration of the syrinx fluid boundaries
 enum
  {
   Syrinx_fluid_upper_inner_poro_interface_boundary_id,
   Syrinx_fluid_central_inner_poro_interface_boundary_id,
   Syrinx_fluid_lower_inner_poro_interface_boundary_id,
   Syrinx_fluid_symmetry_boundary_id,
   Syrinx_bl_boundary_id,
   Syrinx_veloc_extract_boundary_id
  };

 /// Enumeration of the SSS fluid boundaries
 enum
  {
   SSS_fluid_outer_poro_interface_block_boundary_id,
   SSS_fluid_outer_poro_interface_near_outlet_boundary_id,
   SSS_fluid_outflow_boundary_id,
   SSS_fluid_lower_inner_poro_interface_boundary_id,
   SSS_fluid_central_inner_poro_interface_boundary_id,
   SSS_fluid_upper_inner_poro_interface_boundary_id, 
   SSS_fluid_inflow_boundary_id,
   SSS_fluid_outer_poro_interface_near_inlet_boundary_id,
   SSS_bl_near_inlet_boundary_id,
   SSS_bl_near_outlet_boundary_id,
   SSS_downstream_fine_region_boundary_id,
   SSS_upstream_fine_region_boundary_id,
   SSS_gap_under_block_veloc_line_boundary_id
  };


 /// Enumeration of regions
 enum
  {
   Upper_cord_region_id, // Note: not specified therefore implied!
   Upper_cord_pia_bl_region_id,
   Central_upper_cord_region_id, 
   Central_upper_cord_syrinx_cover_bl_region_id,
   Central_upper_cord_pia_bl_region_id,
   Central_lower_cord_region_id, 
   Central_lower_cord_syrinx_cover_bl_region_id,
   Central_lower_cord_pia_bl_region_id,
   Lower_cord_region_id, 
   Lower_cord_pia_bl_region_id,
   Dura_region_id,
   Block_region_id,
   Upper_pia_region_id,
   Central_upper_pia_region_id,
   Central_lower_pia_region_id,
   Central_upper_pia_poro_bl_region_id,
   Central_lower_pia_poro_bl_region_id,
   Lower_pia_region_id,
   Filum_region_id,
   Filum_pia_bl_region_id,
   SSS_upstream_near_block_region_id,
   SSS_central_and_bl_upper_near_block_region_id,
   SSS_central_and_bl_lower_near_block_region_id,
   SSS_downstream_near_block_region_id,
   Upper_cord_syrinx_poro_bl_region_id,
   Lower_cord_syrinx_poro_bl_region_id,
   Small_upper_cord_pia_bl_region_id,
   Small_lower_cord_pia_bl_region_id
  };


 /// \short UNSCALED Z-coordinate of radial line along which we compute fluxes
 /// that go through gap, through porous syrinx cover and through syrinx
 double Z_radial_cut_through_syrinx;


 /// Enumeration of various volumes in global data
 enum
 {
  Syrinx_volume_index,
  Upstream_sss_volume_index,
  Downstream_sss_volume_index
 };

 /// Undeformed syrinx volume computed analytically
 double Analytical_syrinx_volume;

 /// Undeformed upstream SSS volume computed analytically
 double Analytical_upstream_SSS_volume;

 /// Undeformed downstream SSS volume computed analytically
 double Analytical_downstream_SSS_volume;

 /// Vector containing pointers to fluid elements in upstream sss region
 Vector<FLUID_ELEMENT*> Upstream_SSS_el_pt;

 /// Vector containing pointers to fluid elements in downstream sss region
 Vector<FLUID_ELEMENT*> Downstream_SSS_el_pt;

 // Data object that we'll use to store 
 // changes in the three volumes (upstream/downstream sss and syrinx)
 // This allows their rate of change to be computed automatically.
 Data* Volume_change_data_pt;

}; // end_of_problem_class

//===start_of_constructor=============================================
/// Problem constructor:
//====================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
AxisymmetricSpineProblem()
{

// hierher work out from element area
 unsigned npts_on_elliptical_boundaries=100; 

 if (MPI_Helpers::communicator_pt()->my_rank()==0)
  {
   // Open trace file
   string name=Global_Physical_Variables::Directory+"/trace.dat";
   Trace_file.open(name.c_str());
  }


 // Create timesteppers
 Fluid_time_stepper_pt = new BDF<2>(true);
 add_time_stepper_pt(Fluid_time_stepper_pt);
 Poro_time_stepper_pt = new Newmark<2>;
 add_time_stepper_pt(Poro_time_stepper_pt);

 // Create global Data object that we'll use to store 
 // changes in the three volumes (upstream/downstream sss and syrinx)
 // This allows their rate of change to be computed automatically
 Volume_change_data_pt=new Data(Fluid_time_stepper_pt,3);
 add_global_data(Volume_change_data_pt);
 unsigned n=Volume_change_data_pt->nvalue();
 for (unsigned j=0;j<n;j++)
  {
   Volume_change_data_pt->pin(j);
  }

 // Update syrinx-associated coordinates
 double dr_syrinx_fsi=0.0; 
 double dr_sss_fsi=0.0;
 Global_Physical_Variables::update_syrinx_associated_coordinates(dr_syrinx_fsi,dr_sss_fsi);

 // hierher possibly update this

 // Z-coordinate of radial line along which we compute fluxes
 // that go through gap, through porous syrinx cover and through syrinx
 Z_radial_cut_through_syrinx=-150.0;

 oomph_info << std::endl;
 oomph_info << "z coordinate of radial cuts (raw, mm)          : "
            << Z_radial_cut_through_syrinx
            << std::endl;
 oomph_info << "z coordinate of radial cuts (scaled, non-dim)  : "
            << Z_radial_cut_through_syrinx*Global_Physical_Variables::Z_scale
            << std::endl;
 oomph_info << std::endl;
 

 // Create the problem geometry - a polygonal model of the spinal cavity with
 // -------------------------------------------------------------------------
 // poroelastic walls filled with fluid
 // -----------------------------------

 // Update axial scale factor
 Global_Physical_Variables::Z_scale = 
  Global_Physical_Variables::Length_scale/
  Global_Physical_Variables::Z_shrink_factor;
 
 // Update regular output with equivalent axial spacing
 Global_Physical_Variables::Nplot_z_regular=
  unsigned(double(Global_Physical_Variables::Nplot_r_regular)*600.0/11.0
           /Global_Physical_Variables::Z_shrink_factor);
 if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
 {
  oomph_info << "Regular output with: Nplot_r_regular Nplot_z_regular " 
             << Global_Physical_Variables::Nplot_r_regular << " " 
             << Global_Physical_Variables::Nplot_z_regular << std::endl;
 }


 // Line delineating upper/lower poro-elastic bl (next to slanted syrinx 
 // boundaries) (from outer to inner))
 Vector<Vector<double> > Upper_poro_elastic_bl_line(3,Vector<double>(2));
 Vector<Vector<double> > Lower_poro_elastic_bl_line(3,Vector<double>(2));

 // Vertices on upper/central/lower syrinx fsi boundaries
 Vector<Vector<double> > inner_poro_upper_syrinx_fsi_vertices_raw;
 Vector<Vector<double> > inner_poro_central_syrinx_fsi_vertices_raw;
 Vector<Vector<double> > inner_poro_lower_syrinx_fsi_vertices_raw;



 // Poro mesh (inner part)
 // ---------------------
 Vector<TriangleMeshCurveSection*> inner_poro_outer_polyline_boundary_pt(10);

 // Inlet
 {
  Vector<Vector<double> > inner_coords(3, Vector<double>(2));
  inner_coords[0][0]=0.0;
  inner_coords[0][1]=0.0;
  
  inner_coords[1][0]=5.8;
  inner_coords[1][1]=0.0;
  
  inner_coords[2][0]=6.0;
  inner_coords[2][1]=0.0;

  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }

  // Build
  inner_poro_outer_polyline_boundary_pt[Inner_poro_inlet_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,Inner_poro_inlet_boundary_id);
 }


 // FSI interface with SSS (upper)
 {
  Vector<Vector<double> > inner_coords(3, Vector<double>(2));
  
  inner_coords[0][0]=6.0;
  inner_coords[0][1]=0.0;

  inner_coords[2][0]=Global_Physical_Variables::R_syrinx_top_sss_fsi;
  inner_coords[2][1]=Global_Physical_Variables::Z_syrinx_top_fsi;

  // Intersection with line from poro-elastic bl
  {
   Vector<Vector<double> > line1(2,Vector<double>(2));
   Vector<Vector<double> > line2(2,Vector<double>(2));
   
   // FSI interface
   line1[0][0]=inner_coords[0][0];
   line1[0][1]=inner_coords[0][1];
   line1[1][0]=inner_coords[2][0];
   line1[1][1]=inner_coords[2][1];
   
   // Just move radially inwards upwards (to larger z))
   line2[0][0]=inner_coords[2][0];
   line2[0][1]=inner_coords[2][1]+
    Global_Physical_Variables::BL_thick_poro*
    Global_Physical_Variables::BL_poro_scaling_factor;
   line2[1][0]=0.0;
   line2[1][1]=inner_coords[2][1]+
    Global_Physical_Variables::BL_thick_poro*
    Global_Physical_Variables::BL_poro_scaling_factor;

   // Get intersection
   Upper_poro_elastic_bl_line[0]=
    Global_Physical_Variables::bl_intersection(line1,line2,0.0);
   
   oomph_info << "bl intersect at: " 
              << Upper_poro_elastic_bl_line[0][0] << " " 
              << Upper_poro_elastic_bl_line[0][1] << " " 
              << std::endl;
   
   inner_coords[1][0]=Upper_poro_elastic_bl_line[0][0];
   inner_coords[1][1]=Upper_poro_elastic_bl_line[0][1];
  }
  
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt[
   Inner_poro_upper_SSS_interface_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_upper_SSS_interface_boundary_id);
 }


 double r_outer_radial_line_in_syrinx_cover=0.0;

 // FSI interface with SSS (central)
 {
  Vector<Vector<double> > inner_coords(3, Vector<double>(2));

  inner_coords[0][0]=Global_Physical_Variables::R_syrinx_top_sss_fsi;
  inner_coords[0][1]=Global_Physical_Variables::Z_syrinx_top_fsi;

  r_outer_radial_line_in_syrinx_cover=Global_Physical_Variables::R_syrinx_top_sss_fsi+(Global_Physical_Variables::R_syrinx_bottom_sss_fsi-Global_Physical_Variables::R_syrinx_top_sss_fsi)*
   (Z_radial_cut_through_syrinx-(Global_Physical_Variables::Z_syrinx_top_fsi))/((Global_Physical_Variables::Z_syrinx_bottom_fsi)-(Global_Physical_Variables::Z_syrinx_top_fsi));
   
  inner_coords[1][0]=r_outer_radial_line_in_syrinx_cover;
  inner_coords[1][1]=Z_radial_cut_through_syrinx;

  inner_coords[2][0]=Global_Physical_Variables::R_syrinx_bottom_sss_fsi;
  inner_coords[2][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
  
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_central_SSS_interface_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_central_SSS_interface_boundary_id);
 }


 // FSI interface with SSS (lower)
 {
  Vector<Vector<double> > inner_coords(5, Vector<double>(2));

  inner_coords[0][0]=Global_Physical_Variables::R_syrinx_bottom_sss_fsi;
  inner_coords[0][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;

  inner_coords[2][0]=4.24;
  inner_coords[2][1]=-440.0;    

  // Intersection with line from poro-elastic bl
  {
   Vector<Vector<double> > line1(2,Vector<double>(2));
   Vector<Vector<double> > line2(2,Vector<double>(2));
   
   // FSI interface
   line1[0][0]=inner_coords[0][0];
   line1[0][1]=inner_coords[0][1];
   line1[1][0]=inner_coords[2][0];
   line1[1][1]=inner_coords[2][1];
   
   // Just move radially inwards upwards (to smaller z))
   line2[0][0]=inner_coords[0][0];
   line2[0][1]=inner_coords[0][1]-
    Global_Physical_Variables::BL_thick_poro*
    Global_Physical_Variables::BL_poro_scaling_factor;
   line2[1][0]=0.0;
   line2[1][1]=inner_coords[0][1]-
    Global_Physical_Variables::BL_thick_poro*
    Global_Physical_Variables::BL_poro_scaling_factor;
   
   // Get intersection
   Lower_poro_elastic_bl_line[0]=
    Global_Physical_Variables::bl_intersection(line1,line2,0.0);
   
   oomph_info << "BL INTERSECT at: " 
              << Lower_poro_elastic_bl_line[0][0] << " " 
              << Lower_poro_elastic_bl_line[0][1] << " " 
              << std::endl;
   
   inner_coords[1][0]=Lower_poro_elastic_bl_line[0][0];
   inner_coords[1][1]=Lower_poro_elastic_bl_line[0][1];
  }

  
  inner_coords[3][0]=1.25;
  inner_coords[3][1]=-480.0;
  
  inner_coords[4][0]=1.25; 
  inner_coords[4][1]=-600.0;
  
  
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_lower_SSS_interface_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_lower_SSS_interface_boundary_id);
 }


 // Outlet
 {
  Vector<Vector<double> > inner_coords(3, Vector<double>(2));

  inner_coords[0][0]=1.25;
  inner_coords[0][1]=-600.0;
  
  inner_coords[1][0]=1.05;
  inner_coords[1][1]=-600.0;

  inner_coords[2][0]=0.0;
  inner_coords[2][1]=-600.0;
  
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt[Inner_poro_outlet_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,Inner_poro_outlet_boundary_id);
 }



 // Vertex id at which lower poro bl boundary near syrinx connects with
 // symmetry line
 unsigned n_lower_poro_elastic_bl_line_boundary=1;
 
 // Symm line near outlet
 {
  Vector<Vector<double> > inner_coords(3, Vector<double>(2));

  inner_coords[0][0]=0.0;
  inner_coords[0][1]=-600.0;
  
  // Need this one to define boundary with filum!
  inner_coords[1][0]=0.0;
  inner_coords[1][1]=-480.0;


  // Intersection with line from poro-elastic bl
  {
   Vector<Vector<double> > line1(2,Vector<double>(2));
   Vector<Vector<double> > line2(2,Vector<double>(2));
   
   // Symm line moved to neg. radii 
   line1[0][0]=-Global_Physical_Variables::BL_thick_poro*
    Global_Physical_Variables::BL_poro_scaling_factor;
   line1[0][1]=-10.0;
   line1[1][0]=-Global_Physical_Variables::BL_thick_poro*
    Global_Physical_Variables::BL_poro_scaling_factor;
   line1[1][1]=10.0;

   // FSI interface
   line2[0][0]=0.0;
   line2[0][1]=Global_Physical_Variables::Z_syrinx_bottom_centreline;
   line2[1][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
   line2[1][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
    
   // Get intersection
   Lower_poro_elastic_bl_line[2]=
    Global_Physical_Variables::bl_intersection(
     line1,line2,
     Global_Physical_Variables::BL_thick_poro*
     Global_Physical_Variables::BL_poro_scaling_factor);
   
   oomph_info << "2nd BL INTERSECT at: " 
              << Lower_poro_elastic_bl_line[2][0] << " " 
              << Lower_poro_elastic_bl_line[2][1] << " " 
              << std::endl;
   
   inner_coords[2][0]=Lower_poro_elastic_bl_line[2][0];
   inner_coords[2][1]=Lower_poro_elastic_bl_line[2][1];
  }
  n_lower_poro_elastic_bl_line_boundary=inner_coords.size()-1;

  // Sub-divide?
  if ((Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw!=0.0)&
      (Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw!=0.0))
   {
    unsigned n=
     unsigned(
      Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw/
      Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw);
    double dz=
     Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw/
     double(n);
    double offset=(Global_Physical_Variables::Z_syrinx_bottom_centreline-Lower_poro_elastic_bl_line[2][1])-
     Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw;
    for (unsigned j=1;j<n;j++)
     {
      Vector<double> x(2);
      x[0]=0.0;
      x[1]=Lower_poro_elastic_bl_line[2][1]+offset+double(j)*dz;
      inner_coords.push_back(x);
     }
   }
  
  Vector<double> x(2);
  x[0]=0.0;
  x[1]=Global_Physical_Variables::Z_syrinx_bottom_centreline;
  inner_coords.push_back(x);

  // inner_coords[3][0]=0.0;
  // inner_coords[3][1]=Global_Physical_Variables::Z_syrinx_bottom_centreline;

  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_symmetry_near_outlet_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_symmetry_near_outlet_boundary_id);
 }




 // Default
 unsigned n_final_vertex_lower_syrinx=1;

 // FSI interface in syrinx (lower)
 {

  double length=sqrt(Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi*Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi+10.0*10.0);
  unsigned ns=npts_on_elliptical_boundaries; // hierher 2;
  if (CommandLineArgs::command_line_flag_has_been_set(
       "--ds_syrinx_poro_fsi_boundary_raw"))
   {
    ns=unsigned(length/
                Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_raw);
   }

  Vector<Vector<double> > inner_coords(ns, Vector<double>(2));
  double dr=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi/double(ns-1);
  double dz=10.0/double(ns-1);

  inner_coords[0][0]=0.0;
  inner_coords[0][1]=Global_Physical_Variables::Z_syrinx_bottom_centreline;

  for (unsigned j=1;j<(ns-1);j++)
   {
    inner_coords[j][0]=0.0+double(j)*dr;
    inner_coords[j][1]=Global_Physical_Variables::Z_syrinx_bottom_centreline+double(j)*dz;
   }
    
  inner_coords[ns-1][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
  inner_coords[ns-1][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
  


  // Overwrite?
  if ((Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_raw!=0.0)&&
      (Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw!=0.0)&
      (Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw!=0.0))

  {
   oomph_info << "Using non-uniform spacing on lower syrinx poro fsi boundary\n";
   Vector<double> start(2);
   start[0]=0.0;
   start[1]=Global_Physical_Variables::Z_syrinx_bottom_centreline;
   Vector<double> end(2);
   end[0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
   end[1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
   double edge_length_central=
    Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_raw;
   double edge_length_start=
    Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw;
   double edge_length_end=
    Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw;
   double delta_start=
    Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw;
   double delta_end=
    Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw;
   Global_Physical_Variables::create_non_uniform_vertices(start,
                                                          end,
                                                          edge_length_central,
                                                          edge_length_start,
                                                          edge_length_end,
                                                          delta_start,
                                                          delta_end,
                                                          inner_coords);
  }


  if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
   {
    bool upward=false;
    bool reverse=false;
    oomph_info << "start ellipsify\n";
    Global_Physical_Variables::ellipsify(inner_coords,upward,reverse);
    oomph_info << "end ellipsify\n";
   }

  // Rescale
  unsigned n=inner_coords.size();
  inner_poro_lower_syrinx_fsi_vertices_raw.resize(n);
  oomph_info << "hierher inner_poro_lower_syrinx_fsi_vertices_raw size: "
             << n << std::endl;
  for (unsigned j=0;j<n;j++)
   {
    inner_poro_lower_syrinx_fsi_vertices_raw[j]=inner_coords[j];
    oomph_info << "hierher j size " << j << " " << inner_coords[j].size() << std::endl;
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }

  // Remember final vertex
  n_final_vertex_lower_syrinx=n-1;

  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_lower_syrinx_interface_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_lower_syrinx_interface_boundary_id);
 }

 double r_inner_radial_line_in_syrinx_cover=0.0;
 unsigned n_vertex_radial_line_on_syrinx_fsi_boundary=1;

 // FSI interface in syrinx (central)
 {
  r_inner_radial_line_in_syrinx_cover=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi+(Global_Physical_Variables::R_syrinx_top_syrinx_fsi-Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi)*
   (Z_radial_cut_through_syrinx-(Global_Physical_Variables::Z_syrinx_bottom_fsi))/((Global_Physical_Variables::Z_syrinx_top_fsi)-(Global_Physical_Variables::Z_syrinx_bottom_fsi));
   
  // Ignore radial variations here -- it's only approximate
  double length1=std::fabs(std::fabs(Z_radial_cut_through_syrinx)+Global_Physical_Variables::Z_syrinx_bottom_fsi);
  double length2=std::fabs(std::fabs(Z_radial_cut_through_syrinx)+Global_Physical_Variables::Z_syrinx_top_fsi);


  unsigned ns1=2;
  unsigned ns2=2;
  if (CommandLineArgs::command_line_flag_has_been_set(
       "--ds_syrinx_poro_fsi_boundary_raw"))
   {
    ns1=unsigned(length1/
                 Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_raw);
    ns2=unsigned(length2/
                 Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_raw);

   }
  unsigned ns=ns1+ns2-1;

  Vector<Vector<double> > inner_coords(ns, Vector<double>(2));
  double dr=(r_inner_radial_line_in_syrinx_cover-Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi)/double(ns1-1);
  double dz=length1/double(ns1-1);

  inner_coords[0][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
  inner_coords[0][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;  
  for (unsigned j=1;j<(ns1-1);j++)
   {
    inner_coords[j][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi+double(j)*dr;
    inner_coords[j][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi+double(j)*dz;
   }
  inner_coords[ns1-1][0]=r_inner_radial_line_in_syrinx_cover;
  inner_coords[ns1-1][1]=Z_radial_cut_through_syrinx;

  n_vertex_radial_line_on_syrinx_fsi_boundary=ns1-1;
  dr=(Global_Physical_Variables::R_syrinx_top_syrinx_fsi-r_inner_radial_line_in_syrinx_cover)/double(ns2-1);
  dz=length2/double(ns2-1);
  for (unsigned j=1;j<(ns2-1);j++)
   {
    inner_coords[ns1-1+j][0]=r_inner_radial_line_in_syrinx_cover+double(j)*dr;
    inner_coords[ns1-1+j][1]=Z_radial_cut_through_syrinx+double(j)*dz;
   }

  inner_coords[ns-1][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
  inner_coords[ns-1][1]=Global_Physical_Variables::Z_syrinx_top_fsi;


  // Overwrite?
  if ((Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_raw!=0.0)&&
      (Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw!=0.0)&
      (Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw!=0.0))

  {
   oomph_info 
    << "Using non-uniform spacing on central syrinx poro fsi boundary\n";
   Vector<double> start(2);
   start[0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
   start[1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
   Vector<double> end(2);
   end[0]=r_inner_radial_line_in_syrinx_cover;
   end[1]=Z_radial_cut_through_syrinx;
   double edge_length_central=
    Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_raw;
   double edge_length_start=
    Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw;
   double edge_length_end=
    Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw;
   double delta_start=
    Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw;
   double delta_end=0.0; 
   Global_Physical_Variables::create_non_uniform_vertices(start,
                                                          end,
                                                          edge_length_central,
                                                          edge_length_start,
                                                          edge_length_end,
                                                          delta_start,
                                                          delta_end,
                                                          inner_coords);
   n_vertex_radial_line_on_syrinx_fsi_boundary=inner_coords.size()-1;


   Vector<Vector<double> > second_batch;
   start[0]=r_inner_radial_line_in_syrinx_cover;
   start[1]=Z_radial_cut_through_syrinx;
   end[0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
   end[1]=Global_Physical_Variables::Z_syrinx_top_fsi;
   delta_start=0.0;
   delta_end=
    Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw;
   Global_Physical_Variables::create_non_uniform_vertices(start,
                                                          end,
                                                          edge_length_central,
                                                          edge_length_start,
                                                          edge_length_end,
                                                          delta_start,
                                                          delta_end,
                                                          second_batch);

   // Push back
   unsigned n=second_batch.size();
   for (unsigned i=1;i<n;i++)
    {
     inner_coords.push_back(second_batch[i]);
    }
  }


    
  // Rescale
  unsigned n=inner_coords.size();
  inner_poro_central_syrinx_fsi_vertices_raw.resize(n);
  for (unsigned j=0;j<n;j++)
   {
    inner_poro_central_syrinx_fsi_vertices_raw[j]=inner_coords[j];
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_central_syrinx_interface_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_central_syrinx_interface_boundary_id);
 }

 // FSI interface in syrinx (upper)
 {

  double length=sqrt(Global_Physical_Variables::R_syrinx_top_syrinx_fsi*Global_Physical_Variables::R_syrinx_top_syrinx_fsi+10.0*10.0);
    unsigned ns=npts_on_elliptical_boundaries; // hierher 2;
  if (CommandLineArgs::command_line_flag_has_been_set(
       "--ds_syrinx_poro_fsi_boundary_raw"))
   {
    ns=unsigned(length/
                Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_raw);
   }

  Vector<Vector<double> > inner_coords(ns, Vector<double>(2));    
  double dr=Global_Physical_Variables::R_syrinx_top_syrinx_fsi/double(ns-1);
  double dz=10.0/double(ns-1);

  inner_coords[0][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
  inner_coords[0][1]=Global_Physical_Variables::Z_syrinx_top_fsi;
  
  for (unsigned j=1;j<(ns-1);j++)
   {
    inner_coords[j][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi-double(j)*dr;
    inner_coords[j][1]=Global_Physical_Variables::Z_syrinx_top_fsi+double(j)*dz;
   }

  inner_coords[ns-1][0]=0.0;
  inner_coords[ns-1][1]=Global_Physical_Variables::Z_syrinx_top_centreline;
  


  // Overwrite?
  if ((Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_raw!=0.0)&&
      (Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw!=0.0)&
      (Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw!=0.0))

  {
   oomph_info 
    << "Using non-uniform spacing on upper syrinx poro fsi boundary\n";
   Vector<double> start(2);
   start[0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
   start[1]=Global_Physical_Variables::Z_syrinx_top_fsi;
   Vector<double> end(2);
   end[0]=0.0;
   end[1]=Global_Physical_Variables::Z_syrinx_top_centreline;
   double edge_length_central=
    Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_raw;
   double edge_length_start=
    Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw;
   double edge_length_end=
    Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw;
   double delta_start=
    Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw;
   double delta_end=
    Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw;
   Global_Physical_Variables::create_non_uniform_vertices(start,
                                                          end,
                                                          edge_length_central,
                                                          edge_length_start,
                                                          edge_length_end,
                                                          delta_start,
                                                          delta_end,
                                                          inner_coords);
  }




  if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
   {
    bool upward=true;
    bool reverse=true;
    oomph_info << "start ellipsify [2]\n";
    Global_Physical_Variables::ellipsify(inner_coords,upward,reverse);
    oomph_info << "end ellipsify [2]\n";
   }



  // Rescale
  unsigned n=inner_coords.size();
  inner_poro_upper_syrinx_fsi_vertices_raw.resize(n);
  for (unsigned j=0;j<n;j++)
   {
    inner_poro_upper_syrinx_fsi_vertices_raw[j]=inner_coords[j];
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_upper_syrinx_interface_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_upper_syrinx_interface_boundary_id);
 }





 // Vertex id at which upper poro bl boundary near syrinx connects with
 // symmetry line
 unsigned n_upper_poro_elastic_bl_line_boundary=1;
 
 
 // Symm line near inlet
 {
  Vector<Vector<double> > inner_coords(1, Vector<double>(2));
  inner_coords[0][0]=0.0;
  inner_coords[0][1]=Global_Physical_Variables::Z_syrinx_top_centreline;
  

  // Intersection with line from poro-elastic bl
  {
   Vector<Vector<double> > line1(2,Vector<double>(2));
   Vector<Vector<double> > line2(2,Vector<double>(2));
   
   // FSI interface
   line1[0][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
   line1[0][1]=Global_Physical_Variables::Z_syrinx_top_fsi;
   line1[1][0]=0.0;
   line1[1][1]=Global_Physical_Variables::Z_syrinx_top_centreline;
   
   // Symm line moved to neg. radii 
   line2[0][0]=-Global_Physical_Variables::BL_thick_poro*
    Global_Physical_Variables::BL_poro_scaling_factor;
   line2[0][1]=-10.0;
   line2[1][0]=-Global_Physical_Variables::BL_thick_poro*
    Global_Physical_Variables::BL_poro_scaling_factor;
   line2[1][1]=10.0;
   
   // Get intersection
   Upper_poro_elastic_bl_line[2]=
    Global_Physical_Variables::bl_intersection(
     line1,line2,
     Global_Physical_Variables::BL_thick_poro*
     Global_Physical_Variables::BL_poro_scaling_factor);
   
   oomph_info << "2nd bl intersect at: " 
              << Upper_poro_elastic_bl_line[2][0] << " " 
              << Upper_poro_elastic_bl_line[2][1] << " " 
              << std::endl;

   // Sub-divide?
   if ((Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw!=0.0)&
       (Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw!=0.0))
    {
     unsigned n=
      unsigned(
       Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw/
       Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw);
     double dz=
      Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw/
      double(n);
     for (unsigned j=1;j<=n;j++)
      {
       Vector<double> x(2);
       x[0]=0.0;
       x[1]=inner_coords[0][1]+double(j)*dz;
       inner_coords.push_back(x);
      }
    }
  
   inner_coords.push_back(Upper_poro_elastic_bl_line[2]);
   n_upper_poro_elastic_bl_line_boundary=inner_coords.size()-1;

   // inner_coords[1][0]=Upper_poro_elastic_bl_line[2][0];
   // inner_coords[1][1]=Upper_poro_elastic_bl_line[2][1];
  }
         
  Vector<double> x(2,0.0);
  inner_coords.push_back(x);
  // inner_coords[2][0]=0.0;
  // inner_coords[2][1]=0.0;
  
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_symmetry_near_inlet_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_symmetry_near_inlet_boundary_id);

 }
 TriangleMeshClosedCurve* inner_poro_outer_boundary_pt =
  new TriangleMeshClosedCurve(inner_poro_outer_polyline_boundary_pt);




  // Finally: halfway point of bounding lines for poro-elastic bls

  // Intersection with line from upper poro-elastic bl
  {
   Vector<Vector<double> > line1(2,Vector<double>(2));
   Vector<Vector<double> > line2(2,Vector<double>(2));
   
   // Radial line from FSI interface
   line1[0][0]=5.65;
   line1[0][1]=Global_Physical_Variables::Z_syrinx_top_fsi;
   line1[1][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
   line1[1][1]=Global_Physical_Variables::Z_syrinx_top_fsi;
   
   // syrinx upper fsi boundary
   line2[0][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
   line2[0][1]=Global_Physical_Variables::Z_syrinx_top_fsi;
   line2[1][0]=0.0;
   line2[1][1]=Global_Physical_Variables::Z_syrinx_top_centreline;
   
   // Get intersection
   Upper_poro_elastic_bl_line[1]=
    Global_Physical_Variables::bl_intersection(
     line1,line2,Global_Physical_Variables::BL_thick_poro*
     Global_Physical_Variables::BL_poro_scaling_factor);
   
   oomph_info << "3rd bl intersect at: " 
              << Upper_poro_elastic_bl_line[1][0] << " " 
              << Upper_poro_elastic_bl_line[1][1] << " " 
              << std::endl;
  }
  

  // Intersection with line from lower poro-elastic bl
  {
   Vector<Vector<double> > line1(2,Vector<double>(2));
   Vector<Vector<double> > line2(2,Vector<double>(2));
   

   // syrinx lower fsi boundary
   line1[0][0]=0.0;
   line1[0][1]=Global_Physical_Variables::Z_syrinx_bottom_centreline;
   line1[1][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
   line1[1][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;

   // Radial line from FSI interface
   line2[0][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
   line2[0][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
   line2[1][0]=Global_Physical_Variables::R_syrinx_bottom_sss_fsi;
   line2[1][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
   
   
   // Get intersection
   Lower_poro_elastic_bl_line[1]=
    Global_Physical_Variables::bl_intersection(
     line1,line2,Global_Physical_Variables::BL_thick_poro*    
     Global_Physical_Variables::BL_poro_scaling_factor);
   
   oomph_info << "3rd BL INTERSECT at: " 
              << Lower_poro_elastic_bl_line[1][0] << " " 
              << Lower_poro_elastic_bl_line[1][1] << " " 
              << std::endl;
  }
  

  
 // We have seven internal boundaries 
 Vector<TriangleMeshOpenCurve*> inner_inner_open_boundary_pt(9);
 TriangleMeshPolyLine *inner_inner_polyline1_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline2_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline3_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline4_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline5_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline6_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline7_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline8_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline9_pt = 0;

   
 // First internal boundary -- boundary between "pia" and genuine interior
 {
  Vector<Vector<double> > inner_inner_coords(4, Vector<double>(2,0.0));
  
  inner_inner_coords[0][0]=5.8;
  inner_inner_coords[0][1]=0.0;
  
  inner_inner_coords[1][0]=4.04;
  inner_inner_coords[1][1]=-440.0;
  
  inner_inner_coords[2][0]=1.05;
  inner_inner_coords[2][1]=-480.0;
  
  inner_inner_coords[3][0]=1.05;
  inner_inner_coords[3][1]=-600.0;
  
  // Rescale
  for(unsigned i=0;i<4;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_inner_polyline1_pt =
   new TriangleMeshPolyLine(inner_inner_coords,
                            Inner_poro_pia_internal_boundary_id);
  
  // Connect initial vertex to middle vertex on inlet boundary
  inner_inner_polyline1_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>
   (inner_poro_outer_polyline_boundary_pt[Inner_poro_inlet_boundary_id]),1);

  // Connect final vertex to middle vertex on outlet boundary
  inner_inner_polyline1_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>
   (inner_poro_outer_polyline_boundary_pt[Inner_poro_outlet_boundary_id]),1);
  

  // Store in vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section1_pt(1);
  inner_inner_curve_section1_pt[0] = inner_inner_polyline1_pt;
  
  // Create first internal open curve
  inner_inner_open_boundary_pt[0] =
   new TriangleMeshOpenCurve(inner_inner_curve_section1_pt);
 }

 // Second internal boundary -- boundary between "filum" and genuine interior
 {
  Vector<Vector<double> > inner_inner_coords(2, Vector<double>(2,0.0));

  inner_inner_coords[0][0]=0.0;
  inner_inner_coords[0][1]=-480.0;
  
  inner_inner_coords[1][0]=1.05;
  inner_inner_coords[1][1]=-480.0;
  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_inner_polyline2_pt =
   new TriangleMeshPolyLine(inner_inner_coords,
                            Inner_poro_filum_internal_boundary_id);
  
  // Connect initial vertex of separating line with first vertex on 
  // symmetry line
  inner_inner_polyline2_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_symmetry_near_outlet_boundary_id]),1);

  // Conect final vertex to second vertex on pia internal boundary
  inner_inner_polyline2_pt->connect_final_vertex_to_polyline(
   inner_inner_polyline1_pt,2);
  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section2_pt(1);
  inner_inner_curve_section2_pt[0] = inner_inner_polyline2_pt;
  
  // Build
  inner_inner_open_boundary_pt[1] =
   new TriangleMeshOpenCurve(inner_inner_curve_section2_pt);
  
 }



 // Poro-elastic bl thickness in syrinx cover region. Limited
 // to avoid overlap
 double bl_thick_in_syrinx_cover=
  min(0.3,Global_Physical_Variables::BL_thick_poro*
      Global_Physical_Variables::BL_poro_scaling_factor);
      

 // Third internal boundary -- lower boundary of region that Chris
 // usually wants to be the only poro-elastic one
 {
  Vector<Vector<double> > inner_inner_coords(3, Vector<double>(2,0.0));

  inner_inner_coords[0][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
  inner_inner_coords[0][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
  
  inner_inner_coords[1][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi+bl_thick_in_syrinx_cover;
  inner_inner_coords[1][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;

  inner_inner_coords[2][0]=Global_Physical_Variables::R_syrinx_bottom_sss_fsi;
  inner_inner_coords[2][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
  
  // Rescale
  for(unsigned i=0;i<3;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_inner_polyline3_pt =
   new TriangleMeshPolyLine(
    inner_inner_coords,
    Inner_poro_lower_internal_boundary_of_poroelastic_region_boundary_id);
  
  // Connect initial vertex of separating line with final vertex of
  // lower syrinx region boundary
  inner_inner_polyline3_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_lower_syrinx_interface_boundary_id]),
   n_final_vertex_lower_syrinx);

  // Connect final vertex to first vertex of
  // lower poro-sss boundary
  inner_inner_polyline3_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_lower_SSS_interface_boundary_id]),0);
  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section3_pt(1);
  inner_inner_curve_section3_pt[0] = inner_inner_polyline3_pt;
  
  // Build
  inner_inner_open_boundary_pt[2] =
   new TriangleMeshOpenCurve(inner_inner_curve_section3_pt);
 }


 // Fourth internal boundary -- upper boundary of region that Chris
 // usually wants to be the only poro-elastic one
 {
  Vector<Vector<double> > inner_inner_coords(3, Vector<double>(2,0.0));

  inner_inner_coords[0][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
  inner_inner_coords[0][1]=Global_Physical_Variables::Z_syrinx_top_fsi;

  inner_inner_coords[1][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi+bl_thick_in_syrinx_cover;
  inner_inner_coords[1][1]=Global_Physical_Variables::Z_syrinx_top_fsi;
  
  inner_inner_coords[2][0]=Global_Physical_Variables::R_syrinx_top_sss_fsi;
  inner_inner_coords[2][1]=Global_Physical_Variables::Z_syrinx_top_fsi;
  
  // Rescale
  for(unsigned i=0;i<3;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_inner_polyline4_pt =
   new TriangleMeshPolyLine(
    inner_inner_coords,
    Inner_poro_upper_internal_boundary_of_poroelastic_region_boundary_id);
  
  // Connect initial vertex of separating line with first vertex of
  // upper syrinx region boundary
  inner_inner_polyline4_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_upper_syrinx_interface_boundary_id]),0);

  // Conect final vertex to first vertex on central part of 
  // poro-sss boundary
  inner_inner_polyline4_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_central_SSS_interface_boundary_id]),0);

  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section4_pt(1);
  inner_inner_curve_section4_pt[0] = inner_inner_polyline4_pt;
  
  // Build
  inner_inner_open_boundary_pt[3] =
   new TriangleMeshOpenCurve(inner_inner_curve_section4_pt);
 }


 // Fifth internal boundary -- radial line through syrinx cover
 {
  Vector<Vector<double> > inner_inner_coords(2, Vector<double>(2,0.0));

  inner_inner_coords[0][0]=r_inner_radial_line_in_syrinx_cover;
  inner_inner_coords[0][1]=Z_radial_cut_through_syrinx;
  
  inner_inner_coords[1][0]=r_outer_radial_line_in_syrinx_cover;
  inner_inner_coords[1][1]=Z_radial_cut_through_syrinx;
  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_inner_polyline5_pt =
   new TriangleMeshPolyLine(inner_inner_coords,
                            Inner_poro_radial_line_boundary_id);

  
  // Connect initial vertex of radial line with point on syrinx fsi boundary
  inner_inner_polyline5_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_central_syrinx_interface_boundary_id]),
   n_vertex_radial_line_on_syrinx_fsi_boundary);

  // Conect final vertex to vertex on central part of 
  // poro-sss boundary
  inner_inner_polyline5_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_central_SSS_interface_boundary_id]),1);
  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section5_pt(1);
  inner_inner_curve_section5_pt[0] = inner_inner_polyline5_pt;
  
  // Build
  inner_inner_open_boundary_pt[4] =
   new TriangleMeshOpenCurve(inner_inner_curve_section5_pt);
 }



 // Sixth internal boundary -- upper boundary of poroelastic bl near syrinx
 {

  // Rescale
  for(unsigned i=0;i<3;i++)
   {
    Upper_poro_elastic_bl_line[i][0]*=Global_Physical_Variables::R_scale;
    Upper_poro_elastic_bl_line[i][1]*=Global_Physical_Variables::Z_scale;

    oomph_info << "Rescaled: "
               << Upper_poro_elastic_bl_line[i][0] << " "
               << Upper_poro_elastic_bl_line[i][1] << " " 
               << std::endl;
   }

  Vector<Vector<double> > inner_coords(Upper_poro_elastic_bl_line);
  if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
   {
    // Shift inwards to avoid overlap of internal boundaries
    // hierherUpper_poro_elastic_bl_line[1][0]-=0.01;
    Vector<Vector<double> > aux_inner_coords(npts_on_elliptical_boundaries);
    for (unsigned j=0;j<npts_on_elliptical_boundaries;j++)
     {
      aux_inner_coords[j].resize(2);
      for (unsigned i=0;i<2;i++)
       {
        aux_inner_coords[j][i]=Upper_poro_elastic_bl_line[1][i]+
         double (j)/double(npts_on_elliptical_boundaries-1)*
         (Upper_poro_elastic_bl_line[2][i]-
          Upper_poro_elastic_bl_line[1][i]);
       }
     }
    bool upward=true;
    bool reverse=reverse;
    oomph_info << "start ellipsify [3]\n";
    Global_Physical_Variables::ellipsify(aux_inner_coords,upward,reverse);
    oomph_info << "end ellipsify [3]\n";
    inner_coords.resize(npts_on_elliptical_boundaries+1);
    inner_coords[0]=Upper_poro_elastic_bl_line[0];
    for (unsigned j=0;j<npts_on_elliptical_boundaries;j++)
     {
      inner_coords[j+1]=aux_inner_coords[j];
     }
   }

  // Build
  inner_inner_polyline6_pt =
   new TriangleMeshPolyLine(inner_coords, 
                            Upper_poro_elastic_bl_line_boundary_id);
  
  
  // Connect initial vertex of line with point on upper sss fsi boundary
  inner_inner_polyline6_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_upper_SSS_interface_boundary_id]),1); // 1=vertex index

  // Connect final vertex to vertex on upper symm line
  inner_inner_polyline6_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_symmetry_near_inlet_boundary_id]),
   n_upper_poro_elastic_bl_line_boundary); 
  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section6_pt(1);
  inner_inner_curve_section6_pt[0] = inner_inner_polyline6_pt;
  
  // Build
  inner_inner_open_boundary_pt[5] =
   new TriangleMeshOpenCurve(inner_inner_curve_section6_pt);
 }



 // Seventh internal boundary -- lower boundary of poroelastic bl near syrinx
 {
  // Rescale
  for(unsigned i=0;i<3;i++)
   {
    Lower_poro_elastic_bl_line[i][0]*=Global_Physical_Variables::R_scale;
    Lower_poro_elastic_bl_line[i][1]*=Global_Physical_Variables::Z_scale;
   }
  

  Vector<Vector<double> > inner_coords(Lower_poro_elastic_bl_line);
  if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
   {
    // Shift inwards to avoid overlap of internal boundaries
    // hierher Lower_poro_elastic_bl_line[1][0]-=0.02;
    Vector<Vector<double> > aux_inner_coords(npts_on_elliptical_boundaries);
    for (unsigned j=0;j<npts_on_elliptical_boundaries;j++)
     {
      aux_inner_coords[j].resize(2);
      for (unsigned i=0;i<2;i++)
       {
        aux_inner_coords[j][i]=Lower_poro_elastic_bl_line[1][i]+
         double (j)/double(npts_on_elliptical_boundaries-1)*
         (Lower_poro_elastic_bl_line[2][i]-
          Lower_poro_elastic_bl_line[1][i]);
       }
     }
    bool upward=false;
    bool reverse=reverse;
    oomph_info << "start ellipsify [4]\n";
    Global_Physical_Variables::ellipsify(aux_inner_coords,upward,reverse);
    oomph_info << "end ellipsify [4]\n";
    inner_coords.resize(npts_on_elliptical_boundaries+1);
    inner_coords[0]=Lower_poro_elastic_bl_line[0];
    for (unsigned j=0;j<npts_on_elliptical_boundaries;j++)
     {
      inner_coords[j+1]=aux_inner_coords[j];
     }
   }

  // Build
  inner_inner_polyline7_pt =
   new TriangleMeshPolyLine(inner_coords, 
                            Lower_poro_elastic_bl_line_boundary_id);
  
  
  // Connect initial vertex of line with point on upper sss fsi boundary
  inner_inner_polyline7_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_lower_SSS_interface_boundary_id]),1); // 1=vertex index

  // Connect final vertex to vertex on lower symm line
  inner_inner_polyline7_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_symmetry_near_outlet_boundary_id]),
   n_lower_poro_elastic_bl_line_boundary); 
  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section7_pt(1);
  inner_inner_curve_section7_pt[0] = inner_inner_polyline7_pt;
  
  // Build
  inner_inner_open_boundary_pt[6] =
   new TriangleMeshOpenCurve(inner_inner_curve_section7_pt);
 }


 // Eight internal boundary -- outer boundary of poroelastic bl
 // on syrinx 
 {
  Vector<Vector<double> > line_coords(2, Vector<double>(2,0.0));

  line_coords[0][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi+bl_thick_in_syrinx_cover;
  line_coords[0][1]=Global_Physical_Variables::Z_syrinx_top_fsi;

  line_coords[1][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi+bl_thick_in_syrinx_cover;
  line_coords[1][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;

  for(unsigned i=0;i<2;i++)
   {
    line_coords[i][0]*=Global_Physical_Variables::R_scale;
    line_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }

  // Build
  inner_inner_polyline8_pt =
   new TriangleMeshPolyLine(line_coords,
                            Syrinx_cover_poro_elastic_bl_line_boundary_id);
  
  
  // Connect initial vertex of line with middle vertex on upper boundary
  // of syrinx cover
  inner_inner_polyline8_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(inner_inner_polyline4_pt),1); // 1=vertex index

  // Connect final vertex of line with middle vertex on upper boundary
  // of syrinx cover
  inner_inner_polyline8_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(inner_inner_polyline3_pt),1);  // 1=vertex index

  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section8_pt(1);
  inner_inner_curve_section8_pt[0] = inner_inner_polyline8_pt;
  
  // Build
  inner_inner_open_boundary_pt[7] =
   new TriangleMeshOpenCurve(inner_inner_curve_section8_pt);
 }



 // Ninth internal boundary -- poro-elastic bl between "pia" and 
 // genuine interior
 {
  Vector<Vector<double> > inner_inner_coords(4, Vector<double>(2,0.0));
  
  inner_inner_coords[0][0]=5.8-bl_thick_in_syrinx_cover;
  inner_inner_coords[0][1]=0.0;
  
  inner_inner_coords[1][0]=4.04-bl_thick_in_syrinx_cover;
  inner_inner_coords[1][1]=-440.0;
  
  inner_inner_coords[2][0]=1.05-bl_thick_in_syrinx_cover;
  inner_inner_coords[2][1]=-480.0;
  
  inner_inner_coords[3][0]=1.05-bl_thick_in_syrinx_cover;
  inner_inner_coords[3][1]=-600.0;
  
  // Rescale
  for(unsigned i=0;i<4;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_inner_polyline9_pt =
   new TriangleMeshPolyLine(inner_inner_coords,
                            Pia_boundary_poro_elastic_bl_line_boundary_id);
  
  // // Connect initial vertex to middle vertex on inlet boundary
  // inner_inner_polyline1_pt->connect_initial_vertex_to_polyline(
  //  dynamic_cast<TriangleMeshPolyLine*>
  //  (inner_poro_outer_polyline_boundary_pt[Inner_poro_inlet_boundary_id]),1);

  // // Connect final vertex to middle vertex on outlet boundary
  // inner_inner_polyline1_pt->connect_final_vertex_to_polyline(
  //  dynamic_cast<TriangleMeshPolyLine*>
  //  (inner_poro_outer_polyline_boundary_pt[Inner_poro_outlet_boundary_id]),1);
  

  // Store in vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section9_pt(1);
  inner_inner_curve_section9_pt[0] = inner_inner_polyline9_pt;
  
  // Create first internal open curve
  inner_inner_open_boundary_pt[8] =
   new TriangleMeshOpenCurve(inner_inner_curve_section9_pt);
 }



 // Mesh parameters
 TriangleMeshParameters
  inner_poro_mesh_parameters(inner_poro_outer_boundary_pt);
 inner_poro_mesh_parameters.element_area() =
  Global_Physical_Variables::Element_area_poro;

 oomph_info << "Setting area of syrinx cover to " 
            << Global_Physical_Variables::Element_area_syrinx_cover 
            << std::endl;

 double element_area_poro_bl=
  Global_Physical_Variables::BL_poro_scaling_factor*
  Global_Physical_Variables::BL_poro_scaling_factor*
  Global_Physical_Variables::Element_area_syrinx_cover;

 oomph_info << "Setting area of poro bls to " 
            << element_area_poro_bl
            << std::endl;


 // Specify different target area for syrinx cover (four central regions)
 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_upper_cord_region_id,
   Global_Physical_Variables::Element_area_syrinx_cover);

 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_lower_cord_region_id,
   Global_Physical_Variables::Element_area_syrinx_cover);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_upper_pia_region_id,
   Global_Physical_Variables::Element_area_syrinx_cover);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_lower_pia_region_id,
   Global_Physical_Variables::Element_area_syrinx_cover);

 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_upper_pia_poro_bl_region_id,
   Global_Physical_Variables::Element_area_syrinx_cover);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_lower_pia_poro_bl_region_id,
   Global_Physical_Variables::Element_area_syrinx_cover);




 inner_poro_mesh_parameters.set_target_area_for_region
  (Upper_cord_syrinx_poro_bl_region_id,
   element_area_poro_bl);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Lower_cord_syrinx_poro_bl_region_id,
   element_area_poro_bl);

 inner_poro_mesh_parameters.set_target_area_for_region
  (Upper_cord_pia_bl_region_id,
   element_area_poro_bl);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_upper_cord_syrinx_cover_bl_region_id,
   element_area_poro_bl);

 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_upper_cord_pia_bl_region_id,
   element_area_poro_bl);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_lower_cord_syrinx_cover_bl_region_id,
   element_area_poro_bl);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_lower_cord_pia_bl_region_id,
   element_area_poro_bl);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Lower_cord_pia_bl_region_id,
   element_area_poro_bl);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Filum_pia_bl_region_id,
   element_area_poro_bl);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Lower_cord_syrinx_poro_bl_region_id,
   element_area_poro_bl);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Small_upper_cord_pia_bl_region_id,
   element_area_poro_bl);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Small_lower_cord_pia_bl_region_id,
   element_area_poro_bl);
 
 
 // Define inner boundaries
 inner_poro_mesh_parameters.internal_open_curves_pt() =
  inner_inner_open_boundary_pt;



 // Define region coordinates
 Vector<double> region_coords(2);

 ofstream cord_region_points;
 cord_region_points.open("cord_region_points.dat");

 double raw_offset=0.001;
 double z_raw=0.0;
 double r_raw=0.0;

 // Upper cord not specified -- defaults to zero

 // Point inside the uppper cord pia porous bl
 region_coords[0] = Global_Physical_Variables::R_scale*5.73;
 region_coords[1] = Global_Physical_Variables::Z_scale*(-0.01);
 inner_poro_mesh_parameters.add_region_coordinates(Upper_cord_pia_bl_region_id, 
                                                   region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 2 " << std::endl;


 // Point inside the upper central cord
 z_raw=-90.0-raw_offset+Global_Physical_Variables::Shorten_syrinx_by_shift_factor*
  Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=0.5*(Global_Physical_Variables::raw_cord_syrinx_fsi_radius(z_raw)+
            Global_Physical_Variables::raw_cord_pia_boundary_radius(z_raw));
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw; 
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(
  Central_upper_cord_region_id, 
  region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 3 " << std::endl;


 // Point inside the upper central cord syrinx cover porous bl
 z_raw=-90.0-raw_offset+Global_Physical_Variables::Shorten_syrinx_by_shift_factor*
  Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=Global_Physical_Variables::raw_cord_syrinx_fsi_radius(z_raw)+raw_offset;
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw;
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(
  Central_upper_cord_syrinx_cover_bl_region_id, 
  region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 4 " << std::endl;


 // Point inside the upper central cord pia porous bl
 z_raw=-90.0-raw_offset+Global_Physical_Variables::Shorten_syrinx_by_shift_factor*
  Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=Global_Physical_Variables::raw_cord_pia_boundary_radius(z_raw)-raw_offset;
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw;
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(
  Central_upper_cord_pia_bl_region_id, 
  region_coords);
 
 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 5 " << std::endl;


 // Point in pia porous bl just above central part
 z_raw=-90.0+raw_offset+Global_Physical_Variables::Shorten_syrinx_by_shift_factor*
  Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=Global_Physical_Variables::raw_cord_pia_boundary_radius(z_raw)-raw_offset;
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw;
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(
  Small_upper_cord_pia_bl_region_id, 
  region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 6 " << std::endl;

 // Point in pia porous bl just below central part
 z_raw=-210.0-raw_offset+Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=Global_Physical_Variables::raw_cord_pia_boundary_radius(z_raw)-raw_offset;
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw;
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(
  Small_lower_cord_pia_bl_region_id, 
  region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 7 " << std::endl;



 // Point inside the lower central cord
 z_raw=-210.0+raw_offset+Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=0.5*(Global_Physical_Variables::raw_cord_syrinx_fsi_radius(z_raw)+
            Global_Physical_Variables::raw_cord_pia_boundary_radius(z_raw));
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw; 
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(
  Central_lower_cord_region_id, 
  region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 8 " << std::endl;

 // Point inside the lower central cord syrinx cover porous bl
 z_raw=-210.0+raw_offset+Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=Global_Physical_Variables::raw_cord_syrinx_fsi_radius(z_raw)+raw_offset;
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw;
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(
  Central_lower_cord_syrinx_cover_bl_region_id, 
  region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 9 " << std::endl;

 // Point inside the lower central cord pia porous bl
 z_raw=-210.0+raw_offset+Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=Global_Physical_Variables::raw_cord_pia_boundary_radius(z_raw)-raw_offset;
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw;
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(
  Central_lower_cord_pia_bl_region_id, 
  region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 10 " << std::endl;


 // Point inside the lower cord
 region_coords[0] = Global_Physical_Variables::R_scale*(raw_offset);
 region_coords[1] = Global_Physical_Variables::Z_scale*(-440.0+raw_offset);
 inner_poro_mesh_parameters.add_region_coordinates(Lower_cord_region_id, 
                                                   region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 11 " << std::endl;

 // Point inside the lower cord pia porous bl
 region_coords[0] = Global_Physical_Variables::R_scale*(4.04-raw_offset);
 region_coords[1] = Global_Physical_Variables::Z_scale*(-440.0+raw_offset);
 inner_poro_mesh_parameters.add_region_coordinates(Lower_cord_pia_bl_region_id, 
                                                   region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 12 " << std::endl;



 // Point inside the upper pia
 region_coords[0] = Global_Physical_Variables::R_scale*(5.9);
 region_coords[1] = Global_Physical_Variables::Z_scale*(-0.01);
 inner_poro_mesh_parameters.add_region_coordinates(Upper_pia_region_id, 
                                                   region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 13 " << std::endl;


 // Point inside the upper central pia
 z_raw=-90.0-raw_offset+Global_Physical_Variables::Shorten_syrinx_by_shift_factor*
  Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=Global_Physical_Variables::raw_cord_sss_fsi_radius(z_raw)-raw_offset;
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw;
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(Central_upper_pia_region_id, 
                                                   region_coords);
 
 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 14 " << std::endl;


 // Point inside the lower central pia
 z_raw=-210.0+raw_offset+Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=Global_Physical_Variables::raw_cord_sss_fsi_radius(z_raw)-raw_offset;
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw;
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(Central_lower_pia_region_id, 
                                                   region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 15 " << std::endl;

 // Point inside the upper central pia beyond intersection with radial line
 // from syrinx poro bl
 z_raw=-90.0+raw_offset+Global_Physical_Variables::Shorten_syrinx_by_shift_factor*
  Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=Global_Physical_Variables::raw_cord_sss_fsi_radius(z_raw)-raw_offset;
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw;
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates
  (Central_upper_pia_poro_bl_region_id, 
   region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 16 " << std::endl;

 // Point inside the lower central pia beyond intersection with radial line
 // from syrinx poro bl
 z_raw=-210.0-raw_offset+Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=Global_Physical_Variables::raw_cord_sss_fsi_radius(z_raw)-raw_offset;
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw;
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(
  Central_lower_pia_poro_bl_region_id, 
  region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 17 " << std::endl;

 // Point inside the lower pia
 region_coords[0] = Global_Physical_Variables::R_scale*(4.24-raw_offset);
 region_coords[1] = Global_Physical_Variables::Z_scale*(-440.0+raw_offset);
 inner_poro_mesh_parameters.add_region_coordinates(Lower_pia_region_id, 
                                                   region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 18 " << std::endl;

 // Point inside the filum
 region_coords[0] = Global_Physical_Variables::R_scale*0.5;
 region_coords[1] = Global_Physical_Variables::Z_scale*(-500.0);
 inner_poro_mesh_parameters.add_region_coordinates(Filum_region_id, 
                                                   region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 19 " << std::endl;

 // Point inside the filum pia porous bl
 region_coords[0] = Global_Physical_Variables::R_scale*1.0;
 region_coords[1] = Global_Physical_Variables::Z_scale*(-500.0);
 inner_poro_mesh_parameters.add_region_coordinates(Filum_pia_bl_region_id, 
                                                   region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 20 " << std::endl;

 // Point inside the upper syrinx poro bl
 z_raw=-90.0+raw_offset+Global_Physical_Variables::Shorten_syrinx_by_shift_factor*
  Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=Global_Physical_Variables::raw_cord_syrinx_fsi_radius(z_raw)+raw_offset;
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw;
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(
  Upper_cord_syrinx_poro_bl_region_id, 
  region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 21 " << std::endl;

 // Point inside the lower syrinx poro bl
 z_raw=-210.0-raw_offset+Global_Physical_Variables::Raw_syrinx_z_shift;
 r_raw=Global_Physical_Variables::raw_cord_syrinx_fsi_radius(z_raw)+raw_offset;
 region_coords[0] = Global_Physical_Variables::R_scale*r_raw;
 region_coords[1] = Global_Physical_Variables::Z_scale*z_raw;
 inner_poro_mesh_parameters.add_region_coordinates(
  Lower_cord_syrinx_poro_bl_region_id,
  region_coords);

 cord_region_points << region_coords[0] << " " 
                    << region_coords[1] << " 22 " << std::endl;

 cord_region_points.close();

 // Build the mesh
 Inner_poro_mesh_pt = new TriangleMesh<PORO_ELEMENT>(
   inner_poro_mesh_parameters, Poro_time_stepper_pt);


 /// Move actual nodes (incl midside ones!) onto proper ellipse   
 if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
  {
   bool upward=true;
   unsigned b=Inner_poro_upper_syrinx_interface_boundary_id;
   Global_Physical_Variables::ellipsify_boundary_nodes(
    Inner_poro_mesh_pt,b,upward);
   
   upward=false;
   b=Inner_poro_lower_syrinx_interface_boundary_id;
   Global_Physical_Variables::ellipsify_boundary_nodes(
    Inner_poro_mesh_pt,b,upward);
  }

 // Test that all regions have been assigned properly
 {
  double mesh_area=0.0;
  unsigned nel=Inner_poro_mesh_pt->nelement();
  for (unsigned e=0;e<nel;e++)
   {
    mesh_area+=Inner_poro_mesh_pt->finite_element_pt(e)->size();
   }
  
  double region_area=0.0;
  Vector<unsigned> cord_regions_list;
  cord_regions_list.push_back(Upper_cord_region_id); // 1
  cord_regions_list.push_back(Upper_cord_pia_bl_region_id); // 2
  cord_regions_list.push_back(Central_upper_cord_region_id); // 3
  cord_regions_list.push_back(Central_upper_cord_syrinx_cover_bl_region_id); // 4
  cord_regions_list.push_back(Central_upper_cord_pia_bl_region_id); // 5
  cord_regions_list.push_back(Small_upper_cord_pia_bl_region_id); // 6
  cord_regions_list.push_back(Small_lower_cord_pia_bl_region_id); // 7 
  cord_regions_list.push_back(Central_lower_cord_region_id); // 8
  cord_regions_list.push_back(Central_lower_cord_syrinx_cover_bl_region_id); // 9
  cord_regions_list.push_back(Central_lower_cord_pia_bl_region_id); // 10
  cord_regions_list.push_back(Lower_cord_region_id);  // 11
  cord_regions_list.push_back(Lower_cord_pia_bl_region_id); // 12 
  cord_regions_list.push_back(Upper_pia_region_id);  // 13
  cord_regions_list.push_back(Central_upper_pia_region_id); // 14
  cord_regions_list.push_back(Central_lower_pia_region_id); // 15
  cord_regions_list.push_back(Central_upper_pia_poro_bl_region_id); // 16 
  cord_regions_list.push_back(Central_lower_pia_poro_bl_region_id); // 17
  cord_regions_list.push_back(Lower_pia_region_id); // 18
  cord_regions_list.push_back(Filum_region_id); // 19
  cord_regions_list.push_back(Filum_pia_bl_region_id); // 20 
  cord_regions_list.push_back(Upper_cord_syrinx_poro_bl_region_id); // 21
  cord_regions_list.push_back(Lower_cord_syrinx_poro_bl_region_id); // 22
  ofstream cord_junk;
  cord_junk.open("cord_junk.dat");
  unsigned nregion=cord_regions_list.size();
  for (unsigned rr=0;rr<nregion;rr++)
   {
    unsigned r=cord_regions_list[rr];
    unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)->
     nregion_element(r);

    oomph_info << "region, nel: " << r << " " << nel << std::endl;
    if (nel==0)
     {
      oomph_info << "NO ELEMENTS IN " << rr << "-TH REGION (REGION " << r 
                 << ") SOMETHING'S WRONG!" << std::endl;
     }
    
    for (unsigned e=0;e<nel;e++)
     {
      region_area+=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)
       ->region_element_pt(r,e)->size();
      dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)
       ->region_element_pt(r,e)->output(cord_junk,2);
     }
   }
  cord_junk.close();

  oomph_info << "Inner poro mesh total/region area: "
             << mesh_area << " " 
             << region_area << " " 
             << "diff: " << fabs(mesh_area-region_area) << std::endl;
  if (fabs(mesh_area-region_area)>1.0e-12)
   {
    oomph_info << "No good! check region coordinates" << std::endl;
    abort();
   }
  else
   { 
    oomph_info << "All cord regions accounted for!" << std::endl;
   }
 }




 // Properly reorder to ensure correct sequence when restarting (spine
 // node update!)
 Inner_poro_mesh_pt->reorder_nodes();

 // Initialise Mesh as geom object representation of inner poro mesh
 Inner_poro_mesh_geom_obj_pt=0;
 
 // // Extract regularly spaced points
 // if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
 //  {
 //   double t_start = TimingHelpers::timer();
   
 //   // Speed up search (somewhat hand-tuned; adjust if this takes too long
 //   // or misses too many points)
 //   Inner_poro_mesh_geom_obj_pt->max_spiral_level()=4;
 //   unsigned nx_back= Multi_domain_functions::Nx_bin;
 //   Multi_domain_functions::Nx_bin=10;
 //   unsigned ny_back= Multi_domain_functions::Ny_bin;
 //   Multi_domain_functions::Ny_bin=10;
   
 //   // Populate it...
 //   Vector<double> x(2);
 //   Vector<double> s(2);
 //   for (unsigned ir=0;ir<Global_Physical_Variables::Nplot_r_regular;ir++)
 //    {
 //     // outermost radius of dura
 //     x[0]=double(ir)/double(Global_Physical_Variables::Nplot_r_regular-1)*0.55;
 //     for (unsigned iz=0;iz<Global_Physical_Variables::Nplot_z_regular;iz++)
 //      {
 //       x[1]=double(iz)/double(Global_Physical_Variables::Nplot_z_regular-1)*
 //        (-600.0)*Global_Physical_Variables::Z_scale;
       
 //       // Pointer to GeomObject that contains this point
 //       GeomObject* geom_obj_pt=0;
       
 //       // Get it
 //       Inner_poro_mesh_geom_obj_pt->locate_zeta(x,geom_obj_pt,s);
       
 //       // Store it
 //       if (geom_obj_pt!=0)
 //        {
 //         std::pair<PORO_ELEMENT*,Vector<double> > tmp;
 //         tmp = std::make_pair(dynamic_cast<PORO_ELEMENT*>(geom_obj_pt),s);
 //         Inner_poro_regularly_spaced_plot_point.push_back(tmp);
 //        }
 //      }
 //    }

 //   // Reset number of bins in binning method 
 //   Multi_domain_functions::Nx_bin=nx_back;
 //   Multi_domain_functions::Ny_bin=ny_back;
 //   oomph_info << "Took: " << TimingHelpers::timer()-t_start
 //              << " sec to setup regularly spaced points for inner poro mesh\n";
 //  }

 // Output mesh and boundaries
 Inner_poro_mesh_pt->output
  (Global_Physical_Variables::Directory+"/inner_poro_mesh.dat",2);
 Inner_poro_mesh_pt->output_boundaries
  (Global_Physical_Variables::Directory+"/inner_poro_mesh_boundaries.dat");
 

 //-----------------------------------------------------------------------------

 // Poro mesh (outer part)
 // ----------------------
 Vector<TriangleMeshCurveSection*> outer_poro_outer_polyline_boundary_pt(6);

 // Inlet
 {
  Vector<Vector<double> > outer_coords(2, Vector<double>(2));
  outer_coords[0][0]=10.0;
  outer_coords[0][1]=0.0;
  
  outer_coords[1][0]=11.0;
  outer_coords[1][1]=0.0;
  
  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }

  // Build
  outer_poro_outer_polyline_boundary_pt[Outer_poro_inlet_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,Outer_poro_inlet_boundary_id);
 }

 // Outer boundary
 {

  Vector<Vector<double> > outer_coords(3, Vector<double>(2));

  outer_coords[0][0]=11.0;
  outer_coords[0][1]=0.0;
  
  outer_coords[1][0]=8.0;
  outer_coords[1][1]=-500.0;

  outer_coords[2][0]=3.5;
  outer_coords[2][1]=-600.0;

  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }

  // Build
  outer_poro_outer_polyline_boundary_pt[Outer_poro_outer_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,Outer_poro_outer_boundary_id);
 }


 // Outlet
 {
  Vector<Vector<double> > outer_coords(2, Vector<double>(2));

  outer_coords[0][0]=3.5;
  outer_coords[0][1]=-600.0;

  outer_coords[1][0]=2.5;
  outer_coords[1][1]=-600.0;

  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  outer_poro_outer_polyline_boundary_pt[Outer_poro_outlet_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,Outer_poro_outlet_boundary_id);
 }

 // Inner/FSI near outlet
 {
  Vector<Vector<double> > outer_coords(3, Vector<double>(2));

  outer_coords[0][0]=2.5;
  outer_coords[0][1]=-600.0;
  
  outer_coords[1][0]=7.0;
  outer_coords[1][1]=-500.0;

  outer_coords[2][0]=9.01;
  outer_coords[2][1]=-165.0;
  
  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  outer_poro_outer_polyline_boundary_pt[
   Outer_poro_SSS_interface_near_outlet_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,
                            Outer_poro_SSS_interface_near_outlet_boundary_id);
 }



 // Block
 unsigned n_vertex_block=0;
 {
  Vector<Vector<double> > outer_coords(5, Vector<double>(2));

  outer_coords[0][0]=9.01;
  outer_coords[0][1]=-165.0;

  outer_coords[1][0]=5.82;
  outer_coords[1][1]=-160.0;

  double r_outer_radial_line_under_gap=
   5.905+(5.82-5.905)*(Z_radial_cut_through_syrinx-(-140.0))/
   ((-160.0)-(-140.0));

  outer_coords[2][0]=r_outer_radial_line_under_gap;
  outer_coords[2][1]=Z_radial_cut_through_syrinx;
  
  outer_coords[3][0]=5.905;
  outer_coords[3][1]=-140.0;
  
  outer_coords[4][0]=9.19;
  outer_coords[4][1]=-135.0;

  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  outer_poro_outer_polyline_boundary_pt[
   Outer_poro_SSS_interface_block_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,
                            Outer_poro_SSS_interface_block_boundary_id);

  // Remember for connection
  n_vertex_block=n;
 }


 // Inner/FSI near inlet
 {
  Vector<Vector<double> > outer_coords(3, Vector<double>(2));
  
  outer_coords[0][0]=9.19;
  outer_coords[0][1]=-135.0;
  
  // mjr extra vertex necessary because of a possible bug which means you cannot
  // connect an internal boundary line to the second last (!?) vertex of
  // the outer boundary!!! // hierher check
  outer_coords[1][0]=9.595;
  outer_coords[1][1]=-67.5;
  
  outer_coords[2][0]=10.0;
  outer_coords[2][1]=0.0;
  
  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  outer_poro_outer_polyline_boundary_pt[
   Outer_poro_SSS_interface_near_inlet_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,
                            Outer_poro_SSS_interface_near_inlet_boundary_id);
 }

 TriangleMeshClosedCurve * outer_poro_outer_boundary_pt =
  new TriangleMeshClosedCurve(outer_poro_outer_polyline_boundary_pt);

 // Inner boundary -- separation of block from rest
 Vector<TriangleMeshOpenCurve*> outer_inner_open_boundary_pt(1);

 // Boundary of block
 {
  Vector<Vector<double> > outer_coords(2, Vector<double>(2,0.0));
  
  outer_coords[0][0]=9.01;
  outer_coords[0][1]=-165.0;
  
  outer_coords[1][0]=9.19;
  outer_coords[1][1]=-135.0;
  
  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }

  // Build
  TriangleMeshPolyLine *outer_inner_polyline_pt =
   new TriangleMeshPolyLine(outer_coords,
                            Outer_poro_inner_block_boundary_id);
  
  // Connect initial vertex to second (starting from zeroth) vertex 
  // on fsi boundary near outlet
  outer_inner_polyline_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>
   (outer_poro_outer_polyline_boundary_pt
    [Outer_poro_SSS_interface_near_outlet_boundary_id]),2);
  
  // Connect final vertex to final vertex of
  // the block
  outer_inner_polyline_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>
   (outer_poro_outer_polyline_boundary_pt[
    Outer_poro_SSS_interface_block_boundary_id]),n_vertex_block-1);

  // Store in vector
  Vector<TriangleMeshCurveSection*> outer_inner_curve_section_pt(1);
  outer_inner_curve_section_pt[0] = outer_inner_polyline_pt;
  
  // Create internal open curve
  outer_inner_open_boundary_pt[0] =
   new TriangleMeshOpenCurve(outer_inner_curve_section_pt);  
 }
 
 // Mesh parameters
 TriangleMeshParameters
  outer_poro_mesh_parameters(outer_poro_outer_boundary_pt);
 outer_poro_mesh_parameters.element_area() =
  Global_Physical_Variables::Element_area_poro;
 
 // Define inner boundaries
 outer_poro_mesh_parameters.internal_open_curves_pt() =
  outer_inner_open_boundary_pt;


 // Define region coordinates for outer region (dura)
 region_coords[0] = Global_Physical_Variables::R_scale*10.0;
 region_coords[1] = Global_Physical_Variables::Z_scale*(-1.0);
 outer_poro_mesh_parameters.add_region_coordinates(Dura_region_id
                                                   ,region_coords);

 // Define region coordinates for block
 region_coords[0] = Global_Physical_Variables::R_scale*7.3;
 region_coords[1] = Global_Physical_Variables::Z_scale*(-160.0);
 outer_poro_mesh_parameters.add_region_coordinates(Block_region_id,
                                                   region_coords);

 // Build mesh
 Outer_poro_mesh_pt = new TriangleMesh<PORO_ELEMENT>(
   outer_poro_mesh_parameters, Poro_time_stepper_pt);

 // Properly reorder to ensure correct sequence when restarting (spine
 // node update!)
 Outer_poro_mesh_pt->reorder_nodes();

 // Output mesh and boundaries
 Outer_poro_mesh_pt->output
  (Global_Physical_Variables::Directory+"/outer_poro_mesh.dat",2);
 Outer_poro_mesh_pt->output_boundaries
  (Global_Physical_Variables::Directory+"/outer_poro_mesh_boundaries.dat");

 // Initialise Mesh as geom object representation of outer poro mesh
 Outer_poro_mesh_geom_obj_pt=0; 
 
 // ---------------------------------------------------------------------------
 
 // Syrinx fluid mesh
 // -----------------

 Vector<TriangleMeshCurveSection*> syrinx_fluid_outer_polyline_boundary_pt(4);

 Vector<Vector<double> > syrinx_boundary_vertex;

 // FSI boundary (upper)
 {
  unsigned npts=2;
  if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
   {
    npts=npts_on_elliptical_boundaries; // hierher work out from sqrt of element size or something
   }
  Vector<Vector<double> > syrinx_coords(npts, Vector<double>(2));
  for (unsigned j=0;j<npts;j++)
   {    
    syrinx_coords[j][0]=double(j)/double(npts-1)*
     Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
    
    syrinx_coords[j][1]=Global_Physical_Variables::Z_syrinx_top_centreline
     +double(j)/double(npts-1)*
     (Global_Physical_Variables::Z_syrinx_top_fsi-
      Global_Physical_Variables::Z_syrinx_top_centreline);
   }
  
  // syrinx_coords[0][0]=0.0;
  // syrinx_coords[0][1]=Global_Physical_Variables::Z_syrinx_top_centreline;
  
  // syrinx_coords[1][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
  // syrinx_coords[1][1]=Global_Physical_Variables::Z_syrinx_top_fsi;
  
  

  // Match syrinx discretisation
  if (Global_Physical_Variables::Match_syrinx_nst_and_poro_discretisation)
   {
    unsigned n=inner_poro_upper_syrinx_fsi_vertices_raw.size();
    syrinx_coords.resize(n);
    for (unsigned i=0;i<n;i++)
     {
      syrinx_coords[i]=inner_poro_upper_syrinx_fsi_vertices_raw[n-1-i];
      }
   }
  else
   {
    if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
     {
      bool upward=true;
      Global_Physical_Variables::ellipsify(syrinx_coords,upward);
     }
   }
  
  unsigned n=syrinx_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    syrinx_coords[j][0]*=Global_Physical_Variables::R_scale;
    syrinx_coords[j][1]*=Global_Physical_Variables::Z_scale;
    
    // Add to boundary
    syrinx_boundary_vertex.push_back(syrinx_coords[j]);
   }
  
  // Build
  syrinx_fluid_outer_polyline_boundary_pt
   [Syrinx_fluid_upper_inner_poro_interface_boundary_id] =
   new TriangleMeshPolyLine(syrinx_coords,
                            Syrinx_fluid_upper_inner_poro_interface_boundary_id);
 }


 // y coordinate of line along which we extract fluid data in syrinx
 double z_syrinx_extract_line=Z_radial_cut_through_syrinx;
 double r_syrinx_extract_line=0.0;
 unsigned index_of_connection_on_central_syrinx_fsi_boundary=1;

 // FSI boundary (central)
 {
  Vector<Vector<double> > syrinx_coords(3, Vector<double>(2));
  syrinx_coords[0][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
  syrinx_coords[0][1]=Global_Physical_Variables::Z_syrinx_top_fsi;
  
  syrinx_coords[2][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
  syrinx_coords[2][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
  
  // Mid point (endpoint for coordinate extraction) linearly interpolated
  r_syrinx_extract_line=syrinx_coords[0][0]+
   (syrinx_coords[2][0]-syrinx_coords[0][0])*
   (z_syrinx_extract_line-syrinx_coords[0][1])/
   (syrinx_coords[2][1]-syrinx_coords[0][1]);
  
  syrinx_coords[1][0]=r_syrinx_extract_line;
  syrinx_coords[1][1]=z_syrinx_extract_line;


  // Match syrinx discretisation
  if (Global_Physical_Variables::Match_syrinx_nst_and_poro_discretisation)
   {
    bool found=false;
    unsigned n=inner_poro_central_syrinx_fsi_vertices_raw.size();
    syrinx_coords.resize(n);
    for (unsigned i=0;i<n;i++)
     {
      syrinx_coords[i]=inner_poro_central_syrinx_fsi_vertices_raw[n-1-i];
      if ((std::fabs(syrinx_coords[i][0]-r_syrinx_extract_line)+
           std::fabs(syrinx_coords[i][1]-z_syrinx_extract_line))<1.0e-10)
       {
        found=true;
        index_of_connection_on_central_syrinx_fsi_boundary=i;
       }
     }
    if (!found)
     {
      std::ostringstream error_stream;
      error_stream
       << "Didn't find matching point for line through syrinx.\n"
       << "r_syrinx_extract_line z_syrinx_extract_line "
       << r_syrinx_extract_line << " " << z_syrinx_extract_line
       << "\nVERTICES: " << std::endl;
      for (unsigned i=0;i<n;i++)
       {
        error_stream << syrinx_coords[i][0] << " " 
                     << syrinx_coords[i][1] << "\n"; 
       }
      throw OomphLibError(
       error_stream.str(),
       OOMPH_CURRENT_FUNCTION,
       OOMPH_EXCEPTION_LOCATION);
     }
   }

  // Rescale
  unsigned n=syrinx_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    syrinx_coords[j][0]*=Global_Physical_Variables::R_scale;
    syrinx_coords[j][1]*=Global_Physical_Variables::Z_scale;

    // Add to boundary
    syrinx_boundary_vertex.push_back(syrinx_coords[j]);
   }
  
  // Build
  syrinx_fluid_outer_polyline_boundary_pt
   [Syrinx_fluid_central_inner_poro_interface_boundary_id] =
   new TriangleMeshPolyLine(syrinx_coords,
                            Syrinx_fluid_central_inner_poro_interface_boundary_id);
 }


 // FSI boundary (lower)
 {
  unsigned npts=2;
  if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
   {
    npts=npts_on_elliptical_boundaries; // hierher work out from sqrt of element size or something
   }
  Vector<Vector<double> > syrinx_coords(npts, Vector<double>(2));
  Vector<Vector<double> > aux_syrinx_coords(npts, Vector<double>(2));
  for (unsigned j=0;j<npts;j++)
   {    
    aux_syrinx_coords[j][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi*
     (1.0-double(j)/double(npts-1));
    
    aux_syrinx_coords[j][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi
     +double(j)/double(npts-1)*
     (Global_Physical_Variables::Z_syrinx_bottom_centreline-
      Global_Physical_Variables::Z_syrinx_bottom_fsi);
   }


  // syrinx_coords[0][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
  // syrinx_coords[0][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
  
  // syrinx_coords[1][0]=0.0;
  // syrinx_coords[1][1]=Global_Physical_Variables::Z_syrinx_bottom_centreline;
  

  // Match syrinx discretisation
  // hierher
  if (Global_Physical_Variables::Match_syrinx_nst_and_poro_discretisation)
   {
    unsigned n=inner_poro_lower_syrinx_fsi_vertices_raw.size();
    oomph_info << "hierher new n: " << n << std::endl;
    aux_syrinx_coords.resize(n);
    syrinx_coords.resize(n);
    for (unsigned i=0;i<n;i++)
     {
      aux_syrinx_coords[i]=inner_poro_lower_syrinx_fsi_vertices_raw[n-1-i];
     }
   }
  else
   {
    if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
     {
      bool upward=false;
      Global_Physical_Variables::ellipsify(aux_syrinx_coords,upward);
     }
   }
    

  // Rescale
  unsigned n=aux_syrinx_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    if (Global_Physical_Variables::Match_syrinx_nst_and_poro_discretisation)
     {
      syrinx_coords[j].resize(2);
      syrinx_coords[j][0]=aux_syrinx_coords[j][0]*Global_Physical_Variables::R_scale;
      syrinx_coords[j][1]=aux_syrinx_coords[j][1]*Global_Physical_Variables::Z_scale;
     }
    else
     {
      syrinx_coords[j][0]=aux_syrinx_coords[n-1-j][0]*Global_Physical_Variables::R_scale;
      syrinx_coords[j][1]=aux_syrinx_coords[n-1-j][1]*Global_Physical_Variables::Z_scale;
     }

    // Add to boundary
    syrinx_boundary_vertex.push_back(syrinx_coords[j]);
   }
  
  
  // Build
  syrinx_fluid_outer_polyline_boundary_pt
   [Syrinx_fluid_lower_inner_poro_interface_boundary_id] =
   new TriangleMeshPolyLine(syrinx_coords,
                            Syrinx_fluid_lower_inner_poro_interface_boundary_id);
 }



 double z_bl_intersect_lo=0.0;
 double z_bl_intersect_hi=0.0;

 // Symm boundary
 {
  Vector<Vector<double> > syrinx_coords(5, Vector<double>(2));

  Vector<Vector<double> > line1(2,Vector<double>(2));
  Vector<Vector<double> > line2(2,Vector<double>(2));

  syrinx_coords[0][0]=0.0;
  syrinx_coords[0][1]=Global_Physical_Variables::Z_syrinx_bottom_centreline;

  // Symm line (offset; BL to right!)
  line1[0][0]=-Global_Physical_Variables::BL_thick;
  line1[0][1]=-100.0;
  line1[1][0]=-Global_Physical_Variables::BL_thick;
  line1[1][1]=0.0;

  // Downstream face of syrinx (BL to right)
  line2[0][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
  line2[0][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
  line2[1][0]=0.0;
  line2[1][1]=Global_Physical_Variables::Z_syrinx_bottom_centreline;

  // Get intersection
  Vector<double> bl_intersect=
   Global_Physical_Variables::bl_intersection(
    line1,line2,Global_Physical_Variables::BL_thick);

  syrinx_coords[1][0]=0.0;
  syrinx_coords[1][1]=bl_intersect[1];
  z_bl_intersect_lo=bl_intersect[1];

  // Halfway point
  syrinx_coords[2][0]=0.0;
  syrinx_coords[2][1]=z_syrinx_extract_line;

  // Symm line (offset; BL to right!)
  line1[0][0]=-Global_Physical_Variables::BL_thick;
  line1[0][1]=-100.0;
  line1[1][0]=-Global_Physical_Variables::BL_thick;
  line1[1][1]=0.0;

  // Upstream face of syrinx (BL to right)
  line2[0][0]=0.0;
  line2[0][1]=Global_Physical_Variables::Z_syrinx_top_centreline;
  line2[1][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
  line2[1][1]=Global_Physical_Variables::Z_syrinx_top_fsi;

  // Get intersection
  bl_intersect=
   Global_Physical_Variables::bl_intersection(
    line1,line2,Global_Physical_Variables::BL_thick);

  syrinx_coords[3][0]=0.0;
  syrinx_coords[3][1]=bl_intersect[1];
  z_bl_intersect_hi=bl_intersect[1];

  syrinx_coords[4][0]=0.0;
  syrinx_coords[4][1]=Global_Physical_Variables::Z_syrinx_top_centreline;
  
  unsigned n=syrinx_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    syrinx_coords[j][0]*=Global_Physical_Variables::R_scale;
    syrinx_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  syrinx_fluid_outer_polyline_boundary_pt
   [Syrinx_fluid_symmetry_boundary_id] =
   new TriangleMeshPolyLine(syrinx_coords,
                            Syrinx_fluid_symmetry_boundary_id);
 }

 TriangleMeshClosedCurve * syrinx_fluid_outer_boundary_pt =
  new TriangleMeshClosedCurve(syrinx_fluid_outer_polyline_boundary_pt);


 // Two internal boundaries: One for BL; one for velocity extraction
 Vector<TriangleMeshOpenCurve*> inner_open_boundary_pt(2);

 // BL:
 TriangleMeshPolyLine* inner_polyline_pt = 0;
 Vector<Vector<double> > aux_inner_coords(4, Vector<double>(2,0.0));

 unsigned npt=npts_on_elliptical_boundaries; // hierher
 Vector<Vector<double> > elliptical_inner_coords(npt, Vector<double>(2,0.0));

 // hierher becomes 2*npt
 Vector<Vector<double> > inner_coords(2*npt, Vector<double>(2,0.0));

  Vector<Vector<double> > line1(2,Vector<double>(2));
  Vector<Vector<double> > line2(2,Vector<double>(2));

 aux_inner_coords[0][0]=0.0;
 aux_inner_coords[0][1]=z_bl_intersect_lo;
 
 // Inner surface of syrinx (BL to right!)
 line1[0][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
 line1[0][1]=Global_Physical_Variables::Z_syrinx_top_fsi;
 line1[1][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
 line1[1][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
 
 // Downstream face of syrinx (BL to right)
 line2[0][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
 line2[0][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
 line2[1][0]=0.0;
 line2[1][1]=Global_Physical_Variables::Z_syrinx_bottom_centreline;
 
 // Get intersection
 Vector<double> bl_intersect=
  Global_Physical_Variables::bl_intersection(
   line1,line2,Global_Physical_Variables::BL_thick);
 
 aux_inner_coords[1][0]=bl_intersect[0];
 aux_inner_coords[1][1]=bl_intersect[1];
 
 // Create elliptical representation: First line em' up in a straight
 // line:
 for (unsigned j=0;j<npt;j++)
  {
   for (unsigned i=0;i<2;i++)
    {
     elliptical_inner_coords[j][i]=aux_inner_coords[0][i]+
      double(j)/double(npt-1)*
      (aux_inner_coords[1][i]-aux_inner_coords[0][i]);
    }
  }
 if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
  {
   bool upward=false;
   Global_Physical_Variables::ellipsify(elliptical_inner_coords,upward);
  }
 

 // Copy into first part of actual collection of vertices
 for (unsigned j=0;j<npt;j++)
  {
   for (unsigned i=0;i<2;i++)
    {
     inner_coords[j][i]=elliptical_inner_coords[j][i];
    }
  }

 // Inner surface of syrinx (BL to right!)
 line1[0][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
 line1[0][1]=Global_Physical_Variables::Z_syrinx_top_fsi;
 line1[1][0]=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi;
 line1[1][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;
 
 // Upstream face of syrinx (BL to right)
 line2[0][0]=0.0;
 line2[0][1]=Global_Physical_Variables::Z_syrinx_top_centreline;
 line2[1][0]=Global_Physical_Variables::R_syrinx_top_syrinx_fsi;
 line2[1][1]=Global_Physical_Variables::Z_syrinx_top_fsi;
 
 // Get intersection
 bl_intersect=
  Global_Physical_Variables::bl_intersection(
   line1,line2,Global_Physical_Variables::BL_thick);
 
 aux_inner_coords[2][0]=bl_intersect[0];
 aux_inner_coords[2][1]=bl_intersect[1];
 
 aux_inner_coords[3][0]=0.0;
 aux_inner_coords[3][1]=z_bl_intersect_hi;
 

 // Create elliptical representation: First line em' up in a straight
 // line:
 for (unsigned j=0;j<npt;j++)
  {
   for (unsigned i=0;i<2;i++)
    {
     elliptical_inner_coords[j][i]=aux_inner_coords[3][i]+
      double(j)/double(npt-1)*
      (aux_inner_coords[2][i]-aux_inner_coords[3][i]);
    }
  }
 if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
  {
   bool upward=true;
   Global_Physical_Variables::ellipsify(elliptical_inner_coords,upward);
  }
 

 // Copy into first part of actual collection of vertices
 for (unsigned j=0;j<npt;j++)
  {
   for (unsigned i=0;i<2;i++)
    {
     inner_coords[npt+j][i]=elliptical_inner_coords[npt-1-j][i];
    }
  }


 // // hierher this becomes superseded  by the useless do loop below
 // inner_coords[npt][0]=aux_inner_coords[2][0];
 // inner_coords[npt][1]=aux_inner_coords[2][1];

 // inner_coords[npt+1][0]=aux_inner_coords[3][0];
 // inner_coords[npt+1][1]=aux_inner_coords[3][1];


 // // Copy into first part of actual collection of vertices
 // for (unsigned j=0;j<npt;j++)
 //  {
 //   for (unsigned i=0;i<2;i++)
 //    {
 //     // hierher inner_coords[j][i]=elliptical_inner_coords[j][i];
 //    }
 //  }


 // Rescale
 n=inner_coords.size();
 oomph_info << "total number of vertices on syrinx bl: " << n << std::endl;
 for(unsigned i=0;i<n;i++)
  {
   inner_coords[i][0]*=Global_Physical_Variables::R_scale;
   inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
  }
 
 // Build
 inner_polyline_pt =
  new TriangleMeshPolyLine(inner_coords,
                           Syrinx_bl_boundary_id);
 
 // Connect initial vertex to first (from zeroth) vertex on sym boundary
 inner_polyline_pt->connect_initial_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (syrinx_fluid_outer_polyline_boundary_pt[Syrinx_fluid_symmetry_boundary_id]),
  1);
 
 // Connect final vertex to third (from zeroth) vertex on outlet boundary
 inner_polyline_pt->connect_final_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (syrinx_fluid_outer_polyline_boundary_pt[Syrinx_fluid_symmetry_boundary_id]),
  3);

 
 // Store in vector
 Vector<TriangleMeshCurveSection*> inner_curve_section_pt(1);
 inner_curve_section_pt[0] = inner_polyline_pt;
 
 // Create internal open curve
 inner_open_boundary_pt[0] =
  new TriangleMeshOpenCurve(inner_curve_section_pt);


 // Veloc extraction line
 TriangleMeshPolyLine* veloc_extract_polyline_pt = 0;
 Vector<Vector<double> > veloc_extract_coords(2, Vector<double>(2,0.0));

 veloc_extract_coords[0][0]=0.0;
 veloc_extract_coords[0][1]=z_syrinx_extract_line;
  
 veloc_extract_coords[1][0]=r_syrinx_extract_line;
 veloc_extract_coords[1][1]=z_syrinx_extract_line;
  
 // Rescale
 for(unsigned i=0;i<2;i++)
  {
   veloc_extract_coords[i][0]*=Global_Physical_Variables::R_scale;
   veloc_extract_coords[i][1]*=Global_Physical_Variables::Z_scale;
  }
 
 // Build
 veloc_extract_polyline_pt =
  new TriangleMeshPolyLine(veloc_extract_coords,
                           Syrinx_veloc_extract_boundary_id);
 
 // Connect initial vertex to second (from zeroth) vertex on sym boundary
 veloc_extract_polyline_pt->connect_initial_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (syrinx_fluid_outer_polyline_boundary_pt[Syrinx_fluid_symmetry_boundary_id]),
  2);
 
 // Connect final vertex to approriate vertex on central fsi
 // boundary
 veloc_extract_polyline_pt->connect_final_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (syrinx_fluid_outer_polyline_boundary_pt[Syrinx_fluid_central_inner_poro_interface_boundary_id]),index_of_connection_on_central_syrinx_fsi_boundary);
 
 // Store in vector
 Vector<TriangleMeshCurveSection*> veloc_extract_curve_section_pt(1);
 veloc_extract_curve_section_pt[0] = veloc_extract_polyline_pt;
 
 // Create internal open curve
 inner_open_boundary_pt[1]= 
  new TriangleMeshOpenCurve(veloc_extract_curve_section_pt);


 // Mesh parameters
 TriangleMeshParameters
  syrinx_fluid_mesh_parameters(syrinx_fluid_outer_boundary_pt);

 // Generic elements size
 syrinx_fluid_mesh_parameters.element_area() =
  Global_Physical_Variables::Element_area_fluid;
 
 // Specify different target area for syrinx?
 if (CommandLineArgs::command_line_flag_has_been_set("--el_area_syrinx"))
  {
   oomph_info << "Setting area in syrinx to " 
              << Global_Physical_Variables::Element_area_syrinx
              << std::endl;
   syrinx_fluid_mesh_parameters.element_area() =
    Global_Physical_Variables::Element_area_syrinx;
  }

 // Define inner boundaries
 syrinx_fluid_mesh_parameters.internal_open_curves_pt()=inner_open_boundary_pt;
 
 // Build mesh
 Syrinx_fluid_mesh_pt = new TriangleMesh<FLUID_ELEMENT>(
   syrinx_fluid_mesh_parameters, Fluid_time_stepper_pt);

 // Properly reorder to ensure correct sequence when restarting  (spine
 // node update!)
 Syrinx_fluid_mesh_pt->reorder_nodes();
 

 if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
  {
   /// Move actual nodes (incl midside ones!) onto proper ellipse 
   bool upward=true;
   unsigned b=Syrinx_fluid_upper_inner_poro_interface_boundary_id;
   Global_Physical_Variables::ellipsify_boundary_nodes(
    Syrinx_fluid_mesh_pt,b,upward);
   
   upward=false;
   b=Syrinx_fluid_lower_inner_poro_interface_boundary_id;
   Global_Physical_Variables::ellipsify_boundary_nodes(
    Syrinx_fluid_mesh_pt,b,upward);
  }



 // Output mesh and boundaries
 Syrinx_fluid_mesh_pt->output
  (Global_Physical_Variables::Directory+"/syrinx_fluid_mesh.dat",2);
 Syrinx_fluid_mesh_pt->output_boundaries
  (Global_Physical_Variables::Directory+"/syrinx_fluid_mesh_boundaries.dat");

 // Initialise Mesh as geom object representation of syrinx fluid mesh
 Syrinx_fluid_mesh_geom_obj_pt=0;


 // Compute syrinx volume
 Analytical_syrinx_volume=
  Global_Physical_Variables::volume(syrinx_boundary_vertex);

 if (!Global_Physical_Variables::Use_straight_syrinx_boundaries) 
  {   
   Analytical_syrinx_volume=0.0;

   // Top ellipsoid
   double z_top=-DBL_MAX;
   double r_top=0.0;
   double z_straight_top=DBL_MAX;
   double r_straight_top=0.0;
   unsigned n=Syrinx_fluid_mesh_pt->nboundary_node(
    Syrinx_fluid_upper_inner_poro_interface_boundary_id);
   for (unsigned j=0;j<n;j++)
    {
     Node* nod_pt=
      Syrinx_fluid_mesh_pt->boundary_node_pt(
       Syrinx_fluid_upper_inner_poro_interface_boundary_id,j);
     
     double r=nod_pt->x(0);
     double z=nod_pt->x(1);
     
     if (z>z_top)
      {
       z_top=z;
      }
     if (z<z_straight_top)
      {
       z_straight_top=z;
       r_straight_top=r;
      }
    }

   oomph_info << "Bounding coordinates for top ellipsoid: "
              << z_top << " " << r_top << "\n"
              << z_straight_top << " " << r_straight_top << "\n";
   
   double a=abs(z_top-z_straight_top);
   double b=abs(r_top-r_straight_top);

   double vol=2.0/3.0*MathematicalConstants::Pi*a*b*b;
   oomph_info << "Volume of top ellipsoid: " << vol << std::endl;

   Analytical_syrinx_volume+=vol;



   // Bottom ellipsoid
   double z_bottom=DBL_MAX;
   double r_bottom=0.0;
   double z_straight_bottom=-DBL_MAX;
   double r_straight_bottom=0.0;
   n=Syrinx_fluid_mesh_pt->nboundary_node(
    Syrinx_fluid_lower_inner_poro_interface_boundary_id);
   for (unsigned j=0;j<n;j++)
    {
     Node* nod_pt=
      Syrinx_fluid_mesh_pt->boundary_node_pt(
       Syrinx_fluid_lower_inner_poro_interface_boundary_id,j);
     
     double r=nod_pt->x(0);
     double z=nod_pt->x(1);
     
     if (z<z_bottom)
      {
       z_bottom=z;
      }
     if (z>z_straight_bottom)
      {
       z_straight_bottom=z;
       r_straight_bottom=r;
      }
    }

   oomph_info << "Bounding coordinates for bottom ellipsoid: "
              << z_bottom << " " << r_bottom << "\n"
              << z_straight_bottom << " " << r_straight_bottom << "\n";
   
   a=abs(z_bottom-z_straight_bottom);
   b=abs(r_bottom-r_straight_bottom);
   
   vol=2.0/3.0*MathematicalConstants::Pi*a*b*b;
   oomph_info << "Volume of bottom ellipsoid: " << vol << std::endl;

   Analytical_syrinx_volume+=vol;


   // Truncated cone bit in the middle
   double r1=r_straight_bottom;
   double r2=r_straight_top;
   double h=abs(z_straight_bottom-z_straight_top);
   vol=MathematicalConstants::Pi/3.0*(r1*r1+r1*r2+r2*r2)*h;
   oomph_info << "Volume of truncated cone bit: " << vol << std::endl;

   Analytical_syrinx_volume+=vol;
   
  }

 // ----------------------------------------------------------------------------

 // SSS fluid mesh
 // --------------
 Vector<TriangleMeshCurveSection*> sss_fluid_outer_polyline_boundary_pt(8);

 Vector<Vector<double> > upstream_outer_sss_boundary_vertex;
 Vector<Vector<double> > upstream_inner_sss_boundary_vertex;
 Vector<Vector<double> > downstream_outer_sss_boundary_vertex;
 Vector<Vector<double> > downstream_inner_sss_boundary_vertex;


 double z_upstream_fine_region_boundary=
  Global_Physical_Variables::Z_upstream_fine_sss_region; // -105.5;
 double z_downstream_fine_region_boundary=
  Global_Physical_Variables::Z_downstream_fine_sss_region; //-195.0;
 
 double r_inner_radial_line_under_gap=0.0;
 double r_outer_radial_line_under_gap=0.0;
 unsigned sss_radial_line_across_gap_outer_vertex_id=0;
 
 // Block
 {
  Vector<Vector<double> > sss_coords;
  Vector<double> vert(2);
  vert[0]=9.19;
  vert[1]=-135.0;
  sss_coords.push_back(vert);

  // Create equally spaced vertices, separated roughly by the 
  // the thickness of the mesh boundary layer
  Vector<double> start(2);
  start[0]=5.905;
  start[1]=-140.0;

  r_outer_radial_line_under_gap=
   5.905+(5.82-5.905)*(Z_radial_cut_through_syrinx-(-140.0))/
   ((-160.0)-(-140.0));

  Vector<double> end(2);
  end[0]=r_outer_radial_line_under_gap;
  end[1]=Z_radial_cut_through_syrinx;

  
  Global_Physical_Variables::push_back_vertices(
   start,end,Global_Physical_Variables::BL_thick*
   Global_Physical_Variables::Z_shrink_factor,sss_coords);

  sss_radial_line_across_gap_outer_vertex_id=sss_coords.size()-1;

  start[0]=end[0];
  start[1]=end[1];

  end[0]=5.82;
  end[1]=-160.0;

  Global_Physical_Variables::push_back_vertices(
   end,Global_Physical_Variables::BL_thick*
   Global_Physical_Variables::Z_shrink_factor,sss_coords);

  vert[0]=9.01;
  vert[1]=-165.0;
  sss_coords.push_back(vert);

  // Rescale
  unsigned n=sss_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    sss_coords[j][0]*=Global_Physical_Variables::R_scale;
    sss_coords[j][1]*=Global_Physical_Variables::Z_scale;

    if (sss_coords[j][1]<=Z_radial_cut_through_syrinx*
        Global_Physical_Variables::Z_scale)
     {
      downstream_outer_sss_boundary_vertex.push_back(sss_coords[j]);
     }
    if (sss_coords[j][1]>=Z_radial_cut_through_syrinx*
        Global_Physical_Variables::Z_scale)
     {
      upstream_outer_sss_boundary_vertex.push_back(sss_coords[j]);
     }
   }
  
  // Build
  sss_fluid_outer_polyline_boundary_pt[
   SSS_fluid_outer_poro_interface_block_boundary_id]=
   new TriangleMeshPolyLine(sss_coords,
                            SSS_fluid_outer_poro_interface_block_boundary_id);
 }

 Vector<double> sss_downstream_outer_fine_region_boundary(2);
 unsigned sss_downstream_outer_fine_region_boundary_vertex_id=0;

 // Outer FSI interface near outlet
 {
  Vector<Vector<double> > sss_coords(4, Vector<double>(2));
  
  sss_coords[0][0]=9.01;
  sss_coords[0][1]=-165.0;

  sss_downstream_outer_fine_region_boundary[0]=
  9.01+(7.0-9.01)/((-500.0)-(-165.0))*
   (z_downstream_fine_region_boundary-(-165.0));
  sss_downstream_outer_fine_region_boundary[1]=
   z_downstream_fine_region_boundary; 
  sss_downstream_outer_fine_region_boundary_vertex_id=1;

  sss_coords[sss_downstream_outer_fine_region_boundary_vertex_id][0]=
   sss_downstream_outer_fine_region_boundary[0];
  sss_coords[sss_downstream_outer_fine_region_boundary_vertex_id][1]=
   sss_downstream_outer_fine_region_boundary[1];

  sss_coords[2][0]=7.0;
  sss_coords[2][1]=-500.0;
  
  sss_coords[3][0]=2.5;
  sss_coords[3][1]=-600.0;

  // Rescale
  unsigned n=sss_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    sss_coords[j][0]*=Global_Physical_Variables::R_scale;
    sss_coords[j][1]*=Global_Physical_Variables::Z_scale;
    downstream_outer_sss_boundary_vertex.push_back(sss_coords[j]);
   }
  
  // Build
  sss_fluid_outer_polyline_boundary_pt[
   SSS_fluid_outer_poro_interface_near_outlet_boundary_id]=
   new TriangleMeshPolyLine(
    sss_coords,
    SSS_fluid_outer_poro_interface_near_outlet_boundary_id);
 }
 

 // Outlet
 {
  Vector<Vector<double> > sss_coords(4, Vector<double>(2));

  sss_coords[0][0]=2.5;
  sss_coords[0][1]=-600.0;

  sss_coords[1][0]=2.5-Global_Physical_Variables::BL_thick;
  sss_coords[1][1]=-600.0;

  sss_coords[2][0]=1.25+Global_Physical_Variables::BL_thick;
  sss_coords[2][1]=-600.0;

  sss_coords[3][0]=1.25;
  sss_coords[3][1]=-600.0;

  // Rescale
  unsigned n=sss_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    sss_coords[j][0]*=Global_Physical_Variables::R_scale;
    sss_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  sss_fluid_outer_polyline_boundary_pt[
   SSS_fluid_outflow_boundary_id]=
   new TriangleMeshPolyLine(sss_coords,SSS_fluid_outflow_boundary_id);
 }
 
 // Inner FSI (lower)
 {
  Vector<Vector<double> > sss_coords;
  Vector<double> vert(2);
  vert[0]=1.25;
  vert[1]=-600.0;
  sss_coords.push_back(vert);

  vert[0]=1.25;
  vert[1]=-480.0;
  sss_coords.push_back(vert);

  Vector<double> start(2);
  start[0]=4.24;
  start[1]=-440.0;
  
  Vector<double> end(2);
  end[0]=Global_Physical_Variables::R_syrinx_bottom_sss_fsi;
  end[1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;

  Global_Physical_Variables::push_back_vertices(
   start,end,Global_Physical_Variables::BL_thick*
   Global_Physical_Variables::Z_shrink_factor,sss_coords);

  // Rescale
  unsigned n=sss_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    sss_coords[j][0]*=Global_Physical_Variables::R_scale;
    sss_coords[j][1]*=Global_Physical_Variables::Z_scale;
    downstream_inner_sss_boundary_vertex.push_back(sss_coords[j]);
   }
  
  // Build
  sss_fluid_outer_polyline_boundary_pt[
   SSS_fluid_lower_inner_poro_interface_boundary_id]=
   new TriangleMeshPolyLine(sss_coords,
                            SSS_fluid_lower_inner_poro_interface_boundary_id);
 }



 Vector<double> sss_upstream_inner_fine_region_boundary(2);
 unsigned sss_upstream_inner_fine_region_boundary_vertex_id=0;
 Vector<double> sss_downstream_inner_fine_region_boundary(2);
 unsigned sss_downstream_inner_fine_region_boundary_vertex_id=0;
 Vector<double> sss_radial_line_across_gap_inner_vertex(2);
 unsigned sss_radial_line_across_gap_inner_vertex_id=0;


 // Inner FSI (central; syrinx cover)
 {
  Vector<Vector<double> > sss_coords(5, Vector<double>(2));

  sss_coords[0][0]=Global_Physical_Variables::R_syrinx_bottom_sss_fsi;
  sss_coords[0][1]=Global_Physical_Variables::Z_syrinx_bottom_fsi;


  sss_downstream_inner_fine_region_boundary[0]=
   Global_Physical_Variables::R_syrinx_bottom_sss_fsi+(Global_Physical_Variables::R_syrinx_top_sss_fsi-Global_Physical_Variables::R_syrinx_bottom_sss_fsi)/((Global_Physical_Variables::Z_syrinx_top_fsi)-(Global_Physical_Variables::Z_syrinx_bottom_fsi))*
   (z_downstream_fine_region_boundary-(Global_Physical_Variables::Z_syrinx_bottom_fsi));
  sss_downstream_inner_fine_region_boundary[1]=
   z_downstream_fine_region_boundary;
  sss_downstream_inner_fine_region_boundary_vertex_id=1;

  sss_coords[sss_downstream_inner_fine_region_boundary_vertex_id][0]=
   sss_downstream_inner_fine_region_boundary[0];
  sss_coords[sss_downstream_inner_fine_region_boundary_vertex_id][1]=
   sss_downstream_inner_fine_region_boundary[1];


  r_inner_radial_line_under_gap=Global_Physical_Variables::R_syrinx_bottom_sss_fsi+(Global_Physical_Variables::R_syrinx_top_sss_fsi-Global_Physical_Variables::R_syrinx_bottom_sss_fsi)/((Global_Physical_Variables::Z_syrinx_top_fsi)-(Global_Physical_Variables::Z_syrinx_bottom_fsi))*
   (Z_radial_cut_through_syrinx-(Global_Physical_Variables::Z_syrinx_bottom_fsi));
  sss_radial_line_across_gap_inner_vertex[0]=r_inner_radial_line_under_gap;
  sss_radial_line_across_gap_inner_vertex[1]=Z_radial_cut_through_syrinx;
  sss_radial_line_across_gap_inner_vertex_id=2;

  sss_coords[sss_radial_line_across_gap_inner_vertex_id][0]=
   sss_radial_line_across_gap_inner_vertex[0];
  sss_coords[sss_radial_line_across_gap_inner_vertex_id][1]=
   sss_radial_line_across_gap_inner_vertex[1];



  sss_upstream_inner_fine_region_boundary[0]=
   Global_Physical_Variables::R_syrinx_bottom_sss_fsi+(Global_Physical_Variables::R_syrinx_top_sss_fsi-Global_Physical_Variables::R_syrinx_bottom_sss_fsi)/((Global_Physical_Variables::Z_syrinx_top_fsi)-(Global_Physical_Variables::Z_syrinx_bottom_fsi))*
   (z_upstream_fine_region_boundary-(Global_Physical_Variables::Z_syrinx_bottom_fsi));
  sss_upstream_inner_fine_region_boundary[1]=
   z_upstream_fine_region_boundary;
  sss_upstream_inner_fine_region_boundary_vertex_id=3;

  sss_coords[sss_upstream_inner_fine_region_boundary_vertex_id][0]=
   sss_upstream_inner_fine_region_boundary[0];
  sss_coords[sss_upstream_inner_fine_region_boundary_vertex_id][1]=
   sss_upstream_inner_fine_region_boundary[1];
  
  sss_coords[4][0]=Global_Physical_Variables::R_syrinx_top_sss_fsi;
  sss_coords[4][1]=Global_Physical_Variables::Z_syrinx_top_fsi;

  // Rescale
  unsigned n=sss_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    sss_coords[j][0]*=Global_Physical_Variables::R_scale;
    sss_coords[j][1]*=Global_Physical_Variables::Z_scale;

    if (sss_coords[j][1]<=Z_radial_cut_through_syrinx*
        Global_Physical_Variables::Z_scale)
     {
      downstream_inner_sss_boundary_vertex.push_back(sss_coords[j]);
     }
    if (sss_coords[j][1]>=Z_radial_cut_through_syrinx*
        Global_Physical_Variables::Z_scale)
     {
      upstream_inner_sss_boundary_vertex.push_back(sss_coords[j]);
     }

   }
  
  // Build
  sss_fluid_outer_polyline_boundary_pt[
   SSS_fluid_central_inner_poro_interface_boundary_id]=
   new TriangleMeshPolyLine(sss_coords,
                            SSS_fluid_central_inner_poro_interface_boundary_id);
 }


 // Inner FSI (upper)
 {
  Vector<Vector<double> > sss_coords;

  Vector<double> start(2);
  start[0]=Global_Physical_Variables::R_syrinx_top_sss_fsi;
  start[1]=Global_Physical_Variables::Z_syrinx_top_fsi;
  
  Vector<double> end(2);
  end[0]=6.0;
  end[1]=0.0;

  Global_Physical_Variables::push_back_vertices(
   start,end,Global_Physical_Variables::BL_thick*
   Global_Physical_Variables::Z_shrink_factor,sss_coords);

  // Rescale
  unsigned n=sss_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    sss_coords[j][0]*=Global_Physical_Variables::R_scale;
    sss_coords[j][1]*=Global_Physical_Variables::Z_scale;

    upstream_inner_sss_boundary_vertex.push_back(sss_coords[j]);
   }
  
  // Build
  sss_fluid_outer_polyline_boundary_pt[
   SSS_fluid_upper_inner_poro_interface_boundary_id]=
   new TriangleMeshPolyLine(sss_coords,
                            SSS_fluid_upper_inner_poro_interface_boundary_id);
 }
 

 // Inlet
 {
  Vector<Vector<double> > sss_coords(4, Vector<double>(2));

  sss_coords[0][0]=6.0;
  sss_coords[0][1]=0.0;

  sss_coords[1][0]=6.0+Global_Physical_Variables::BL_thick;
  sss_coords[1][1]=0.0;

  sss_coords[2][0]=10.0-Global_Physical_Variables::BL_thick;
  sss_coords[2][1]=0.0;
  
  sss_coords[3][0]=10.0;
  sss_coords[3][1]=0.0;

  // Rescale
  unsigned n=sss_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    sss_coords[j][0]*=Global_Physical_Variables::R_scale;
    sss_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  sss_fluid_outer_polyline_boundary_pt[
   SSS_fluid_inflow_boundary_id]=
   new TriangleMeshPolyLine(sss_coords,
                            SSS_fluid_inflow_boundary_id);
 }



 Vector<double> sss_upstream_outer_fine_region_boundary(2);
 unsigned sss_upstream_outer_fine_region_boundary_vertex_id=0;

 // Outer FSI interface near inlet
 {
  Vector<Vector<double> > sss_coords(3, Vector<double>(2));

  sss_coords[0][0]=10.0;
  sss_coords[0][1]=0.0;

  sss_upstream_outer_fine_region_boundary[0]=
   10.0+(9.19-10.0)/((-135.0)-(0.0))*
   (z_upstream_fine_region_boundary-(0.0));
  sss_upstream_outer_fine_region_boundary[1]=z_upstream_fine_region_boundary;
  sss_upstream_outer_fine_region_boundary_vertex_id=1;

  sss_coords[sss_upstream_outer_fine_region_boundary_vertex_id][0]=
   sss_upstream_outer_fine_region_boundary[0]; 
  sss_coords[sss_upstream_outer_fine_region_boundary_vertex_id][1]=
   sss_upstream_outer_fine_region_boundary[1]; 

  sss_coords[2][0]=9.19;
  sss_coords[2][1]=-135.0;
  
  // Rescale
  unsigned n=sss_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    sss_coords[j][0]*=Global_Physical_Variables::R_scale;
    sss_coords[j][1]*=Global_Physical_Variables::Z_scale;

    upstream_outer_sss_boundary_vertex.push_back(sss_coords[j]);
   }
  
  // Build
  sss_fluid_outer_polyline_boundary_pt[
   SSS_fluid_outer_poro_interface_near_inlet_boundary_id]=
   new TriangleMeshPolyLine(
    sss_coords,
    SSS_fluid_outer_poro_interface_near_inlet_boundary_id);
 }
 
 TriangleMeshClosedCurve * sss_fluid_outer_boundary_pt =
  new TriangleMeshClosedCurve(sss_fluid_outer_polyline_boundary_pt);


 // Five internal boundaries
 inner_open_boundary_pt.resize(5); 
 TriangleMeshPolyLine* inner_polyline1_pt = 0;
 TriangleMeshPolyLine* inner_polyline2_pt = 0;
 TriangleMeshPolyLine* inner_polyline3_pt = 0;
 TriangleMeshPolyLine* inner_polyline4_pt = 0;
 TriangleMeshPolyLine* inner_polyline5_pt = 0;

 // First one
 {
  // Boundary lines
  Vector<Vector<double> > line1(2,Vector<double>(2));
  Vector<Vector<double> > line2(2,Vector<double>(2));
  
  Vector<Vector<double> > inner_coords(4, Vector<double>(2,0.0));
  
  inner_coords[0][0]=6.0+Global_Physical_Variables::BL_thick;
  inner_coords[0][1]=0.0;
  
  // Inner wall (BL to right!)
  line1[0][0]=4.24;
  line1[0][1]=-440.0;
  line1[1][0]=6.0;
  line1[1][1]=0.0;

  // Upstream edge of block (BL to right!)
  line2[0][0]=9.19;
  line2[0][1]=-135.0;
  line2[1][0]=5.905;
  line2[1][1]=-140.0;

  // Get intersection
  Vector<double> bl_intersect=
   Global_Physical_Variables::bl_intersection(
    line1,line2,Global_Physical_Variables::BL_thick);

  // Set it...
  inner_coords[1][0]=bl_intersect[0];
  inner_coords[1][1]=bl_intersect[1];


  // Upstream edge of block (BL to right!)
  line1[0][0]=9.19;
  line1[0][1]=-135.0;
  line1[1][0]=5.905;
  line1[1][1]=-140.0;

  // Outer wall (BL to right)
  line2[0][0]=10.0;
  line2[0][1]=0.0;
  line2[1][0]=9.19;
  line2[1][1]=-135.0;

  // Get intersection
  bl_intersect=
   Global_Physical_Variables::bl_intersection(
    line1,line2,Global_Physical_Variables::BL_thick);

  // Set it
  inner_coords[2][0]=bl_intersect[0];
  inner_coords[2][1]=bl_intersect[1];
  
  // Final one
  inner_coords[3][0]=10.0-Global_Physical_Variables::BL_thick;
  inner_coords[3][1]=0.0;
  
  // Rescale
  for(unsigned i=0;i<4;i++)
   {
    inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_polyline1_pt =
   new TriangleMeshPolyLine(inner_coords,
                            SSS_bl_near_inlet_boundary_id);
 }

 // Connect initial vertex to first (from zeroth) vertex on inlet boundary
 inner_polyline1_pt->connect_initial_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (sss_fluid_outer_polyline_boundary_pt[SSS_fluid_inflow_boundary_id]),1);
 
 // Connect final vertex to second (from zeroth) vertex on inlet boundary
 inner_polyline1_pt->connect_final_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (sss_fluid_outer_polyline_boundary_pt[SSS_fluid_inflow_boundary_id]),2);
 

 // Second one
 {
  // Boundary lines
  Vector<Vector<double> > line1(2,Vector<double>(2));
  Vector<Vector<double> > line2(2,Vector<double>(2));
  
  Vector<Vector<double> > inner_coords(7, Vector<double>(2,0.0));
  
  inner_coords[0][0]=2.5-Global_Physical_Variables::BL_thick;
  inner_coords[0][1]=-600.0;
  
  // Lowest part of inner wall (BL to right!)
  line1[0][0]=7.0;
  line1[0][1]=-500.0;
  line1[1][0]=2.5-Global_Physical_Variables::BL_thick;
  line1[1][1]=-600.0;
  
  // Upper part of inner wall (BL to right)
  line2[0][0]=9.01;
  line2[0][1]=-165.0;
  line2[1][0]=7.0;
  line2[1][1]=-500.0;
  
  // Get intersection
  Vector<double> bl_intersect=
   Global_Physical_Variables::bl_intersection(
    line1,line2,Global_Physical_Variables::BL_thick);

  // Set it...
  inner_coords[1][0]=bl_intersect[0];
  inner_coords[1][1]=bl_intersect[1];

  // Downstream edge of block (BL to right!)
  line1[0][0]=5.82;
  line1[0][1]=-160.0;
  line1[1][0]=9.01;
  line1[1][1]=-165.0;

  // Upper part of inner wall (BL to right)
  line2[0][0]=9.01;
  line2[0][1]=-165.0;
  line2[1][0]=7.0;
  line2[1][1]=-500.0;

  // Get intersection
  bl_intersect=
   Global_Physical_Variables::bl_intersection(
    line1,line2,Global_Physical_Variables::BL_thick);

  // Set it...
  inner_coords[2][0]=bl_intersect[0];
  inner_coords[2][1]=bl_intersect[1];
  
  // Downstream edge of block (BL to right!)
  line1[0][0]=5.82;
  line1[0][1]=-160.0;
  line1[1][0]=9.01;
  line1[1][1]=-165.0;

  // Upper part of inner wall (BL to right!)
  line2[0][0]=4.24;
  line2[0][1]=-440.0;
  line2[1][0]=6.0;
  line2[1][1]=0.0;
  
  // Get intersection
  bl_intersect=
   Global_Physical_Variables::bl_intersection(
    line1,line2,Global_Physical_Variables::BL_thick);

  // Set it...
  inner_coords[3][0]=bl_intersect[0];
  inner_coords[3][1]=bl_intersect[1];
  

  // Middle part of inner wall (BL to right!)
  line1[0][0]=1.25;
  line1[0][1]=-480.0;
  line1[1][0]=4.24;
  line1[1][1]=-440.0;

  // Upper part of inner wall (BL to right!)
  line2[0][0]=4.24;
  line2[0][1]=-440.0;
  line2[1][0]=6.0;
  line2[1][1]=0.0;
  
  // Get intersection
  bl_intersect=
   Global_Physical_Variables::bl_intersection(
    line1,line2,Global_Physical_Variables::BL_thick);

  // Set it...
  inner_coords[4][0]=bl_intersect[0];
  inner_coords[4][1]=bl_intersect[1];
  

  // Middle part of inner wall (BL to right!)
  line1[0][0]=1.25;
  line1[0][1]=-480.0;
  line1[1][0]=4.24;
  line1[1][1]=-440.0;

  // Lower part of inner wall (BL to right!)
  line2[0][0]=1.25;
  line2[0][1]=-600.0;
  line2[1][0]=1.25;
  line2[1][1]=-480.0;
  
  // Get intersection
  bl_intersect=
   Global_Physical_Variables::bl_intersection(
    line1,line2,Global_Physical_Variables::BL_thick);

  // Set it...
  inner_coords[5][0]=bl_intersect[0];
  inner_coords[5][1]=bl_intersect[1];
  

  // Final one
  inner_coords[6][0]=1.25+Global_Physical_Variables::BL_thick;
  inner_coords[6][1]=-600.0;
  

  // Rescale
  for(unsigned i=0;i<7;i++)
   {
    inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_polyline2_pt =
   new TriangleMeshPolyLine(inner_coords,
                            SSS_bl_near_outlet_boundary_id);
 }


 // Connect initial vertex to first (from zeroth) vertex on outlet boundary
 inner_polyline2_pt->connect_initial_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (sss_fluid_outer_polyline_boundary_pt[SSS_fluid_outflow_boundary_id]),1);
 
 // Connect final vertex to second (from zeroth) vertex on outlet boundary
 inner_polyline2_pt->connect_final_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (sss_fluid_outer_polyline_boundary_pt[SSS_fluid_outflow_boundary_id]),2);
 



 // Third one: downstream boundary of fine region
 {
  Vector<Vector<double> > inner_coords(2, Vector<double>(2,0.0));
  
  inner_coords[0][0]=sss_downstream_inner_fine_region_boundary[0];
  inner_coords[0][1]=sss_downstream_inner_fine_region_boundary[1];
  
  inner_coords[1][0]=sss_downstream_outer_fine_region_boundary[0];
  inner_coords[1][1]=sss_downstream_outer_fine_region_boundary[1];
  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_polyline3_pt =
   new TriangleMeshPolyLine(inner_coords,
                            SSS_downstream_fine_region_boundary_id);
 }

 // Connect initial vertex to relevant vertex on inner fsi boundary
 inner_polyline3_pt->connect_initial_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (sss_fluid_outer_polyline_boundary_pt
   [SSS_fluid_central_inner_poro_interface_boundary_id]),
  sss_downstream_inner_fine_region_boundary_vertex_id);
 
 // Connect final vertex to relevant vertex on outer fsi boundary
 inner_polyline3_pt->connect_final_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (sss_fluid_outer_polyline_boundary_pt
   [SSS_fluid_outer_poro_interface_near_outlet_boundary_id]),
  sss_downstream_outer_fine_region_boundary_vertex_id);
 


 // Fourth one: upstream boundary of fine region
 {
  Vector<Vector<double> > inner_coords(2, Vector<double>(2,0.0));
  
  inner_coords[0][0]=sss_upstream_inner_fine_region_boundary[0];
  inner_coords[0][1]=sss_upstream_inner_fine_region_boundary[1];
  
  inner_coords[1][0]=sss_upstream_outer_fine_region_boundary[0];
  inner_coords[1][1]=sss_upstream_outer_fine_region_boundary[1];
  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_polyline4_pt =
   new TriangleMeshPolyLine(inner_coords,
                            SSS_upstream_fine_region_boundary_id);
 }

 // Connect initial vertex to relevant vertex on inner fsi boundary
 inner_polyline4_pt->connect_initial_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (sss_fluid_outer_polyline_boundary_pt
   [SSS_fluid_central_inner_poro_interface_boundary_id]),
  sss_upstream_inner_fine_region_boundary_vertex_id);
 
 // Connect final vertex to relevant vertex on outer fsi boundary
 inner_polyline4_pt->connect_final_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (sss_fluid_outer_polyline_boundary_pt
   [SSS_fluid_outer_poro_interface_near_inlet_boundary_id]),
  sss_upstream_outer_fine_region_boundary_vertex_id);
 

 // Fifth one: radial line in gap under block
 {
  Vector<Vector<double> > gap_coords(2, Vector<double>(2,0.0));
  
  gap_coords[0][0]=r_inner_radial_line_under_gap;
  gap_coords[0][1]=Z_radial_cut_through_syrinx;
  
  gap_coords[1][0]=r_outer_radial_line_under_gap;
  gap_coords[1][1]=Z_radial_cut_through_syrinx;
  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    gap_coords[i][0]*=Global_Physical_Variables::R_scale;
    gap_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_polyline5_pt =
   new TriangleMeshPolyLine(gap_coords,
                            SSS_gap_under_block_veloc_line_boundary_id);
 }

 // Connect initial vertex to relevant vertex on inner fsi boundary
 inner_polyline5_pt->connect_initial_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (sss_fluid_outer_polyline_boundary_pt
   [SSS_fluid_central_inner_poro_interface_boundary_id]),
  sss_radial_line_across_gap_inner_vertex_id);
 
 // Connect final vertex to relevant vertex on outer fsi boundary
 inner_polyline5_pt->connect_final_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (sss_fluid_outer_polyline_boundary_pt
   [SSS_fluid_outer_poro_interface_block_boundary_id]),
  sss_radial_line_across_gap_outer_vertex_id);
 
 // Store in vector
 inner_curve_section_pt.resize(1);
 inner_curve_section_pt[0] = inner_polyline1_pt;
 
 // Create internal open curve
 inner_open_boundary_pt[0] = new TriangleMeshOpenCurve(inner_curve_section_pt);

 // Store in vector
 inner_curve_section_pt.resize(1);
 inner_curve_section_pt[0] = inner_polyline2_pt;
 
 // Create internal open curve
 inner_open_boundary_pt[1] = new TriangleMeshOpenCurve(inner_curve_section_pt);

 // Store in vector
 inner_curve_section_pt.resize(1);
 inner_curve_section_pt[0] = inner_polyline3_pt;
 
 // Create internal open curve
 inner_open_boundary_pt[2] = new TriangleMeshOpenCurve(inner_curve_section_pt);

 // Store in vector
 inner_curve_section_pt.resize(1);
 inner_curve_section_pt[0] = inner_polyline4_pt;
 
 // Create internal open curve
 inner_open_boundary_pt[3] = new TriangleMeshOpenCurve(inner_curve_section_pt);

 // Store in vector
 inner_curve_section_pt.resize(1);
 inner_curve_section_pt[0] = inner_polyline5_pt;
 
 // Create internal open curve
 inner_open_boundary_pt[4] = new TriangleMeshOpenCurve(inner_curve_section_pt);

 
 // Mesh parameters
 TriangleMeshParameters
  sss_fluid_mesh_parameters(sss_fluid_outer_boundary_pt);

 sss_fluid_mesh_parameters.element_area() =
  Global_Physical_Variables::Element_area_fluid;

 // Define region coordinates
 region_coords.resize(2);

 // Point in central fluid domain near block (upstream)
 region_coords[0] = (0.5*(sss_upstream_inner_fine_region_boundary[0]+
                          sss_upstream_outer_fine_region_boundary[0]))*
  Global_Physical_Variables::R_scale;
 region_coords[1] = (0.5*(sss_upstream_inner_fine_region_boundary[1]+
                          sss_upstream_outer_fine_region_boundary[1])-0.2)*
  Global_Physical_Variables::Z_scale;
 sss_fluid_mesh_parameters.add_region_coordinates(
  SSS_upstream_near_block_region_id, 
  region_coords);

 // Point in central fluid domain near block (upper central)
 region_coords[0] = (5.75)*Global_Physical_Variables::R_scale;
 region_coords[1] = (-145.0)*Global_Physical_Variables::Z_scale;
 sss_fluid_mesh_parameters.add_region_coordinates(
  SSS_central_and_bl_upper_near_block_region_id, 
  region_coords);


 // Point in central fluid domain near block (lower central)
 region_coords[0] = (5.5)*Global_Physical_Variables::R_scale;
 region_coords[1] = (-155.0)*Global_Physical_Variables::Z_scale;
 sss_fluid_mesh_parameters.add_region_coordinates(
  SSS_central_and_bl_lower_near_block_region_id, 
  region_coords);

 // Point in central fluid domain near block (downstream)
 region_coords[0] = (0.5*(sss_downstream_inner_fine_region_boundary[0]+
                          sss_downstream_outer_fine_region_boundary[0]))*
  Global_Physical_Variables::R_scale;
 region_coords[1] = (0.5*(sss_downstream_inner_fine_region_boundary[1]+
                          sss_downstream_outer_fine_region_boundary[1])+0.2)*
  Global_Physical_Variables::Z_scale;
 sss_fluid_mesh_parameters.add_region_coordinates(
  SSS_downstream_near_block_region_id, 
  region_coords);


 // Specify different target area SSS near block (two regions)
 oomph_info << "Setting area in SSS near (upstream/downstream) block to " 
            << Global_Physical_Variables::Element_area_sss_near_block 
            << std::endl;

 sss_fluid_mesh_parameters.set_target_area_for_region
  (SSS_upstream_near_block_region_id,
   Global_Physical_Variables::Element_area_sss_near_block);
 
 sss_fluid_mesh_parameters.set_target_area_for_region
  (SSS_downstream_near_block_region_id,
   Global_Physical_Variables::Element_area_sss_near_block);
 
 // Specify different target area SSS under block and in adjacent bl
 oomph_info << "Setting area in SSS under block and adjacent bl to " 
            << Global_Physical_Variables::Element_area_sss_under_block_and_bl
            << std::endl;

 sss_fluid_mesh_parameters.set_target_area_for_region
  (SSS_central_and_bl_upper_near_block_region_id,
   Global_Physical_Variables::Element_area_sss_under_block_and_bl);
 
 sss_fluid_mesh_parameters.set_target_area_for_region
  (SSS_central_and_bl_lower_near_block_region_id,
   Global_Physical_Variables::Element_area_sss_under_block_and_bl);
 
 // Define inner boundaries
 sss_fluid_mesh_parameters.internal_open_curves_pt()=inner_open_boundary_pt;
 
 // Build mesh
 SSS_fluid_mesh_pt = new TriangleMesh<FLUID_ELEMENT>(
   sss_fluid_mesh_parameters, Fluid_time_stepper_pt);
 
 // Properly reorder to ensure correct sequence when restarting (spine
 // node update!)
 SSS_fluid_mesh_pt->reorder_nodes();
 
  // Output mesh and boundaries
 SSS_fluid_mesh_pt->output
  (Global_Physical_Variables::Directory+"/sss_fluid_mesh.dat",2);
 SSS_fluid_mesh_pt->output_boundaries
  (Global_Physical_Variables::Directory+"/sss_fluid_mesh_boundaries.dat");
 
 // Mesh as geom object representation of syrinx fluid mesh
 SSS_fluid_mesh_geom_obj_pt=0;

 // Compute upstream SSS volume
 Analytical_upstream_SSS_volume=
  Global_Physical_Variables::volume(upstream_outer_sss_boundary_vertex)-
  Global_Physical_Variables::volume(upstream_inner_sss_boundary_vertex);

 // Compute downstream SSS volume
 Analytical_downstream_SSS_volume=
  Global_Physical_Variables::volume(downstream_outer_sss_boundary_vertex)-
  Global_Physical_Variables::volume(downstream_inner_sss_boundary_vertex);

 // ---------------------------------------------------------------------------

 // Create the FSI meshes between inner poro and syrinx fluid
 Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt=new Mesh;
 Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt=new Mesh;
 Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt=new Mesh;
 Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt=new Mesh;
 Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt=new Mesh;
 Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt=new Mesh;

 
 // Create the FSI meshes between inner poro and SSS fluid
 Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt=new Mesh;
 Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt=new Mesh;
 Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt=new Mesh;
 SSS_fluid_upper_inner_poro_FSI_surface_mesh_pt=new Mesh;
 SSS_fluid_central_inner_poro_FSI_surface_mesh_pt=new Mesh;
 SSS_fluid_lower_inner_poro_FSI_surface_mesh_pt=new Mesh;

 // Create the FSI meshes between outer poro and SSS fluid
 Outer_poro_SSS_fluid_near_inlet_FSI_surface_mesh_pt=new Mesh;
 SSS_fluid_outer_poro_near_inlet_FSI_surface_mesh_pt=new Mesh;
 Outer_poro_SSS_fluid_near_outlet_FSI_surface_mesh_pt=new Mesh; 
 SSS_fluid_outer_poro_near_outlet_FSI_surface_mesh_pt=new Mesh;

 // Geometry meshes
 Outer_SSS_FSI_geometry_mesh_pt=new Mesh;
 Inner_SSS_FSI_geometry_mesh_pt=new Mesh;
 Syrinx_FSI_geometry_mesh_pt=new Mesh;

 // Create the FSI meshes beteween outer poro and SSS fluid for block
 Outer_poro_SSS_fluid_block_FSI_surface_mesh_pt=new Mesh;
 SSS_fluid_outer_poro_block_FSI_surface_mesh_pt=new Mesh;

 // Create the surface mesh for the inflow boundary
 Inflow_fluid_surface_mesh_pt = new Mesh;
 
 // Create meshes for elements that allow the computation of the fluid volumes
 Syrinx_volume_analysis_mesh_pt = new Mesh;
 Downstream_SSS_volume_analysis_mesh_pt = new Mesh;
 Upstream_SSS_volume_analysis_mesh_pt = new Mesh;

 // Meshes of elements that can be used to analyse the
 // syrinx volume. Attaches to syrinx mesh.
 Syrinx_volume_from_upper_syrinx_mesh_pt=new Mesh;
 Syrinx_volume_from_lower_syrinx_mesh_pt=new Mesh;
 Syrinx_volume_from_line_through_syrinx_mesh_pt=new Mesh;

 // Create meshes for elements that allow visualisation/postprocessing 
 // of NSt veloc
 Syrinx_veloc_line_mesh_pt = new Mesh;
 SSS_gap_under_block_veloc_line_mesh_pt = new Mesh;
 Inner_poro_radial_line_mesh_pt =new Mesh;
 SSS_veloc_line_mesh_pt = new Mesh;
 Test_volume_flux_out_of_sss_mesh_pt = new Mesh;
 SSS_inner_fsi_boundary_veloc_line_mesh_pt = new Mesh;
 Syrinx_centreline_veloc_line_mesh_pt = new Mesh;


 // ---------------------------------------------------------------------------

 // Make surface meshes
 create_fluid_face_elements();
 create_poro_face_elements();

 // // ---------------------------------------------------------------------------

 // // Undo z-scaling?
 // if (CommandLineArgs::command_line_flag_has_been_set
 //     ("--undo_z_scaling_by_element_stretching"))
 //  {
 //   Vector<Mesh*> all_bulk_mesh_pt;
 //   all_bulk_mesh_pt.push_back(SSS_fluid_mesh_pt);
 //   all_bulk_mesh_pt.push_back(Syrinx_fluid_mesh_pt);
 //   all_bulk_mesh_pt.push_back(Outer_poro_mesh_pt);
 //   all_bulk_mesh_pt.push_back(Inner_poro_mesh_pt);
 //   unsigned n=all_bulk_mesh_pt.size();
 //   for (unsigned m=0;m<n;m++)
 //    {
 //     Mesh* my_mesh_pt=all_bulk_mesh_pt[m];
 //     unsigned nnod=my_mesh_pt->nnode();
 //     for (unsigned j=0;j<nnod;j++)
 //      {
 //       my_mesh_pt->node_pt(j)->x(1)*=Global_Physical_Variables::Z_shrink_factor;
 //      }
 //    }

 //   // Update volumes
 //   Analytical_syrinx_volume*=Global_Physical_Variables::Z_shrink_factor;
 //   Analytical_upstream_SSS_volume*=Global_Physical_Variables::Z_shrink_factor;
 //   Analytical_downstream_SSS_volume*=Global_Physical_Variables::Z_shrink_factor;

 //  }



 // oomph_info << "Analytical syrinx volume: " << Analytical_syrinx_volume 
 //            << std::endl;

 // oomph_info << "Analytical upstream SSS volume: " 
 //            << Analytical_upstream_SSS_volume << std::endl;
 
 // oomph_info << "Analytical downstream SSS volume: " 
 //            << Analytical_downstream_SSS_volume << std::endl;
 

 // // ----------------------------------------------------------------------------

 // Complete problem setup (boundary conditions, pointers etc.)
 complete_problem_setup();

 // Add the poro submeshes to the problem
 add_sub_mesh(Inner_poro_mesh_pt);
 add_sub_mesh(Outer_poro_mesh_pt);

 // Add the fluid submeshes to the problem
 add_sub_mesh(Syrinx_fluid_mesh_pt);
 add_sub_mesh(SSS_fluid_mesh_pt);

 // Add the fluid inflow traction mesh to the problem
 add_sub_mesh(Inflow_fluid_surface_mesh_pt);

 // Add the FSI meshes to the problem
 add_sub_mesh(Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt);
 add_sub_mesh(Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt);
 add_sub_mesh(Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt);

 add_sub_mesh(Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt);
 add_sub_mesh(Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt);
 add_sub_mesh(Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt);

 add_sub_mesh(Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt);
 add_sub_mesh(Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt);
 add_sub_mesh(Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt);
 add_sub_mesh(SSS_fluid_upper_inner_poro_FSI_surface_mesh_pt);
 add_sub_mesh(SSS_fluid_central_inner_poro_FSI_surface_mesh_pt);
 add_sub_mesh(SSS_fluid_lower_inner_poro_FSI_surface_mesh_pt);

 add_sub_mesh(Outer_poro_SSS_fluid_near_inlet_FSI_surface_mesh_pt);
 add_sub_mesh(SSS_fluid_outer_poro_near_inlet_FSI_surface_mesh_pt);

 add_sub_mesh(Outer_poro_SSS_fluid_near_outlet_FSI_surface_mesh_pt);
 add_sub_mesh(SSS_fluid_outer_poro_near_outlet_FSI_surface_mesh_pt);

 add_sub_mesh(Outer_poro_SSS_fluid_block_FSI_surface_mesh_pt);
 add_sub_mesh(SSS_fluid_outer_poro_block_FSI_surface_mesh_pt);
 
 // Now build the global mesh
 build_global_mesh();


 #ifdef OOMPH_HAS_MUMPS

 // Set solver
 oomph_info << "Using mumps\n";
 MumpsSolver* mumps_solver_pt= new MumpsSolver;
 linear_solver_pt() = mumps_solver_pt;
 mumps_solver_pt->enable_suppress_warning_about_MPI_COMM_WORLD();

#endif


 /// Setup mesh motion?
 if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update!=-1)
  {
   setup_mesh_motion();

   // Allow for gap
   AxisymmetricPoroelasticityTractionElementHelper::Allow_gap_in_FSI=true;
  }

 // ---------------------------------------------------------------------------

 // Undo z-scaling?
 if (CommandLineArgs::command_line_flag_has_been_set
     ("--undo_z_scaling_by_element_stretching"))
  {
   Vector<Mesh*> all_bulk_mesh_pt;
   all_bulk_mesh_pt.push_back(SSS_fluid_mesh_pt);
   all_bulk_mesh_pt.push_back(Syrinx_fluid_mesh_pt);
   all_bulk_mesh_pt.push_back(Outer_poro_mesh_pt);
   all_bulk_mesh_pt.push_back(Inner_poro_mesh_pt);
   unsigned n=all_bulk_mesh_pt.size();
   for (unsigned m=0;m<n;m++)
    {
     Mesh* my_mesh_pt=all_bulk_mesh_pt[m];
     unsigned nnod=my_mesh_pt->nnode();
     for (unsigned j=0;j<nnod;j++)
      {
       my_mesh_pt->node_pt(j)->x(1)*=Global_Physical_Variables::Z_shrink_factor;
      }
    }

   // Update volumes
   Analytical_syrinx_volume*=Global_Physical_Variables::Z_shrink_factor;
   Analytical_upstream_SSS_volume*=Global_Physical_Variables::Z_shrink_factor;
   Analytical_downstream_SSS_volume*=Global_Physical_Variables::Z_shrink_factor;

  }


 oomph_info << "Analytical syrinx volume: " << Analytical_syrinx_volume 
            << std::endl;

 oomph_info << "Analytical upstream SSS volume: " 
            << Analytical_upstream_SSS_volume << std::endl;
 
 oomph_info << "Analytical downstream SSS volume: " 
            << Analytical_downstream_SSS_volume << std::endl;
 

 
 // Compute upstream and downstream syrinx volumes via line integrals  on syrinx mesh
 // ---------------------------------------------------------------------------------
 double total_syrinx_volume_from_syrinx_boundary_integral=0.0;
 compute_syrinx_volumes_from_boundary_integral(
  Initial_upper_syrinx_volume,
  Initial_lower_syrinx_volume,
  total_syrinx_volume_from_syrinx_boundary_integral);
 
 oomph_info << "Initial upper syrinx volume from syrinx boundary integral: " 
            << Initial_upper_syrinx_volume
            << std::endl;

 oomph_info << "Initial lower syrinx volume from syrinx boundary integral: " 
            << Initial_lower_syrinx_volume
            << std::endl;

 oomph_info << "Initial total syrinx volume from syrinx boundary integral: " 
            << total_syrinx_volume_from_syrinx_boundary_integral
            << std::endl;

 // ----------------------------------------------------------------------------

 // Assign equation numbers
 oomph_info << assign_eqn_numbers() << " equations assigned" << std::endl;
 
} // end_of_constructor




//===start_of_complete_problem_setup=================================
/// Complete problem setup 
//===================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
complete_problem_setup()
{
 // Mesh as geom object representation of syrinx fluid mesh
 Syrinx_fluid_mesh_geom_obj_pt=new MeshAsGeomObject(Syrinx_fluid_mesh_pt);
 
 // Extract regularly spaced points
 if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
  {
   double t_start = TimingHelpers::timer();

   // Speed up search (somewhat hand-tuned; adjust if this takes too long
   // or misses too many points)
   Syrinx_fluid_mesh_geom_obj_pt->max_spiral_level()=4;
   unsigned nx_back= Multi_domain_functions::Nx_bin;
   Multi_domain_functions::Nx_bin=10;
   unsigned ny_back= Multi_domain_functions::Ny_bin;
   Multi_domain_functions::Ny_bin=10;
   
   // Populate it...
   Vector<double> x(2);
   Vector<double> s(2);
   Syrinx_fluid_regularly_spaced_plot_point.clear();
   for (unsigned ir=0;ir<Global_Physical_Variables::Nplot_r_regular;ir++)
    {
     // outermost radius of dura
     x[0]=double(ir)/double(Global_Physical_Variables::Nplot_r_regular-1)*0.55;
     for (unsigned iz=0;iz<Global_Physical_Variables::Nplot_z_regular;iz++)
      {
       x[1]=double(iz)/double(Global_Physical_Variables::Nplot_z_regular-1)*
        (-600.0)*Global_Physical_Variables::Z_scale;
       
       // Pointer to GeomObject that contains this point
       GeomObject* geom_obj_pt=0;
       
       // Get it
       Syrinx_fluid_mesh_geom_obj_pt->locate_zeta(x,geom_obj_pt,s);
       
       // Store it
       if (geom_obj_pt!=0)
        {
         std::pair<FLUID_ELEMENT*,Vector<double> > tmp;
         tmp = std::make_pair(dynamic_cast<FLUID_ELEMENT*>(geom_obj_pt),s);
         Syrinx_fluid_regularly_spaced_plot_point.push_back(tmp);
        }
      }
    }

   // Reset number of bins in binning method 
   Multi_domain_functions::Nx_bin=nx_back;
   Multi_domain_functions::Ny_bin=ny_back;
   oomph_info << "Took: " << TimingHelpers::timer()-t_start
              << " sec to setup regularly spaced points for syrinx mesh\n";
  }


 // Mesh as geom object representation of inner poro mesh
 Inner_poro_mesh_geom_obj_pt=new MeshAsGeomObject(Inner_poro_mesh_pt);
 
 // Extract regularly spaced points
 if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
  {
   double t_start = TimingHelpers::timer();
   
   // Speed up search (somewhat hand-tuned; adjust if this takes too long
   // or misses too many points)
   Inner_poro_mesh_geom_obj_pt->max_spiral_level()=4;
   unsigned nx_back= Multi_domain_functions::Nx_bin;
   Multi_domain_functions::Nx_bin=10;
   unsigned ny_back= Multi_domain_functions::Ny_bin;
   Multi_domain_functions::Ny_bin=10;
   
   // Populate it...
   Vector<double> x(2);
   Vector<double> s(2);
   Inner_poro_regularly_spaced_plot_point.clear();
   for (unsigned ir=0;ir<Global_Physical_Variables::Nplot_r_regular;ir++)
    {
     // outermost radius of dura
     x[0]=double(ir)/double(Global_Physical_Variables::Nplot_r_regular-1)*0.55;
     for (unsigned iz=0;iz<Global_Physical_Variables::Nplot_z_regular;iz++)
      {
       x[1]=double(iz)/double(Global_Physical_Variables::Nplot_z_regular-1)*
        (-600.0)*Global_Physical_Variables::Z_scale;
       
       // Pointer to GeomObject that contains this point
       GeomObject* geom_obj_pt=0;
       
       // Get it
       Inner_poro_mesh_geom_obj_pt->locate_zeta(x,geom_obj_pt,s);
       
       // Store it
       if (geom_obj_pt!=0)
        {
         std::pair<PORO_ELEMENT*,Vector<double> > tmp;
         tmp = std::make_pair(dynamic_cast<PORO_ELEMENT*>(geom_obj_pt),s);
         Inner_poro_regularly_spaced_plot_point.push_back(tmp);
        }
      }
    }

   // Reset number of bins in binning method 
   Multi_domain_functions::Nx_bin=nx_back;
   Multi_domain_functions::Ny_bin=ny_back;
   oomph_info << "Took: " << TimingHelpers::timer()-t_start
              << " sec to setup regularly spaced points for inner poro mesh\n";
  }


 // Line visualiser for radial line through syrinx cover
 if (!CommandLineArgs::command_line_flag_has_been_set("--suppress_line_visualisers"))
 {
  double t_start = TimingHelpers::timer();

  // Where do you want it? Halfway between lower vertex of syrinx
  // and lower vertex of gap under block
  double z=-180.0; // -185.0;
  
  // Max. range of syrinx cover
  double r_min=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi+(Global_Physical_Variables::R_syrinx_top_syrinx_fsi-Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi)*(z-(Global_Physical_Variables::Z_syrinx_bottom_fsi))/((Global_Physical_Variables::Z_syrinx_top_fsi)-(Global_Physical_Variables::Z_syrinx_bottom_fsi));
  double r_max=Global_Physical_Variables::R_syrinx_bottom_sss_fsi +(Global_Physical_Variables::R_syrinx_top_sss_fsi -Global_Physical_Variables::R_syrinx_bottom_sss_fsi )*(z-(Global_Physical_Variables::Z_syrinx_bottom_fsi))/((Global_Physical_Variables::Z_syrinx_top_fsi)-(Global_Physical_Variables::Z_syrinx_bottom_fsi));
  r_min*=Global_Physical_Variables::R_scale;
  r_max*=Global_Physical_Variables::R_scale;
  z*=Global_Physical_Variables::Z_scale;

  // How many points to you want?
  unsigned npt=100;
  Vector<Vector<double> > coord_vec(npt);
  for (unsigned j=0;j<npt;j++)
   {
    coord_vec[j].resize(2);
    coord_vec[j][0]=r_min+(r_max-r_min)*double(j)/unsigned(npt-1);
    coord_vec[j][1]=z;
   }
  
  // Setup line visualiser 
  double max_search_radius=2.0*(r_max-r_min);
  Lower_radial_line_through_syrinx_cover_visualiser_pt=
   new LineVisualiser(Inner_poro_mesh_pt,
                      coord_vec,
                      max_search_radius);
  
  oomph_info 
   << "Took: " << TimingHelpers::timer()-t_start
   << " sec to set up lower line visualiser \n"
   << " between " << r_min << " and " << r_max 
   << " at z = " << z << std::endl;
 }


 // Line visualiser for radial line through syrinx cover 
 if (!CommandLineArgs::command_line_flag_has_been_set("--suppress_line_visualisers"))
 {
  double t_start = TimingHelpers::timer();

  // Where do you want it? Halfway between upper vertex of syrinx
  // and upper vertex of gap under block
  double z=-115.0;
  
  // Max. range of syrinx cover
  double r_min=Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi+(Global_Physical_Variables::R_syrinx_top_syrinx_fsi-Global_Physical_Variables::R_syrinx_bottom_syrinx_fsi)*(z-(Global_Physical_Variables::Z_syrinx_bottom_fsi))/((Global_Physical_Variables::Z_syrinx_top_fsi)-(Global_Physical_Variables::Z_syrinx_bottom_fsi));
  double r_max=Global_Physical_Variables::R_syrinx_bottom_sss_fsi +(Global_Physical_Variables::R_syrinx_top_sss_fsi -Global_Physical_Variables::R_syrinx_bottom_sss_fsi )*(z-(Global_Physical_Variables::Z_syrinx_bottom_fsi))/((Global_Physical_Variables::Z_syrinx_top_fsi)-(Global_Physical_Variables::Z_syrinx_bottom_fsi));
  r_min*=Global_Physical_Variables::R_scale;
  r_max*=Global_Physical_Variables::R_scale;
  z*=Global_Physical_Variables::Z_scale;

  // How many points to you want?
  unsigned npt=100;
  Vector<Vector<double> > coord_vec(npt);
  for (unsigned j=0;j<npt;j++)
   {
    coord_vec[j].resize(2);
    coord_vec[j][0]=r_min+(r_max-r_min)*double(j)/unsigned(npt-1);
    coord_vec[j][1]=z;
   }
  
  // Setup line visualiser 
  double max_search_radius=2.0*(r_max-r_min);
  Upper_radial_line_through_syrinx_cover_visualiser_pt=
   new LineVisualiser(Inner_poro_mesh_pt,
                      coord_vec,
                      max_search_radius);
  
  oomph_info 
   << "Took: " << TimingHelpers::timer()-t_start
   << " sec to set up upper line visualiser for points \n"
   << " between " << r_min << " and " << r_max 
   << " at z = " << z << std::endl;
 }

 

 // Mesh as geom object representation of outer poro mesh
 Outer_poro_mesh_geom_obj_pt=new MeshAsGeomObject(Outer_poro_mesh_pt);
 
 // Extract regularly spaced points
 if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
  {
   double t_start = TimingHelpers::timer();

   // Speed up search (somewhat hand-tuned; adjust if this takes too long
   // or misses too many points)
   Outer_poro_mesh_geom_obj_pt->max_spiral_level()=4;
   unsigned nx_back= Multi_domain_functions::Nx_bin;
   Multi_domain_functions::Nx_bin=10;
   unsigned ny_back= Multi_domain_functions::Ny_bin;
   Multi_domain_functions::Ny_bin=10;
   
   // Populate it...
   Vector<double> x(2);
   Vector<double> s(2);
   Outer_poro_regularly_spaced_plot_point.clear();
   for (unsigned ir=0;ir<Global_Physical_Variables::Nplot_r_regular;ir++)
    {
     // outermost radius of dura
     x[0]=double(ir)/double(Global_Physical_Variables::Nplot_r_regular-1)*0.55;
     for (unsigned iz=0;iz<Global_Physical_Variables::Nplot_z_regular;iz++)
      {
       x[1]=double(iz)/double(Global_Physical_Variables::Nplot_z_regular-1)*
        (-600.0)*Global_Physical_Variables::Z_scale;
       
       // Pointer to GeomObject that contains this point
       GeomObject* geom_obj_pt=0;
       
       // Get it
       Outer_poro_mesh_geom_obj_pt->locate_zeta(x,geom_obj_pt,s);
       
       // Store it
       if (geom_obj_pt!=0)
        {
         std::pair<PORO_ELEMENT*,Vector<double> > tmp;
         tmp = std::make_pair(dynamic_cast<PORO_ELEMENT*>(geom_obj_pt),s);
         Outer_poro_regularly_spaced_plot_point.push_back(tmp);
        }
      }
    }

   // Reset number of bins in binning method 
   Multi_domain_functions::Nx_bin=nx_back;
   Multi_domain_functions::Ny_bin=ny_back;
   oomph_info << "Took: " << TimingHelpers::timer()-t_start
              << " sec to setup regularly spaced points for outer poro mesh\n";
  }
 
 // Mesh as geom object representation of syrinx fluid mesh
 SSS_fluid_mesh_geom_obj_pt=new MeshAsGeomObject(SSS_fluid_mesh_pt);
 
 // Extract regularly spaced points
 if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
  {
   double t_start = TimingHelpers::timer();

   // Speed up search (somewhat hand-tuned; adjust if this takes too long
   // or misses too many points)
   SSS_fluid_mesh_geom_obj_pt->max_spiral_level()=4;
   unsigned nx_back= Multi_domain_functions::Nx_bin;
   Multi_domain_functions::Nx_bin=10;
   unsigned ny_back= Multi_domain_functions::Ny_bin;
   Multi_domain_functions::Ny_bin=10;

   // Populate it...
   Vector<double> x(2);
   Vector<double> s(2);
   SSS_fluid_regularly_spaced_plot_point.clear();
   for (unsigned ir=0;ir<Global_Physical_Variables::Nplot_r_regular;ir++)
    {
     // outermost radius of dura
     x[0]=double(ir)/double(Global_Physical_Variables::Nplot_r_regular-1)*0.55; 
     for (unsigned iz=0;iz<Global_Physical_Variables::Nplot_z_regular;iz++)
      {
       x[1]=double(iz)/double(Global_Physical_Variables::Nplot_z_regular-1)*
        (-600.0)*Global_Physical_Variables::Z_scale;
       
       // Pointer to GeomObject that contains this point
       GeomObject* geom_obj_pt=0;
       
       // Get it
       SSS_fluid_mesh_geom_obj_pt->locate_zeta(x,geom_obj_pt,s);

       // Store it
       if (geom_obj_pt!=0)
        {
         std::pair<FLUID_ELEMENT*,Vector<double> > tmp;
         tmp = std::make_pair(dynamic_cast<FLUID_ELEMENT*>(geom_obj_pt),s);
         SSS_fluid_regularly_spaced_plot_point.push_back(tmp);
        }
      }
    }

   // Reset number of bins in binning method 
   Multi_domain_functions::Nx_bin=nx_back;
   Multi_domain_functions::Ny_bin=ny_back;
   oomph_info << "Took: " << TimingHelpers::timer()-t_start
              << " sec to setup regularly spaced points for sss mesh\n";
  }

 // ----------------------------------------------------------------------------

 // Complete the problem setup to make the elements fully functional
 
 // Do edge sign setup for inner poro mesh
 Global_Physical_Variables::edge_sign_setup<PORO_ELEMENT>(Inner_poro_mesh_pt);
 
 // Loop over the elements in cord region of inner poro mesh
 //---------------------------------------------------------
 {
  Vector<unsigned> cord_region_id;
  cord_region_id.push_back(Upper_cord_region_id);
  cord_region_id.push_back(Central_upper_cord_region_id);
  cord_region_id.push_back(Central_lower_cord_region_id);
  cord_region_id.push_back(Lower_cord_region_id);
  cord_region_id.push_back(Upper_cord_syrinx_poro_bl_region_id);
  cord_region_id.push_back(Lower_cord_syrinx_poro_bl_region_id);
  cord_region_id.push_back(Upper_cord_pia_bl_region_id);
  cord_region_id.push_back(Central_upper_cord_syrinx_cover_bl_region_id);
  cord_region_id.push_back(Central_upper_cord_pia_bl_region_id);
  cord_region_id.push_back(Central_lower_cord_syrinx_cover_bl_region_id);
  cord_region_id.push_back(Central_lower_cord_pia_bl_region_id);
  cord_region_id.push_back(Lower_cord_pia_bl_region_id);
  cord_region_id.push_back(Filum_pia_bl_region_id);
  cord_region_id.push_back(Small_upper_cord_pia_bl_region_id);
  cord_region_id.push_back(Small_lower_cord_pia_bl_region_id);


  unsigned nr=cord_region_id.size();
  for (unsigned rr=0;rr<nr;rr++)
   {
    // By default only syrinx cover is porous
    bool is_porous_region=false;

    unsigned r=cord_region_id[rr];
    unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)->
     nregion_element(r);

    ofstream some_file;
    std::string name;
    switch(r)
     {
     case Upper_cord_region_id:
      name=Global_Physical_Variables::Directory+"/upper_cord_region.dat";
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;
      
     case Central_upper_cord_region_id:
      name=Global_Physical_Variables::Directory+"/central_upper_cord_region.dat";
      //...is always porous
      is_porous_region=true;      
      break;

     case Central_lower_cord_region_id:
      name=Global_Physical_Variables::Directory+"/central_lower_cord_region.dat";
      //...is always porous
      is_porous_region=true;
      break;
      
     case Lower_cord_region_id:
      name=Global_Physical_Variables::Directory+"/lower_cord_region.dat";
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;
      
     case Upper_cord_syrinx_poro_bl_region_id:
      name=Global_Physical_Variables::Directory+"/upper_cord_syrinx_poro_bl_region.dat";
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;

     case Lower_cord_syrinx_poro_bl_region_id:
      name=Global_Physical_Variables::Directory+"/lower_cord_syrinx_poro_bl_region.dat";
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;
      
     case Upper_cord_pia_bl_region_id:
      name=Global_Physical_Variables::Directory+"/upper_cord_pia_bl_region.dat";
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;

     case Central_upper_cord_syrinx_cover_bl_region_id:
      name=Global_Physical_Variables::Directory+"/central_upper_cord_syrinx_cover_bl_region.dat";
      // always porous
      is_porous_region=true;
      break;

     case Central_upper_cord_pia_bl_region_id:
      name=Global_Physical_Variables::Directory+"/central_upper_cord_pia_bl_region.dat";
      // always porous
      is_porous_region=true;
      break;

     case Central_lower_cord_syrinx_cover_bl_region_id:
      name=Global_Physical_Variables::Directory+"/central_lower_cord_syrinx_cover_bl_region.dat";
      // always porous
      is_porous_region=true;
      break;
      
     case Central_lower_cord_pia_bl_region_id:
      name=Global_Physical_Variables::Directory+"/central_lower_cord_pia_bl_region.dat";
      // always porous
      is_porous_region=true;
      break;

     case Lower_cord_pia_bl_region_id:
      name=Global_Physical_Variables::Directory+"/lower_cord_pia_bl_region.dat";
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;

     case Filum_pia_bl_region_id:
      name=Global_Physical_Variables::Directory+"/filum_pia_bl_region.dat";   
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;

     case Small_upper_cord_pia_bl_region_id:
      name=Global_Physical_Variables::Directory+"/small_upper_cord_pia_bl_region.dat";
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;

     case Small_lower_cord_pia_bl_region_id:
      name=Global_Physical_Variables::Directory+"/small_lower_cord_pia_bl_region.dat";  
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;

     default:
      throw OomphLibError(
       "Wrong region id",
       OOMPH_CURRENT_FUNCTION,
       OOMPH_EXCEPTION_LOCATION);
     }
    
    // Final word:
    if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
     {
      is_porous_region=false;
     }
    

    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      some_file.open(name.c_str());
     }
    for(unsigned e=0;e<nel;e++)
     {
      // Cast to a bulk element
      PORO_ELEMENT *el_pt=dynamic_cast<PORO_ELEMENT*>(
       dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)
       ->region_element_pt(r,e));

      if (MPI_Helpers::communicator_pt()->my_rank()==0)
       {
        // Output region just using vertices
        el_pt->output(some_file,2);
       }

      // Set the pointer to the Lambda^2 parameter (universal)
      el_pt->lambda_sq_pt() = &Global_Physical_Variables::Lambda_sq;
      
      // Set the pointer to non-dim permeability (universal)
      el_pt->permeability_pt()= &Global_Physical_Variables::Permeability;
      
      // Set the pointer to the Biot parameter (same for all; incompressible
      // fluid and solid)
      el_pt->alpha_pt()=&Global_Physical_Variables::Alpha;
      
      // Set the pointer to the density ratio (pore fluid to solid) (same 
      // for all)
      el_pt->density_ratio_pt()= &Global_Physical_Variables::Density_ratio;

      // Set the pointer to Poisson's ratio      
      if (is_porous_region)
       {
        el_pt->nu_pt() = &Global_Physical_Variables::Nu_drained;
       }
      else
       {
        el_pt->nu_pt() = &Global_Physical_Variables::Nu_cord;
        el_pt->switch_off_darcy();
       }

      // Set the pointer to non-dim Young's modulus (ratio of Young's modulus
      // to Young's modulus used in the non-dimensionalisation of the
      // equations
      el_pt->youngs_modulus_pt() = 
       &Global_Physical_Variables::Stiffness_ratio_cord;
      
      // Set the pointer to permeability ratio (ratio of actual permeability
      // to the one used in the non-dim of the equations)
      el_pt->permeability_ratio_pt()=
       &Global_Physical_Variables::Permeability_ratio_cord;
      
      // Set the pointer to the porosity
      el_pt->porosity_pt()=&Global_Physical_Variables::Porosity_cord;
      
      // Set the internal q dofs' timestepper to the problem timestepper
      el_pt->set_q_internal_timestepper(Poro_time_stepper_pt);
      
     }// end loop over cord elements
    
    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      some_file.close();
     }
   }
 }

 // Loop over the elements in pia region of inner poro mesh
 //---------------------------------------------------------
 {
  Vector<unsigned> pia_region_id;
  pia_region_id.push_back(Upper_pia_region_id);
  pia_region_id.push_back(Central_upper_pia_region_id);
  pia_region_id.push_back(Central_lower_pia_region_id);
  pia_region_id.push_back(Lower_pia_region_id);
  pia_region_id.push_back(Central_upper_pia_poro_bl_region_id);
  pia_region_id.push_back(Central_lower_pia_poro_bl_region_id);
  unsigned nr=pia_region_id.size();
  for (unsigned rr=0;rr<nr;rr++)
   {
    // By default only syrinx cover is porous
    bool is_porous_region=false;

    unsigned r=pia_region_id[rr];
    unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)->
     nregion_element(r);

    ofstream some_file;
    std::string name;
    switch(r)
     {
     case Upper_pia_region_id:
      name=Global_Physical_Variables::Directory+"/upper_pia_region.dat";
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;

     case Central_upper_pia_region_id:
      name=Global_Physical_Variables::Directory+"/central_upper_pia_region.dat";
      // always porous
      is_porous_region=true;
      break;

     case Central_lower_pia_region_id:
      name=Global_Physical_Variables::Directory+"/central_lower_pia_region.dat";
      // always porous
      is_porous_region=true;
      break;

     case Central_upper_pia_poro_bl_region_id:
      name=Global_Physical_Variables::Directory+"/central_upper_pia_poro_bl_region.dat"; 
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;

     case Central_lower_pia_poro_bl_region_id:
      name=Global_Physical_Variables::Directory+"/central_lower_pia_poro_bl_region.dat";
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;

     case Lower_pia_region_id:
      name=Global_Physical_Variables::Directory+"/lower_pia_region.dat";
      // ...not porous by default
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--cord_pia_and_filum_are_porous"))
       {
        is_porous_region=true;
       }
      break;

     default:
      throw OomphLibError(
       "Wrong region id",
       OOMPH_CURRENT_FUNCTION,
       OOMPH_EXCEPTION_LOCATION);
     }
        
    // Final word:
    if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
     {
      is_porous_region=false;
     }

    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      some_file.open(name.c_str());
     }
    for(unsigned e=0;e<nel;e++)
     {
      // Cast to a bulk element
      PORO_ELEMENT *el_pt=dynamic_cast<PORO_ELEMENT*>(
       dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)
       ->region_element_pt(r,e));
      
      if (MPI_Helpers::communicator_pt()->my_rank()==0)
       {
        // Output region just using vertices
        el_pt->output(some_file,2);
       }

      // Set the pointer to the Lambda^2 parameter (universal)
      el_pt->lambda_sq_pt() = &Global_Physical_Variables::Lambda_sq;
      
      // Set the pointer to non-dim permeability (universal)
      el_pt->permeability_pt()= &Global_Physical_Variables::Permeability;
      
      // Set the pointer to the Biot parameter (same for all; incompressible
      // fluid and solid)
      el_pt->alpha_pt()=&Global_Physical_Variables::Alpha;
      
      // Set the pointer to the density ratio (pore fluid to solid) (same 
      // for all)
      el_pt->density_ratio_pt()= &Global_Physical_Variables::Density_ratio;
      
      // Set the pointer to Poisson's ratio      
      if (is_porous_region)
       {
        el_pt->nu_pt() = &Global_Physical_Variables::Nu_drained;
       }
      else
       {
        el_pt->nu_pt() = &Global_Physical_Variables::Nu_pia;
        el_pt->switch_off_darcy();
       }

      // Set the pointer to non-dim Young's modulus (ratio of Young's modulus
      // to Young's modulus used in the non-dimensionalisation of the
      // equations
      el_pt->youngs_modulus_pt() = 
       &Global_Physical_Variables::Stiffness_ratio_pia;
      
      // Set the pointer to permeability ratio (ratio of actual permeability
      // to the one used in the non-dim of the equations)
      el_pt->permeability_ratio_pt()=
       &Global_Physical_Variables::Permeability_ratio_pia;
      
      // Set the pointer to the porosity
      el_pt->porosity_pt()=&Global_Physical_Variables::Porosity_pia;
      
      // Set the internal q dofs' timestepper to the problem timestepper
      el_pt->set_q_internal_timestepper(Poro_time_stepper_pt);
            
     }// end loop over pia elements
    
    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      some_file.close();
     }
   }
 }

 // Loop over the elements in filum region of inner poro mesh
 //----------------------------------------------------------
 {

  // ...not porous by default
  bool is_porous_region=false;
  if (CommandLineArgs::command_line_flag_has_been_set
      ("--cord_pia_and_filum_are_porous"))
   {
    is_porous_region=true;
   }
  // Final word
  if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
   {
    is_porous_region=false;
   }
  
  ofstream some_file;
  std::string name;
  name=Global_Physical_Variables::Directory+"/filum_region.dat";
  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.open(name.c_str());
   }
  unsigned r=Filum_region_id;
  unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)->
   nregion_element(r);
  for(unsigned e=0;e<nel;e++)
   {
    // Cast to a bulk element
    PORO_ELEMENT *el_pt=dynamic_cast<PORO_ELEMENT*>(
     dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)
     ->region_element_pt(r,e));
    
    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      // Output region just using vertices
      el_pt->output(some_file,2);
     }

    // Set the pointer to the Lambda^2 parameter (universal)
    el_pt->lambda_sq_pt() = &Global_Physical_Variables::Lambda_sq;
    
    // Set the pointer to non-dim permeability (universal)
    el_pt->permeability_pt()= &Global_Physical_Variables::Permeability;
    
    // Set the pointer to the Biot parameter (same for all; incompressible
    // fluid and solid)
    el_pt->alpha_pt()=&Global_Physical_Variables::Alpha;
    
    // Set the pointer to the density ratio (pore fluid to solid) (same for all)
    el_pt->density_ratio_pt()= &Global_Physical_Variables::Density_ratio;
    
    // Set the pointer to Poisson's ratio      
    if (is_porous_region)
     {
      el_pt->nu_pt() = &Global_Physical_Variables::Nu_drained;
     }
    else
     {
      // Set the pointer to Poisson's ratio
      el_pt->nu_pt() = &Global_Physical_Variables::Nu_filum;     
      el_pt->switch_off_darcy();
     }

    // Set the pointer to non-dim Young's modulus (ratio of Young's modulus
    // to Young's modulus used in the non-dimensionalisation of the
    // equations
    el_pt->youngs_modulus_pt() = 
     &Global_Physical_Variables::Stiffness_ratio_filum;
    
    // Set the pointer to permeability ratio (ratio of actual permeability
    // to the one used in the non-dim of the equations)
    el_pt->permeability_ratio_pt()=
     &Global_Physical_Variables::Permeability_ratio_filum;
    
    // Set the pointer to the porosity
    el_pt->porosity_pt()=&Global_Physical_Variables::Porosity_filum;
    
    // Set the internal q dofs' timestepper to the problem timestepper
    el_pt->set_q_internal_timestepper(Poro_time_stepper_pt);

   }// end loop over filum elements

  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.close();
   }
 }


 // Do edge sign setup for outer poro mesh
 Global_Physical_Variables::edge_sign_setup<PORO_ELEMENT>(Outer_poro_mesh_pt);


 // Loop over the elements in block region of outer poro mesh
 //----------------------------------------------------------
 {

  ofstream some_file;
  std::string name;
  name=Global_Physical_Variables::Directory+"/block_region.dat";
  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.open(name.c_str());
   }
  unsigned r=Block_region_id;
  unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Outer_poro_mesh_pt)->
   nregion_element(r);
  for(unsigned e=0;e<nel;e++)
   {
    // Cast to a bulk element
    PORO_ELEMENT *el_pt=dynamic_cast<PORO_ELEMENT*>(
     dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Outer_poro_mesh_pt)
     ->region_element_pt(r,e));

    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      // Output region just using vertices
      el_pt->output(some_file,2);
     }

    // Set the pointer to the Lambda^2 parameter (universal)
    el_pt->lambda_sq_pt() = &Global_Physical_Variables::Lambda_sq;
    
    // Set the pointer to non-dim permeability (universal)
    el_pt->permeability_pt()= &Global_Physical_Variables::Permeability;
    
    // Set the pointer to the Biot parameter (same for all; incompressible
    // fluid and solid)
    el_pt->alpha_pt()=&Global_Physical_Variables::Alpha;
    
    // Set the pointer to the density ratio (pore fluid to solid) (same for all)
    el_pt->density_ratio_pt()= &Global_Physical_Variables::Density_ratio;
    
    // Set the pointer to Poisson's ratio
    el_pt->nu_pt() = &Global_Physical_Variables::Nu_block;
    
    // Set the pointer to non-dim Young's modulus (ratio of Young's modulus
    // to Young's modulus used in the non-dimensionalisation of the
    // equations
    el_pt->youngs_modulus_pt() = 
     &Global_Physical_Variables::Stiffness_ratio_block;
    
    // Set the pointer to permeability ratio (ratio of actual permeability
    // to the one used in the non-dim of the equations)
    el_pt->permeability_ratio_pt()=
     &Global_Physical_Variables::Permeability_ratio_block;
    
    // Set the pointer to the porosity
    el_pt->porosity_pt()=&Global_Physical_Variables::Porosity_block;
    
    // Set the internal q dofs' timestepper to the problem timestepper
    el_pt->set_q_internal_timestepper(Poro_time_stepper_pt);
    
    // Switch off Darcy flow?
    if ((CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))||
        (!CommandLineArgs::command_line_flag_has_been_set
         ("--everything_is_porous")))
     {
      el_pt->switch_off_darcy();
     }

   }// end loop over block elements

  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.close();
   }
 }

 // Loop over the elements in dura region of outer poro mesh
 //----------------------------------------------------------
 {
  ofstream some_file;
  std::string name;
  name=Global_Physical_Variables::Directory+"/dura_region.dat";
  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.open(name.c_str());
   }
  unsigned r=Dura_region_id;
  unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Outer_poro_mesh_pt)->
   nregion_element(r);
  for(unsigned e=0;e<nel;e++)
   {
    // Cast to a bulk element
    PORO_ELEMENT *el_pt=dynamic_cast<PORO_ELEMENT*>(
     dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Outer_poro_mesh_pt)
     ->region_element_pt(r,e));

    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      // Output region just using vertices
      el_pt->output(some_file,2);
     }

    // Set the pointer to the Lambda^2 parameter (universal)
    el_pt->lambda_sq_pt() = &Global_Physical_Variables::Lambda_sq;
    
    // Set the pointer to non-dim permeability (universal)
    el_pt->permeability_pt()= &Global_Physical_Variables::Permeability;
    
    // Set the pointer to the Biot parameter (same for all; incompressible
    // fluid and solid)
    el_pt->alpha_pt()=&Global_Physical_Variables::Alpha;
    
    // Set the pointer to the density ratio (pore fluid to solid) (same for all)
    el_pt->density_ratio_pt()= &Global_Physical_Variables::Density_ratio;
    
    // Set the pointer to Poisson's ratio
    el_pt->nu_pt() = &Global_Physical_Variables::Nu_dura;
    
    // Set the pointer to non-dim Young's modulus (ratio of Young's modulus
    // to Young's modulus used in the non-dimensionalisation of the
    // equations
    el_pt->youngs_modulus_pt() = 
     &Global_Physical_Variables::Stiffness_ratio_dura;
    
    // Set the pointer to permeability ratio (ratio of actual permeability
    // to the one used in the non-dim of the equations)
    el_pt->permeability_ratio_pt()=
     &Global_Physical_Variables::Permeability_ratio_dura;
    
    // Set the pointer to the porosity
    el_pt->porosity_pt()=&Global_Physical_Variables::Porosity_dura;
    
    // Set the internal q dofs' timestepper to the problem timestepper
    el_pt->set_q_internal_timestepper(Poro_time_stepper_pt);
    
    // Switch off Darcy flow?
    if ((CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))||
        (!CommandLineArgs::command_line_flag_has_been_set
         ("--everything_is_porous")))
     {
      el_pt->switch_off_darcy();
     }

   }// end loop over dura elements

  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.close();
   }
 }


 //----------------------------------------------------------------------
  
 // Use non-stress-divergence form?
 if (CommandLineArgs::command_line_flag_has_been_set
     ("--use_non_stress_divergence_form_for_navier_stokes"))
  {
   oomph_info << "\nUsing non-stress divergence form of viscous terms\n";
   FLUID_ELEMENT::Gamma[0] = 0.0; //r-momentum
   FLUID_ELEMENT::Gamma[1] = 0.0; //z-momentum
  }
 else
  {
   oomph_info << "\nUsing stress divergence form of viscous terms\n";
  }


 //----------------------------------------------------------------------


 // Loop over the elements in syrinx fluid mesh to set physical parameters etc
 //--------------------------------------------------------------------------
 unsigned n_el_syrinx_fluid = Syrinx_fluid_mesh_pt->nelement();
 for(unsigned e=0;e<n_el_syrinx_fluid;e++)
  {
   // Cast to the particular element type
   FLUID_ELEMENT *el_pt = dynamic_cast<FLUID_ELEMENT*>
    (Syrinx_fluid_mesh_pt->element_pt(e));

   // There is no need for ALE (unless the mesh is moving!)
   if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update==-1)
    {
     el_pt->disable_ALE();
    }

   // Set the Reynolds number for each element
   el_pt->re_pt() = &Global_Physical_Variables::Re;

   // Set the product of Reynolds and Strouhal numbers
   el_pt->re_st_pt() = &Global_Physical_Variables::Wo;

   // Pin u_theta at all nodes (no swirl velocity)
   unsigned n_node = el_pt->nnode();
   for(unsigned n=0;n<n_node;n++)
    {
     Node *nod_pt = el_pt->node_pt(n);
     nod_pt->pin(2);
    }
  }

 // Loop over the elements in SSS fluid mesh to set physical parameters etc
 //------------------------------------------------------------------------
 unsigned n_el_sss_fluid = SSS_fluid_mesh_pt->nelement();
 if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update!=-1)
  {   
   Upstream_SSS_el_pt.clear();
   Downstream_SSS_el_pt.clear();
   Upstream_SSS_el_pt.reserve(unsigned(0.5*double(n_el_sss_fluid)));
   Downstream_SSS_el_pt.reserve(unsigned(0.5*double(n_el_sss_fluid)));
  }
 for(unsigned e=0;e<n_el_sss_fluid;e++)
  {
   // Cast to the particular element type
   FLUID_ELEMENT *el_pt = dynamic_cast<FLUID_ELEMENT*>
    (SSS_fluid_mesh_pt->element_pt(e));

   // There is no need for ALE (unless the mesh is moving!)
   if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update==-1)
    {
     el_pt->disable_ALE();
    }

   // Set the Reynolds number for each element
   el_pt->re_pt() = &Global_Physical_Variables::Re;

   // Set the product of Reynolds and Strouhal numbers
   el_pt->re_st_pt() = &Global_Physical_Variables::Wo;

   // Pin u_theta at all nodes (no swirl velocity)
   unsigned n_node = el_pt->nnode();
   for(unsigned n=0;n<n_node;n++)
    {
     Node *nod_pt = el_pt->node_pt(n);
     nod_pt->pin(2);
    }



   // Add to upstream/downstream SSS elements (for determination of
   // volume in case of moving fluid mesh)
   if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update!=-1)
    {   
     // Are all the element's nodes upstream or downstream of 
     // radial control line?
     unsigned upstream_count=0;
     unsigned downstream_count=0;
     double nnod=el_pt->nnode();
     for (unsigned j=0;j<nnod;j++)
      {
       double z=el_pt->node_pt(j)->x(1);
       if (z>=Z_radial_cut_through_syrinx*Global_Physical_Variables::Z_scale)
        {
         upstream_count++;
        }
       if (z<=Z_radial_cut_through_syrinx*Global_Physical_Variables::Z_scale)
        {
         downstream_count++;
        }
      }
     
     // Add to the appropriate mesh
     if (upstream_count==nnod)
      {
       Upstream_SSS_el_pt.push_back(el_pt);
       if (downstream_count==nnod)
        {
         throw OomphLibError(
          "Element is both upstream and downstream! Something's wrong.",
          OOMPH_CURRENT_FUNCTION,
          OOMPH_EXCEPTION_LOCATION);
        }
      }
     else if (downstream_count==nnod)
      {
       Downstream_SSS_el_pt.push_back(el_pt);
      }
     else
      {
       std::ostringstream error_stream;
       error_stream
        << "Element is neither upstream nor downstream! Something's wrong.",
        throw OomphLibError(
         error_stream.str(),
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
      }
    }
  }

 // Output upstream finer region near block in SSS mesh:
 {
  ofstream some_file;
  std::string name;
  name=Global_Physical_Variables::Directory+
   "/sss_upstream_near_block_region.dat";

  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.open(name.c_str());
   }
  unsigned r=SSS_upstream_near_block_region_id;
  unsigned nel=dynamic_cast<TriangleMesh<FLUID_ELEMENT>*>(SSS_fluid_mesh_pt)->
   nregion_element(r);
  for(unsigned e=0;e<nel;e++)
   {
    // Cast to a bulk element
    FLUID_ELEMENT *el_pt=dynamic_cast<FLUID_ELEMENT*>(
     dynamic_cast<TriangleMesh<FLUID_ELEMENT>*>(SSS_fluid_mesh_pt)
     ->region_element_pt(r,e));
    
    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      // Output region just using vertices
      el_pt->output(some_file,2);
     }
   }

  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.close();
   }
 }


 // Output downstream finer region near block in SSS mesh:
 {
  ofstream some_file;
  std::string name;
  name=Global_Physical_Variables::Directory+
   "/sss_downstream_near_block_region.dat";
  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.open(name.c_str());
   }
  unsigned r=SSS_downstream_near_block_region_id;
  unsigned nel=dynamic_cast<TriangleMesh<FLUID_ELEMENT>*>(SSS_fluid_mesh_pt)->
   nregion_element(r);
  for(unsigned e=0;e<nel;e++)
   {
    // Cast to a bulk element
    FLUID_ELEMENT *el_pt=dynamic_cast<FLUID_ELEMENT*>(
     dynamic_cast<TriangleMesh<FLUID_ELEMENT>*>(SSS_fluid_mesh_pt)
     ->region_element_pt(r,e));
    
    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      // Output region just using vertices
      el_pt->output(some_file,2);
     }
   }

  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.close();
   }
 }


 // Output upper central finer region near block in SSS mesh:
 {
  ofstream some_file;
  std::string name;
  name=Global_Physical_Variables::Directory+
   "/sss_central_upper_near_block_region.dat";
  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.open(name.c_str());
   }
  
  unsigned r=SSS_central_and_bl_upper_near_block_region_id;
  unsigned nel=dynamic_cast<TriangleMesh<FLUID_ELEMENT>*>(SSS_fluid_mesh_pt)->
   nregion_element(r);
  for(unsigned e=0;e<nel;e++)
   {
    // Cast to a bulk element
    FLUID_ELEMENT *el_pt=dynamic_cast<FLUID_ELEMENT*>(
     dynamic_cast<TriangleMesh<FLUID_ELEMENT>*>(SSS_fluid_mesh_pt)
     ->region_element_pt(r,e));
    
    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      // Output region just using vertices
      el_pt->output(some_file,2);
     }
   }

  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.close();
   }
 }


 // Output lower central finer region near block in SSS mesh:
 {
  ofstream some_file;
  std::string name;
  name=Global_Physical_Variables::Directory+
   "/sss_central_lower_near_block_region.dat";
  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.open(name.c_str());
   }
  unsigned r=SSS_central_and_bl_lower_near_block_region_id;
  unsigned nel=dynamic_cast<TriangleMesh<FLUID_ELEMENT>*>(SSS_fluid_mesh_pt)->
   nregion_element(r);
  for(unsigned e=0;e<nel;e++)
   {
    // Cast to a bulk element
    FLUID_ELEMENT *el_pt=dynamic_cast<FLUID_ELEMENT*>(
     dynamic_cast<TriangleMesh<FLUID_ELEMENT>*>(SSS_fluid_mesh_pt)
     ->region_element_pt(r,e));
    
    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      // Output region just using vertices
      el_pt->output(some_file,2);
     }
   }
  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.close();
   }
 }


 // ------------------------------------------------------------------------

 // Boundary conditions for the inner poroelasticity mesh
 // ----------------------------------------------------
 
 if (CommandLineArgs::command_line_flag_has_been_set
     ("--pin_cord"))
  {
   oomph_info << "\n\nPINNING CORD\n\n";
   unsigned num_nod=Inner_poro_mesh_pt->nnode();
   for(unsigned inod=0;inod<num_nod;inod++)
    {
     Node* nod_pt=Inner_poro_mesh_pt->node_pt(inod);
     unsigned nval=nod_pt->nvalue();
     for (unsigned j=0;j<nval;j++)
      {
       nod_pt->pin(j);
      }
    }
   unsigned nel=Inner_poro_mesh_pt->nelement();
   for (unsigned e=0;e<nel;e++)
    {
     FiniteElement* el_pt=Inner_poro_mesh_pt->finite_element_pt(e);
     unsigned ninternal=el_pt->ninternal_data();
     for (unsigned i=0;i<ninternal;i++)
      {
       Data* dat_pt=el_pt->internal_data_pt(i);
       unsigned nval=dat_pt->nvalue();
       for (unsigned j=0;j<nval;j++)
        {
         dat_pt->pin(j);
        }
      }
    }
  }


 // Axisymmetric conditions on these boundaries
 {

  // Vector of the boundaries we will pin on the inner poro mesh
  Vector<unsigned> inner_poro_pinned_boundaries;
  inner_poro_pinned_boundaries.push_back(
   Inner_poro_symmetry_near_inlet_boundary_id);
  inner_poro_pinned_boundaries.push_back(
   Inner_poro_symmetry_near_outlet_boundary_id);

  // Get the number of boundaries from the vector
  unsigned n_b=inner_poro_pinned_boundaries.size();
  
  // Set boundary conditions for the inner poro mesh
  for(unsigned ibound=0;ibound<n_b;ibound++)
   {
    unsigned num_nod=Inner_poro_mesh_pt->nboundary_node(
     inner_poro_pinned_boundaries[ibound]);
    for(unsigned inod=0;inod<num_nod;inod++)
     {
      // Get pointer to node
      Node* nod_pt=Inner_poro_mesh_pt->boundary_node_pt(
       inner_poro_pinned_boundaries[ibound],inod);
      
      // Axisymmetric conditions and u_z = 0 on r = 0
      
      // Pin u_r
      nod_pt->pin(0);
      
      // If this node stores edge flux dofs, pin these
      if(nod_pt->nvalue()==4)
       {
        nod_pt->pin(2); 
        nod_pt->pin(3); 
       }
      
     } // end_of_loop_over_nodes
   } // end_of_loop_over_inner_poro_boundaries
 }
 
 // No porous flux or solid displacement on these
 {
  Vector<unsigned> inner_poro_pinned_boundaries;
  inner_poro_pinned_boundaries.push_back(Inner_poro_inlet_boundary_id);
  inner_poro_pinned_boundaries.push_back(Inner_poro_outlet_boundary_id);
  
  // Get the number of boundaries from the vector
  unsigned n_b=inner_poro_pinned_boundaries.size();
  
  // Set boundary conditions for the inner poro mesh
  for(unsigned ibound=0;ibound<n_b;ibound++)
   {
    unsigned num_nod=Inner_poro_mesh_pt->nboundary_node(
     inner_poro_pinned_boundaries[ibound]);
    for(unsigned inod=0;inod<num_nod;inod++)
     {
      // Get pointer to node
      Node* nod_pt=Inner_poro_mesh_pt->boundary_node_pt(
       inner_poro_pinned_boundaries[ibound],inod);
      
      // No porous flux or solid displacement on the inlet and outlet
      
      // Pin u_r, u_z
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--radially_fixed_poro"))
       {
        nod_pt->pin(0);
       }
      nod_pt->pin(1);
           
      // If this node stores edge flux dofs, pin these
      if(nod_pt->nvalue()==4)
       {
        nod_pt->pin(2);
        nod_pt->pin(3);
       }
      
     } // end_of_loop_over_nodes
   } // end_of_loop_over_inner_poro_boundaries
 }

 // Boundary conditions for the outer poroelasticity mesh
 // --------------------------------------------------------

 // Vector of the boundaries we will pin on the outer poro mesh
 std::vector<unsigned> outer_poro_pinned_boundaries;

 // No porous flux or solid displacement on these boundaries
 outer_poro_pinned_boundaries.push_back(Outer_poro_inlet_boundary_id);
 outer_poro_pinned_boundaries.push_back(Outer_poro_outlet_boundary_id);

 // Get the number of boundaries from the vector
 unsigned n_b=outer_poro_pinned_boundaries.size();

 // Set boundary conditions for the outer poro mesh
 for(unsigned ibound=0;ibound<n_b;ibound++)
  {
   unsigned num_nod=Outer_poro_mesh_pt->nboundary_node(
    outer_poro_pinned_boundaries[ibound]);
   for(unsigned inod=0;inod<num_nod;inod++)
    {
     // Get pointer to node
     Node* nod_pt=Outer_poro_mesh_pt->boundary_node_pt(
      outer_poro_pinned_boundaries[ibound],inod);

     // Pin in r and z
     if (CommandLineArgs::command_line_flag_has_been_set
         ("--radially_fixed_poro"))
      {
       nod_pt->pin(0);
      }
     nod_pt->pin(1);
     
     // If this node stores edge flux dofs, pin these
     if(nod_pt->nvalue()==4)
      {
       nod_pt->pin(2);
       nod_pt->pin(3);
      }
    }
  } // end_of_loop_over_outer_poro_boundaries


 // Boundary conditions for the SSS fluid mesh
 // ------------------------------------------

 // Inlet
 {
  unsigned ibound=SSS_fluid_inflow_boundary_id;
  unsigned num_nod=SSS_fluid_mesh_pt->nboundary_node(ibound);
  for(unsigned inod=0;inod<num_nod;inod++)
   {
    // Get pointer to node
    Node* nod_pt=SSS_fluid_mesh_pt->boundary_node_pt(ibound,inod);
    
    // Pin radial velocity (parallel in/outflow)
    if (CommandLineArgs::command_line_flag_has_been_set
        ("--pin_radial_veloc_at_in_and_outlet"))
     {
      nod_pt->pin(0);
     }

    // If the nod is also on either the inner or outer poro-elsatic 
    // boundary then pin the axial velocity too
    if( (nod_pt->is_on_boundary(
          SSS_fluid_outer_poro_interface_near_inlet_boundary_id))||
        (nod_pt->is_on_boundary(
         SSS_fluid_upper_inner_poro_interface_boundary_id)))
     {
      nod_pt->pin(0);
      if (!CommandLineArgs::command_line_flag_has_been_set
          ("--radially_fixed_poro"))
       {
        nod_pt->unpin(0);
       }
      nod_pt->pin(1);
     }
   }
 }

 // Outlet -- pin everything
 {
  unsigned ibound=SSS_fluid_outflow_boundary_id;
  unsigned num_nod=SSS_fluid_mesh_pt->nboundary_node(ibound);
  for(unsigned inod=0;inod<num_nod;inod++)
   {
    // Get pointer to node
    Node* nod_pt=SSS_fluid_mesh_pt->boundary_node_pt(ibound,inod);
    
    // Pin radial and axial velocity
    if (CommandLineArgs::command_line_flag_has_been_set
        ("--pin_radial_veloc_at_in_and_outlet"))
     {
      nod_pt->pin(0);
     }
    nod_pt->pin(1);
    
    // If the nod is also on either the inner or outer poro-elastic 
    // boundary then unpin the radial velocity
    if( (nod_pt->is_on_boundary(
          SSS_fluid_outer_poro_interface_near_outlet_boundary_id))||
        (nod_pt->is_on_boundary(
         SSS_fluid_lower_inner_poro_interface_boundary_id)))
     {
      nod_pt->pin(0);
      if (!CommandLineArgs::command_line_flag_has_been_set
          ("--radially_fixed_poro"))
       {
        nod_pt->unpin(0);
       }
     }
   }
 }


 // Boundary conditions for the syrinx fluid mesh
 // ---------------------------------------------

 // Vector of the boundaries we will pin on the syrinx fluid mesh
 std::vector<unsigned> syrinx_fluid_pinned_boundaries;

 // Add the symmetry boundary to the vector
 syrinx_fluid_pinned_boundaries.push_back(Syrinx_fluid_symmetry_boundary_id);

 // Get the number of boundaries from the vector
 n_b=syrinx_fluid_pinned_boundaries.size();

 // Set the boundary conditions on the syrinx fluid mesh
 for(unsigned ibound=0;ibound<n_b;ibound++)
  {
   unsigned num_nod=Syrinx_fluid_mesh_pt->nboundary_node(
    syrinx_fluid_pinned_boundaries[ibound]);
   for(unsigned inod=0;inod<num_nod;inod++)
    {
     // Get pointer to node
     Node* nod_pt=Syrinx_fluid_mesh_pt->boundary_node_pt(
      syrinx_fluid_pinned_boundaries[ibound],inod);

     // Pin u_r
     nod_pt->pin(0);
    }
  }

 // ----------------------------------------------------------------------------

 // Pin relevant Lagrange multipliers in the
 // syrinx fluid <-> inner poro FSI surface meshes
  
 {
  Vector<Mesh*> fsi_mesh_pt;
  fsi_mesh_pt.push_back(Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt);
  fsi_mesh_pt.push_back(Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt);
  fsi_mesh_pt.push_back(Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt);
  unsigned nm=fsi_mesh_pt.size();
  for (unsigned m=0;m<nm;m++)
   {
    // Loop over the elements
    unsigned nel=fsi_mesh_pt[m]->nelement(); 
    for(unsigned e=0;e<nel;e++)
     {
      // Get a face element
      LinearisedAxisymPoroelasticBJS_FSIElement
       <FLUID_ELEMENT,PORO_ELEMENT>*
       traction_element_pt=
       dynamic_cast<LinearisedAxisymPoroelasticBJS_FSIElement
       <FLUID_ELEMENT,PORO_ELEMENT>*>
       (fsi_mesh_pt[m]->element_pt(e));
      
      // Get the number of nodes in element e
      unsigned n_node=traction_element_pt->nnode();
      
      // Loop over the nodes
      for(unsigned n=0;n<n_node;n++)
       {
        // Get a node
        Node *nod_pt=traction_element_pt->node_pt(n);
        
        // Cast to a boundary node
        BoundaryNodeBase *bnod_pt=dynamic_cast<BoundaryNodeBase*>(nod_pt);
        
        // Get the index of the first nodal value associated with this
        // FaceElement
        unsigned first_index=
         bnod_pt->index_of_first_value_assigned_by_face_element(
          Lagrange_id_syrinx_fluid_inner_poro);
        
        // Pin swirl component of Lagrange multiplier
        nod_pt->pin(first_index+2);
        
        // Pin radial component if on the symmetry boundary
        if(bnod_pt->is_on_boundary(Syrinx_fluid_symmetry_boundary_id))
         {       
          nod_pt->pin(first_index+0);
         }
       } //end_of_loop_over_nodes
     } //end_of_loop_over_elements
   }
 }

 // Pin relevant Lagrange multipliers in the
 // SSS fluid <-> inner poro FSI surface mesh
 

 Vector<Mesh*> fsi_mesh_pt;
 fsi_mesh_pt.push_back(SSS_fluid_upper_inner_poro_FSI_surface_mesh_pt);
 fsi_mesh_pt.push_back(SSS_fluid_central_inner_poro_FSI_surface_mesh_pt);
 fsi_mesh_pt.push_back(SSS_fluid_lower_inner_poro_FSI_surface_mesh_pt);
 unsigned nm=fsi_mesh_pt.size();
 for (unsigned m=0;m<nm;m++)
  {
   // Loop over the elements
   unsigned nel=fsi_mesh_pt[m]->nelement(); 
   for(unsigned e=0;e<nel;e++)
    {
     // Get a face element
     LinearisedAxisymPoroelasticBJS_FSIElement
      <FLUID_ELEMENT,PORO_ELEMENT>*
      traction_element_pt=dynamic_cast<LinearisedAxisymPoroelasticBJS_FSIElement
      <FLUID_ELEMENT,PORO_ELEMENT>*>
      (fsi_mesh_pt[m]->element_pt(e));
      
     // Get the number of nodes in element e
     unsigned n_node=traction_element_pt->nnode();
      
     // Loop over the nodes
     for(unsigned n=0;n<n_node;n++)
      {
       // Get a node
       Node *nod_pt=traction_element_pt->node_pt(n);
        
       // Cast to a boundary node
       BoundaryNodeBase *bnod_pt=dynamic_cast<BoundaryNodeBase*>(nod_pt);
        
       // Get the index of the first nodal value associated with this
       // FaceElement
       unsigned first_index=
        bnod_pt->index_of_first_value_assigned_by_face_element(
         Lagrange_id_SSS_fluid_inner_poro);
        
       // Pin swirl component of Lagrange multiplier
       nod_pt->pin(first_index+2);
        
       // Pin radial and axial component if on inflow boundary
       if(bnod_pt->is_on_boundary(SSS_fluid_inflow_boundary_id))
        {
         if (CommandLineArgs::command_line_flag_has_been_set
             ("--radially_fixed_poro"))
          {
           nod_pt->pin(first_index+0);
          }
         nod_pt->pin(first_index+1);
        }
        
       // Pin radial and axial component if on outflow boundary
       if(bnod_pt->is_on_boundary(SSS_fluid_outflow_boundary_id))
        {
         if (CommandLineArgs::command_line_flag_has_been_set
             ("--radially_fixed_poro"))
          {
           nod_pt->pin(first_index+0);
          }
         nod_pt->pin(first_index+1);
        }
      } //end_of_loop_over_nodes
    } //end_of_loop_over_elements
  }


 // Pin relevant Lagrange multipliers in each of the 
 // SSS fluid <-> outer poro FSI surface meshes [non-block]
 Vector<Mesh*> tmp_mesh_pt;
 tmp_mesh_pt.push_back(SSS_fluid_outer_poro_near_inlet_FSI_surface_mesh_pt);
 tmp_mesh_pt.push_back(SSS_fluid_outer_poro_near_outlet_FSI_surface_mesh_pt);
 unsigned n_mesh=tmp_mesh_pt.size();
 
 // Loop over the meshes
 for(unsigned i=0;i<n_mesh;i++)
  {
   // Get the number of elements
   unsigned nel=tmp_mesh_pt[i]->nelement();
   
   // Loop over the elements
   for(unsigned e=0;e<nel;e++)
    {
     // Get a face element
     LinearisedAxisymPoroelasticBJS_FSIElement
      <FLUID_ELEMENT,PORO_ELEMENT>*
      traction_element_pt=dynamic_cast<LinearisedAxisymPoroelasticBJS_FSIElement
      <FLUID_ELEMENT,PORO_ELEMENT>*>
      (tmp_mesh_pt[i]->element_pt(e));
     
     // Get the number of nodes in element e
     unsigned n_node=traction_element_pt->nnode();
     
     // Loop over the nodes
     for(unsigned n=0;n<n_node;n++)
      {
       // Get a node
       Node *nod_pt=traction_element_pt->node_pt(n);
       
       // Cast to a boundary node
       BoundaryNodeBase *bnod_pt=dynamic_cast<BoundaryNodeBase*>(nod_pt);
       
       // Get the index of the first nodal value associated with this
       // FaceElement
       unsigned first_index=
        bnod_pt->index_of_first_value_assigned_by_face_element(
         Lagrange_id_SSS_fluid_outer_poro);
       
       // Pin swirl component of Lagrange multiplier
       nod_pt->pin(first_index+2);
       
       // Pin radial and axial component if on inflow boundary
       if(bnod_pt->is_on_boundary(SSS_fluid_inflow_boundary_id))
        {
         if (CommandLineArgs::command_line_flag_has_been_set
             ("--radially_fixed_poro"))
          {
           nod_pt->pin(first_index+0);
          }
         nod_pt->pin(first_index+1);
        }
       
       // Pin radial and axial component if on outflow boundary
       if(bnod_pt->is_on_boundary(SSS_fluid_outflow_boundary_id))
        {
         if (CommandLineArgs::command_line_flag_has_been_set
             ("--radially_fixed_poro"))
          {
           nod_pt->pin(first_index+0);
          }
         nod_pt->pin(first_index+1);
        }
      } //end_of_loop_over_nodes
    } //end_of_loop_over_elements
  } //end_of_loop_over_meshes


 // Pin relevant Lagrange multipliers in the
 // SSS fluid <-> inner poro FSI surface mesh [block]
 
 // Loop over the elements
 unsigned nel=SSS_fluid_outer_poro_block_FSI_surface_mesh_pt->nelement();
 for(unsigned e=0;e<nel;e++)
  {
   // Get a face element
   LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>*
    traction_element_pt=dynamic_cast<LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>*>
    (SSS_fluid_outer_poro_block_FSI_surface_mesh_pt->element_pt(e));
   
   // Get the number of nodes in element e
   unsigned n_node=traction_element_pt->nnode();
   
   // Loop over the nodes
   for(unsigned n=0;n<n_node;n++)
    {
     // Get a node
     Node *nod_pt=traction_element_pt->node_pt(n);
     
     // Cast to a boundary node
     BoundaryNodeBase *bnod_pt=dynamic_cast<BoundaryNodeBase*>(nod_pt);
     
     // Get the index of the first nodal value associated with this
     // FaceElement
     unsigned first_index=
      bnod_pt->index_of_first_value_assigned_by_face_element(
       Lagrange_id_SSS_fluid_outer_poro);
     
     // Pin swirl component of Lagrange multiplier
     nod_pt->pin(first_index+2);
     
    } //end_of_loop_over_nodes
  } //end_of_loop_over_elements

 
 // -----------------------------------------------------------------------

 // Setup FSI
 setup_fsi();

}



//===start_of_delete_face_elements==============================
/// Delete face elements
//===================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
delete_face_elements()
{
 Vector<Mesh*> all_face_mesh_pt;
 all_face_mesh_pt.push_back(Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Syrinx_volume_analysis_mesh_pt);
 all_face_mesh_pt.push_back(Downstream_SSS_volume_analysis_mesh_pt);
 all_face_mesh_pt.push_back(Upstream_SSS_volume_analysis_mesh_pt);
 all_face_mesh_pt.push_back(Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Outer_poro_SSS_fluid_near_inlet_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Outer_poro_SSS_fluid_near_outlet_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Outer_poro_SSS_fluid_block_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Inflow_fluid_surface_mesh_pt);
 all_face_mesh_pt.push_back(Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(SSS_fluid_upper_inner_poro_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(SSS_fluid_central_inner_poro_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(SSS_fluid_lower_inner_poro_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(SSS_fluid_outer_poro_near_inlet_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(SSS_fluid_outer_poro_near_outlet_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(SSS_fluid_outer_poro_block_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Syrinx_veloc_line_mesh_pt);
 all_face_mesh_pt.push_back(SSS_inner_fsi_boundary_veloc_line_mesh_pt);
 all_face_mesh_pt.push_back(Syrinx_centreline_veloc_line_mesh_pt);
 all_face_mesh_pt.push_back(SSS_gap_under_block_veloc_line_mesh_pt);
 all_face_mesh_pt.push_back(Inner_poro_radial_line_mesh_pt);
 all_face_mesh_pt.push_back(SSS_veloc_line_mesh_pt);
 all_face_mesh_pt.push_back(Test_volume_flux_out_of_sss_mesh_pt);
 all_face_mesh_pt.push_back(Syrinx_volume_from_upper_syrinx_mesh_pt);
 all_face_mesh_pt.push_back(Syrinx_volume_from_lower_syrinx_mesh_pt);
 // Don't do this one because it's storing elements that are stored
 // elsewhere too; we're flushing it below...
 //all_face_mesh_pt.push_back(Syrinx_volume_from_line_through_syrinx_mesh_pt);
 unsigned n=all_face_mesh_pt.size();
 for (unsigned m=0;m<n;m++)
  {
   Mesh* my_mesh_pt=all_face_mesh_pt[m];
   unsigned nel=my_mesh_pt->nelement();
   for (unsigned e=0;e<nel;e++)
    {
     delete my_mesh_pt->element_pt(e);
    }
   my_mesh_pt->flush_element_and_node_storage();
  }

 // Just flush...
 Syrinx_volume_from_line_through_syrinx_mesh_pt->flush_element_and_node_storage();

}



//===================================================================
//// Compute upstream and downstream syrinx volumes via line integrals  
/// on syrinx mesh
//===================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
compute_syrinx_volumes_from_boundary_integral(
 double& upper_syrinx_volume_from_syrinx_boundary_integral,
 double& lower_syrinx_volume_from_syrinx_boundary_integral,
 double& total_syrinx_volume_from_syrinx_boundary_integral)
{
 double upper_syrinx_volume_boundary_term=0.0;
 {
  unsigned nel = Syrinx_volume_from_upper_syrinx_mesh_pt->nelement();
  for(unsigned e=0;e<nel;e++)
   {
    AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*
     el_pt=dynamic_cast<
     AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*>
     (Syrinx_volume_from_upper_syrinx_mesh_pt->element_pt(e));
    
    upper_syrinx_volume_boundary_term+=el_pt->contribution_to_enclosed_volume();
   }
 } 
 double lower_syrinx_volume_boundary_term=0.0;
 {
  unsigned nel = Syrinx_volume_from_lower_syrinx_mesh_pt->nelement();
  for(unsigned e=0;e<nel;e++)
   {
    AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*
     el_pt=dynamic_cast<
     AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*>
     (Syrinx_volume_from_lower_syrinx_mesh_pt->element_pt(e));
    
    lower_syrinx_volume_boundary_term+=el_pt->contribution_to_enclosed_volume();
   }
 } 
 double cut_through_syrinx_volume_boundary_term=0.0;
 {
  unsigned nel = Syrinx_volume_from_line_through_syrinx_mesh_pt->nelement();
  for(unsigned e=0;e<nel;e++)
   {
    AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*
     el_pt=dynamic_cast<
     AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*>
     (Syrinx_volume_from_line_through_syrinx_mesh_pt->element_pt(e));
    
    cut_through_syrinx_volume_boundary_term+=el_pt->contribution_to_enclosed_volume();
   }
 } 
 
 
 // oomph_info << "RAW UPPER, LOWER, CENTRAL SYRINX VOLUME LINE TERMS AND TOTAL VOLUME: " 
 //            << upper_syrinx_volume_boundary_term << " "
 //            << lower_syrinx_volume_boundary_term << " "
 //            << cut_through_syrinx_volume_boundary_term << " "
 //            << upper_syrinx_volume_boundary_term+lower_syrinx_volume_boundary_term << " "
 //            << std::endl;
 // oomph_info << "UPPER, LOWER SYRINX VOLUME FROM LINE INTEGRALS (+): "
 //            << upper_syrinx_volume_boundary_term+cut_through_syrinx_volume_boundary_term << " " 
 //            << lower_syrinx_volume_boundary_term+cut_through_syrinx_volume_boundary_term << " "
 //            << std::endl;
 // oomph_info << "UPPER, LOWER SYRINX VOLUME FROM LINE INTEGRALS (-): "
 //            << upper_syrinx_volume_boundary_term-cut_through_syrinx_volume_boundary_term << " " 
 //            << lower_syrinx_volume_boundary_term-cut_through_syrinx_volume_boundary_term << " "
 //            << std::endl;
 
 upper_syrinx_volume_from_syrinx_boundary_integral=
  upper_syrinx_volume_boundary_term+cut_through_syrinx_volume_boundary_term;
 
 lower_syrinx_volume_from_syrinx_boundary_integral=
  lower_syrinx_volume_boundary_term-cut_through_syrinx_volume_boundary_term;
 
 total_syrinx_volume_from_syrinx_boundary_integral=
  upper_syrinx_volume_boundary_term+lower_syrinx_volume_boundary_term;
 
}


//===start_of_create_poro_face_elements==============================
/// Make poro face elements
//===================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
create_poro_face_elements()
{

 // ----------------------------------
 // Create the poro FSI surface meshes
 // ----------------------------------

 // Inner poro mesh (from syrinx fluid mesh)
 // ---------------------------------------
 Vector<unsigned> fsi_interface;
 fsi_interface.push_back(Inner_poro_lower_syrinx_interface_boundary_id);
 fsi_interface.push_back(Inner_poro_central_syrinx_interface_boundary_id);
 fsi_interface.push_back(Inner_poro_upper_syrinx_interface_boundary_id);
 unsigned nb=fsi_interface.size();
 for (unsigned bb=0;bb<nb;bb++)
  {
   // Now loop over the bulk elements and create the face elements
   unsigned b=fsi_interface[bb];
   unsigned nel = Inner_poro_mesh_pt->nboundary_element(b);
   for(unsigned e=0;e<nel;e++)
    {
      // Create the face element that allows location of point on FSI
      // boundary of poro mesh based on z coordinate
      EulerianBasedFaceElement<PORO_ELEMENT>* geom_face_element_pt=
       new EulerianBasedFaceElement<PORO_ELEMENT>
       (Inner_poro_mesh_pt->boundary_element_pt(b,e),
        Inner_poro_mesh_pt->face_index_at_boundary(b,e));
      
      // Add to mesh
      Syrinx_FSI_geometry_mesh_pt->add_element_pt(
       geom_face_element_pt);

     // Create the face element that applies fluid traction to poroelastic
     // solid
     FSILinearisedAxisymPoroelasticTractionElement
      <PORO_ELEMENT,FLUID_ELEMENT>* face_element_pt=
      new FSILinearisedAxisymPoroelasticTractionElement
      <PORO_ELEMENT,FLUID_ELEMENT>
      (Inner_poro_mesh_pt->boundary_element_pt(b,e),
       Inner_poro_mesh_pt->face_index_at_boundary(b,e));
     
     // Add to the appropriate surface mesh
     switch (b)
      {
      case Inner_poro_upper_syrinx_interface_boundary_id:
       Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt->
        add_element_pt(face_element_pt);
       break;

      case Inner_poro_central_syrinx_interface_boundary_id:
       Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt->
        add_element_pt(face_element_pt);
       break;

      case Inner_poro_lower_syrinx_interface_boundary_id:
       Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt->
        add_element_pt(face_element_pt);
       break;
       
      default: 
       throw OomphLibError(
        "Wrong boundary id",
        OOMPH_CURRENT_FUNCTION,
        OOMPH_EXCEPTION_LOCATION);
      }

     // Set the FSI parameter
     face_element_pt->q_pt()=&Global_Physical_Variables::Q;
     
     // Associate element with bulk boundary (to allow it to access
     // the boundary coordinates in the bulk mesh)
     face_element_pt->set_boundary_number_in_bulk_mesh(b);

     // Create the face element that allows postprocessing
     AxisymmetricPoroelasticityLinePostProcessingElement
      <PORO_ELEMENT>* analysis_element_pt=
      new AxisymmetricPoroelasticityLinePostProcessingElement
      <PORO_ELEMENT>(Inner_poro_mesh_pt->boundary_element_pt(b,e),
                     Inner_poro_mesh_pt->face_index_at_boundary(b,e));
     
     // Add element
     Syrinx_volume_analysis_mesh_pt->add_element_pt(analysis_element_pt);
    }
  }
 
 // Inner poro mesh (from SSS fluid mesh)
 // -------------------------------------
 {
  Vector<unsigned> fsi_interface;
  fsi_interface.push_back(Inner_poro_upper_SSS_interface_boundary_id);
  fsi_interface.push_back(Inner_poro_central_SSS_interface_boundary_id);
  fsi_interface.push_back(Inner_poro_lower_SSS_interface_boundary_id);
  unsigned nb=fsi_interface.size();
  for (unsigned bb=0;bb<nb;bb++)
   {
    // Now loop over the bulk elements and create the face elements
    unsigned b=fsi_interface[bb];
    unsigned nel = Inner_poro_mesh_pt->nboundary_element(b);
    for(unsigned e=0;e<nel;e++)
     {
      // Create the face element that allows location of point on FSI
      // boundary of poro mesh based on z coordinate
      EulerianBasedFaceElement<PORO_ELEMENT>* geom_face_element_pt=
       new EulerianBasedFaceElement<PORO_ELEMENT>
       (Inner_poro_mesh_pt->boundary_element_pt(b,e),
        Inner_poro_mesh_pt->face_index_at_boundary(b,e));
      
      // Add to mesh
      Inner_SSS_FSI_geometry_mesh_pt->add_element_pt(
       geom_face_element_pt);


      // Create the face element that applies fluid traction to poroelastic
      // solid
      FSILinearisedAxisymPoroelasticTractionElement
       <PORO_ELEMENT,FLUID_ELEMENT>* face_element_pt=
       new FSILinearisedAxisymPoroelasticTractionElement
       <PORO_ELEMENT,FLUID_ELEMENT>
       (Inner_poro_mesh_pt->boundary_element_pt(b,e),
        Inner_poro_mesh_pt->face_index_at_boundary(b,e));
      
      // Add to the appropriate surface mesh
      switch (b)
       {
       case Inner_poro_upper_SSS_interface_boundary_id:
        Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt->
         add_element_pt(face_element_pt);
        break;
        
       case Inner_poro_central_SSS_interface_boundary_id:
        Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt->
         add_element_pt(face_element_pt);
        break;
        
       case Inner_poro_lower_SSS_interface_boundary_id:
        Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt->
         add_element_pt(face_element_pt);
        break;
        
       default: 
        throw OomphLibError(
         "Wrong boundary id",
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
       }
      
      // Set the FSI parameter
      face_element_pt->q_pt()=&Global_Physical_Variables::Q;
      
      // Associate element with bulk boundary (to allow it to access
      // the boundary coordinates in the bulk mesh)
      face_element_pt->set_boundary_number_in_bulk_mesh(b);
      
      // Create the face element that allows postprocessing of volume
      AxisymmetricPoroelasticityLinePostProcessingElement
       <PORO_ELEMENT>* analysis_element_pt=
       new AxisymmetricPoroelasticityLinePostProcessingElement
       <PORO_ELEMENT>(Inner_poro_mesh_pt->boundary_element_pt(b,e),
                      Inner_poro_mesh_pt->face_index_at_boundary(b,e));
      
      
      // Are all the element's nodes upstream or downstream of 
      // radial control line?
      unsigned upstream_count=0;
      unsigned downstream_count=0;
      double nnod=analysis_element_pt->nnode();
      for (unsigned j=0;j<nnod;j++)
       {
        double z=analysis_element_pt->node_pt(j)->x(1);
        if (z>=Z_radial_cut_through_syrinx*Global_Physical_Variables::Z_scale)
         {
          upstream_count++;
         }
        if (z<=Z_radial_cut_through_syrinx*Global_Physical_Variables::Z_scale)
         {
          downstream_count++;
         }
       }

      // Add to the appropriate mesh
      if (upstream_count==nnod)
       {
        Upstream_SSS_volume_analysis_mesh_pt->
         add_element_pt(analysis_element_pt);
        if (downstream_count==nnod)
         {
          throw OomphLibError(
           "Element is both upstream and downstream! Something's wrong.",
           OOMPH_CURRENT_FUNCTION,
           OOMPH_EXCEPTION_LOCATION);
         }
       }
      else if (downstream_count==nnod)
       {
        Downstream_SSS_volume_analysis_mesh_pt->
         add_element_pt(analysis_element_pt);
       }
      else
       {
        std::ostringstream error_stream;
        error_stream
         << "Element is neither upstream nor downstream! Something's wrong.",
         throw OomphLibError(
          error_stream.str(),
          OOMPH_CURRENT_FUNCTION,
          OOMPH_EXCEPTION_LOCATION);
       }
     }
   }
 }




 // Post-processing elements along radial line through syrinx cover
 // ---------------------------------------------------------------
 {
  unsigned b=Inner_poro_radial_line_boundary_id;
  unsigned nel = Inner_poro_mesh_pt->nboundary_element(b);
  for(unsigned e=0;e<nel;e++)
   {
    // Create the face element that allows postprocessing
    AxisymmetricPoroelasticityTractionElement
     <PORO_ELEMENT>* analysis_element_pt=
     new AxisymmetricPoroelasticityTractionElement
     <PORO_ELEMENT>(Inner_poro_mesh_pt->boundary_element_pt(b,e),
                    Inner_poro_mesh_pt->face_index_at_boundary(b,e));
    
    // We're attaching FaceElements on both sides of this internal
    // boundary! Only keep the one whose outer unit normal points
    // in the negative z direction
    
    // Get the outer unit normal
    Vector<double> s(1,0.5);
    Vector<double> interpolated_normal(2);
    analysis_element_pt->outer_unit_normal(s,interpolated_normal);
    
    // Add element?
    if (interpolated_normal[1]<0.0)
     {
      Inner_poro_radial_line_mesh_pt->add_element_pt(analysis_element_pt);
     }
    else
     {
      delete analysis_element_pt;
     }
   }
 }

 // Outer poro mesh near inlet (from SSS fluid mesh)
 // ------------------------------------------------
 unsigned b = Outer_poro_SSS_interface_near_inlet_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 unsigned nel = Outer_poro_mesh_pt->nboundary_element(b);
 for(unsigned e=0;e<nel;e++)
  {

   // Create the face element that allows location of point on FSI
   // boundary of poro mesh based on z coordinate
   EulerianBasedFaceElement<PORO_ELEMENT>* geom_face_element_pt=
    new EulerianBasedFaceElement<PORO_ELEMENT>
    (Outer_poro_mesh_pt->boundary_element_pt(b,e),
     Outer_poro_mesh_pt->face_index_at_boundary(b,e));
   
   // Add to mesh
   Outer_SSS_FSI_geometry_mesh_pt->add_element_pt(
    geom_face_element_pt);


   // Create the face element that applies fluid traction to poroelastic
   // solid
   FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>* face_element_pt=
    new FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>
    (Outer_poro_mesh_pt->boundary_element_pt(b,e),
     Outer_poro_mesh_pt->face_index_at_boundary(b,e));
   
   // Add to mesh
   Outer_poro_SSS_fluid_near_inlet_FSI_surface_mesh_pt->add_element_pt(
    face_element_pt);
   
   // Set the FSI parameter
   face_element_pt->q_pt()=&Global_Physical_Variables::Q;
   
   // Associate element with bulk boundary (to allow it to access
   // the boundary coordinates in the bulk mesh)
   face_element_pt->set_boundary_number_in_bulk_mesh(b);

   // Create the face element that allows postprocessing of volume
   AxisymmetricPoroelasticityLinePostProcessingElement
    <PORO_ELEMENT>* analysis_element_pt=
    new AxisymmetricPoroelasticityLinePostProcessingElement
    <PORO_ELEMENT>(Outer_poro_mesh_pt->boundary_element_pt(b,e),
                   Outer_poro_mesh_pt->face_index_at_boundary(b,e));
   
   // Add to mesh
   Upstream_SSS_volume_analysis_mesh_pt->
    add_element_pt(analysis_element_pt);
  }


 // Outer poro mesh near outlet (from SSS fluid mesh)
 // ------------------------------------------------
 b = Outer_poro_SSS_interface_near_outlet_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = Outer_poro_mesh_pt->nboundary_element(b);
 for(unsigned e=0;e<nel;e++)
  {

   // Create the face element that allows location of point on FSI
   // boundary of poro mesh based on z coordinate
   EulerianBasedFaceElement<PORO_ELEMENT>* geom_face_element_pt=
    new EulerianBasedFaceElement<PORO_ELEMENT>
    (Outer_poro_mesh_pt->boundary_element_pt(b,e),
     Outer_poro_mesh_pt->face_index_at_boundary(b,e));
   
   // Add to mesh
   Outer_SSS_FSI_geometry_mesh_pt->add_element_pt(
    geom_face_element_pt);


   // Create the face element that applies fluid traction to poroelastic
   // solid
   FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>* face_element_pt=
    new FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>
    (Outer_poro_mesh_pt->boundary_element_pt(b,e),
     Outer_poro_mesh_pt->face_index_at_boundary(b,e));
   
   // Add to mesh
   Outer_poro_SSS_fluid_near_outlet_FSI_surface_mesh_pt->add_element_pt(
    face_element_pt);
   
   // Set the FSI parameter
   face_element_pt->q_pt()=&Global_Physical_Variables::Q;
   
   // Associate element with bulk boundary (to allow it to access
   // the boundary coordinates in the bulk mesh)
   face_element_pt->set_boundary_number_in_bulk_mesh(b);

   // Create the face element that allows postprocessing of volume
   AxisymmetricPoroelasticityLinePostProcessingElement
    <PORO_ELEMENT>* analysis_element_pt=
    new AxisymmetricPoroelasticityLinePostProcessingElement
    <PORO_ELEMENT>(Outer_poro_mesh_pt->boundary_element_pt(b,e),
                   Outer_poro_mesh_pt->face_index_at_boundary(b,e));
   
   // Add to mesh
   Downstream_SSS_volume_analysis_mesh_pt->
    add_element_pt(analysis_element_pt);
  }


 // Outer poro mesh (block) (from SSS fluid mesh)
 // ---------------------------------------------
 b = Outer_poro_SSS_interface_block_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = Outer_poro_mesh_pt->nboundary_element(b);
 for(unsigned e=0;e<nel;e++)
  {

   // Create the face element that allows location of point on FSI
   // boundary of poro mesh based on z coordinate
   EulerianBasedFaceElement<PORO_ELEMENT>* geom_face_element_pt=
    new EulerianBasedFaceElement<PORO_ELEMENT>
    (Outer_poro_mesh_pt->boundary_element_pt(b,e),
     Outer_poro_mesh_pt->face_index_at_boundary(b,e));
   
   // Add to mesh
   Outer_SSS_FSI_geometry_mesh_pt->add_element_pt(
    geom_face_element_pt);

   // Create the face element that applies fluid traction to poroelastic
   // solid
   FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>* face_element_pt=
    new FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>
    (Outer_poro_mesh_pt->boundary_element_pt(b,e),
     Outer_poro_mesh_pt->face_index_at_boundary(b,e));
   
   // Add to mesh
   Outer_poro_SSS_fluid_block_FSI_surface_mesh_pt->add_element_pt(
    face_element_pt);
   
   // Set the FSI parameter
   face_element_pt->q_pt()=&Global_Physical_Variables::Q;
   
   // Associate element with bulk boundary (to allow it to access
   // the boundary coordinates in the bulk mesh)
   face_element_pt->set_boundary_number_in_bulk_mesh(b);
   
   // Create the face element that allows postprocessing of volume
   AxisymmetricPoroelasticityLinePostProcessingElement
    <PORO_ELEMENT>* analysis_element_pt=
    new AxisymmetricPoroelasticityLinePostProcessingElement
    <PORO_ELEMENT>(Outer_poro_mesh_pt->boundary_element_pt(b,e),
                   Outer_poro_mesh_pt->face_index_at_boundary(b,e));
   
   // Are all the element's nodes upstream or downstream of 
   // radial control line?
   unsigned upstream_count=0;
   unsigned downstream_count=0;
   double nnod=analysis_element_pt->nnode();
   for (unsigned j=0;j<nnod;j++)
    {
     double z=analysis_element_pt->node_pt(j)->x(1);
     if (z>=Z_radial_cut_through_syrinx*Global_Physical_Variables::Z_scale)
      {
       upstream_count++;
      }
     if (z<=Z_radial_cut_through_syrinx*Global_Physical_Variables::Z_scale)
      {
       downstream_count++;
      }
    }
   
   // Add to the appropriate mesh
   if (upstream_count==nnod)
    {
     Upstream_SSS_volume_analysis_mesh_pt->
      add_element_pt(analysis_element_pt);
     if (downstream_count==nnod)
      {
       throw OomphLibError(
        "Element is both upstream and downstream! Something's wrong.",
        OOMPH_CURRENT_FUNCTION,
        OOMPH_EXCEPTION_LOCATION);
      }
    }
   else if (downstream_count==nnod)
    {
     Downstream_SSS_volume_analysis_mesh_pt->
      add_element_pt(analysis_element_pt);
    }
   else
    {
     std::ostringstream error_stream;
     error_stream
      << "Element is neither upstream nor downstream! Something's wrong.",
      throw OomphLibError(
       error_stream.str(),
       OOMPH_CURRENT_FUNCTION,
       OOMPH_EXCEPTION_LOCATION);
    }
  }

} // end_of_create_poro_face_elements



//==start_of_create_fluid_face_elements===============================
/// Create the fluid traction elements
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
create_fluid_face_elements()
{

 // Inflow:
 //--------
 unsigned bound=SSS_fluid_inflow_boundary_id;

 // Now loop over bulk elements and create the face elements
 unsigned nel=SSS_fluid_mesh_pt->nboundary_element(bound);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that applies pressure boundary condition
   // at inflow
   AxisymmetricNavierStokesTractionElement<FLUID_ELEMENT>* traction_element_pt
    = new AxisymmetricNavierStokesTractionElement<FLUID_ELEMENT>
    (SSS_fluid_mesh_pt->boundary_element_pt(bound,e),
     SSS_fluid_mesh_pt->face_index_at_boundary(bound,e));

   // Add to mesh
   Inflow_fluid_surface_mesh_pt->add_element_pt(traction_element_pt);

   // Set the applied traction
   traction_element_pt->traction_fct_pt() =
    &Global_Physical_Variables::fluid_inflow_boundary_traction;

   // Create the face element that allows postprocessing of volume
   AxisymmetricPoroelasticityLinePostProcessingElement
    <PORO_ELEMENT>* analysis_element_pt=
    new AxisymmetricPoroelasticityLinePostProcessingElement
    <PORO_ELEMENT>
    (SSS_fluid_mesh_pt->boundary_element_pt(bound,e),
     SSS_fluid_mesh_pt->face_index_at_boundary(bound,e));
   
   // Add to mesh
   Upstream_SSS_volume_analysis_mesh_pt->
    add_element_pt(analysis_element_pt);
   
   // ...but change direction of normal (to make it consistent with 
   // the normals of the elements attached to the solid elements -- they
   // point into the fluid volume!
   analysis_element_pt->set_negative_normal();
  }

 // Outflow
 //--------
 bound=SSS_fluid_outflow_boundary_id;

 // Now loop over bulk elements and create the face elements
 nel=SSS_fluid_mesh_pt->nboundary_element(bound);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that allows postprocessing of volume
   AxisymmetricPoroelasticityLinePostProcessingElement
    <PORO_ELEMENT>* analysis_element_pt=
    new AxisymmetricPoroelasticityLinePostProcessingElement
    <PORO_ELEMENT>
    (SSS_fluid_mesh_pt->boundary_element_pt(bound,e),
     SSS_fluid_mesh_pt->face_index_at_boundary(bound,e));
   
   // Add to mesh
   Downstream_SSS_volume_analysis_mesh_pt->
    add_element_pt(analysis_element_pt);
   
   // ...but change direction of normal (to make it consistent with 
   // the normals of the elements attached to the solid elements -- they
   // point into the fluid volume!
   analysis_element_pt->set_negative_normal();
  }


 // -----------------------------------
 // Create the fluid FSI surface meshes
 // -----------------------------------

 // Syrinx fluid mesh (from inner poro mesh)
 // ----------------------------------------
 Vector<unsigned> fsi_interface;
 fsi_interface.push_back(Syrinx_fluid_upper_inner_poro_interface_boundary_id);
 fsi_interface.push_back(Syrinx_fluid_central_inner_poro_interface_boundary_id);
 fsi_interface.push_back(Syrinx_fluid_lower_inner_poro_interface_boundary_id);
 unsigned nb=fsi_interface.size();
 for (unsigned bb=0;bb<nb;bb++)
  {
   // Now loop over the bulk elements and create the face elements
   unsigned bound=fsi_interface[bb];
   unsigned nel = Syrinx_fluid_mesh_pt->nboundary_element(bound);
   for(unsigned e=0;e<nel;e++)
    {
     // Create the face element that imposes Beavers Joseph Saffman condition
     // at FSI interface
     LinearisedAxisymPoroelasticBJS_FSIElement
      <FLUID_ELEMENT,PORO_ELEMENT>*
      traction_element_pt=new
      LinearisedAxisymPoroelasticBJS_FSIElement
      <FLUID_ELEMENT,PORO_ELEMENT>
      (Syrinx_fluid_mesh_pt->boundary_element_pt(bound,e),
       Syrinx_fluid_mesh_pt->face_index_at_boundary(bound,e),
       Lagrange_id_syrinx_fluid_inner_poro);
     

     // Add to the appropriate surface mesh
     switch (bound)
      {
      case Syrinx_fluid_upper_inner_poro_interface_boundary_id:
       Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt->add_element_pt(
        traction_element_pt);
       break;

      case Syrinx_fluid_central_inner_poro_interface_boundary_id:
       Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt->add_element_pt(
        traction_element_pt);
       break;

      case Syrinx_fluid_lower_inner_poro_interface_boundary_id:
       Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt->add_element_pt(
        traction_element_pt);
       break;

      default: 
       throw OomphLibError(
        "Wrong boundary id",
        OOMPH_CURRENT_FUNCTION,
        OOMPH_EXCEPTION_LOCATION);
      }

     // Associate element with bulk boundary (to allow it to access the boundary
     // coordinates in the bulk mesh)
     traction_element_pt->set_boundary_number_in_bulk_mesh(bound);
     
     // Set Strouhal number
     traction_element_pt->st_pt()=&Global_Physical_Variables::St;
     
     // Set inverse slip coefficient
     traction_element_pt->inverse_slip_rate_coefficient_pt()=
      &Global_Physical_Variables::Inverse_slip_rate_coefficient;
    }
  }

   
 // SSS fluid mesh (from inner poro mesh)
 // ------------------------------------ 
 {
  Vector<unsigned> fsi_interface;
  fsi_interface.push_back(SSS_fluid_upper_inner_poro_interface_boundary_id);
  fsi_interface.push_back(SSS_fluid_central_inner_poro_interface_boundary_id);
  fsi_interface.push_back(SSS_fluid_lower_inner_poro_interface_boundary_id);
  unsigned nb=fsi_interface.size();
  for (unsigned bb=0;bb<nb;bb++)
   {
    // Now loop over the bulk elements and create the face elements
    unsigned bound=fsi_interface[bb];
    unsigned nel = SSS_fluid_mesh_pt->nboundary_element(bound);
    for(unsigned e=0;e<nel;e++)
     {
      // Create the face element that imposes Beavers Joseph Saffman condition
      // at FSI interface
      LinearisedAxisymPoroelasticBJS_FSIElement
       <FLUID_ELEMENT,PORO_ELEMENT>*
       traction_element_pt=new
       LinearisedAxisymPoroelasticBJS_FSIElement
       <FLUID_ELEMENT,PORO_ELEMENT>
       (SSS_fluid_mesh_pt->boundary_element_pt(bound,e),
        SSS_fluid_mesh_pt->face_index_at_boundary(bound,e),
        Lagrange_id_SSS_fluid_inner_poro);
      
      // Add to the appropriate surface mesh
      switch (bound)
       {
       case SSS_fluid_upper_inner_poro_interface_boundary_id:
        SSS_fluid_upper_inner_poro_FSI_surface_mesh_pt->add_element_pt(
         traction_element_pt);
        break;

       case SSS_fluid_central_inner_poro_interface_boundary_id:
        SSS_fluid_central_inner_poro_FSI_surface_mesh_pt->add_element_pt(
         traction_element_pt);
        break;

       case SSS_fluid_lower_inner_poro_interface_boundary_id:
        SSS_fluid_lower_inner_poro_FSI_surface_mesh_pt->add_element_pt(
         traction_element_pt);
        break;

       default: 
        throw OomphLibError(
         "Wrong boundary id",
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
       }
      
      // Associate element with bulk boundary (to allow it to access the boundary
      // coordinates in the bulk mesh)
      traction_element_pt->set_boundary_number_in_bulk_mesh(bound);
      
      // Set Strouhal number
      traction_element_pt->st_pt()=&Global_Physical_Variables::St;
      
      // Set inverse slip coefficient
      traction_element_pt->inverse_slip_rate_coefficient_pt()=
       &Global_Physical_Variables::Inverse_slip_rate_coefficient;


      // Create the face element that allows post-processing of veloc             
      //--------------------------------------------------------------
      AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*
       plot_element_pt=new
       AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>
       (SSS_fluid_mesh_pt->boundary_element_pt(bound,e),
        SSS_fluid_mesh_pt->face_index_at_boundary(bound,e));

      // Add element?                                                             
      SSS_inner_fsi_boundary_veloc_line_mesh_pt->add_element_pt(plot_element_pt);

     }
   }
 }
 

 // SSS fluid mesh near inlet (from outer poro mesh)
 // ------------------------------------------------
 bound=SSS_fluid_outer_poro_interface_near_inlet_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = SSS_fluid_mesh_pt->nboundary_element(bound);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that imposes Beavers Joseph Saffman condition at
   // FSI interface
   LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>*
    traction_element_pt=new
    LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>
    (SSS_fluid_mesh_pt->boundary_element_pt(bound,e),
     SSS_fluid_mesh_pt->face_index_at_boundary(bound,e),
     Lagrange_id_SSS_fluid_outer_poro);
   
   // Add to mesh
   SSS_fluid_outer_poro_near_inlet_FSI_surface_mesh_pt->add_element_pt(
    traction_element_pt);
   
   // Associate element with bulk boundary (to allow it to access the boundary
   // coordinates in the bulk mesh)
   traction_element_pt->set_boundary_number_in_bulk_mesh(bound);
   
   // Set Strouhal number
   traction_element_pt->st_pt()=&Global_Physical_Variables::St;
   
   // Set inverse slip coefficient
   traction_element_pt->inverse_slip_rate_coefficient_pt()=
    &Global_Physical_Variables::Inverse_slip_rate_coefficient;
  }


 // SSS fluid mesh near outlet (from outer poro mesh)
 // --------------------------------------------------
 bound=SSS_fluid_outer_poro_interface_near_outlet_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = SSS_fluid_mesh_pt->nboundary_element(bound);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that imposes Beavers Joseph Saffman condition at
   // FSI interface
   LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>*
    traction_element_pt=new
    LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>
    (SSS_fluid_mesh_pt->boundary_element_pt(bound,e),
     SSS_fluid_mesh_pt->face_index_at_boundary(bound,e),
     Lagrange_id_SSS_fluid_outer_poro);
   
   // Add to mesh
   SSS_fluid_outer_poro_near_outlet_FSI_surface_mesh_pt->add_element_pt(
    traction_element_pt);
   
   // Associate element with bulk boundary (to allow it to access the boundary
   // coordinates in the bulk mesh)
   traction_element_pt->set_boundary_number_in_bulk_mesh(bound);
   
   // Set Strouhal number
   traction_element_pt->st_pt()=&Global_Physical_Variables::St;
   
   // Set inverse slip coefficient
   traction_element_pt->inverse_slip_rate_coefficient_pt()=
    &Global_Physical_Variables::Inverse_slip_rate_coefficient;
  }


 // SSS fluid mesh (block) (from outer poro mesh)
 // --------------------------------------------------
 bound=SSS_fluid_outer_poro_interface_block_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = SSS_fluid_mesh_pt->nboundary_element(bound);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that imposes Beavers Joseph Saffman condition at
   // FSI interface
   LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>*
    traction_element_pt=new
    LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>
    (SSS_fluid_mesh_pt->boundary_element_pt(bound,e),
     SSS_fluid_mesh_pt->face_index_at_boundary(bound,e),
     Lagrange_id_SSS_fluid_outer_poro);
   
   // Add to mesh
   SSS_fluid_outer_poro_block_FSI_surface_mesh_pt->add_element_pt(
    traction_element_pt);
   
   // Associate element with bulk boundary (to allow it to access the boundary
   // coordinates in the bulk mesh)
   traction_element_pt->set_boundary_number_in_bulk_mesh(bound);
   
   // Set Strouhal number
   traction_element_pt->st_pt()=&Global_Physical_Variables::St;
   
   // Set inverse slip coefficient
   traction_element_pt->inverse_slip_rate_coefficient_pt()=
    &Global_Physical_Variables::Inverse_slip_rate_coefficient;
  }


 //Attach veloc visualisation elements along radial line through SSS under block
 //------------------- --------------------------------------------------------
 {
  unsigned bound=SSS_gap_under_block_veloc_line_boundary_id;
  unsigned nel = SSS_fluid_mesh_pt->nboundary_element(bound);
  for(unsigned e=0;e<nel;e++)
   {
    // Create the face element that allows post-processing of veloc
    AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*
     plot_element_pt=new
     AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>
     (SSS_fluid_mesh_pt->boundary_element_pt(bound,e),
      SSS_fluid_mesh_pt->face_index_at_boundary(bound,e));
   
    // Create the face element that allows postprocessing of volume
    // Note: Build with poro-element as template argument.
    // Function checks if bulk element is actually a solid
    // element and, if not, sets displacement to zero
    AxisymmetricPoroelasticityLinePostProcessingElement
     <PORO_ELEMENT>* analysis_element_pt=
     new AxisymmetricPoroelasticityLinePostProcessingElement<PORO_ELEMENT>
     (SSS_fluid_mesh_pt->boundary_element_pt(bound,e),
      SSS_fluid_mesh_pt->face_index_at_boundary(bound,e));
     
    // We're attaching FaceElements on both sides of this internal
    // boundary! Only keep the one whose outer unit normal points
    // in the negative z direction
    
    // Get the outer unit normal
    Vector<double> s(1,0.5);
    Vector<double> interpolated_normal(2);
    plot_element_pt->outer_unit_normal(s,interpolated_normal);
    
    // Outer unit normal points in negative z direction: We've attached
    // an element to the upstream bulk element
    if (interpolated_normal[1]<0.0)
     {
      // Add element
      SSS_gap_under_block_veloc_line_mesh_pt->add_element_pt(plot_element_pt);

      //Add element  to upstram mesh
      Upstream_SSS_volume_analysis_mesh_pt->
       add_element_pt(analysis_element_pt);

      // ...but change direction of normal (to make it consistent with 
      // the normals of the elements attached to the solid elements -- they
      // point into the fluid volume!
      analysis_element_pt->set_negative_normal();
     }
    // Outer unit normal points in positive z direction: We've attached
    // an element to the downstream bulk element
    else
     {
      // Kill element; we only want flux through line once
      delete plot_element_pt;

      //Add element to downstream mesh 
      Downstream_SSS_volume_analysis_mesh_pt->
       add_element_pt(analysis_element_pt);

      // ...but change direction of normal (to make it consistent with 
      // the normals of the elements attached to the solid elements -- they
      // point into the fluid volume!
      analysis_element_pt->set_negative_normal();
     }
   }
 }
 


 // Attach veloc visualisation elements along radial line across syrinx
 // -------------------------------------------------------------------
 {
  unsigned bound=Syrinx_veloc_extract_boundary_id;
  unsigned nel = Syrinx_fluid_mesh_pt->nboundary_element(bound);
  for(unsigned e=0;e<nel;e++)
   {
    // Create the face element that allows post-processing of veloc
    AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*
     plot_element_pt=new
     AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>
     (Syrinx_fluid_mesh_pt->boundary_element_pt(bound,e),
      Syrinx_fluid_mesh_pt->face_index_at_boundary(bound,e));
    
    // We're attaching FaceElements on both sides of this internal
    // boundary! Only keep the one whose outer unit normal points
    // in the negative z direction
    
    // Get the outer unit normal
    Vector<double> s(1,0.5);
    Vector<double> interpolated_normal(2);
    plot_element_pt->outer_unit_normal(s,interpolated_normal);
    
    // Add element?
    if (interpolated_normal[1]<0.0)
     {
      Syrinx_veloc_line_mesh_pt->add_element_pt(plot_element_pt);
      Syrinx_volume_from_line_through_syrinx_mesh_pt->add_element_pt(plot_element_pt);
     }
    else
     {
      delete plot_element_pt;
     }
   }
 }

 // Attach post-processing elements to fsi boundary of syrinx
 //----------------------------------------------------------
 {
  // Upper bit
  {
   unsigned bound=Syrinx_fluid_upper_inner_poro_interface_boundary_id;
   unsigned nel = Syrinx_fluid_mesh_pt->nboundary_element(bound);
   for(unsigned e=0;e<nel;e++)
    {
     // Create the face element that allows post-processing of veloc
     AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*
      plot_element_pt=new
      AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>
      (Syrinx_fluid_mesh_pt->boundary_element_pt(bound,e),
       Syrinx_fluid_mesh_pt->face_index_at_boundary(bound,e));
     
     // Add element?
     Syrinx_volume_from_upper_syrinx_mesh_pt->add_element_pt(plot_element_pt);
    }
  }
  
  // Lower bit
  {
   unsigned bound=Syrinx_fluid_lower_inner_poro_interface_boundary_id;
   unsigned nel = Syrinx_fluid_mesh_pt->nboundary_element(bound);
   for(unsigned e=0;e<nel;e++)
    {
     // Create the face element that allows post-processing of veloc
     AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*
      plot_element_pt=new
      AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>
      (Syrinx_fluid_mesh_pt->boundary_element_pt(bound,e),
       Syrinx_fluid_mesh_pt->face_index_at_boundary(bound,e));
     
     // Add element?
     Syrinx_volume_from_lower_syrinx_mesh_pt->add_element_pt(plot_element_pt);
   }
  }

   
  // Central bit -- added to upper or lower
  {
   unsigned bound=Syrinx_fluid_central_inner_poro_interface_boundary_id;
   unsigned nel = Syrinx_fluid_mesh_pt->nboundary_element(bound);
   for(unsigned e=0;e<nel;e++)
    {
     // Create the face element that allows post-processing of veloc
     AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*
      plot_element_pt=new
      AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>
      (Syrinx_fluid_mesh_pt->boundary_element_pt(bound,e),
       Syrinx_fluid_mesh_pt->face_index_at_boundary(bound,e));
     
     // Are all the element's nodes upstream or downstream of 
     // radial control line?
     unsigned upstream_count=0;
     unsigned downstream_count=0;
     double nnod=plot_element_pt->nnode();
     for (unsigned j=0;j<nnod;j++)
      {
       double z=plot_element_pt->node_pt(j)->x(1);
       if (z>=Z_radial_cut_through_syrinx*Global_Physical_Variables::Z_scale)
        {
         upstream_count++;
        }
       if (z<=Z_radial_cut_through_syrinx*Global_Physical_Variables::Z_scale)
        {
         downstream_count++;
        }
      }
     
     // Add to the appropriate mesh
     if (upstream_count==nnod)
      {
       Syrinx_volume_from_upper_syrinx_mesh_pt->add_element_pt(plot_element_pt);
       if (downstream_count==nnod)
        {
         throw OomphLibError(
          "Element is both upstream and downstream! Something's wrong.",
          OOMPH_CURRENT_FUNCTION,
          OOMPH_EXCEPTION_LOCATION);
        }
      }
     else if (downstream_count==nnod)
      {
       Syrinx_volume_from_lower_syrinx_mesh_pt->add_element_pt(plot_element_pt);
      }
     else
      {
       std::ostringstream error_stream;
       error_stream
        << "Element is neither upstream nor downstream! Something's wrong.",
        throw OomphLibError(
         error_stream.str(),
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
      }
    }
  }
 }


 // Attach veloc visualisation elements along centreline (r=0) of syrinx
 // --------------------------------------------------------------------
 // (mainly used for average pressure)
 //-----------------------------------
 {
  unsigned bound=Syrinx_fluid_symmetry_boundary_id;
  unsigned nel = Syrinx_fluid_mesh_pt->nboundary_element(bound);
  for(unsigned e=0;e<nel;e++)
   {
    // Create the face element that allows post-processing of veloc
    AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*
     plot_element_pt=new
     AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>
     (Syrinx_fluid_mesh_pt->boundary_element_pt(bound,e),
      Syrinx_fluid_mesh_pt->face_index_at_boundary(bound,e));
    
    // Add element?
    Syrinx_centreline_veloc_line_mesh_pt->add_element_pt(plot_element_pt);
   }
 }



 // Attach veloc visualisation elements along entire boundary
 // ---------------------------------------------------------
 // of SSS mesh to check that the flux adds up to zero (validation
 //---------------------------------------------------------------
 // of volume flux computation)
 //----------------------------
 {
  Vector<unsigned> sss_mesh_boundary;
  sss_mesh_boundary.push_back(
   SSS_fluid_outer_poro_interface_block_boundary_id);
  sss_mesh_boundary.push_back(
   SSS_fluid_outer_poro_interface_near_outlet_boundary_id);
  sss_mesh_boundary.push_back(
   SSS_fluid_outflow_boundary_id);
  sss_mesh_boundary.push_back(
   SSS_fluid_lower_inner_poro_interface_boundary_id);
  sss_mesh_boundary.push_back(
   SSS_fluid_central_inner_poro_interface_boundary_id);
  sss_mesh_boundary.push_back(
   SSS_fluid_upper_inner_poro_interface_boundary_id); 
  sss_mesh_boundary.push_back(
   SSS_fluid_inflow_boundary_id);
  sss_mesh_boundary.push_back(
   SSS_fluid_outer_poro_interface_near_inlet_boundary_id);
  unsigned nb=sss_mesh_boundary.size();
  for (unsigned b=0;b<nb;b++)
   {
    unsigned bound=sss_mesh_boundary[b];
    unsigned nel = SSS_fluid_mesh_pt->nboundary_element(bound);
    for(unsigned e=0;e<nel;e++)
     {
      // Create the face element that allows post-processing of veloc
      AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*
       plot_element_pt=new
       AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>
       (SSS_fluid_mesh_pt->boundary_element_pt(bound,e),
        SSS_fluid_mesh_pt->face_index_at_boundary(bound,e));
      
      // Add to mesh
      Test_volume_flux_out_of_sss_mesh_pt->add_element_pt(plot_element_pt);
     }
   }
 }

 // Attach veloc visualisation elements along inlet
 // -----------------------------------------------
 {
  unsigned bound=SSS_fluid_inflow_boundary_id;
  unsigned nel = SSS_fluid_mesh_pt->nboundary_element(bound);
  for(unsigned e=0;e<nel;e++)
   {
    // Create the face element that allows post-processing of veloc
    AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>*
     plot_element_pt=new
     AxisymmetricNavierStokesLinePostProcessingElement<FLUID_ELEMENT>
     (SSS_fluid_mesh_pt->boundary_element_pt(bound,e),
      SSS_fluid_mesh_pt->face_index_at_boundary(bound,e));
    
    // Add to mesh
    SSS_veloc_line_mesh_pt->add_element_pt(plot_element_pt);
   }
 }

} // end_of_create_fluid_face_elements

//==start_of_setup_fsi====================================================
/// Setup interaction between the poroelasticity and fluid meshes
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
setup_fsi()
{

 // Inner poro <-> Syrinx fluid coupling
 // -----------------------------------

 // Setup syrinx fluid traction on inner poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   Syrinx_fluid_lower_inner_poro_interface_boundary_id,
   Syrinx_fluid_mesh_pt,
   Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt);
 
 // Setup syrinx fluid traction on inner poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   Syrinx_fluid_central_inner_poro_interface_boundary_id,
   Syrinx_fluid_mesh_pt,
   Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt);

 // Setup syrinx fluid traction on inner poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   Syrinx_fluid_upper_inner_poro_interface_boundary_id,
   Syrinx_fluid_mesh_pt,
   Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt);



//--

 // Set up volume conserving mass transfer in bjs boundary condition
 if (!CommandLineArgs::command_line_flag_has_been_set
     ("--suppress_volume_conserving_flux_transfer_in_bjs"))
  {

   oomph_info << "Going in for setup of volume conserving mass transfer..." 
              << std::endl;
   
   // Set face elements for mass conserving transfer of seepage flux
   unsigned geom_interaction_index=1;
   Multi_domain_functions::setup_multi_domain_interaction
    <FSILinearisedAxisymPoroelasticTractionElement
     <PORO_ELEMENT,FLUID_ELEMENT> >
    (this,
     Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt,
     Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt,
     geom_interaction_index);
   
   // Set face elements for mass conserving transfer of seepage flux
   Multi_domain_functions::setup_multi_domain_interaction
    <FSILinearisedAxisymPoroelasticTractionElement
     <PORO_ELEMENT,FLUID_ELEMENT> >
    (this,
     Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt,
     Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt,
     geom_interaction_index);
   
   // Set face elements for mass conserving transfer of seepage flux
   Multi_domain_functions::setup_multi_domain_interaction
    <FSILinearisedAxisymPoroelasticTractionElement
     <PORO_ELEMENT,FLUID_ELEMENT> >
    (this,
     Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt,
     Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt,
     geom_interaction_index);

   oomph_info << "... back!" << std::endl;

  }

//--


 // Setup BJS bc for syrinx fluid from inner poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Inner_poro_upper_syrinx_interface_boundary_id,
   Inner_poro_mesh_pt,
   Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt);

 // Setup BJS bc for syrinx fluid from inner poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Inner_poro_central_syrinx_interface_boundary_id,
   Inner_poro_mesh_pt,
   Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt);

 // Setup BJS bc for syrinx fluid from inner poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Inner_poro_lower_syrinx_interface_boundary_id,
   Inner_poro_mesh_pt,
   Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt);
 

 // Inner poro <-> SSS fluid coupling
 // --------------------------------

 // Setup SSS fluid traction on inner poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   SSS_fluid_upper_inner_poro_interface_boundary_id,
   SSS_fluid_mesh_pt,
   Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt);

 // Setup SSS fluid traction on inner poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   SSS_fluid_central_inner_poro_interface_boundary_id,
   SSS_fluid_mesh_pt,
   Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt);

 // Setup SSS fluid traction on inner poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   SSS_fluid_lower_inner_poro_interface_boundary_id,
   SSS_fluid_mesh_pt,
   Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt);


//-- new bit --

 // Set up volume conserving mass transfer in bjs boundary condition
 if (!CommandLineArgs::command_line_flag_has_been_set
     ("--suppress_volume_conserving_flux_transfer_in_bjs"))
  {

   oomph_info << "Going in for setup of volume conserving mass transfer..." 
              << std::endl;
   
   // Set face elements for mass conserving transfer of seepage flux
   unsigned geom_interaction_index=1;
   Multi_domain_functions::setup_multi_domain_interaction
    <FSILinearisedAxisymPoroelasticTractionElement
     <PORO_ELEMENT,FLUID_ELEMENT> >
    (this,
     SSS_fluid_upper_inner_poro_FSI_surface_mesh_pt,
     Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt,
     geom_interaction_index);
   
   // Set face elements for mass conserving transfer of seepage flux
   Multi_domain_functions::setup_multi_domain_interaction
    <FSILinearisedAxisymPoroelasticTractionElement
     <PORO_ELEMENT,FLUID_ELEMENT> >
    (this,
     SSS_fluid_central_inner_poro_FSI_surface_mesh_pt,
     Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt,
     geom_interaction_index);
   
   // Set face elements for mass conserving transfer of seepage flux
   Multi_domain_functions::setup_multi_domain_interaction
    <FSILinearisedAxisymPoroelasticTractionElement
     <PORO_ELEMENT,FLUID_ELEMENT> >
    (this,
     SSS_fluid_lower_inner_poro_FSI_surface_mesh_pt,
     Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt,
     geom_interaction_index);

   oomph_info << "... back!" << std::endl;

  }

//--


 if (CommandLineArgs::command_line_flag_has_been_set
              ("--everything_is_porous"))
  {
   if (!CommandLineArgs::command_line_flag_has_been_set
       ("--pin_darcy"))
    {
     throw OomphLibError(
      "Haven't set up mass conserving bjs for outer poro\n",
      OOMPH_CURRENT_FUNCTION,
      OOMPH_EXCEPTION_LOCATION);
    }
  }

 // Setup BJS bc for SSS fluid from inner poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Inner_poro_upper_SSS_interface_boundary_id,
   Inner_poro_mesh_pt,
   SSS_fluid_upper_inner_poro_FSI_surface_mesh_pt);

 // Setup BJS bc for SSS fluid from inner poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Inner_poro_central_SSS_interface_boundary_id,
   Inner_poro_mesh_pt,
   SSS_fluid_central_inner_poro_FSI_surface_mesh_pt);

 // Setup BJS bc for SSS fluid from inner poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Inner_poro_lower_SSS_interface_boundary_id,
   Inner_poro_mesh_pt,
   SSS_fluid_lower_inner_poro_FSI_surface_mesh_pt);


 // Outer poro <-> SSS fluid coupling
 // ---------------------------------

 // Near inlet

 // Setup SSS fluid traction on outer poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   SSS_fluid_outer_poro_interface_near_inlet_boundary_id,
   SSS_fluid_mesh_pt,
   Outer_poro_SSS_fluid_near_inlet_FSI_surface_mesh_pt);


 // Setup BJS bc for SSS fluid from outer poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Outer_poro_SSS_interface_near_inlet_boundary_id,
   Outer_poro_mesh_pt,
   SSS_fluid_outer_poro_near_inlet_FSI_surface_mesh_pt);


 // Near outlet

 // Setup SSS fluid traction on outer poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   SSS_fluid_outer_poro_interface_near_outlet_boundary_id,
   SSS_fluid_mesh_pt,
   Outer_poro_SSS_fluid_near_outlet_FSI_surface_mesh_pt);

 // Setup BJS bc for SSS fluid from outer poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Outer_poro_SSS_interface_near_outlet_boundary_id,
   Outer_poro_mesh_pt,
   SSS_fluid_outer_poro_near_outlet_FSI_surface_mesh_pt);



 // Block

 // Setup SSS fluid traction on outer poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   SSS_fluid_outer_poro_interface_block_boundary_id,
   SSS_fluid_mesh_pt,
   Outer_poro_SSS_fluid_block_FSI_surface_mesh_pt);

 // Setup BJS bc for SSS fluid from outer poro wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Outer_poro_SSS_interface_block_boundary_id,
   Outer_poro_mesh_pt,
   SSS_fluid_outer_poro_block_FSI_surface_mesh_pt);



}




//========================================================================
/// Do only steady run for inner poro for zero flux
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
steady_inner_poro_only_for_zero_flux_run(DocInfo& doc_info)
{


 // Avoid accepting (non-)solutions with small initial residuals;
 // the problem is linear!
 Problem::Always_take_one_newton_step=true;


 // Pin everything apart from inner poro mesh
 //------------------------------------------
 Vector<Mesh*> all_pin_mesh_pt;
 all_pin_mesh_pt.push_back(Outer_poro_mesh_pt);
 all_pin_mesh_pt.push_back(Syrinx_fluid_mesh_pt);
 all_pin_mesh_pt.push_back(SSS_fluid_mesh_pt);

 unsigned npin=all_pin_mesh_pt.size();
 for (unsigned m=0;m<npin;m++)
  {
   Mesh* mesh_pt=all_pin_mesh_pt[m];
   unsigned nnod=mesh_pt->nnode();
   for (unsigned j=0;j<nnod;j++)
    {
     Node* nod_pt=mesh_pt->node_pt(j);
     unsigned nval=nod_pt->nvalue();
     for (unsigned i=0;i<nval;i++)
      {
       nod_pt->pin(i);
      }
    }
   unsigned nel=mesh_pt->nelement();
   for (unsigned e=0;e<nel;e++)
    {
     GeneralisedElement*el_pt=mesh_pt->element_pt(e);
     unsigned nint=el_pt->ninternal_data();
     for (unsigned j=0;j<nint;j++)
      {
       Data* data_pt=el_pt->internal_data_pt(j);      
       unsigned nval=data_pt->nvalue();
       for (unsigned i=0;i<nval;i++)
        {
         data_pt->pin(i);
        }
      }
    }
  }


 // Create the non-FSI surface traction meshes
 // ------------------------------------------


 // Inner poro from syrinx
 {
  Inner_poro_syrinx_non_fsi_traction_mesh_pt=new Mesh;
  
  // Inner poro mesh (from syrinx fluid mesh)
  Vector<unsigned> fsi_interface;
  fsi_interface.push_back(Inner_poro_lower_syrinx_interface_boundary_id);
  fsi_interface.push_back(Inner_poro_central_syrinx_interface_boundary_id);
  fsi_interface.push_back(Inner_poro_upper_syrinx_interface_boundary_id);
  unsigned nb=fsi_interface.size();
  for (unsigned bb=0;bb<nb;bb++)
   {
    // Now loop over the bulk elements and create the face elements
    unsigned b=fsi_interface[bb];
    unsigned nel = Inner_poro_mesh_pt->nboundary_element(b);
    for(unsigned e=0;e<nel;e++)
     {
      // Create the face element that applies steady traction to poroelastic
      // solid
      AxisymmetricPoroelasticityTractionElement
       <PORO_ELEMENT>* face_element_pt=
       new AxisymmetricPoroelasticityTractionElement
       <PORO_ELEMENT>
       (Inner_poro_mesh_pt->boundary_element_pt(b,e),
        Inner_poro_mesh_pt->face_index_at_boundary(b,e));
      
      // Add to the surface mesh
      Inner_poro_syrinx_non_fsi_traction_mesh_pt->
       add_element_pt(face_element_pt);
      
      // Set the traction/pressure function pointers
      face_element_pt->pressure_fct_pt()=
       &Global_Physical_Variables::steady_state_syrinx_pressure;
      face_element_pt->traction_fct_pt()=
       &Global_Physical_Variables::steady_state_syrinx_traction;
      
      // Associate element with bulk boundary (to allow it to access
      // the boundary coordinates in the bulk mesh)
      face_element_pt->set_boundary_number_in_bulk_mesh(b);
     }
   }
  
  add_sub_mesh(Inner_poro_syrinx_non_fsi_traction_mesh_pt);
 }


 // Inner poro from sss
 {
  Inner_poro_sss_non_fsi_traction_mesh_pt=new Mesh;
  
  Vector<unsigned> fsi_interface;
  fsi_interface.push_back(Inner_poro_upper_SSS_interface_boundary_id);
  fsi_interface.push_back(Inner_poro_central_SSS_interface_boundary_id);
  fsi_interface.push_back(Inner_poro_lower_SSS_interface_boundary_id);
  unsigned nb=fsi_interface.size();
  for (unsigned bb=0;bb<nb;bb++)
   {
    // Now loop over the bulk elements and create the face elements
    unsigned b=fsi_interface[bb];
    unsigned nel = Inner_poro_mesh_pt->nboundary_element(b);
    for(unsigned e=0;e<nel;e++)
     {
      // Create the face element that applies steady traction to poroelastic
      // solid
      AxisymmetricPoroelasticityTractionElement
       <PORO_ELEMENT>* face_element_pt=
       new AxisymmetricPoroelasticityTractionElement
       <PORO_ELEMENT>
       (Inner_poro_mesh_pt->boundary_element_pt(b,e),
        Inner_poro_mesh_pt->face_index_at_boundary(b,e));
      
      // Add to the surface mesh
      Inner_poro_sss_non_fsi_traction_mesh_pt->
       add_element_pt(face_element_pt);
      
      // Set the traction/pressure function pointers
      face_element_pt->pressure_fct_pt()=
       Global_Physical_Variables::steady_state_sss_pressure;
      face_element_pt->traction_fct_pt()=
       Global_Physical_Variables::steady_state_sss_traction;
      
      // Associate element with bulk boundary (to allow it to access
      // the boundary coordinates in the bulk mesh)
      face_element_pt->set_boundary_number_in_bulk_mesh(b);
     }
   }
  add_sub_mesh(Inner_poro_sss_non_fsi_traction_mesh_pt);
 }

 // Rebuild...
 rebuild_global_mesh();


   
 // Set up impulsive start from rest (which also initialises the timestep)
 double dt=1.0;
 assign_initial_values_impulsive(dt);
 
 oomph_info << "reassigned dt to 1.0 (fake)" << std::endl;

 // Reassign equation numbers
 //--------------------------
 oomph_info << "ndof before pinning: " << ndof() << std::endl;
 oomph_info << "ndof after  pinning: " << assign_eqn_numbers() << std::endl;

 
 char filename[1000];
 sprintf(filename,"%s/steady_trace.dat",doc_info.directory().c_str());
 ofstream trace_file;
 trace_file.open(filename);
 trace_file << "VARIABLES = ";
 trace_file << "\"P_steady_syrinx\" ";
 trace_file << "\"P_steady_sss_upper\" "; 
 trace_file << "\"P_steady_sss_lower\" ";
 trace_file << "\"Z_at_p_sss_change_start_raw\" ";
 trace_file << "\"Z_sss_p_shift_raw\" ";
 trace_file << "\"Seepage_flux_over_syrinx_fsi_boundary\" ";
 trace_file << "\"Seepage_flux_over_syrinx_fsi_boundary (poro)\" ";
 trace_file << "\"Seepage_flux_over_syrinx_fsi_boundary (NSt)\" ";
 trace_file << "\"Change_in_syrinx_volume\" ";
 trace_file << std::endl;


 sprintf(filename,"%s/zero_flux_steady_trace.dat",doc_info.directory().c_str());
 ofstream zero_flux_trace_file;
 zero_flux_trace_file.open(filename);
 zero_flux_trace_file << "VARIABLES = ";
 zero_flux_trace_file << "\"P_steady_syrinx\" ";
 zero_flux_trace_file << "\"P_steady_sss_upper\" "; 
 zero_flux_trace_file << "\"P_steady_sss_lower\" ";
 zero_flux_trace_file << "\"Z_at_p_sss_change_start_raw\" ";
 zero_flux_trace_file << "\"Z_sss_p_shift_raw\" ";
 zero_flux_trace_file << "\"Seepage_flux_over_syrinx_fsi_boundary\" ";
 zero_flux_trace_file << "\"Seepage_flux_over_syrinx_fsi_boundary (poro)\" ";
 zero_flux_trace_file << "\"Seepage_flux_over_syrinx_fsi_boundary (NSt)\" ";
 zero_flux_trace_file << "\"Change_in_syrinx_volume\" ";
 zero_flux_trace_file << std::endl;


 // Do steady solves
 //-----------------

 // Setup spline representation of k=0 sss pressure?
 if (CommandLineArgs::command_line_flag_has_been_set(
      "--sss_cycle_average_pressure_filename"))
  {
   Global_Physical_Variables::setup_spline(
    Global_Physical_Variables::SSS_cycle_average_pressure_filename,
    Global_Physical_Variables::SSS_pressure_spline_pt);

   // Best point to switch over (based on trial and error)
   Global_Physical_Variables::Z_at_p_sss_change_start_raw=-142.0;
  }

 // Average upper sss pressure is always zero -- actual value is
 // actually irrelevant anyway -- try it!
 Global_Physical_Variables::P_steady_state_sss_upper=0.0;

 // Pressure increment for lower sss (doesn't really matter what
 // we use here -- this is close to the actual "equilibrium value"
 // it's being adjusted anyway to get zero net flux)
 double p_sss_lower_step=925.0; 

 // Outer loop over syrinx pressure
 //--------------------------------

 // Syrinx pressure (on fluid pressure scale) -- appears to be 
 // remain approximately constant when going porous; let's loop over
 // modest range to check the sensitivity of syrinx volume to
 // its value
 double p_steady_state_syrinx_start=348.4; // hierher 340.0;
 double p_steady_state_syrinx_end=351.5;
 
 // Only need two steps -- it's linear! 
 unsigned n_syrinx=2;
 for (unsigned i_syrinx=0;i_syrinx<n_syrinx;i_syrinx++)
  {
   Global_Physical_Variables::P_steady_state_syrinx=
    p_steady_state_syrinx_start+
    (p_steady_state_syrinx_end-p_steady_state_syrinx_start)*
    double(i_syrinx)/double(n_syrinx-1);
      
   trace_file << "ZONE" << std::endl;
   
   // Inner loop over lower sss pressure -- three steps to find 
   //-----------------------------------------------------------
   // the value for zero net flux
   //----------------------------
   unsigned nstep_sss_lower=3;
   Vector<double> p_sss_lower(nstep_sss_lower,0.0);
   Vector<double> flux_syr(nstep_sss_lower,0.0);

   // Start with zero pressure
   Global_Physical_Variables::P_steady_state_sss_lower=0.0;

   // Loop over three cases
   for (unsigned i=0;i<nstep_sss_lower;i++)
    {
     oomph_info << "Steady solve for i = " << i 
                << " : P_syrinx, P_sss_upper, P_sss_lower = "
                << Global_Physical_Variables::P_steady_state_syrinx << " " 
                << Global_Physical_Variables::P_steady_state_sss_upper << " " 
                << Global_Physical_Variables::P_steady_state_sss_lower << " " 
                << std::endl;


     // Solve the bastard
     //------------------
     steady_newton_solve();
     

     // Doc only zero-flux solutions?
     bool doc_all=false;
     if (doc_all)
      {
       doc_solution(doc_info);
      }
     else
      {
       if (i==2)
        {
         doc_solution(doc_info);
        }
      }
     
     // Compute syrinx volume
     //----------------------
     double dvol_via_fluid=0.0;
     {
      // ofstream junk_file;
      // //junk_file.open("junk_syrinx_volume.dat");
      // double vol_syrinx_via_integral=0.0;
      // unsigned nel=Syrinx_volume_analysis_mesh_pt->nelement();
      // for (unsigned e=0;e<nel;e++)
      //  {
      //   AxisymmetricPoroelasticityLinePostProcessingElement
      //    <PORO_ELEMENT>* analysis_element_pt=
      //    dynamic_cast<AxisymmetricPoroelasticityLinePostProcessingElement
      //                 <PORO_ELEMENT>*>(Syrinx_volume_analysis_mesh_pt->
      //                                  element_pt(e));
        
      //   analysis_element_pt->output(junk_file,5);

      //   double dvol_contrib_via_integral=0.0;
      //   double vol_contrib_via_integral=analysis_element_pt->
      //    contribution_to_enclosed_volume(dvol_contrib_via_integral);
      //   vol_syrinx_via_integral+=vol_contrib_via_integral;
      //   dvol_syrinx_via_integral+=dvol_contrib_via_integral;
      //  }
      // junk_file.close();

      // Moving fluid mesh: compute volume from there
      if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update
          !=-1)
       {
        // Compute current volume from fluid elements
        double current_vol_syrinx=0.0;
        unsigned nel=Syrinx_fluid_mesh_pt->nelement();
        for (unsigned e=0;e<nel;e++)
         {
          current_vol_syrinx+=dynamic_cast<FLUID_ELEMENT*>(
           Syrinx_fluid_mesh_pt->element_pt(e))->compute_physical_size();
         }
        
        dvol_via_fluid=current_vol_syrinx - Analytical_syrinx_volume;
        oomph_info << "TOTAL VOLUME "
                   << current_vol_syrinx << " " 
                   <<  " CHANGE: "
                   << dvol_via_fluid << " " 
                   << std::endl;
       }
     }
     
     
     // Compute seepage flux over syrinx fsi boundary from poro
     //--------------------------------------------------------
     double seepage_flux_from_poro=0.0;
     {
      Vector<Mesh*> all_mesh_pt;
      all_mesh_pt.push_back(
       Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt);
      all_mesh_pt.push_back(
       Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt);
      all_mesh_pt.push_back(
       Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt);
      unsigned nm=all_mesh_pt.size();
      for (unsigned m=0;m<nm;m++)
       {
        unsigned nel=all_mesh_pt[m]->nelement();
        for (unsigned e=0;e<nel;e++)
         {
          double skeleton_flux_contrib=0.0;
          double seepage_flux_contrib=0.0;
          FSILinearisedAxisymPoroelasticTractionElement
           <PORO_ELEMENT,FLUID_ELEMENT>* el_pt=
           dynamic_cast<FSILinearisedAxisymPoroelasticTractionElement
                        <PORO_ELEMENT,FLUID_ELEMENT>*>(
                         all_mesh_pt[m]->element_pt(e));
          el_pt->contribution_to_total_porous_flux(
           skeleton_flux_contrib,
           seepage_flux_contrib);
          
          seepage_flux_from_poro+=
           Global_Physical_Variables::St*seepage_flux_contrib;
         }
       }
      oomph_info << "Total SEEPAGE flux over syrinx fsi boundary [from poro] : "
                 << seepage_flux_from_poro << std::endl;
     }
     
     
     // Compute seepage flux over syrinx fsi boundary (from nst domain)
     //----------------------------------------------------------------
     double seepage_flux_from_nst=0.0;
     {
      Vector<Mesh*> all_mesh_pt;
      
      all_mesh_pt.push_back(
       Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt);
      all_mesh_pt.push_back(
       Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt);
      all_mesh_pt.push_back(
       Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt);
      unsigned nm=all_mesh_pt.size();
      for (unsigned m=0;m<nm;m++)
       {
        unsigned nel=all_mesh_pt[m]->nelement();
        for (unsigned e=0;e<nel;e++)
         {
          double skeleton_flux_contrib=0.0;
          double seepage_flux_contrib=0.0;
          double nst_flux_contrib=0.0;
          LinearisedAxisymPoroelasticBJS_FSIElement
           <FLUID_ELEMENT,PORO_ELEMENT>* el_pt=
           dynamic_cast<LinearisedAxisymPoroelasticBJS_FSIElement
                        <FLUID_ELEMENT,PORO_ELEMENT>*>(
                         all_mesh_pt[m]->element_pt(e));
          el_pt->contribution_to_total_porous_flux(skeleton_flux_contrib,
                                                   seepage_flux_contrib,
                                                   nst_flux_contrib);
          seepage_flux_from_nst+=
           Global_Physical_Variables::St*nst_flux_contrib; // seepage_flux_contrib; hierher
         }
       }
      oomph_info << "Total SEEPAGE flux over syrinx fsi boundary [from NSt] : "
                 << seepage_flux_from_nst << std::endl;
     }
   

     double seepage_flux=seepage_flux_from_nst;
     bool compute_seepage_from_poro=true;
     if (compute_seepage_from_poro) seepage_flux=seepage_flux_from_poro;
     oomph_info 
      << "Total SEEPAGE flux over syrinx fsi boundary [for zero flux assessment] : "
      << seepage_flux << std::endl;
     
     // Doc properly
     trace_file 
      <<  Global_Physical_Variables::P_steady_state_syrinx << " ";  // column 1
     trace_file 
      <<  Global_Physical_Variables::P_steady_state_sss_upper<< " ";// column 2
     trace_file 
      <<  Global_Physical_Variables::P_steady_state_sss_lower<< " ";// column 3
     trace_file 
      << Global_Physical_Variables::Z_at_p_sss_change_start_raw // column 4
      << " ";
     trace_file << Global_Physical_Variables::Z_sss_p_shift_raw // column 5
      << " ";
     trace_file << seepage_flux << " "; // column 6
     trace_file << seepage_flux_from_poro << " "; // column 7
     trace_file << seepage_flux_from_nst << " "; // column 8
     trace_file << dvol_via_fluid << " "; // column 9
     trace_file << std::endl;
   

     // Doc properly
     if (i==2)
      {
       zero_flux_trace_file 
        <<  Global_Physical_Variables::P_steady_state_syrinx << " "; // column 1
       zero_flux_trace_file 
        <<  Global_Physical_Variables::P_steady_state_sss_upper<<" ";//column 2
       zero_flux_trace_file 
        <<  Global_Physical_Variables::P_steady_state_sss_lower<< " ";//column 3
       zero_flux_trace_file 
        << Global_Physical_Variables::Z_at_p_sss_change_start_raw // column 4
        << " ";
       zero_flux_trace_file 
        << Global_Physical_Variables::Z_sss_p_shift_raw // column 5
        << " ";
       zero_flux_trace_file << seepage_flux << " "; // column 6
       zero_flux_trace_file << seepage_flux_from_poro << " "; // column 7
       zero_flux_trace_file << seepage_flux_from_nst << " "; // column 8
       zero_flux_trace_file << dvol_via_fluid << " "; // column 9
       zero_flux_trace_file << std::endl;
      }

     // Extrapolate to zero flux
     if (i<2)
      {
       // Record
       p_sss_lower[i]=Global_Physical_Variables::P_steady_state_sss_lower;
       flux_syr[i]=seepage_flux;
       
       // Increase lower sss pressure
       Global_Physical_Variables::P_steady_state_sss_lower=p_sss_lower_step;

       oomph_info << "recorded for i=" << i << std::endl;
      }
     if (i==1)
      {
       // Extrapolate to syrinx pressure that should give us zero net 
       // seepage flux
       Global_Physical_Variables::P_steady_state_sss_lower=
        p_sss_lower[0]-(p_sss_lower[1]-p_sss_lower[0])*
        flux_syr[0]/(flux_syr[1]-flux_syr[0]);
       
       oomph_info << "extrapolated for i=" << i << std::endl;
      }
    }
   
   
   // Doc zero flux sss pressure distribution
   {
    sprintf(filename,"%s/zero_flux_steady_sss_pressure_distribution%i.dat",
            doc_info.directory().c_str(),i_syrinx);
    ofstream sss_file;
    sss_file.open(filename);
    unsigned nplot=10000;
    Vector<double> x(2,0.0);
    Vector<double> n(2,0.0);
    double sss_press=0.0;
    double z_min=-600.0*Global_Physical_Variables::Z_scale* 
                  Global_Physical_Variables::Z_shrink_factor;
    double z_max=0.0; 
    double z_sss_p_shift=Global_Physical_Variables::Z_sss_p_shift_raw*
     Global_Physical_Variables::Z_scale*
     Global_Physical_Variables::Z_shrink_factor;

    for (unsigned i=0;i<nplot;i++)
     {
      double z=z_min+(z_max-z_min)*double(i)/double(nplot-1);
      double time=time_pt()->time();
      x[1]=z; 
      
      // Get adjusted pressure
      Global_Physical_Variables::steady_state_sss_pressure(time,
                                                           x,n,sss_press);
      
      // coordinate, new sss pressure, orig sss presure (all on fluid scale)
      sss_file << z << " " 
               << sss_press/Global_Physical_Variables::Q << " ";
      if (Global_Physical_Variables::SSS_pressure_spline_pt!=0)
       {
        sss_file
               << Global_Physical_Variables::SSS_pressure_spline_pt->
         spline(z-z_sss_p_shift);
       }
      sss_file << std::endl;
     }
    sss_file.close();
   }
   
   // Doc zero flux syrinx pressure distribution
   {
    sprintf(filename,"%s/zero_flux_steady_syrinx_pressure_distribution%i.dat",
            doc_info.directory().c_str(),i_syrinx);
    ofstream syrinx_file;
    syrinx_file.open(filename);
    unsigned nplot=10;
    Vector<double> x(2,0.0);
    Vector<double> n(2,0.0);
    double syrinx_press=0.0;
    for (unsigned i=0;i<nplot;i++)
     {
      double z=Global_Physical_Variables::Z_syrinx_top_centreline*
       Global_Physical_Variables::Z_scale*
       Global_Physical_Variables::Z_shrink_factor-
       (220.0+Global_Physical_Variables::Z_syrinx_top_centreline)*
       Global_Physical_Variables::Z_scale*
       Global_Physical_Variables::Z_shrink_factor*
       double(i)/double(nplot-1);
      double time=time_pt()->time();
      x[1]=z;
      
      // Get adjusted pressure
      Global_Physical_Variables::steady_state_syrinx_pressure(time,
                                                              x,n,syrinx_press);
      
      // coordinate, new syrinx pressure (on fluid scale)
      syrinx_file << z << " " 
                  << syrinx_press/Global_Physical_Variables::Q << " " 
                  << std::endl;
     }
    syrinx_file.close();
   }
  }
 
#ifdef OOMPH_HAS_MPI 
 
 // Wait for all doc to finish
 MPI_Barrier(MPI_COMM_WORLD);

 // Cleanup
 MPI_Helpers::finalize();

#endif

 trace_file.close();
 zero_flux_trace_file.close();

 exit(0);

}



///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////


//========================================================================
/// Do single steady run for inner poro with read-in pressures
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
steady_inner_poro_only_run(DocInfo& doc_info)
{


 // Avoid accepting (non-)solutions with small initial residuals;
 // the problem is linear!
 Problem::Always_take_one_newton_step=true;


 // Pin everything apart from inner poro mesh
 //------------------------------------------
 Vector<Mesh*> all_pin_mesh_pt;
 all_pin_mesh_pt.push_back(Outer_poro_mesh_pt);
 all_pin_mesh_pt.push_back(Syrinx_fluid_mesh_pt);
 all_pin_mesh_pt.push_back(SSS_fluid_mesh_pt);

 unsigned npin=all_pin_mesh_pt.size();
 for (unsigned m=0;m<npin;m++)
  {
   Mesh* mesh_pt=all_pin_mesh_pt[m];
   unsigned nnod=mesh_pt->nnode();
   for (unsigned j=0;j<nnod;j++)
    {
     Node* nod_pt=mesh_pt->node_pt(j);
     unsigned nval=nod_pt->nvalue();
     for (unsigned i=0;i<nval;i++)
      {
       nod_pt->pin(i);
      }
    }
   unsigned nel=mesh_pt->nelement();
   for (unsigned e=0;e<nel;e++)
    {
     GeneralisedElement*el_pt=mesh_pt->element_pt(e);
     unsigned nint=el_pt->ninternal_data();
     for (unsigned j=0;j<nint;j++)
      {
       Data* data_pt=el_pt->internal_data_pt(j);      
       unsigned nval=data_pt->nvalue();
       for (unsigned i=0;i<nval;i++)
        {
         data_pt->pin(i);
        }
      }
    }
  }


 // Create the non-FSI surface traction meshes
 // ------------------------------------------


 // Inner poro from syrinx
 {
  Inner_poro_syrinx_non_fsi_traction_mesh_pt=new Mesh;
  
  // Inner poro mesh (from syrinx fluid mesh)
  Vector<unsigned> fsi_interface;
  fsi_interface.push_back(Inner_poro_lower_syrinx_interface_boundary_id);
  fsi_interface.push_back(Inner_poro_central_syrinx_interface_boundary_id);
  fsi_interface.push_back(Inner_poro_upper_syrinx_interface_boundary_id);
  unsigned nb=fsi_interface.size();
  for (unsigned bb=0;bb<nb;bb++)
   {
    // Now loop over the bulk elements and create the face elements
    unsigned b=fsi_interface[bb];
    unsigned nel = Inner_poro_mesh_pt->nboundary_element(b);
    for(unsigned e=0;e<nel;e++)
     {
      // Create the face element that applies steady traction to poroelastic
      // solid
      AxisymmetricPoroelasticityTractionElement
       <PORO_ELEMENT>* face_element_pt=
       new AxisymmetricPoroelasticityTractionElement
       <PORO_ELEMENT>
       (Inner_poro_mesh_pt->boundary_element_pt(b,e),
        Inner_poro_mesh_pt->face_index_at_boundary(b,e));
      
      // Add to the surface mesh
      Inner_poro_syrinx_non_fsi_traction_mesh_pt->
       add_element_pt(face_element_pt);
      

      // Set the traction/pressure function pointers
      face_element_pt->pressure_fct_pt()=
       &Global_Physical_Variables::read_in_syrinx_pressure;
      face_element_pt->traction_fct_pt()=
       &Global_Physical_Variables::read_in_syrinx_traction;
      
      // Associate element with bulk boundary (to allow it to access
      // the boundary coordinates in the bulk mesh)
      face_element_pt->set_boundary_number_in_bulk_mesh(b);
     }
   }
  
  add_sub_mesh(Inner_poro_syrinx_non_fsi_traction_mesh_pt);
 }


 // Inner poro from sss
 {
  Inner_poro_sss_non_fsi_traction_mesh_pt=new Mesh;
  
  Vector<unsigned> fsi_interface;
  fsi_interface.push_back(Inner_poro_upper_SSS_interface_boundary_id);
  fsi_interface.push_back(Inner_poro_central_SSS_interface_boundary_id);
  fsi_interface.push_back(Inner_poro_lower_SSS_interface_boundary_id);
  unsigned nb=fsi_interface.size();
  for (unsigned bb=0;bb<nb;bb++)
   {
    // Now loop over the bulk elements and create the face elements
    unsigned b=fsi_interface[bb];
    unsigned nel = Inner_poro_mesh_pt->nboundary_element(b);
    for(unsigned e=0;e<nel;e++)
     {
      // Create the face element that applies steady traction to poroelastic
      // solid
      AxisymmetricPoroelasticityTractionElement
       <PORO_ELEMENT>* face_element_pt=
       new AxisymmetricPoroelasticityTractionElement
       <PORO_ELEMENT>
       (Inner_poro_mesh_pt->boundary_element_pt(b,e),
        Inner_poro_mesh_pt->face_index_at_boundary(b,e));
      
      // Add to the surface mesh
      Inner_poro_sss_non_fsi_traction_mesh_pt->
       add_element_pt(face_element_pt);
      
      // Set the traction/pressure function pointers
      face_element_pt->pressure_fct_pt()=
       Global_Physical_Variables::read_in_sss_pressure;
      face_element_pt->traction_fct_pt()=
       Global_Physical_Variables::read_in_sss_traction;
      
      // Associate element with bulk boundary (to allow it to access
      // the boundary coordinates in the bulk mesh)
      face_element_pt->set_boundary_number_in_bulk_mesh(b);
     }
   }
  add_sub_mesh(Inner_poro_sss_non_fsi_traction_mesh_pt);
 }

 // Rebuild...
 rebuild_global_mesh();


 // Reassign equation numbers
 //--------------------------
 oomph_info << "ndof before pinning: " << ndof() << std::endl;
 oomph_info << "ndof after  pinning: " << assign_eqn_numbers() << std::endl;


 // Setup spline representation sss pressure
 Global_Physical_Variables::setup_spline(
  Global_Physical_Variables::SSS_cycle_average_pressure_filename,
  Global_Physical_Variables::SSS_pressure_spline_pt);

 // Setup spline representation syrinx pressure
 Global_Physical_Variables::setup_spline(
  Global_Physical_Variables::Syrinx_cycle_average_pressure_filename,
  Global_Physical_Variables::Syrinx_pressure_spline_pt);
 

// ---

 // Setup spline representation sss radial traction
 if (CommandLineArgs::command_line_flag_has_been_set(
      "--sss_cycle_average_radial_traction_filename"))
  {
   Global_Physical_Variables::setup_spline(
    Global_Physical_Variables::SSS_cycle_average_radial_traction_filename,
    Global_Physical_Variables::SSS_radial_traction_spline_pt);
  }

 // Setup spline representation sss axial traction
 if (CommandLineArgs::command_line_flag_has_been_set(
      "--sss_cycle_average_axial_traction_filename"))
  {
   Global_Physical_Variables::setup_spline(
    Global_Physical_Variables::SSS_cycle_average_axial_traction_filename,
    Global_Physical_Variables::SSS_axial_traction_spline_pt);
  }


 // Setup spline representation syrinx radial traction
 if (CommandLineArgs::command_line_flag_has_been_set(
      "--syrinx_cycle_average_radial_traction_filename"))
  {
   Global_Physical_Variables::setup_spline(
    Global_Physical_Variables::Syrinx_cycle_average_radial_traction_filename,
    Global_Physical_Variables::Syrinx_radial_traction_spline_pt);
  }

 // Setup spline representation syrinx axial traction
 if (CommandLineArgs::command_line_flag_has_been_set(
      "--syrinx_cycle_average_axial_traction_filename"))
  {
   Global_Physical_Variables::setup_spline(
    Global_Physical_Variables::Syrinx_cycle_average_axial_traction_filename,
    Global_Physical_Variables::Syrinx_axial_traction_spline_pt);
  }

// ---

 // Solve the bastard
 //------------------
 steady_newton_solve();
 
 // Doc 
 doc_solution(doc_info);
 
 // Compute syrinx volume
 //----------------------
 double dvol_via_fluid=0.0;
 {
  // Moving fluid mesh: compute volume from there
  if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update
      !=-1)
   {
    // Compute current volume from fluid elements
    double current_vol_syrinx=0.0;
    unsigned nel=Syrinx_fluid_mesh_pt->nelement();
    for (unsigned e=0;e<nel;e++)
     {
      current_vol_syrinx+=dynamic_cast<FLUID_ELEMENT*>(
       Syrinx_fluid_mesh_pt->element_pt(e))->compute_physical_size();
     }
    
    dvol_via_fluid=current_vol_syrinx - Analytical_syrinx_volume;
    oomph_info << "TOTAL VOLUME "
               << current_vol_syrinx << " " 
               <<  " CHANGE: "
               << dvol_via_fluid << " " 
               << std::endl;
   }
 }
 
 
 // Compute seepage flux over syrinx fsi boundary from poro
 //--------------------------------------------------------
 double seepage_flux_from_poro=0.0;
 {
  Vector<Mesh*> all_mesh_pt;
  all_mesh_pt.push_back(
   Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt);
  all_mesh_pt.push_back(
   Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt);
  all_mesh_pt.push_back(
   Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt);
  unsigned nm=all_mesh_pt.size();
  for (unsigned m=0;m<nm;m++)
   {
    unsigned nel=all_mesh_pt[m]->nelement();
    for (unsigned e=0;e<nel;e++)
     {
      double skeleton_flux_contrib=0.0;
      double seepage_flux_contrib=0.0;
      FSILinearisedAxisymPoroelasticTractionElement
       <PORO_ELEMENT,FLUID_ELEMENT>* el_pt=
       dynamic_cast<FSILinearisedAxisymPoroelasticTractionElement
                    <PORO_ELEMENT,FLUID_ELEMENT>*>(
                     all_mesh_pt[m]->element_pt(e));
      el_pt->contribution_to_total_porous_flux(
       skeleton_flux_contrib,
       seepage_flux_contrib);
      
      seepage_flux_from_poro+=
       Global_Physical_Variables::St*seepage_flux_contrib;
     }
   }
  oomph_info << "Total SEEPAGE flux over syrinx fsi boundary [from poro] : "
             << seepage_flux_from_poro << std::endl;
 }
 
 
 // Compute seepage flux over syrinx fsi boundary (from nst domain)
 //----------------------------------------------------------------
 double seepage_flux_from_nst=0.0;
 {
  Vector<Mesh*> all_mesh_pt;
  
  all_mesh_pt.push_back(
   Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt);
  all_mesh_pt.push_back(
   Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt);
  all_mesh_pt.push_back(
   Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt);
  unsigned nm=all_mesh_pt.size();
  for (unsigned m=0;m<nm;m++)
   {
    unsigned nel=all_mesh_pt[m]->nelement();
    for (unsigned e=0;e<nel;e++)
     {
      double skeleton_flux_contrib=0.0;
      double seepage_flux_contrib=0.0;
      double nst_flux_contrib=0.0;
      LinearisedAxisymPoroelasticBJS_FSIElement
       <FLUID_ELEMENT,PORO_ELEMENT>* el_pt=
       dynamic_cast<LinearisedAxisymPoroelasticBJS_FSIElement
                    <FLUID_ELEMENT,PORO_ELEMENT>*>(
                     all_mesh_pt[m]->element_pt(e));
      el_pt->contribution_to_total_porous_flux(skeleton_flux_contrib,
                                               seepage_flux_contrib,
                                               nst_flux_contrib);
      seepage_flux_from_nst+=
       Global_Physical_Variables::St*nst_flux_contrib; // seepage_flux_contrib; hierher
     }
   }
  oomph_info << "Total SEEPAGE flux over syrinx fsi boundary [from NSt] : "
             << seepage_flux_from_nst << std::endl;
 }

 char filename[1000];
 
 // Doc sss pressure distribution
 {
  sprintf(filename,"%s/read_in_sss_pressure_distribution.dat",
          doc_info.directory().c_str());
  ofstream sss_file;
  sss_file.open(filename);
  unsigned nplot=10000;
  Vector<double> x(2,0.0);
  Vector<double> n(2,0.0);
  double sss_press=0.0;
  double z_min=-600.0*Global_Physical_Variables::Z_scale* 
   Global_Physical_Variables::Z_shrink_factor;
  double z_max=0.0;   
  for (unsigned i=0;i<nplot;i++)
   {
    double z=z_min+(z_max-z_min)*double(i)/double(nplot-1);
    double time=time_pt()->time();
    x[1]=z; 
    
    // Get pressure
    Global_Physical_Variables::read_in_sss_pressure(time,
                                                    x,n,sss_press);
    
    // coordinate, new sss pressure, orig sss presure (all on solid scale)
    sss_file << z << " " 
             << sss_press<< " ";
    if (Global_Physical_Variables::SSS_pressure_spline_pt!=0)
     {
      sss_file
       << Global_Physical_Variables::SSS_pressure_spline_pt->spline(z);
     }
    sss_file<< std::endl;
   }
  sss_file.close();
 }
 
 // Doc zero flux syrinx pressure distribution
 {
  sprintf(filename,"%s/read_in_steady_syrinx_pressure_distribution.dat",
          doc_info.directory().c_str());
  ofstream syrinx_file;
  syrinx_file.open(filename);
  unsigned nplot=10;
  Vector<double> x(2,0.0);
  Vector<double> n(2,0.0);
  double syrinx_press=0.0;
  for (unsigned i=0;i<nplot;i++)
   {
    double z=Global_Physical_Variables::Z_syrinx_top_centreline*
     Global_Physical_Variables::Z_scale*
     Global_Physical_Variables::Z_shrink_factor-
     (220.0+Global_Physical_Variables::Z_syrinx_top_centreline)*
     Global_Physical_Variables::Z_scale*
     Global_Physical_Variables::Z_shrink_factor*
     double(i)/double(nplot-1);
    double time=time_pt()->time();
    x[1]=z;
    

    // Get adjusted pressure
    Global_Physical_Variables::read_in_syrinx_pressure(time,
                                                       x,n,syrinx_press);
    
    // coordinate, new syrinx pressure (on solid scale)
    syrinx_file << z << " " 
                << syrinx_press << " " ;
    if (Global_Physical_Variables::Syrinx_pressure_spline_pt!=0)
     {
      syrinx_file 
       << Global_Physical_Variables::Syrinx_pressure_spline_pt->spline(z);
     }
    syrinx_file << std::endl;
   }
  syrinx_file.close();
 }
 
 
#ifdef OOMPH_HAS_MPI 
 
 // Wait for all doc to finish
 MPI_Barrier(MPI_COMM_WORLD);
 
 // Cleanup
 MPI_Helpers::finalize();
 
#endif
  
 exit(0);
 
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////



//==start_of_doc_solution=================================================
/// Doc the solution
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
doc_solution(DocInfo& doc_info, unsigned npts)
{

 if (MPI_Helpers::communicator_pt()->my_rank()==0)
  {
   double t_start=TimingHelpers::timer();

   ofstream some_file;
   char filename[1000];

   oomph_info << "About to doc for step: " << doc_info.number() << std::endl;

   // Write restart file
   if (!CommandLineArgs::command_line_flag_has_been_set
       ("--suppress_restart_files"))
    {
     sprintf(filename,"%s/restart%i.dat",doc_info.directory().c_str(),
             doc_info.number());
     ofstream dump_file;
     dump_file.open(filename);
     dump_file.precision(20); 
     dump_it(doc_info,dump_file);
     dump_file.close();
    }

   if (!CommandLineArgs::command_line_flag_has_been_set
       ("--suppress_all_output_apart_from_trace_and_restart"))
    {

     // Write tecplot files for bulk?
     if (!CommandLineArgs::command_line_flag_has_been_set
         ("--suppress_tecplot_bulk_output"))
      {
       // Output solution on inner poro mesh
       sprintf(filename,"%s/soln-inner-poro%i.dat",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Inner_poro_mesh_pt->output(some_file,npts);
       some_file.close();
   
       // Output solution on outer poro mesh
       sprintf(filename,"%s/soln-outer-poro%i.dat",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Outer_poro_mesh_pt->output(some_file,npts);
       some_file.close();

       // Output solution on syrinx fluid mesh
       sprintf(filename,"%s/soln-syrinx-fluid%i.dat",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       // oomph_info << "hierher: Set precision for syrinx fluid data to 20\n";
       // some_file.precision(20);
       Syrinx_fluid_mesh_pt->output(some_file,npts);
       some_file.close();

       // Output solution on SSS fluid mesh
       sprintf(filename,"%s/soln-sss-fluid%i.dat",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       SSS_fluid_mesh_pt->output(some_file,npts);
       some_file.close();
  
      } // end tecplot bulk output




     //Output Poisson's ratio and non-dim permeability for all solid elements
     sprintf(filename,"%s/constitutive_properties%i.dat",doc_info.directory().c_str(),
             doc_info.number());
     some_file.open(filename);
     some_file << "VARIABLES=\"r\","
               << "\"z\","
               << "\"nu\","
               << "\"E\","
               << "\"Lambda^2\","
               << "\"R_rho\","
               << "\"permeability\","
               << "\"permeability ratio\","
               << "\"porosity\","
               << "\"alpha\""
               << std::endl;
     Vector<Mesh*> tmp_solid_mesh_pt;
     tmp_solid_mesh_pt.push_back(Inner_poro_mesh_pt);
     tmp_solid_mesh_pt.push_back(Outer_poro_mesh_pt);
     unsigned nm=tmp_solid_mesh_pt.size();
     for (unsigned m=0;m<nm;m++)
      {
       Mesh* m_pt=tmp_solid_mesh_pt[m];
       unsigned nel = m_pt->nelement();
       for(unsigned e=0;e<nel;e++)
        {
         PORO_ELEMENT* el_pt=dynamic_cast<PORO_ELEMENT*>(m_pt->element_pt(e));
         double nu=*(el_pt->nu_pt());
         double perm=*(el_pt->permeability_pt());
         double perm_ratio=*(el_pt->permeability_ratio_pt());
         if (el_pt->darcy_is_switched_off())
          {
           perm=0.0;
           perm_ratio=0.0;
          }
         double young_e=el_pt->youngs_modulus();
         double lambda_squared=el_pt->lambda_sq();
         double density_ratio=el_pt->density_ratio();
         double porosity=el_pt->porosity();
         double alpha=el_pt->alpha();
         unsigned nnod=el_pt->nnode();
         for (unsigned j=0;j<nnod;j++)
          {
           Node* nod_pt=el_pt->node_pt(j);
           some_file << nod_pt->x(0) << " " 
                     << nod_pt->x(1) << " " 
                     << nu << " " 
                     << young_e << " " 
                     << lambda_squared << " " 
                     << density_ratio << " " 
                     << perm << " " 
                     << perm_ratio << " "
                     << porosity << " "
                     << alpha << " "
                     << std::endl;
          }
        }
      }
     some_file.close();
 
     // Output solution on syrinx cover
     sprintf(filename,"%s/soln-inner-poro-syrinx-cover%i.dat",
             doc_info.directory().c_str(),
             doc_info.number());
     some_file.open(filename);
     Vector<unsigned> region_id;
     region_id.push_back(Central_lower_cord_region_id);
     region_id.push_back(Central_upper_cord_region_id);
     region_id.push_back(Central_lower_pia_region_id);
     region_id.push_back(Central_upper_pia_region_id);
     region_id.push_back(Central_lower_pia_poro_bl_region_id);
     region_id.push_back(Central_upper_pia_poro_bl_region_id);
     unsigned nr=region_id.size();
     for (unsigned rr=0;rr<nr;rr++)
      {
       unsigned r=region_id[rr];
       unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>
        (Inner_poro_mesh_pt)->nregion_element(r);
       for(unsigned e=0;e<nel;e++)
        {
         // Cast to a bulk element
         PORO_ELEMENT *el_pt=dynamic_cast<PORO_ELEMENT*>(
          dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)
          ->region_element_pt(r,e));
     
         // Output
         el_pt->output(some_file,npts);
        }
      }
     some_file.close();
 
 

     // Output poroelastic stuff along radial line through syrinx cover
     if (!CommandLineArgs::command_line_flag_has_been_set("--suppress_line_visualisers"))
     {
      oomph_info << " about to do output from upper line visualiser"
                 << std::endl;

      double t_start=TimingHelpers::timer();
      sprintf(filename,"%s/upper_radial_line_through_syrinx_cover%i.dat",
              doc_info.directory().c_str(),
              doc_info.number());
      some_file.open(filename);
      Upper_radial_line_through_syrinx_cover_visualiser_pt->output(some_file);
      some_file.close();
      oomph_info << "Took: " << TimingHelpers::timer()-t_start
                 << " sec to do output from upper line visualiser \n";
     }

     // Output poroelastic stuff along radial line through syrinx cover
     if (!CommandLineArgs::command_line_flag_has_been_set("--suppress_line_visualisers"))
     {
      oomph_info << " about to do output from lower line visualiser"
                 << std::endl;

      double t_start=TimingHelpers::timer();
      sprintf(filename,"%s/lower_radial_line_through_syrinx_cover%i.dat",
              doc_info.directory().c_str(),
              doc_info.number());
      some_file.open(filename);
      Lower_radial_line_through_syrinx_cover_visualiser_pt->output(some_file);
      some_file.close();
      oomph_info << "Took: " << TimingHelpers::timer()-t_start
                 << " sec to do output from lower line visualiser \n";
     }

     // Outer poro at regularly spaced points
     {
      sprintf(filename,"%s/regular_outer_poro%i.dat",
              doc_info.directory().c_str(),
              doc_info.number());
      some_file.open(filename);
      Vector<double> x(2);
      Vector<double> s(2);
      unsigned n=Outer_poro_regularly_spaced_plot_point.size();
      for (unsigned i=0;i<n;i++)
       {
        // Pointer to element
        PORO_ELEMENT* el_pt=Outer_poro_regularly_spaced_plot_point[i].first;
    
        // Coordinates in it
        s=Outer_poro_regularly_spaced_plot_point[i].second;
    
        // Get coords
        el_pt->interpolated_x(s,x);
    
        some_file << x[0] << " "
                  << x[1] << " "
                  << el_pt->interpolated_u(s,0) << " "
                  << el_pt->interpolated_u(s,1) << " "
                  << el_pt->interpolated_q(s,0) << " "
                  << el_pt->interpolated_q(s,1) << " "
                  << el_pt->interpolated_p(s) << " "
                  << std::endl;
       }
      some_file.close();
     }

     // Inner poro at regularly spaced points
     {
      sprintf(filename,"%s/regular_inner_poro%i.dat",
              doc_info.directory().c_str(),
              doc_info.number());
      some_file.open(filename);
      Vector<double> x(2);
      Vector<double> s(2);
      unsigned n=Inner_poro_regularly_spaced_plot_point.size();
      for (unsigned i=0;i<n;i++)
       {
        // Pointer to element
        PORO_ELEMENT* el_pt=Inner_poro_regularly_spaced_plot_point[i].first;
    
        // Coordinates in it
        s=Inner_poro_regularly_spaced_plot_point[i].second;
    
        // Get coords
        el_pt->interpolated_x(s,x);
    
        some_file << x[0] << " "
                  << x[1] << " "
                  << el_pt->interpolated_u(s,0) << " "
                  << el_pt->interpolated_u(s,1) << " "
                  << el_pt->interpolated_q(s,0) << " "
                  << el_pt->interpolated_q(s,1) << " "
                  << el_pt->interpolated_p(s) << " "
                  << std::endl;
       }
      some_file.close();
     }


     // Syrinx Fluid at regularly spaced points
     {
      sprintf(filename,"%s/regular_syrinx_fluid%i.dat",
              doc_info.directory().c_str(),
              doc_info.number());
      some_file.open(filename);
      unsigned npts=Syrinx_fluid_regularly_spaced_plot_point.size();
      Vector<double> x(2);
      Vector<double> s(2);
      Vector<double> veloc(3);
      for (unsigned i=0;i<npts;i++)
       {
        // Pointer to element
        FLUID_ELEMENT* el_pt=Syrinx_fluid_regularly_spaced_plot_point[i].first;
    
        // Coordinates in it
        s=Syrinx_fluid_regularly_spaced_plot_point[i].second;
    
        // Get coords
        el_pt->interpolated_x(s,x);
    
        // Get velocity
        el_pt->interpolated_u_axi_nst(s,veloc);
    
        some_file << x[0] << " "
                  << x[1] << " "
                  << veloc[0] << " "
                  << veloc[1] << " "
                  << veloc[2] << " "
                  << el_pt->interpolated_p_axi_nst(s) << " "
                  << std::endl;
       }
      some_file.close();
     }


     // SSS Fluid at regularly spaced points
     {
      sprintf(filename,"%s/regular_sss_fluid%i.dat",
              doc_info.directory().c_str(),
              doc_info.number());
      some_file.open(filename);
      unsigned npts=SSS_fluid_regularly_spaced_plot_point.size();
      Vector<double> x(2);
      Vector<double> s(2);
      Vector<double> veloc(3);
      for (unsigned i=0;i<npts;i++)
       {
        // Pointer to element
        FLUID_ELEMENT* el_pt=SSS_fluid_regularly_spaced_plot_point[i].first;
    
        // Coordinates in it
        s=SSS_fluid_regularly_spaced_plot_point[i].second;
    
        // Get coords
        el_pt->interpolated_x(s,x);
    
        // Get velocity
        el_pt->interpolated_u_axi_nst(s,veloc);
    
        some_file << x[0] << " "
                  << x[1] << " "
                  << veloc[0] << " "
                  << veloc[1] << " "
                  << veloc[2] << " "
                  << el_pt->interpolated_p_axi_nst(s) << " "
                  << std::endl;
       }
      some_file.close();
     }

     // Update SSS fluid mesh?
     if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update!=-1)
      {
       // Plot "spines"
       //--------------
       sprintf(filename,"%s/sss_and_syrinx_spines%i.dat",
               doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       doc_spines(some_file);
       some_file.close();       
      }

     // Doc FSI traction acting on poro-elastic materials:
     //---------------------------------------------------
     if(!CommandLineArgs::command_line_flag_has_been_set(
         "--steady_solve"))
      {
       sprintf(filename,"%s/poro_fsi_syrinx%i.dat",
               doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt->output(some_file);
       Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt->output(some_file);
       Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt->output(some_file);
       some_file.close();
       
       sprintf(filename,"%s/poro_fsi_sss%i.dat",
               doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt->output(some_file);
       Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt->output(some_file);
       Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt->output(some_file);
       Outer_poro_SSS_fluid_near_inlet_FSI_surface_mesh_pt->output(some_file);
       Outer_poro_SSS_fluid_near_outlet_FSI_surface_mesh_pt->output(some_file);
       Outer_poro_SSS_fluid_block_FSI_surface_mesh_pt->output(some_file);
       some_file.close();
       
       
       sprintf(filename,"%s/poro_fsi_inner_sss%i.dat",
               doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt->output(some_file);
       Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt->output(some_file);
       Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt->output(some_file);
       some_file.close();
       
       sprintf(filename,"%s/poro_fsi_outer_sss%i.dat",
               doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Outer_poro_SSS_fluid_near_inlet_FSI_surface_mesh_pt->output(some_file);
       Outer_poro_SSS_fluid_near_outlet_FSI_surface_mesh_pt->output(some_file);
       Outer_poro_SSS_fluid_block_FSI_surface_mesh_pt->output(some_file);
       some_file.close();
       
       
       
//---
       
       // Now output original poroelastic information
       //--------------------------------------------
       {
        // Syrinx fsi boundary
        sprintf(filename,"%s/poro_output_on_fsi_syrinx_boundary%i.dat",
                doc_info.directory().c_str(),
                doc_info.number());
        some_file.open(filename);
        
        Vector<Mesh*> all_mesh_pt;
        
        all_mesh_pt.push_back(Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt);
        unsigned nm=all_mesh_pt.size();
        for (unsigned m=0;m<nm;m++)
         {
          unsigned nel=all_mesh_pt[m]->nelement();
          for (unsigned e=0;e<nel;e++)
           {
            dynamic_cast<AxisymmetricPoroelasticityTractionElement
                         <PORO_ELEMENT>*>(all_mesh_pt[m]->element_pt(e))->
             AxisymmetricPoroelasticityTractionElement<PORO_ELEMENT>::output
             (some_file,5);
           }
         }
        some_file.close();
        
        
        // All SSS fsi boundary
        all_mesh_pt.clear();
        all_mesh_pt.push_back(Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Outer_poro_SSS_fluid_near_inlet_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Outer_poro_SSS_fluid_near_outlet_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Outer_poro_SSS_fluid_block_FSI_surface_mesh_pt);
        
        sprintf(filename,"%s/poro_output_on_fsi_sss_boundary%i.dat",
              doc_info.directory().c_str(),
                doc_info.number());
        some_file.open(filename);
        nm=all_mesh_pt.size();
        for (unsigned m=0;m<nm;m++)
         {
          unsigned nel=all_mesh_pt[m]->nelement();
          for (unsigned e=0;e<nel;e++)
           {
            dynamic_cast<AxisymmetricPoroelasticityTractionElement
                         <PORO_ELEMENT>*>(all_mesh_pt[m]->element_pt(e))->
             AxisymmetricPoroelasticityTractionElement<PORO_ELEMENT>::output
             (some_file,5);
           }
         }
        some_file.close();
        
        
        // Inner SSS fsi boundary
        all_mesh_pt.clear();
        all_mesh_pt.push_back(Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt);
        sprintf(filename,"%s/poro_output_on_fsi_inner_sss_boundary%i.dat",
                doc_info.directory().c_str(),
                doc_info.number());
        some_file.open(filename);
        nm=all_mesh_pt.size();
        for (unsigned m=0;m<nm;m++)
         {
          unsigned nel=all_mesh_pt[m]->nelement();
          for (unsigned e=0;e<nel;e++)
           {
            dynamic_cast<AxisymmetricPoroelasticityTractionElement
                         <PORO_ELEMENT>*>(all_mesh_pt[m]->element_pt(e))->
             AxisymmetricPoroelasticityTractionElement<PORO_ELEMENT>::output
             (some_file,5);
           }
         }
        some_file.close();
        
        
        
        // Outer SSS fsi boundary
        all_mesh_pt.clear();
        all_mesh_pt.push_back(Outer_poro_SSS_fluid_near_inlet_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Outer_poro_SSS_fluid_near_outlet_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Outer_poro_SSS_fluid_block_FSI_surface_mesh_pt);
        sprintf(filename,"%s/poro_output_on_fsi_outer_sss_boundary%i.dat",
                doc_info.directory().c_str(),
                doc_info.number());
        some_file.open(filename);
        nm=all_mesh_pt.size();
        for (unsigned m=0;m<nm;m++)
         {
          unsigned nel=all_mesh_pt[m]->nelement();
          for (unsigned e=0;e<nel;e++)
           {
            dynamic_cast<AxisymmetricPoroelasticityTractionElement
                         <PORO_ELEMENT>*>(all_mesh_pt[m]->element_pt(e))->
             AxisymmetricPoroelasticityTractionElement<PORO_ELEMENT>::output
             (some_file,5);
           }
         }
        some_file.close();
       }
       
//---
       

       // Check external elements for poro FSI meshes
       {
        Vector<Mesh*> all_mesh_pt;
        all_mesh_pt.push_back(Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Outer_poro_SSS_fluid_near_inlet_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Outer_poro_SSS_fluid_near_outlet_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Outer_poro_SSS_fluid_block_FSI_surface_mesh_pt);
        sprintf(filename,"%s/poro_fsi_check%i.dat",
                doc_info.directory().c_str(),
                doc_info.number());
        some_file.open(filename);
        sprintf(filename,"%s/poro_boundary_coord%i.dat",
                doc_info.directory().c_str(),
                doc_info.number());
        ofstream some_file2;
        some_file2.open(filename);
        unsigned nm=all_mesh_pt.size();
        for (unsigned m=0;m<nm;m++)
         {
          unsigned nel=all_mesh_pt[m]->nelement();
          for (unsigned e=0;e<nel;e++)
           {
            dynamic_cast<ElementWithExternalElement*>
             (all_mesh_pt[m]->element_pt(e))->
             output_external_elements(some_file,0);
            
            unsigned nplot=5;
            dynamic_cast<FaceElement*>
             (all_mesh_pt[m]->element_pt(e))->
             output_zeta(some_file2,nplot);
           }
         }
        some_file.close();
        some_file2.close();
       }
       
       // Doc BJS boundary condition for fluid
       //-------------------------------------
       
       sprintf(filename,"%s/bjs_fsi_syrinx%i.dat",
               doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt->output(some_file);
       Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt->output(some_file);
       Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt->output(some_file);
       some_file.close();
       
       sprintf(filename,"%s/bjs_fsi_sss%i.dat",
               doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       SSS_fluid_upper_inner_poro_FSI_surface_mesh_pt->output(some_file);
       SSS_fluid_central_inner_poro_FSI_surface_mesh_pt->output(some_file);
       SSS_fluid_lower_inner_poro_FSI_surface_mesh_pt->output(some_file);
       SSS_fluid_outer_poro_near_inlet_FSI_surface_mesh_pt->output(some_file);
       SSS_fluid_outer_poro_block_FSI_surface_mesh_pt->output(some_file);
       SSS_fluid_outer_poro_near_outlet_FSI_surface_mesh_pt->output(some_file);
       some_file.close();
       
       sprintf(filename,"%s/bjs_fsi_inner_sss%i.dat",
               doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       SSS_fluid_upper_inner_poro_FSI_surface_mesh_pt->output(some_file);
       SSS_fluid_central_inner_poro_FSI_surface_mesh_pt->output(some_file);
       SSS_fluid_lower_inner_poro_FSI_surface_mesh_pt->output(some_file);
       some_file.close();
       
       
       
       // Check external elements for fluid FSI meshes
       {
        Vector<Mesh*> all_mesh_pt;
        all_mesh_pt.push_back(SSS_fluid_upper_inner_poro_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(SSS_fluid_central_inner_poro_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(SSS_fluid_lower_inner_poro_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(SSS_fluid_outer_poro_near_inlet_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(SSS_fluid_outer_poro_block_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(SSS_fluid_outer_poro_near_outlet_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt);
        all_mesh_pt.push_back(Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt);
        sprintf(filename,"%s/fluid_fsi_check%i.dat",
                doc_info.directory().c_str(),
                doc_info.number());
        some_file.open(filename);
        sprintf(filename,"%s/fluid_boundary_coord%i.dat",
                doc_info.directory().c_str(),
                doc_info.number());
        ofstream some_file2;      some_file2.open(filename);
        unsigned nm=all_mesh_pt.size();
        for (unsigned m=0;m<nm;m++)
         {
          unsigned nel=all_mesh_pt[m]->nelement();
          for (unsigned e=0;e<nel;e++)
           {
            dynamic_cast<ElementWithExternalElement*>
             (all_mesh_pt[m]->element_pt(e))->
             output_external_elements(some_file,0);
            
            unsigned nplot=5;
            dynamic_cast<FaceElement*>
             (all_mesh_pt[m]->element_pt(e))->
             output_zeta(some_file2,nplot);
           }
         }
        some_file.close();
        some_file2.close();
       }

      } // end if for no steady (inner poro only) solve

     // Output bulk meshes in Paraview format
     bool output_in_paraview_format=true;
     if(output_in_paraview_format)
      {
       // Output solution on inner poro mesh in Paraview format (with padded 0s)
       sprintf(filename,"%s/soln-inner-poro%05i.vtu",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Inner_poro_mesh_pt->output_paraview(some_file,npts);
       some_file.close();

       // Output solution on outer poro mesh in Paraview format (with padded 0s)
       sprintf(filename,"%s/soln-outer-poro%05i.vtu",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Outer_poro_mesh_pt->output_paraview(some_file,npts);
       some_file.close();

       // Output solution on syrinx fluid mesh in Paraview format (with padded 0s)
       sprintf(filename,"%s/soln-syrinx-fluid%05i.vtu",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Syrinx_fluid_mesh_pt->output_paraview(some_file,npts);
       some_file.close();

       // Output solution on SSS fluid mesh in Paraview format (with padded 0s)
       sprintf(filename,"%s/soln-sss-fluid%05i.vtu",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       SSS_fluid_mesh_pt->output_paraview(some_file,npts);
       some_file.close();



       // Coarse (to visualise mesh)
       unsigned npts_coarse=2;

       // Output solution on inner poro mesh in Paraview format (with padded 0s)
       sprintf(filename,"%s/soln-inner-poro_mesh%05i.vtu",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Inner_poro_mesh_pt->output_paraview(some_file,npts_coarse);
       some_file.close();

       // Output solution on outer poro mesh in Paraview format (with padded 0s)
       sprintf(filename,"%s/soln-outer-poro_mesh%05i.vtu",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Outer_poro_mesh_pt->output_paraview(some_file,npts_coarse);
       some_file.close();

       // Output solution on syrinx fluid mesh in Paraview format (with padded 0s)
       sprintf(filename,"%s/soln-syrinx-fluid_mesh%05i.vtu",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Syrinx_fluid_mesh_pt->output_paraview(some_file,npts_coarse);
       some_file.close();

       // Output solution on SSS fluid mesh in Paraview format (with padded 0s)
       sprintf(filename,"%s/soln-sss-fluid_mesh%05i.vtu",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       SSS_fluid_mesh_pt->output_paraview(some_file,npts_coarse);
       some_file.close();


      } // end paraview bulk output
    }
   

   // Plot NSt veloc on centreline (r=0) of syrinx (mainly for
   //---------------------------------------------------------
   // (comp of cycle average pressure)
   //---------------------------------
   sprintf(filename,"%s/syrinx_centreline_veloc_line%i.dat",
           doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   Syrinx_centreline_veloc_line_mesh_pt->output(some_file,npts);
   some_file.close();

   // Plot NSt veloc on inner fsi boundary of sss (mainly for
   //--------------------------------------------------------
   // (comp of cycle average pressure)
   //---------------------------------
   sprintf(filename,"%s/sss_inner_fsi_boundary_veloc_line%i.dat",
           doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   SSS_inner_fsi_boundary_veloc_line_mesh_pt->output(some_file,npts);
   some_file.close();
   
   // Plot porous stuff in radial line across syrinx cover
   //-----------------------------------------------------
   sprintf(filename,"%s/radial_line_through_syrinx_cover_veloc_line%i.dat",
           doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   Inner_poro_radial_line_mesh_pt->output(some_file,npts);
   some_file.close();
 
   // Compute volume flux across radial line in syrinx cover
   double syrinx_cover_radial_line_seepage_flux=0.0;
   double syrinx_cover_radial_line_skeleton_flux=0.0;
   {
    unsigned nel=Inner_poro_radial_line_mesh_pt->nelement();
    for (unsigned e=0;e<nel;e++)
     {
      double syrinx_cover_radial_line_seepage_flux_contrib=0.0;
      double syrinx_cover_radial_line_skeleton_flux_contrib=0.0;
      dynamic_cast<AxisymmetricPoroelasticityTractionElement
       <PORO_ELEMENT>*>(Inner_poro_radial_line_mesh_pt->element_pt(e))->
       contribution_to_total_porous_flux(
        syrinx_cover_radial_line_skeleton_flux_contrib,
        syrinx_cover_radial_line_seepage_flux_contrib);
      syrinx_cover_radial_line_seepage_flux+=
       Global_Physical_Variables::St*
       syrinx_cover_radial_line_seepage_flux_contrib;
      syrinx_cover_radial_line_skeleton_flux+=
       Global_Physical_Variables::St*
       syrinx_cover_radial_line_skeleton_flux_contrib;
     }
   }


   // Plot/analyse NSt veloc in radial line across gap under block
   //-------------------------------------------------------------
   sprintf(filename,"%s/gap_under_block_veloc_line%i.dat",
           doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   SSS_gap_under_block_veloc_line_mesh_pt->output(some_file,npts);
   some_file.close();
 
   // Compute volume flux at radial line across gap under block
   double gap_under_block_radial_line_flux=0.0;
   {
    unsigned nel=SSS_gap_under_block_veloc_line_mesh_pt->nelement();
    for (unsigned e=0;e<nel;e++)
     {
      gap_under_block_radial_line_flux+=
       dynamic_cast<AxisymmetricNavierStokesLinePostProcessingElement
       <FLUID_ELEMENT>*>(SSS_gap_under_block_veloc_line_mesh_pt->element_pt(e))->
       contribution_to_total_volume_flux()/Global_Physical_Variables::St;
     }
   }


   // Plot/analyse NSt veloc in radial line across syrinx
   //-----------------------------------------------------
   sprintf(filename,"%s/syrinx_veloc_line%i.dat",doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   Syrinx_veloc_line_mesh_pt->output(some_file,npts);
   some_file.close();

   // Compute volume flux at radial line across syrinx
   double syrinx_radial_line_flux=0.0;
   {
    unsigned nel=Syrinx_veloc_line_mesh_pt->nelement();
    for (unsigned e=0;e<nel;e++)
     {
      syrinx_radial_line_flux+=
       dynamic_cast<AxisymmetricNavierStokesLinePostProcessingElement
       <FLUID_ELEMENT>*>(Syrinx_veloc_line_mesh_pt->element_pt(e))->
       contribution_to_total_volume_flux()/Global_Physical_Variables::St;
     }
   }

   // Plot/analyse NSt veloc in line across inlet
   //---------------------------------------------
   sprintf(filename,"%s/sss_veloc_line%i.dat",doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   SSS_veloc_line_mesh_pt->output(some_file,npts);
   some_file.close();

   // Compute volume flux at inlet
   double inlet_flux=0.0;
   {
    unsigned nel=SSS_veloc_line_mesh_pt->nelement();
    for (unsigned e=0;e<nel;e++)
     {
      inlet_flux+=
       dynamic_cast<AxisymmetricNavierStokesLinePostProcessingElement
       <FLUID_ELEMENT>*>(SSS_veloc_line_mesh_pt->element_pt(e))->
       contribution_to_total_volume_flux()/Global_Physical_Variables::St;
     }
   }

   // Plot/analyse NSt veloc along entire boundary of sss mesh
   //---------------------------------------------------------
   sprintf(filename,"%s/sss_boundary_veloc_line%i.dat",
           doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   Test_volume_flux_out_of_sss_mesh_pt->output(some_file,npts);
   some_file.close();
 

   // Compute volume flux over entire SSS boundary (should be zero)
   double total_sss_boundary_flux=0.0;
   {
    unsigned nel=Test_volume_flux_out_of_sss_mesh_pt->nelement();
    for (unsigned e=0;e<nel;e++)
     {
      total_sss_boundary_flux+=
       dynamic_cast<AxisymmetricNavierStokesLinePostProcessingElement
       <FLUID_ELEMENT>*>(Test_volume_flux_out_of_sss_mesh_pt->element_pt(e))->
       contribution_to_total_volume_flux()/Global_Physical_Variables::St;
     }
   }


   // Compute porous flux over inner syrinx cover boundary
   //------------------------------------------------------
   double inner_syrinx_cover_seepage_flux=0.0;
   double inner_upstream_syrinx_cover_seepage_flux=0.0;
   double inner_downstream_syrinx_cover_seepage_flux=0.0;
   unsigned nel=Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt->nelement();
   for (unsigned e=0;e<nel;e++)
    {
     double inner_syrinx_cover_seepage_flux_contrib=0.0;
     double inner_syrinx_cover_skeleton_flux_contrib=0.0;
     
     FSILinearisedAxisymPoroelasticTractionElement
      <PORO_ELEMENT,FLUID_ELEMENT>* el_pt=
      dynamic_cast<FSILinearisedAxisymPoroelasticTractionElement
      <PORO_ELEMENT,FLUID_ELEMENT>*>(
       Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt->element_pt(e));
     el_pt->contribution_to_total_porous_flux(
      inner_syrinx_cover_skeleton_flux_contrib,
      inner_syrinx_cover_seepage_flux_contrib);
     
     inner_syrinx_cover_seepage_flux+=
      Global_Physical_Variables::St*
      inner_syrinx_cover_seepage_flux_contrib;
     
     // Are all the element's nodes upstream or downstream of 
     // radial control line?
     unsigned upstream_count=0;
     unsigned downstream_count=0;
     double nnod=el_pt->nnode();
     for (unsigned j=0;j<nnod;j++)
      {
       double z=el_pt->node_pt(j)->x(1);
       if (z>=Z_radial_cut_through_syrinx*
           Global_Physical_Variables::Z_scale*
           Global_Physical_Variables::Z_shrink_factor)
        {
         upstream_count++;
        }
       if (z<=Z_radial_cut_through_syrinx*
           Global_Physical_Variables::Z_scale*
           Global_Physical_Variables::Z_shrink_factor)
        {
         downstream_count++;
        }
      }
     
     // Add to the appropriate flux
     if (upstream_count==nnod)
      {
       inner_upstream_syrinx_cover_seepage_flux+=
        Global_Physical_Variables::St*
        inner_syrinx_cover_seepage_flux_contrib;
       if (downstream_count==nnod)
        {
         throw OomphLibError(
          "Element is both upstream and downstream! Something's wrong.",
          OOMPH_CURRENT_FUNCTION,
          OOMPH_EXCEPTION_LOCATION);
        }
      }
     else if (downstream_count==nnod)
      {
       inner_downstream_syrinx_cover_seepage_flux+=
        Global_Physical_Variables::St*
        inner_syrinx_cover_seepage_flux_contrib;
      }
     else
      {
       std::ostringstream error_stream;
       error_stream
        << "Element is neither upstream nor downstream! Something's wrong.",
        throw OomphLibError(
         error_stream.str(),
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
      }
    }



   // Compute porous flux over outer syrinx cover boundary
   //-----------------------------------------------------
   double outer_upstream_syrinx_cover_seepage_flux=0.0;
   double outer_downstream_syrinx_cover_seepage_flux=0.0;
   nel=Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt->nelement();
   for (unsigned e=0;e<nel;e++)
    {
     double syrinx_cover_seepage_flux_contrib=0.0;
     double syrinx_cover_skeleton_flux_contrib=0.0;

     FSILinearisedAxisymPoroelasticTractionElement
      <PORO_ELEMENT,FLUID_ELEMENT>* el_pt=
      dynamic_cast<FSILinearisedAxisymPoroelasticTractionElement
      <PORO_ELEMENT,FLUID_ELEMENT>*>(
       Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt->element_pt(e));
     el_pt->contribution_to_total_porous_flux(
      syrinx_cover_skeleton_flux_contrib,
      syrinx_cover_seepage_flux_contrib);
   
     // Are all the element's nodes upstream or downstream of 
     // radial control line?
     unsigned upstream_count=0;
     unsigned downstream_count=0;
     double nnod=el_pt->nnode();
     for (unsigned j=0;j<nnod;j++)
      {
       double z=el_pt->node_pt(j)->x(1);
       if (z>=Z_radial_cut_through_syrinx*
           Global_Physical_Variables::Z_scale*
           Global_Physical_Variables::Z_shrink_factor)
        {
         upstream_count++;
        }
       if (z<=Z_radial_cut_through_syrinx*
           Global_Physical_Variables::Z_scale*
           Global_Physical_Variables::Z_shrink_factor)
        {
         downstream_count++;
        }
      }
   
     // Add to the appropriate flux
     if (upstream_count==nnod)
      {
       outer_upstream_syrinx_cover_seepage_flux+=
        Global_Physical_Variables::St*
        syrinx_cover_seepage_flux_contrib;
       if (downstream_count==nnod)
        {
         throw OomphLibError(
          "Element is both upstream and downstream! Something's wrong.",
          OOMPH_CURRENT_FUNCTION,
          OOMPH_EXCEPTION_LOCATION);
        }
      }
     else if (downstream_count==nnod)
      {
       outer_downstream_syrinx_cover_seepage_flux+=
        Global_Physical_Variables::St*
        syrinx_cover_seepage_flux_contrib;
      }
     else
      {
       std::ostringstream error_stream;
       error_stream
        << "Element is neither upstream nor downstream! Something's wrong.",
        throw OomphLibError(
         error_stream.str(),
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
      }
    }
 
   // Compute syrinx volume via line integral  on syrinx mesh
   // -------------------------------------------------------
   Vector<Mesh*> fsi_interface_mesh_pt;
   fsi_interface_mesh_pt.push_back(
    Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt);
   fsi_interface_mesh_pt.push_back(
    Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt);
   fsi_interface_mesh_pt.push_back(
    Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt);
   double vol_syrinx_via_integral=0.0;
   unsigned nb=fsi_interface_mesh_pt.size();
   for (unsigned bb=0;bb<nb;bb++)
    {
     unsigned nel = fsi_interface_mesh_pt[bb]->nelement();
     for(unsigned e=0;e<nel;e++)
      {
       LinearisedAxisymPoroelasticBJS_FSIElement<FLUID_ELEMENT,PORO_ELEMENT>*
        el_pt=dynamic_cast<
        LinearisedAxisymPoroelasticBJS_FSIElement<FLUID_ELEMENT,PORO_ELEMENT>*>
        (fsi_interface_mesh_pt[bb]->element_pt(e));
              
       vol_syrinx_via_integral+=el_pt->contribution_to_enclosed_volume();
       
      }
    } 



   // Compute upstream and downstream syrinx volumes via line integrals  on syrinx mesh
   // ---------------------------------------------------------------------------------
   double upper_syrinx_volume_from_syrinx_boundary_integral=0.0;
   double lower_syrinx_volume_from_syrinx_boundary_integral=0.0;
   double total_syrinx_volume_from_syrinx_boundary_integral=0.0;
   compute_syrinx_volumes_from_boundary_integral(
    upper_syrinx_volume_from_syrinx_boundary_integral,
    lower_syrinx_volume_from_syrinx_boundary_integral,
    total_syrinx_volume_from_syrinx_boundary_integral);

   // Doc change
   double upper_syrinx_volume_change=
    upper_syrinx_volume_from_syrinx_boundary_integral-
    Initial_upper_syrinx_volume;
   double lower_syrinx_volume_change=    
    lower_syrinx_volume_from_syrinx_boundary_integral-
    Initial_lower_syrinx_volume;
   oomph_info << "Change in lower syrinx volume FROM LINE INTEGRALS: " 
              << lower_syrinx_volume_change
              << std::endl;
   oomph_info << "Change in upper syrinx volume FROM LINE INTEGRALS: " 
              << upper_syrinx_volume_change
              << std::endl;
   oomph_info << "Change in total syrinx volume  FROM LINE INTEGRALS: " 
              << upper_syrinx_volume_change+lower_syrinx_volume_change
              << std::endl;


   // Compute syrinx volume
   //----------------------
   double vol_syrinx=0.0;
   double dvol_syrinx=0.0;
   {
    unsigned nel=Syrinx_volume_analysis_mesh_pt->nelement();
    for (unsigned e=0;e<nel;e++)
     {
      AxisymmetricPoroelasticityLinePostProcessingElement
       <PORO_ELEMENT>* analysis_element_pt=
       dynamic_cast<AxisymmetricPoroelasticityLinePostProcessingElement
       <PORO_ELEMENT>*>(Syrinx_volume_analysis_mesh_pt->element_pt(e));
    
      double dvol_contrib=0.0;
      double vol_contrib=analysis_element_pt->
       contribution_to_enclosed_volume(dvol_contrib);
      vol_syrinx+=vol_contrib;
      dvol_syrinx+=dvol_contrib;
     }
    
    oomph_info << "SYRINX VOLUME AND INCREMENT VIA LINEARISED BOUNDARY INTEGRAL: "
               << vol_syrinx << " " << dvol_syrinx << std::endl;
    double lin_dvol=dvol_syrinx;

    // Moving fluid mesh: compute volume from there
    double current_vol_syrinx=0.0;
    if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update!=-1)
     {
      // Compute current volume from fluid elements
      unsigned nel=Syrinx_fluid_mesh_pt->nelement();
      for (unsigned e=0;e<nel;e++)
       {
        current_vol_syrinx+=dynamic_cast<FLUID_ELEMENT*>(
         Syrinx_fluid_mesh_pt->element_pt(e))->compute_physical_size();
       }

      // Over-write original volume with analytical value
      // (sign change to be consistent with previous version)
      vol_syrinx=-Analytical_syrinx_volume;

      // Overwrite (sign change to be consistent with previous version)
      dvol_syrinx=-(current_vol_syrinx+vol_syrinx);

      oomph_info << "ABSOLUTE DIFFERENCE BETWEEN LINEARISED AND NONLINEAR CHANGE IN SYRINX VOLUME: "
                 << std::fabs(lin_dvol)-std::fabs(dvol_syrinx) 
                 << " COMPARED TO NONLIN CHANGE: " << dvol_syrinx << std::endl;
     }

    set_change_in_syrinx_volume(-dvol_syrinx);
    oomph_info << "Undeformed syrinx volume  : " <<  -vol_syrinx << std::endl;
    oomph_info << "Increment to syrinx volume: " << -dvol_syrinx << std::endl;

    // Check
    if (std::fabs(-vol_syrinx-Analytical_syrinx_volume)>1.0e-10)
     {
      oomph_info << "Error in syrinx volume: " 
                 << std::fabs(-vol_syrinx-Analytical_syrinx_volume)
                 << std::endl;
      // Have to tolerate this for now because volume is worked
      // out by attaching analysis elements to Navier Stokes (now moving)
      // and poro elements (though there's actually no problem here
      // because there's no contribution from the centreline).
      if (AxisymmetricPoroelasticityTractionElementHelper::Allow_gap_in_FSI)
       {
        oomph_info << "...tolerated\n";
       }
      else
       {
        throw OomphLibError(
         "Error in syrinx volume",
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
       }
     }

    
    oomph_info 
     << "SYRINX VOLUME VIA SYRINX BOUNDARY INTEGRAL: "
     << vol_syrinx_via_integral << " " 
     << " CORRESPONDING TO CHANGE OF: "
     << fabs(vol_syrinx_via_integral)-std::fabs(Analytical_syrinx_volume)
     << " DIFFERENCE TO VOLUMETRIC METHOD: "
     << fabs(vol_syrinx_via_integral)-std::fabs(current_vol_syrinx)
     << std::endl;
    
   }


     



   // Compute downstream SSS volume
   //------------------------------
   sprintf(filename,"%s/downstream_sss_volume%i.dat",doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   double vol_downstream_sss=0.0;
   double dvol_downstream_sss=0.0;
   {
    unsigned nel=Downstream_SSS_volume_analysis_mesh_pt->nelement();
    for (unsigned e=0;e<nel;e++)
     {
      AxisymmetricPoroelasticityLinePostProcessingElement
       <PORO_ELEMENT>* analysis_element_pt=
       dynamic_cast<AxisymmetricPoroelasticityLinePostProcessingElement
       <PORO_ELEMENT>*>(Downstream_SSS_volume_analysis_mesh_pt->element_pt(e));
    
      analysis_element_pt->output(some_file);
      double dvol_contrib=0.0;
      double vol_contrib=analysis_element_pt->
       contribution_to_enclosed_volume(dvol_contrib);
      vol_downstream_sss+=vol_contrib;
      dvol_downstream_sss+=dvol_contrib;
     }
    
    // Moving fluid mesh: compute volume from there
    if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update!=-1)
     {
      // Compute current volume from fluid elements
      double current_vol_downstream_sss=0.0;
      unsigned nel=Downstream_SSS_el_pt.size();
      for (unsigned e=0;e<nel;e++)
       {
        current_vol_downstream_sss+=
         Downstream_SSS_el_pt[e]->compute_physical_size();
       }

      // Over-write original volume with analytical value
      // (sign change to be consistent with previous version)
      vol_downstream_sss=-Analytical_downstream_SSS_volume;

      // Overwrite (sign change to be consistent with previous version)
      dvol_downstream_sss=-(current_vol_downstream_sss+vol_downstream_sss);
     }

    set_change_in_downstream_sss_volume(-dvol_downstream_sss);
    oomph_info << "Undeformed downstream sss volume  : " 
               <<  -vol_downstream_sss << std::endl;
    oomph_info << "Increment to downstream sss volume: " 
               << -dvol_downstream_sss << std::endl;

    // Check
    if (std::fabs(-vol_downstream_sss-Analytical_downstream_SSS_volume)>1.0e-10)
     {
      oomph_info 
       << "Error in downstream sss volume: " 
       << std::fabs(-vol_downstream_sss-Analytical_downstream_SSS_volume)
       << std::endl;
      // Have to tolerate this for now because volume is worked
      // out by attaching analysis elements to Navier Stokes (now moving)
      // and poro elements.
      if (AxisymmetricPoroelasticityTractionElementHelper::Allow_gap_in_FSI)
       {
        oomph_info << "...tolerated\n";
       }
      else
       {
        throw OomphLibError(
         "Error in downstream sss volume",
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
       }
     }
   }
   some_file.close();



   // Compute upstream SSS volume
   //------------------------------
   sprintf(filename,"%s/upstream_sss_volume%i.dat",doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   double vol_upstream_sss=0.0;
   double dvol_upstream_sss=0.0;
   {
    unsigned nel=Upstream_SSS_volume_analysis_mesh_pt->nelement();
    for (unsigned e=0;e<nel;e++)
     {
      AxisymmetricPoroelasticityLinePostProcessingElement
       <PORO_ELEMENT>* analysis_element_pt=
       dynamic_cast<AxisymmetricPoroelasticityLinePostProcessingElement
       <PORO_ELEMENT>*>(Upstream_SSS_volume_analysis_mesh_pt->element_pt(e));
    
      analysis_element_pt->output(some_file);
      double dvol_contrib=0.0;
      double vol_contrib=analysis_element_pt->
       contribution_to_enclosed_volume(dvol_contrib);
      vol_upstream_sss+=vol_contrib;
      dvol_upstream_sss+=dvol_contrib;
     }

    // Moving fluid mesh: compute volume from there
    if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update!=-1)
     {
      // Compute current volume from fluid elements
      double current_vol_upstream_sss=0.0;
      unsigned nel=Upstream_SSS_el_pt.size();
      for (unsigned e=0;e<nel;e++)
       {
        current_vol_upstream_sss+=
         Upstream_SSS_el_pt[e]->compute_physical_size();
       }

      // Over-write original volume with analytical value
      // (sign change to be consistent with previous version)
      vol_upstream_sss=-Analytical_upstream_SSS_volume;

      // Overwrite (sign change to be consistent with previous version)
      dvol_upstream_sss=-(current_vol_upstream_sss+vol_upstream_sss);
     }


    set_change_in_upstream_sss_volume(-dvol_upstream_sss);
    oomph_info << "Undeformed upstream sss volume  : " 
               <<  -vol_upstream_sss << std::endl;
    oomph_info << "Increment to upstream sss volume: " 
               << -dvol_upstream_sss << std::endl;

    // Check
    if (std::fabs(-vol_upstream_sss-Analytical_upstream_SSS_volume)>1.0e-10)
     {
      oomph_info 
       << "Error in upstream sss volume: " 
       << std::fabs(-vol_upstream_sss-Analytical_upstream_SSS_volume)
       << std::endl;
      // Have to tolerate this for now because volume is worked
      // out by attaching analysis elements to Navier Stokes (now moving)
      // and poro elements.
      if (AxisymmetricPoroelasticityTractionElementHelper::Allow_gap_in_FSI)
       {
        oomph_info << "...tolerated\n";
       }
      else
       {
        throw OomphLibError(
         "Error in upstream sss volume",
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
       }
     }
   }
   some_file.close();


     
   // Compute seepage flux over syrinx fsi boundary (from poro domain)
   //-----------------------------------------------------------------
   double total_syrinx_seepage_flux=0.0;
   {
    Vector<Mesh*> all_mesh_pt;
    all_mesh_pt.push_back(
     Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt);
    all_mesh_pt.push_back(
     Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt);
    all_mesh_pt.push_back(
     Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt);
    unsigned nm=all_mesh_pt.size();
    for (unsigned m=0;m<nm;m++)
     {
      unsigned nel=all_mesh_pt[m]->nelement();
      for (unsigned e=0;e<nel;e++)
       {
        double skeleton_flux_contrib=0.0;
        double seepage_flux_contrib=0.0;
        FSILinearisedAxisymPoroelasticTractionElement
         <PORO_ELEMENT,FLUID_ELEMENT>* el_pt=
         dynamic_cast<FSILinearisedAxisymPoroelasticTractionElement
                        <PORO_ELEMENT,FLUID_ELEMENT>*>(
                         all_mesh_pt[m]->element_pt(e));
        el_pt->contribution_to_total_porous_flux(
         skeleton_flux_contrib,
         seepage_flux_contrib);
        
        total_syrinx_seepage_flux+=
         Global_Physical_Variables::St*seepage_flux_contrib;
       }
     }
    oomph_info << "Total seepage flux over syrinx fsi boundary [from poro]: "
               << total_syrinx_seepage_flux << std::endl;
   }
   


     
   // Compute seepage flux over syrinx fsi boundary (from nst domain)
   //----------------------------------------------------------------
   double total_nst_syrinx_seepage_flux=0.0;
   double total_nst_syrinx_nst_flux=0.0;
   double total_nst_syrinx_skeleton_flux=0.0;
   {
    Vector<Mesh*> all_mesh_pt;

    all_mesh_pt.push_back(
     Syrinx_fluid_upper_inner_poro_FSI_surface_mesh_pt);
    all_mesh_pt.push_back(
     Syrinx_fluid_central_inner_poro_FSI_surface_mesh_pt);
    all_mesh_pt.push_back(
     Syrinx_fluid_lower_inner_poro_FSI_surface_mesh_pt);
    unsigned nm=all_mesh_pt.size();
    for (unsigned m=0;m<nm;m++)
     {
      unsigned nel=all_mesh_pt[m]->nelement();
      for (unsigned e=0;e<nel;e++)
       {
        double skeleton_flux_contrib=0.0;
        double seepage_flux_contrib=0.0;
        double nst_flux_contrib=0.0;
        LinearisedAxisymPoroelasticBJS_FSIElement
         <FLUID_ELEMENT,PORO_ELEMENT>* el_pt=
         dynamic_cast<LinearisedAxisymPoroelasticBJS_FSIElement
                      <FLUID_ELEMENT,PORO_ELEMENT>*>(
                       all_mesh_pt[m]->element_pt(e));
        el_pt->contribution_to_total_porous_flux(skeleton_flux_contrib,
                                                 seepage_flux_contrib,
                                                 nst_flux_contrib);
        
        total_nst_syrinx_nst_flux+=nst_flux_contrib;
        total_nst_syrinx_seepage_flux+=
         Global_Physical_Variables::St*seepage_flux_contrib;
        total_nst_syrinx_skeleton_flux+=
         Global_Physical_Variables::St*skeleton_flux_contrib;
       }
     }
    oomph_info 
     << "Total seepage flux over syrinx fsi boundary [from NSt]: "
     << total_nst_syrinx_seepage_flux << std::endl;
    
    oomph_info 
     << "Total Navier Stokes flux over syrinx fsi boundary [from NSt]: "
     << total_nst_syrinx_nst_flux << std::endl;
    
    oomph_info 
     << "Total skeleton flux over syrinx fsi boundary [from NSt]: "
     << total_nst_syrinx_skeleton_flux << std::endl;
   }
   



   // Keeps getting over-written but it's easier to maintain if
   // if info is generated here
   sprintf(filename,"%s/trace_header.dat",doc_info.directory().c_str());
   some_file.open(filename);
   some_file << "VARIABLES = ";
   double t=time_pt()->time();
   Trace_file << t << " "; // column 1
   some_file << "\"time\", ";
   
   Trace_file << Global_Physical_Variables::Re*
    Global_Physical_Variables::time_dependent_magnitude_of_driving_pressure(t) 
    << " ";  // column 2
   some_file << "\"entry pressure\", ";

   // Navier Stokes influx -- computed from NSt.
   Trace_file << inlet_flux << " "; // column 3
   some_file << "\"inlet flux\", ";

   // Net flux through radial control line in syrinx -- computed from NSt.
   Trace_file << syrinx_radial_line_flux << " "; // column 4
   some_file << "\"vol flux across radial line across syrinx\", ";

   // Net porous seepage flux through radial control line in syrinx cover
   Trace_file << syrinx_cover_radial_line_seepage_flux << " "; // column 5
   some_file << "\"seepage vol flux across radial line across syrinx cover\", ";

   // Net skeleton flux through radial control line in syrinx cover
   Trace_file << syrinx_cover_radial_line_skeleton_flux << " "; // column 6
   some_file <<"\"skeleton vol flux across radial line across syrinx cover\", ";

   // Net skeleton+seepage flux through radial control line in syrinx cover
   Trace_file <<  syrinx_cover_radial_line_seepage_flux
    +syrinx_cover_radial_line_skeleton_flux  << " "; // column 7
    some_file <<"\"vol flux across radial line across syrinx cover\", ";

   // Net flux through radial control line in gap under block
   Trace_file << gap_under_block_radial_line_flux << " "; // column 8
   some_file << "\"vol flux across radial line across sss under block\", ";

   // Total Navier Stokes flux over SSS boundaries (should be zero) -- from Nst
   Trace_file << total_sss_boundary_flux << " "; // column 9
   some_file << "\"integral continuity check for sss (should be zero)\", ";

   // Syrinx volume and change (minus because volume was computed for 
   // volume of solid) -- from poro
   Trace_file << -vol_syrinx << " ";  // column 10
   some_file << "\"syrinx volume\", ";

   Trace_file  << -dvol_syrinx << " "; // column 11
   some_file << "\"change in syrinx volume\", ";

   // Net porous seepage flux through inner syrinx cover -- from poro
   Trace_file << inner_syrinx_cover_seepage_flux << " "; // column 12
   some_file << "\"seepage flux over syrinx cover (through syrinx fsi surface)\", ";

   // Net porous seepage flux through upstream inner syrinx cover -- from poro
   Trace_file << inner_upstream_syrinx_cover_seepage_flux << " "; // column 13
   some_file << "\"seepage flux over syrinx cover (through upstream syrinx fsi surface)\", ";

   // Net porous seepage flux through downstream inner syrinx cover -- from poro
   Trace_file << inner_downstream_syrinx_cover_seepage_flux << " "; // column 14
   some_file << "\"seepage flux over syrinx cover (through downstream syrinx fsi surface)\", ";

   // Upstream SSS volume and change (minus because volume was computed for 
   // volume of solid)  -- from poro
   Trace_file << -vol_upstream_sss << " ";  // column 15
   some_file << "\"upstream sss volume\", ";

   Trace_file << -dvol_upstream_sss << " "; // column 16
    some_file << "\"change in upstream sss volume\", ";

   // Net porous seepage flux through outer upstream syrinx cover -- from poro
   Trace_file << outer_upstream_syrinx_cover_seepage_flux << " "; // column 17
    some_file << "\"seepage flux over syrinx cover (through upstream sss fsi surface)\", ";


   // Downstream SSS volume and change (minus because volume was computed for 
   // volume of solid)  -- from poro
    Trace_file << -vol_downstream_sss << " ";  // column 18
   some_file << "\"downstream sss volume\", ";

   Trace_file << -dvol_downstream_sss << " "; // column 19
   some_file << "\"change in downstream sss volume\", ";
   
   // Net porous seepage flux through outer downstream syrinx cover -- from poro
   Trace_file << outer_downstream_syrinx_cover_seepage_flux << " "; // column 20
   some_file << "\"seepage flux over syrinx cover (through downstream sss fsi surface)\", ";

   // Reference for error in volume fluxes: volume flux corresponding 
   // to entire syrinx volume being drained over one period of the oscillation.
   // ...at least it's constant; all other quantities give mad traces
   // because reference goes through zero.
   double ref=(-vol_syrinx/Global_Physical_Variables::Excitation_period);

   /// Check consistency of syrinx volume change
   double syrinx_vol_error=rate_of_change_in_syrinx_volume()-
    inner_syrinx_cover_seepage_flux;
   double rel_error=0.0;
   // double ref=std::max(std::fabs(rate_of_change_in_syrinx_volume()),
   //                     std::fabs(inner_syrinx_cover_seepage_flux));
   if (ref!=0.0)
    {
     rel_error=syrinx_vol_error/ref;
    }
   oomph_info << "Error in syrinx volume balance: " 
              << syrinx_vol_error  << " [ "
              << rel_error*100.0 << " % ]\n";
   Trace_file << rel_error*100.0 << " "; // column 21
   some_file << "\"error in syrinx volume balance (%)\", ";

 
   /// Check consistency of upstream sss volume change
   double upstream_sss_vol_error=rate_of_change_in_upstream_sss_volume()-
    outer_upstream_syrinx_cover_seepage_flux+
    inlet_flux+
    gap_under_block_radial_line_flux;
   rel_error=0.0;
   // ref=std::max(std::fabs(rate_of_change_in_upstream_sss_volume()),
   //              std::fabs(outer_upstream_syrinx_cover_seepage_flux));
   // ref=std::max(ref,std::fabs(gap_under_block_radial_line_flux));
   // ref=std::max(ref,std::fabs(inlet_flux));
   if (ref!=0.0)
    {
     rel_error=upstream_sss_vol_error/ref;
    }
   oomph_info << "Error in upstream sss volume balance: " 
              << upstream_sss_vol_error  << " [ "
              << rel_error*100.0 << " % ]\n";
   Trace_file << rel_error*100.0 << " "; // column 22
   some_file << "\"error in upstream sss volume balance (%)\", ";

   /// Check consistency of downstream sss volume change
   double downstream_sss_vol_error=rate_of_change_in_downstream_sss_volume()-
    outer_downstream_syrinx_cover_seepage_flux-
    gap_under_block_radial_line_flux;
   rel_error=0.0;
   // ref=std::max(std::fabs(rate_of_change_in_downstream_sss_volume()),
   //              std::fabs(outer_downstream_syrinx_cover_seepage_flux));
   // ref=std::max(ref,std::fabs(gap_under_block_radial_line_flux));
   if (ref!=0.0)
    {
     rel_error=downstream_sss_vol_error/ref;
    }
   oomph_info << "Error in downstream sss volume balance: " 
              << downstream_sss_vol_error  << " [ "
              << rel_error*100.0 << " % ]\n";
   Trace_file << rel_error*100.0 << " "; // column 23
   some_file << "\"error in downstream sss volume balance (%)\", ";


   // Step number
   Trace_file << doc_info.number() // column 24
              << " ";
   some_file << "\"doc step\" ";

   // Total syrinx seepage flux
   Trace_file << total_syrinx_seepage_flux << " "; // column 25
   some_file << "\"Total seepage flux over syrinx fsi boundary \" ";


   // Seepage flux from Nst
   Trace_file << total_nst_syrinx_seepage_flux << " "; // column 26
   some_file << "\"Total seepage flux over syrinx fsi boundary [from NSt]\" ";


   // Navier Stokes flux
   Trace_file << total_nst_syrinx_nst_flux << " "; // column 27
   some_file << "\"Total Navier Stokes flux over syrinx fsi boundary [from NSt]\" ";


   // Skeleton flux from Nst
   Trace_file << total_nst_syrinx_skeleton_flux << " "; // column 28
   some_file << "\"Total skeleton flux over syrinx fsi boundary [from NSt]\" ";


   // Steady data
   if(CommandLineArgs::command_line_flag_has_been_set(
       "--steady_solve"))
    {
     Trace_file 
      <<  Global_Physical_Variables::P_steady_state_syrinx // column 29
      << " ";
     some_file << "\"P_steady_syrinx\" ";
     
     Trace_file 
      <<  Global_Physical_Variables::P_steady_state_sss_upper // column 30
      << " ";
     some_file << "\"P_steady_sss_upper\" ";

     Trace_file 
      <<  Global_Physical_Variables::P_steady_state_sss_lower // column 31
      << " ";
     some_file << "\"P_steady_sss_lower\" ";


     Trace_file 
      << Global_Physical_Variables::Z_at_p_sss_change_start_raw // column 32
      << " ";
     some_file << "\"Z_at_p_sss_change_start_raw\" ";

     Trace_file 
      << Global_Physical_Variables::Z_sss_p_shift_raw // column 33 
      << " ";
     some_file << "\"Z_sss_p_shift_raw\" ";

    }
   
   
   Trace_file  << std::endl;
   some_file << std::endl;
   some_file.close();
   Trace_file.flush();

   // Finally create file that indicates that all the data for
   // this timestep has been written so an external shell script
   // can start moving them off the disk...
   sprintf(filename,"%s/written_step%i.dat",
           doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   some_file << "Step has been written. Time t = " 
             << time_pt()->time()  << " "
             << "Timestep dt = " << time_pt()->dt() << "\n";
   some_file.close();

   oomph_info << "Time for doc_solution: "
              << TimingHelpers::timer()-t_start << std::endl;
  }
 
 // Bump counter
 doc_info.number()++;
 
} // end_of_doc_solution




//========================================================================
/// Impose fake cord motion for step i out of n (in a time periodic 
/// axial displacement of cord FSI surface) -- assignment made to
/// history value that's used for node update.
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
impose_fake_cord_motion(const unsigned& i, const unsigned& n)
{
 
 double ampl=0.04/Global_Physical_Variables::Z_shrink_factor;
 double displ=ampl*sin(2.0*MathematicalConstants::Pi*double(i)/double(n-1));
 
 Vector<unsigned> fsi_interface;
 fsi_interface.push_back(Inner_poro_upper_SSS_interface_boundary_id);
 fsi_interface.push_back(Inner_poro_central_SSS_interface_boundary_id);
 fsi_interface.push_back(Inner_poro_lower_SSS_interface_boundary_id);
 unsigned nb=fsi_interface.size();
 for (unsigned bb=0;bb<nb;bb++)
  {
   // Now loop over the bulk elements and create the face elements
   unsigned b=fsi_interface[bb];
   unsigned nnod = Inner_poro_mesh_pt->nboundary_node(b);
   for(unsigned j=0;j<nnod;j++)
    {
     Node* nod_pt=Inner_poro_mesh_pt->boundary_node_pt(b,j);
     
     // Where are we?
     double z=nod_pt->x(1);
     
     // Change z displacement
     if (Global_Physical_Variables::
         History_level_for_fluid_mesh_node_update!=-1)
      {
       unsigned z_displ_index=1;
       nod_pt->set_value
        (Global_Physical_Variables::History_level_for_fluid_mesh_node_update,
         z_displ_index,
         displ*sin(MathematicalConstants::Pi*
                   z/(-30.0/Global_Physical_Variables::Z_shrink_factor)));
      }
    }
  }
}


//========================================================================
 /// Doc "spines"
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::doc_spines
(ofstream& spine_file)
{

 PORO_ELEMENT* inner_el_pt=0;
 Vector<double> inner_s(2);
 PORO_ELEMENT* outer_el_pt=0;
 Vector<double> outer_s(2);
 Vector<double> x_inner(2);
 Vector<double> x_outer(2);
 
 unsigned t_hist=unsigned(
  Global_Physical_Variables::History_level_for_fluid_mesh_node_update);
 
 // Loop over all nodes in SSS mesh
 {
  unsigned nnod=SSS_fluid_mesh_pt->nnode();
  for (unsigned j=0;j<nnod;j++)
   {
    outer_el_pt=SSS_fluid_mesh_outer_reference_point[j].first;
    outer_s=SSS_fluid_mesh_outer_reference_point[j].second;
    inner_el_pt=SSS_fluid_mesh_inner_reference_point[j].first;
    inner_s=SSS_fluid_mesh_inner_reference_point[j].second;
    
    // Get deformed position of point on outer wall
    outer_el_pt->interpolated_x(outer_s,x_outer);
    x_outer[0]+=outer_el_pt->interpolated_u(t_hist,outer_s,0);
    x_outer[1]+=outer_el_pt->interpolated_u(t_hist,outer_s,1);
    
    // Get deformed position of point on inner wall 
    inner_el_pt->interpolated_x(inner_s,x_inner);
    x_inner[0]+=inner_el_pt->interpolated_u(t_hist,inner_s,0);
    x_inner[1]+=inner_el_pt->interpolated_u(t_hist,inner_s,1);
        
    // Plot
    spine_file << x_inner[0] << " "
               << x_inner[1] << " "
               << x_outer[0]-x_inner[0] << " "
               << x_outer[1]-x_inner[1] << "\n";
   }


  // Loop over all nodes in Syrinx mesh
  {
   unsigned nnod=Syrinx_fluid_mesh_pt->nnode();
   for (unsigned j=0;j<nnod;j++)
    {
     outer_el_pt=Syrinx_fluid_mesh_reference_point[j].first;
     outer_s=Syrinx_fluid_mesh_reference_point[j].second;
     //double omega=Syrinx_fluid_mesh_omega[j];
     
     // Get deformed position of point on outer wall 
     outer_el_pt->interpolated_x(outer_s,x_outer);
     x_outer[0]+=outer_el_pt->interpolated_u(t_hist,outer_s,0);
     x_outer[1]+=outer_el_pt->interpolated_u(t_hist,outer_s,1);
     
    // Plot
    spine_file << 0.0 << " "
               << x_outer[1] << " "
               << x_outer[0] << " "
               << 0.0 << "\n";
    
    }
  }
 }
 
 
}
              


//========================================================================
/// Check computation of fluid volumes (for moving fluid mesh)
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
check_computation_of_fluid_volumes()
{

 // Set up doc info
 DocInfo doc_info;

 // Set output directory
 doc_info.set_directory(Global_Physical_Variables::Directory);

 // Doc before stretching
 doc_solution(doc_info);

 // Stretch all axial/radial dimensions of poro meshes by some factor
 double r_factor=1.5;
 double z_factor=1.2;

 // Stretch 'em
 unsigned nnod=Inner_poro_mesh_pt->nnode();
 for (unsigned j=0;j<nnod;j++)
  {
   Node* nod_pt=Inner_poro_mesh_pt->node_pt(j);
   nod_pt->set_value(0,nod_pt->x(0)*(r_factor-1.0));
   nod_pt->set_value(1,nod_pt->x(1)*(z_factor-1.0));
  }
 nnod=Outer_poro_mesh_pt->nnode();
 for (unsigned j=0;j<nnod;j++)
  {
   Node* nod_pt=Outer_poro_mesh_pt->node_pt(j);
   nod_pt->set_value(0,nod_pt->x(0)*(r_factor-1.0));
   nod_pt->set_value(1,nod_pt->x(1)*(z_factor-1.0));
  }
 
 // Update fluid mesh?
 if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update!=-1)
  {
   oomph_info << "Updating fluid mesh in check_computation_of_fluid_volumes()\n";
   move_fluid_nodes(
    Global_Physical_Variables::History_level_for_fluid_mesh_node_update);
  }
 else
  {
   oomph_info 
    << "Not doing fluid node update in check_computation_of_fluid_volumes()\n";
  }

 // Doc before stretching
 doc_solution(doc_info);


 oomph_info
  << "old/new/change-in syrinx volume should be: "
  << Analytical_syrinx_volume << " " 
  << r_factor*r_factor*z_factor*Analytical_syrinx_volume << " " 
  << (r_factor*r_factor*z_factor-1.0)*Analytical_syrinx_volume << " "
  << std::endl;
 
 oomph_info 
  << "old/new/change-in upstream_SSS volume should be: "
  << Analytical_upstream_SSS_volume << " " 
  << r_factor*r_factor*z_factor*Analytical_upstream_SSS_volume << " " 
  << (r_factor*r_factor*z_factor-1.0)*Analytical_upstream_SSS_volume << " "
  << std::endl;
 
 
 oomph_info 
  << "old/new/change-in downstream_SSS volume should be: "
  << Analytical_downstream_SSS_volume << " " 
  << r_factor*r_factor*z_factor*Analytical_downstream_SSS_volume << " " 
  << (r_factor*r_factor*z_factor-1.0)*Analytical_downstream_SSS_volume << " "
  << std::endl;
 
}




//========================================================================
/// Doc fluid node update
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
doc_node_update(const unsigned& t_hist, ofstream& doc_file)
{

 PORO_ELEMENT* inner_el_pt=0;
 Vector<double> inner_s(2);
 PORO_ELEMENT* outer_el_pt=0;
 Vector<double> outer_s(2);
 double omega=0.0;
 Vector<double> x_inner(2);
 Vector<double> x_outer(2);
 
 // Loop over all nodes in SSS mesh
 //--------------------------------
 {
  unsigned nnod=SSS_fluid_mesh_pt->nnode();
  for (unsigned j=0;j<nnod;j++)
   {
    outer_el_pt=SSS_fluid_mesh_outer_reference_point[j].first;
    outer_s=SSS_fluid_mesh_outer_reference_point[j].second;
    inner_el_pt=SSS_fluid_mesh_inner_reference_point[j].first;
    inner_s=SSS_fluid_mesh_inner_reference_point[j].second;
    omega=SSS_fluid_mesh_omega[j];
    
    // Get deformed position of point on outer wall
    outer_el_pt->interpolated_x(outer_s,x_outer);
    x_outer[0]+=outer_el_pt->interpolated_u(t_hist,outer_s,0);
    x_outer[1]+=outer_el_pt->interpolated_u(t_hist,outer_s,1);
    
    // Get deformed position of point on inner wall 
    inner_el_pt->interpolated_x(inner_s,x_inner);
    x_inner[0]+=inner_el_pt->interpolated_u(t_hist,inner_s,0);
    x_inner[1]+=inner_el_pt->interpolated_u(t_hist,inner_s,1);
        
    //Doc pos update
    // Current pos of node
    doc_file << SSS_fluid_mesh_pt->node_pt(j)->x(0) << " "
             << SSS_fluid_mesh_pt->node_pt(j)->x(1) << " "
     // Vector to inner ref point
             << x_inner[0]-SSS_fluid_mesh_pt->node_pt(j)->x(0) << " " 
             << x_inner[1]-SSS_fluid_mesh_pt->node_pt(j)->x(1) << " " 
     // Vector to outer ref point
             << x_outer[0]-SSS_fluid_mesh_pt->node_pt(j)->x(0) << " " 
             << x_outer[1]-SSS_fluid_mesh_pt->node_pt(j)->x(1) << " " 
     // Displacement to new position
             << x_inner[0]+omega*(x_outer[0]-x_inner[0])-SSS_fluid_mesh_pt->node_pt(j)->x(0) << " "
             << x_inner[1]+omega*(x_outer[1]-x_inner[1])-SSS_fluid_mesh_pt->node_pt(j)->x(1) << " "
             << std::endl;
   }
 }
 

 // Loop over all nodes in Syrinx mesh
 //------------------------------------
 {
  unsigned nnod=Syrinx_fluid_mesh_pt->nnode();
  for (unsigned j=0;j<nnod;j++)
   {
    outer_el_pt=Syrinx_fluid_mesh_reference_point[j].first;
    outer_s=Syrinx_fluid_mesh_reference_point[j].second;
    omega=Syrinx_fluid_mesh_omega[j];
    
    // Get deformed position of point on outer wall 
    outer_el_pt->interpolated_x(outer_s,x_outer);
    x_outer[0]+=outer_el_pt->interpolated_u(t_hist,outer_s,0);
    x_outer[1]+=outer_el_pt->interpolated_u(t_hist,outer_s,1);
            

    //Doc pos update
    // Current pos of node
    doc_file << Syrinx_fluid_mesh_pt->node_pt(j)->x(0) << " " 
             << Syrinx_fluid_mesh_pt->node_pt(j)->x(1) << " " 
     // Vector to inner ref point
             << 0.0-Syrinx_fluid_mesh_pt->node_pt(j)->x(0) << " " 
             << x_outer[1]-Syrinx_fluid_mesh_pt->node_pt(j)->x(1) << " " 
     // Vector to outer ref point
             << x_outer[0]-Syrinx_fluid_mesh_pt->node_pt(j)->x(0) << " " 
             << x_outer[1]-Syrinx_fluid_mesh_pt->node_pt(j)->x(1) << " " 
     // Displacement to new position
             << omega*x_outer[0]-Syrinx_fluid_mesh_pt->node_pt(j)->x(0) << " "
             << x_outer[1]-Syrinx_fluid_mesh_pt->node_pt(j)->x(1) << " "
             << std::endl;
   }
 }

}


//==start_of_move_fluid_nodes=============================================
/// Move fluid nodes -- perform node update based on history
/// value t_hist; default 0 means we're using the current value
/// (= fully implicit); otherwise we use history values. Defaults to 0,
/// i.e. fully implicit.
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
move_fluid_nodes(const unsigned& t_hist)
{
 oomph_info << "About to do fluid node update with t_hist=" << t_hist << "\n";

 PORO_ELEMENT* inner_el_pt=0;
 Vector<double> inner_s(2);
 PORO_ELEMENT* outer_el_pt=0;
 Vector<double> outer_s(2);
 double omega=0.0;
 Vector<double> x_inner(2);
 Vector<double> x_outer(2);
 
 // Loop over all nodes in SSS mesh
 //--------------------------------
 {
  unsigned nnod=SSS_fluid_mesh_pt->nnode();
  for (unsigned j=0;j<nnod;j++)
   {
    outer_el_pt=SSS_fluid_mesh_outer_reference_point[j].first;
    outer_s=SSS_fluid_mesh_outer_reference_point[j].second;
    inner_el_pt=SSS_fluid_mesh_inner_reference_point[j].first;
    inner_s=SSS_fluid_mesh_inner_reference_point[j].second;
    omega=SSS_fluid_mesh_omega[j];
    
    // Get deformed position of point on outer wall
    outer_el_pt->interpolated_x(outer_s,x_outer);
    x_outer[0]+=outer_el_pt->interpolated_u(t_hist,outer_s,0);
    x_outer[1]+=outer_el_pt->interpolated_u(t_hist,outer_s,1);
    
    // Get deformed position of point on inner wall 
    inner_el_pt->interpolated_x(inner_s,x_inner);
    x_inner[0]+=inner_el_pt->interpolated_u(t_hist,inner_s,0);
    x_inner[1]+=inner_el_pt->interpolated_u(t_hist,inner_s,1);
        
    // Update position
    SSS_fluid_mesh_pt->node_pt(j)->x(0)=x_inner[0]+
     omega*(x_outer[0]-x_inner[0]);
    SSS_fluid_mesh_pt->node_pt(j)->x(1)=x_inner[1]+
     omega*(x_outer[1]-x_inner[1]);
   }
 }


 // Loop over all nodes in Syrinx mesh
 //------------------------------------
 {
  unsigned nnod=Syrinx_fluid_mesh_pt->nnode();
  for (unsigned j=0;j<nnod;j++)
   {
    outer_el_pt=Syrinx_fluid_mesh_reference_point[j].first;
    outer_s=Syrinx_fluid_mesh_reference_point[j].second;
    omega=Syrinx_fluid_mesh_omega[j];
    
    // Get deformed position of point on outer wall 
    outer_el_pt->interpolated_x(outer_s,x_outer);
    x_outer[0]+=outer_el_pt->interpolated_u(t_hist,outer_s,0);
    x_outer[1]+=outer_el_pt->interpolated_u(t_hist,outer_s,1);
            
    // Update position
    Syrinx_fluid_mesh_pt->node_pt(j)->x(0)=omega*x_outer[0];
    Syrinx_fluid_mesh_pt->node_pt(j)->x(1)=x_outer[1];
   }
 }

}


//==start_of_setup_mesh_motion============================================
/// Setup mesh motion
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void AxisymmetricSpineProblem<PORO_ELEMENT, FLUID_ELEMENT>::
setup_mesh_motion()
{

 oomph_info << "Setting up fluid mesh motion\n";

 // Create mesh as geom object from outer/inner poro/FSI boundary
 MeshAsGeomObject* outer_geom_obj_outer_pt=new MeshAsGeomObject(
  Outer_SSS_FSI_geometry_mesh_pt);
 MeshAsGeomObject* inner_geom_obj_outer_pt=new MeshAsGeomObject(
  Inner_SSS_FSI_geometry_mesh_pt);
 MeshAsGeomObject* syrinx_geom_obj_pt=new MeshAsGeomObject(
  Syrinx_FSI_geometry_mesh_pt);
 
 // Storage for stuff
 Vector<double> zeta(1);
 Vector<double> r(2);
 GeomObject* geom_object_pt=0;
 Vector<double> s(1);
 Vector<double> s_bulk(2);
 Vector<double> r_bulk(2);
 Vector<double> r_bulk_inner(2);
 Vector<double> r_bulk_outer(2);
 Vector<double> r_fluid(2);
 PORO_ELEMENT* bulk_el_pt=0;

 // SSS mesh:
 //----------
 {
  Vector<MeshAsGeomObject*> mesh_geom_obj_pt;
  mesh_geom_obj_pt.push_back(outer_geom_obj_outer_pt);
  mesh_geom_obj_pt.push_back(inner_geom_obj_outer_pt);
  
  // Loop over all nodes in SSS mesh
  unsigned nnod=SSS_fluid_mesh_pt->nnode();
  SSS_fluid_mesh_outer_reference_point.resize(nnod);
  SSS_fluid_mesh_inner_reference_point.resize(nnod);
  SSS_fluid_mesh_omega.resize(nnod);
  for (unsigned j=0;j<nnod;j++)
   {
    // Get fluid node
    Node* fluid_node_pt=SSS_fluid_mesh_pt->node_pt(j);

    // Get coordinates
    r_fluid[0]=fluid_node_pt->x(0);
    r_fluid[1]=fluid_node_pt->x(1);
    
    // Loop over inner/outer FSI boundary of SSS mesh
    unsigned n=mesh_geom_obj_pt.size();
    for (unsigned m=0;m<n;m++)
     {
      MeshAsGeomObject* current_geom_obj_outer_pt=mesh_geom_obj_pt[m];
      
      // Do matching based on z coordinate
      zeta[0]=r_fluid[1];
      current_geom_obj_outer_pt->locate_zeta(zeta,geom_object_pt,s);
      if (geom_object_pt==0)
       {
        throw OomphLibError(
         "Geom object not found",
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
       }
      else
       {
        EulerianBasedFaceElement<PORO_ELEMENT>* geom_face_element_pt=
         dynamic_cast<EulerianBasedFaceElement<PORO_ELEMENT>*>
         (geom_object_pt);
        if (geom_face_element_pt==0)
         {
          throw OomphLibError(
           "Cast failed",
           OOMPH_CURRENT_FUNCTION,
           OOMPH_EXCEPTION_LOCATION);
         }
        else
         {
          bulk_el_pt=dynamic_cast<PORO_ELEMENT*>(
           geom_face_element_pt->bulk_element_pt());
          if (bulk_el_pt==0)
           {
            throw OomphLibError(
             "Cast failed",
             OOMPH_CURRENT_FUNCTION,
             OOMPH_EXCEPTION_LOCATION);
           }
          else
           {
            s_bulk=geom_face_element_pt->local_coordinate_in_bulk(s);
            bulk_el_pt->interpolated_x(s_bulk,r_bulk);
           }
         }
       }
      
      //Check
      geom_object_pt->position(s,r);
      double error=sqrt(pow(r_bulk[0]-r[0],2)+
                        pow(r_bulk[1]-r[1],2));
      double tol=1.0e-13;
      if (error>tol)
       {
        oomph_info << "Bulk and face don't match; error = " 
                   << error << std::endl;
        throw OomphLibError(
         "no match between bulk and face",
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
       }
      
      
      switch(m)
       {
       case 0:
        r_bulk_outer=r_bulk;
        SSS_fluid_mesh_outer_reference_point[j]=
         std::make_pair(bulk_el_pt,s_bulk);
        break;
       
       case 1:
        r_bulk_inner=r_bulk;
        SSS_fluid_mesh_inner_reference_point[j]=
         std::make_pair(bulk_el_pt,s_bulk);
        break;
       
       default:
        throw OomphLibError(
         "Wrong case",
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
       }
     }
   
    double total_length=sqrt(pow(r_bulk_outer[0]-r_bulk_inner[0],2)+
                             pow(r_bulk_outer[1]-r_bulk_inner[1],2));
   
    double distance_from_inner=sqrt(pow(r_fluid[0]-r_bulk_inner[0],2)+
                                    pow(r_fluid[1]-r_bulk_inner[1],2));
    SSS_fluid_mesh_omega[j]=distance_from_inner/total_length;
   }
 }
 




 // Pre-warp SSS mesh:
 //-------------------
 {
  // Loop over all nodes in SSS mesh
  unsigned nnod=SSS_fluid_mesh_pt->nnode();
  SSS_fluid_mesh_inner_reference_point.resize(nnod);
  for (unsigned j=0;j<nnod;j++)
   {
    // Get fluid node
    Node* fluid_node_pt=SSS_fluid_mesh_pt->node_pt(j);

    // Get coordinates
    r_fluid[0]=fluid_node_pt->x(0);
    r_fluid[1]=fluid_node_pt->x(1);
    
    // Do matching based on z coordinate
    zeta[0]=r_fluid[1];
    
    double z_up=  -6.0/Global_Physical_Variables::Z_shrink_factor;
    double z_down=-9.0/Global_Physical_Variables::Z_shrink_factor;
    double ampl=0.04/Global_Physical_Variables::Z_shrink_factor;
    if ((zeta[0]<z_up)&&(zeta[0]>z_down))
     {
      zeta[0]-=ampl*sin(2.0*MathematicalConstants::Pi*(zeta[0]-z_down)/
                        (z_up-z_down));
     }
    inner_geom_obj_outer_pt->locate_zeta(zeta,geom_object_pt,s);
    if (geom_object_pt==0)
     {
      throw OomphLibError(
       "Geom object not found",
       OOMPH_CURRENT_FUNCTION,
       OOMPH_EXCEPTION_LOCATION);
     }
    else
     {
      EulerianBasedFaceElement<PORO_ELEMENT>* geom_face_element_pt=
       dynamic_cast<EulerianBasedFaceElement<PORO_ELEMENT>*>
       (geom_object_pt);
      if (geom_face_element_pt==0)
       {
        throw OomphLibError(
         "Cast failed",
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
       }
      else
       {
        bulk_el_pt=dynamic_cast<PORO_ELEMENT*>(
         geom_face_element_pt->bulk_element_pt());
        if (bulk_el_pt==0)
         {
          throw OomphLibError(
           "Cast failed",
           OOMPH_CURRENT_FUNCTION,
           OOMPH_EXCEPTION_LOCATION);
         }
        else
         {
          s_bulk=geom_face_element_pt->local_coordinate_in_bulk(s);
          bulk_el_pt->interpolated_x(s_bulk,r_bulk);
         }
       }
     }
    
    //Check
    geom_object_pt->position(s,r);
    double error=sqrt(pow(r_bulk[0]-r[0],2)+
                      pow(r_bulk[1]-r[1],2));
    double tol=1.0e-13;
    if (error>tol)
     {
      oomph_info << "Bulk and face don't match; error = " 
                 << error << std::endl;
      throw OomphLibError(
       "no match between bulk and face",
       OOMPH_CURRENT_FUNCTION,
       OOMPH_EXCEPTION_LOCATION);
     }

    SSS_fluid_mesh_inner_reference_point[j]=
     std::make_pair(bulk_el_pt,s_bulk);
   }

  // // Doc shape determining wall nodes 
  //  {

  //   ofstream some_file;
  //   char filename[1000];
  //   sprintf(filename,"sss_shape_determining_solid_nodes.dat");
  //   some_file.open(filename);
    
  //   unsigned nel=SSS_fluid_mesh_pt->nelement();
  //   for (unsigned e=0;e<nel;e++)
  //    {
  //    // Get element
  //     FLUID_ELEMENT* el_pt=dynamic_cast<FLUID_ELEMENT*>(
  //      SSS_fluid_mesh_pt->element_pt(e));
      
  //     Vector<double> s(2);
  //     Vector<double> x_centre(2);
  //     s[0]=1.0/3.0;
  //     s[1]=1.0/3.0;
  //     el_pt->interpolated_x(s,x_centre);
  //     some_file << "ZONE\n";
  //     unsigned next=el_pt->nexternal_data();
  //     for (unsigned j=0;j<next;j++)
  //      {
  //       Data* data_pt=el_pt->external_data_pt(j);
  //       Node* nod_pt=dynamic_cast<Node*>(data_pt);
  //       some_file << x_centre[0] << " " 
  //                 << x_centre[1] << " "
  //                 << nod_pt->x(0)-x_centre[0] << " "
  //                 << nod_pt->x(1)-x_centre[1] << "\n";
  //      }
  //    }
  //   some_file.close();
  //  }

 }
 

 
 // Syrinx mesh:
 //-------------
 {
  // Loop over all nodes in SSS mesh
  unsigned nnod=Syrinx_fluid_mesh_pt->nnode();
  Syrinx_fluid_mesh_reference_point.resize(nnod);
  Syrinx_fluid_mesh_omega.resize(nnod);
  for (unsigned j=0;j<nnod;j++)
   {
    // Get coordinates
    r_fluid[0]=Syrinx_fluid_mesh_pt->node_pt(j)->x(0);
    r_fluid[1]=Syrinx_fluid_mesh_pt->node_pt(j)->x(1);
    
    // FSI boundary of syrinx mesh
    MeshAsGeomObject* current_geom_obj_outer_pt=syrinx_geom_obj_pt;
    
    // Do matching based on z coordinate
    zeta[0]=r_fluid[1];
    current_geom_obj_outer_pt->locate_zeta(zeta,geom_object_pt,s);
    if (geom_object_pt==0)
     {
      throw OomphLibError(
       "Geom object not found",
       OOMPH_CURRENT_FUNCTION,
       OOMPH_EXCEPTION_LOCATION);
     }
    else
     {
      EulerianBasedFaceElement<PORO_ELEMENT>* geom_face_element_pt=
       dynamic_cast<EulerianBasedFaceElement<PORO_ELEMENT>*>
       (geom_object_pt);
      if (geom_face_element_pt==0)
       {
        throw OomphLibError(
         "Cast failed",
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
       }
      else
       {
        bulk_el_pt=dynamic_cast<PORO_ELEMENT*>(
         geom_face_element_pt->bulk_element_pt());
        if (bulk_el_pt==0)
         {
          throw OomphLibError(
           "Cast failed",
           OOMPH_CURRENT_FUNCTION,
           OOMPH_EXCEPTION_LOCATION);
         }
        else
         {
          s_bulk=geom_face_element_pt->local_coordinate_in_bulk(s);
          bulk_el_pt->interpolated_x(s_bulk,r_bulk);
         }
       }
     }
    
    //Check
    geom_object_pt->position(s,r);
    double error=sqrt(pow(r_bulk[0]-r[0],2)+
                      pow(r_bulk[1]-r[1],2));
    double tol=1.0e-13;
    if (error>tol)
     {
      oomph_info << "Bulk and face don't match; error = " 
                 << error << std::endl;
      throw OomphLibError(
       "no match between bulk and face",
       OOMPH_CURRENT_FUNCTION,
       OOMPH_EXCEPTION_LOCATION);
     }
        
    Syrinx_fluid_mesh_reference_point[j]=std::make_pair(bulk_el_pt,s_bulk);
    
    // Fraction based on radius
    double total_length=r_bulk[0];
    double distance_from_inner=r_fluid[0];
    if (total_length!=0.0)
     {
      Syrinx_fluid_mesh_omega[j]=distance_from_inner/total_length;
     }
    else
     {
      Syrinx_fluid_mesh_omega[j]=1.0;
     }
   }
 }


 // ofstream some_file;
 // char filename[1000];
 // unsigned npts=3;
 
 // // Output solution on fluid mesh
 // sprintf(filename,"fluid_before.dat");
 // some_file.open(filename);
 // SSS_fluid_mesh_pt->output(some_file,npts);
 // Syrinx_fluid_mesh_pt->output(some_file,npts);
 // some_file.close();

 // // {
 // //  unsigned nnod=Outer_poro_mesh_pt->nnode();
 // //  for (unsigned j=0;j<nnod;j++)
 // //   {
 // //    Node* nod_pt=Outer_poro_mesh_pt->node_pt(j);
 // //    nod_pt->set_value(0,-0.05*(nod_pt->x(0)*nod_pt->x(1)));
 // //    nod_pt->set_value(1,-0.01*(nod_pt->x(0)*nod_pt->x(1)));
 // //   }
 // // }


 // // {
 // //  unsigned nnod=Inner_poro_mesh_pt->nnode();
 // //  for (unsigned j=0;j<nnod;j++)
 // //   {
 // //    Node* nod_pt=Inner_poro_mesh_pt->node_pt(j);
 // //    nod_pt->set_value(0,0.05*(nod_pt->x(0)*nod_pt->x(1)));
 // //    nod_pt->set_value(1,0.01*(nod_pt->x(0)*nod_pt->x(1)));
 // //   }
 // // }

 // // Update SSS fluid mesh
 // move_fluid_nodes(1);

 // // Output solution on fluid mesh
 // sprintf(filename,"fluid_after.dat");
 // some_file.open(filename);
 // SSS_fluid_mesh_pt->output(some_file,npts);
 // Syrinx_fluid_mesh_pt->output(some_file,npts);
 // some_file.close();

 // // Output solution on outer poro mesh
 // sprintf(filename,"solid_after.dat");
 // some_file.open(filename);
 // Outer_poro_mesh_pt->output(some_file,npts);
 // Inner_poro_mesh_pt->output(some_file,npts);
 // some_file.close();

 // //exit(0);


 delete outer_geom_obj_outer_pt;
 delete inner_geom_obj_outer_pt;
 delete syrinx_geom_obj_pt;
 
}



///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////


namespace oomph
{

//======================================================================
/// Wrapped fluid element -- does derivatives w.r.t. to external data
/// by finite differencing
//======================================================================
class MyFluidElement : public virtual AxisymmetricTTaylorHoodElement
{
 
public:
 

 /// Constructor
 MyFluidElement() : AxisymmetricTTaylorHoodElement()
  {
   Global_node_number_in_mesh.resize(nnode(),UINT_MAX);
  }


 /// \short Add the elemental contribution to the jacobian matrix.
 /// and the residuals vector. Note that
 /// this function will NOT initialise the residuals vector or the jacobian
 /// matrix. It must be called after the residuals vector and 
 /// jacobian matrix have been initialised to zero. The default
 /// is to use finite differences to calculate the jacobian
 void fill_in_contribution_to_jacobian(Vector<double> &residuals,
                                       DenseMatrix<double> &jacobian) 
  {
   // Get Jacobian of underlying element
   AxisymmetricTTaylorHoodElement::fill_in_contribution_to_jacobian(residuals,
                                                                    jacobian);

   //Calculate the contributions from the external dofs
   //(finite-difference the lot by default)
   fill_in_jacobian_from_external_by_fd(residuals,jacobian,true);

   // //Add the contribution to the residuals
   // fill_in_contribution_to_residuals(residuals);


   // //Allocate storage for the full residuals (residuals of entire element)
   // unsigned n_dof = ndof();
   // Vector<double> full_residuals(n_dof);
   // //Get the residuals for the entire element
   // get_residuals(full_residuals);
   // //Calculate the contributions from the internal dofs
   // //(finite-difference the lot by default)
   // fill_in_jacobian_from_internal_by_fd(full_residuals,jacobian,true);
   // //Calculate the contributions from the external dofs
   // //(finite-difference the lot by default)
   // fill_in_jacobian_from_external_by_fd(full_residuals,jacobian,true);
   // //Calculate the contributions from the nodal dofs
   // fill_in_jacobian_from_nodal_by_fd(full_residuals,jacobian);
  }

 /// Read/write global node number of local node j_local
 unsigned& global_node_number_in_mesh(const unsigned& j_local)
  {
   return Global_node_number_in_mesh[j_local];
  }

  /// Read/write  access to problem pointer
 AxisymmetricSpineProblem<TAxisymmetricPoroelasticityElement<1>,
                          MyFluidElement>*& problem_pt()
  {
   return Problem_pt;
  }

private:

 /// Global node number of local node j_local
 Vector<unsigned> Global_node_number_in_mesh;

 /// Pointer to problem
 AxisymmetricSpineProblem<TAxisymmetricPoroelasticityElement<1>,
                          MyFluidElement>* Problem_pt;

};


//===========start_face_geometry==============================================
/// FaceGeometry of wrapped element is the same as the underlying element
//============================================================================
 template<>
class FaceGeometry<MyFluidElement> :
 public virtual FaceGeometry<AxisymmetricTTaylorHoodElement>
{
public:

  FaceGeometry() : TElement<1,3>() {}

};

}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


//===start_of_main======================================================
/// Driver code
//======================================================================
int main(int argc, char* argv[])
{
 //feenableexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW);

#ifdef OOMPH_HAS_MPI

 // Initialise MPI
 MPI_Helpers::init(argc,argv);
 
  // Swtich off output modifier
 oomph_info.output_modifier_pt() = &default_output_modifier;
 
 // switch off oomph_info output for all processors but rank 0
 if (MPI_Helpers::communicator_pt()->my_rank()!=0)
  {
   oomph_info.stream_pt() = &oomph_nullstream;
   OomphLibWarning::set_stream_pt(&oomph_nullstream);
   OomphLibError::set_stream_pt(&oomph_nullstream);
  }
 else
  {
   oomph_info << "\n\n=====================================================\n";
   oomph_info << "Number of processors: " 
              << MPI_Helpers::communicator_pt()->nproc() << "\n";
  }
#endif

  // Store command line arguments
 CommandLineArgs::setup(argc,argv);

 // Define possible command line arguments and parse the ones that
 // were actually specified


 /// Use ellipsified boundaries
 CommandLineArgs::specify_command_line_flag(
  "--use_ellipsified_syrinx_boundaries");

 // History value for fluid node update (defaults to -1, meaning it's
 // not done. 0 means fully implicit;  1 means lagged implementation.
 CommandLineArgs::specify_command_line_flag(
  "--history_level_for_fluid_mesh_node_update",
  &Global_Physical_Variables::History_level_for_fluid_mesh_node_update);


 // Inverse slip rate coefficient for BJS
 CommandLineArgs::specify_command_line_flag(
  "--inverse_slip_rate_coefficient",
  &Global_Physical_Variables::Inverse_slip_rate_coefficient);


 // Biot alpha parameter
 CommandLineArgs::specify_command_line_flag(
  "--alpha",
  &Global_Physical_Variables::Alpha);

 // Drained Poisson's ratio of porous media
 CommandLineArgs::specify_command_line_flag(
  "--drained_nu",
  &Global_Physical_Variables::Nu_drained);

 // Permeability of all materials (currently) to reference value
 CommandLineArgs::specify_command_line_flag(
  "--permeability_ratio",&Global_Physical_Variables::Permeability_ratio);
   
 // Name of restart file
 CommandLineArgs::specify_command_line_flag
  ("--restart_file",
   &Global_Physical_Variables::Restart_file);
  
 // Reynolds number
 CommandLineArgs::specify_command_line_flag(
   "--re",
   &Global_Physical_Variables::Re);

 // Non-default value of porosity (same for all!)
 double porosity=0.3;
 CommandLineArgs::specify_command_line_flag(
  "--porosity",&porosity);

 // Pin darcy?
 CommandLineArgs::specify_command_line_flag(
   "--pin_darcy");

 // Pin cord
 CommandLineArgs::specify_command_line_flag(
   "--pin_cord");

 // Leave everything porous (rather than the default which
 // is to only make the region above the syrinx poro-elastic
  CommandLineArgs::specify_command_line_flag
   ("--everything_is_porous");

 // Make only cord, pia and filum porous (rather than the default which
 // is to only make the region above the syrinx poro-elastic
  CommandLineArgs::specify_command_line_flag
   ("--cord_pia_and_filum_are_porous");

 // No wall inertia
 CommandLineArgs::specify_command_line_flag(
   "--suppress_wall_inertia");

 // Start from rest?
 CommandLineArgs::specify_command_line_flag(
   "--steady_solve");

 // Target element area for fluid meshes
 CommandLineArgs::specify_command_line_flag(
   "--el_area_fluid",
   &Global_Physical_Variables::Element_area_fluid);

 // Target element area for poro meshes
 CommandLineArgs::specify_command_line_flag(
   "--el_area_poro",
   &Global_Physical_Variables::Element_area_poro);

 // Target element area for poro meshes
 CommandLineArgs::specify_command_line_flag(
   "--el_area_syrinx_cover",
   &Global_Physical_Variables::Element_area_syrinx_cover);


 // z coordinate of upstream end of fine mesh in sss
 CommandLineArgs::specify_command_line_flag(
   "--z_upstream_fine_sss_region",
   &Global_Physical_Variables::Z_upstream_fine_sss_region);

 // z coordinate of downstream end of fine mesh in sss
 CommandLineArgs::specify_command_line_flag(
   "--z_downstream_fine_sss_region",
   &Global_Physical_Variables::Z_downstream_fine_sss_region);


 // Target element area for sss mesh in upstream/downstream vicinity of block 
 CommandLineArgs::specify_command_line_flag(
   "--el_area_sss_near_block",
   &Global_Physical_Variables::Element_area_sss_near_block);

 // Target element area for sss mesh under block and in adjacent bl  
 CommandLineArgs::specify_command_line_flag(
   "--el_area_sss_under_block",
   &Global_Physical_Variables::Element_area_sss_under_block_and_bl);

 // Target element area for fluid elements in syrinx
 CommandLineArgs::specify_command_line_flag(
   "--el_area_syrinx",
   &Global_Physical_Variables::Element_area_syrinx);

 // Target edge length for elements on syrinx poro fsi boundary
 CommandLineArgs::specify_command_line_flag(
   "--ds_syrinx_poro_fsi_boundary_raw",
   &Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_raw);

 // Extent of region near sharp corners on syrinx poro fsi boundary
 // where we impose smaller edge lengths
 CommandLineArgs::specify_command_line_flag(
   "--delta_syrinx_poro_fsi_boundary_near_sing_raw",
   &Global_Physical_Variables::Delta_syrinx_poro_fsi_boundary_near_sing_raw);

 // Edge length of elements in region near sharp corners on syrinx 
 // poro fsi boundary where we impose smaller edge lengths
 CommandLineArgs::specify_command_line_flag(
   "--ds_syrinx_poro_fsi_boundary_near_sing_raw",
   &Global_Physical_Variables::Ds_syrinx_poro_fsi_boundary_near_sing_raw);


 /// \short Scaling factor for poro-elastic bl (acts on thickness and
 /// target element area
 CommandLineArgs::specify_command_line_flag(
  "--bl_poro_scaling_factor",
  &Global_Physical_Variables::BL_poro_scaling_factor);
 
 // Axial scaling parameter -- shrinks aspect ratio of
 // domain by this factor
 CommandLineArgs::specify_command_line_flag(
   "--z_shrink_factor",
   &Global_Physical_Variables::Z_shrink_factor);

 // Axial scaling parameter -- shrinks aspect ratio of
 // domain by this factor
 CommandLineArgs::specify_command_line_flag(
  "--undo_z_scaling_by_element_stretching");

 // Output directory
 CommandLineArgs::specify_command_line_flag(
   "--dir",
   &Global_Physical_Variables::Directory);

 // Timestep
 double dt=0.00001;
 CommandLineArgs::specify_command_line_flag(
   "--dt",&dt);

 // Timestep
 double dt_in_seconds=0.0;
 CommandLineArgs::specify_command_line_flag(
   "--dt_in_seconds",&dt_in_seconds);

 // Set timestep from number of steps per unit distance of wavetravel
 unsigned nstep_per_unit_wave_travel=0;
 CommandLineArgs::specify_command_line_flag("--nsteps_per_unit_wave_travel",
                                            &nstep_per_unit_wave_travel);

 // Number of timesteps to perform
 unsigned nstep=1000;
 CommandLineArgs::specify_command_line_flag(
   "--nstep",
   &nstep);

 // Validation?
 CommandLineArgs::specify_command_line_flag(
   "--validation");

 /// Use non-stress-divergence form of viscous terms in Navier-Stokes?
 CommandLineArgs::specify_command_line_flag
  ("--use_non_stress_divergence_form_for_navier_stokes");

 // Pin radial displacement at "inlet/outlet"
 CommandLineArgs::specify_command_line_flag("--radially_fixed_poro");

 // Pin radial velocity at inlet
 CommandLineArgs::specify_command_line_flag
  ("--pin_radial_veloc_at_in_and_outlet");

 // Suppress output of tecplot files for bulk (already written in
 // paraview format)
 CommandLineArgs::specify_command_line_flag("--suppress_tecplot_bulk_output");

 // Suppress writing of restart files (they're big!)
 CommandLineArgs::specify_command_line_flag("--suppress_restart_files");

 // Suppress all output apart from trace and restart
 CommandLineArgs::specify_command_line_flag
  ("--suppress_all_output_apart_from_trace_and_restart");

 // Suppress output of data on regularly spaced points
 CommandLineArgs::specify_command_line_flag
  ("--suppress_regularly_spaced_output");

 // Raw shift of syrinx in z direction
 CommandLineArgs::specify_command_line_flag(
   "--raw_syrinx_z_shift",
   &Global_Physical_Variables::Raw_syrinx_z_shift);


 // Interpret (positive) shift of syrinx as shortening
 // (upper end moves downwards by shift; lower end moves
 // upwards
 CommandLineArgs::specify_command_line_flag(
  "--shorten_syrinx_by_z_shift");
 

 // Do heart-beat-like excitation with specified period
 CommandLineArgs::specify_command_line_flag(
  "--heart_beat_like_forcing",
  &Global_Physical_Variables::Excitation_period_in_seconds);
 

 // Filename of file containing cycle average syrinx pressure
 // (if specified we only compute one steady solve without any
 // adjustments -- this is a sanity check for cycle averages
 // computed from unsteady runs)
 CommandLineArgs::specify_command_line_flag(
   "--syrinx_cycle_average_pressure_filename",
   &Global_Physical_Variables::Syrinx_cycle_average_pressure_filename);

 // Filename of file containing cycle average sss pressure
 // whose "lower" end is being adjusted to get zero net flux into
 // syrinx [adjustments only if syrinx pressure isn't specified too]
 CommandLineArgs::specify_command_line_flag(
   "--sss_cycle_average_pressure_filename",
   &Global_Physical_Variables::SSS_cycle_average_pressure_filename);

//--

 // Filename of file containing cycle average syrinx radial traction
 CommandLineArgs::specify_command_line_flag(
   "--syrinx_cycle_average_radial_traction_filename",
   &Global_Physical_Variables::Syrinx_cycle_average_radial_traction_filename);


 // Filename of file containing cycle average syrinx axial traction
 CommandLineArgs::specify_command_line_flag(
   "--syrinx_cycle_average_axial_traction_filename",
   &Global_Physical_Variables::Syrinx_cycle_average_axial_traction_filename);

 // Filename of file containing cycle average SSS radial traction
 CommandLineArgs::specify_command_line_flag(
   "--sss_cycle_average_radial_traction_filename",
   &Global_Physical_Variables::SSS_cycle_average_radial_traction_filename);


 // Filename of file containing cycle average SSS axial traction
 CommandLineArgs::specify_command_line_flag(
   "--sss_cycle_average_axial_traction_filename",
   &Global_Physical_Variables::SSS_cycle_average_axial_traction_filename);

//------

 /// Raw shift for steady sss profile
 CommandLineArgs::specify_command_line_flag(
  "--z_sss_p_shift_raw",
  &Global_Physical_Variables::Z_sss_p_shift_raw);

 // Suppress matching of Nst and poro discretisation along syrinx fsi 
 // interface?
 CommandLineArgs::specify_command_line_flag(
  "--suppress_match_syrinx_nst_and_poro_discretisation");

 // Suppress volume conserving mass transfer in bjs boundary condition
 CommandLineArgs::specify_command_line_flag(
  "--suppress_volume_conserving_flux_transfer_in_bjs");

 // Suppress line visualisers (for shortened syrinxes)
 CommandLineArgs::specify_command_line_flag(
  ("--suppress_line_visualisers"));

 // Parse command line
 CommandLineArgs::parse_and_assign();

 // Doc what has actually been specified on the command line
 CommandLineArgs::doc_specified_flags();


 // Interpret (positive) shift of syrinx as shortening
 // (upper end moves downwards by shift; lower end moves
 // upwards
 Global_Physical_Variables::Shorten_syrinx_by_shift_factor=1.0;
 if (CommandLineArgs::command_line_flag_has_been_set(
      "--shorten_syrinx_by_z_shift"))
  {
   oomph_info << "Shortening syrinx by positive z shift" << std::endl;
   Global_Physical_Variables::Shorten_syrinx_by_shift_factor=-1.0;
  }

 // Ellipsify syrinx boundaries?
 if (CommandLineArgs::command_line_flag_has_been_set(
      "--use_ellipsified_syrinx_boundaries"))
  {
   Global_Physical_Variables::Use_straight_syrinx_boundaries=false;
  }


 // Suppress matching of Nst and poro discretisation along syrinx fsi 
 // interface?
 if (CommandLineArgs::command_line_flag_has_been_set
     ("--suppress_match_syrinx_nst_and_poro_discretisation"))
  {
   Global_Physical_Variables::Match_syrinx_nst_and_poro_discretisation=false;
  }

 // Kill regularly spaced output?
 if (CommandLineArgs::command_line_flag_has_been_set
     ("--suppress_regularly_spaced_output"))
  {
   Global_Physical_Variables::Suppress_regularly_spaced_output=true;
  }

 
 /// Flags have to be specied together
 if ( (
       (CommandLineArgs::command_line_flag_has_been_set(
        "--delta_syrinx_poro_fsi_boundary_near_sing_raw")&&
        (!CommandLineArgs::command_line_flag_has_been_set(
         "--ds_syrinx_poro_fsi_boundary_near_sing_raw"))
        ) ||
       (!CommandLineArgs::command_line_flag_has_been_set(
        "--delta_syrinx_poro_fsi_boundary_near_sing_raw")&&
        (CommandLineArgs::command_line_flag_has_been_set(
         "--ds_syrinx_poro_fsi_boundary_near_sing_raw"))
        )
       ) )
  {
   std::ostringstream error_stream;
   error_stream
    << " --delta_syrinx_poro_fsi_boundary_near_sing_raw and \n "
    << " --ds_syrinx_poro_fsi_boundary_near_sing_raw have to be \n"
    << "specified together\n";
   throw OomphLibError(
    error_stream.str(),
    OOMPH_CURRENT_FUNCTION,
    OOMPH_EXCEPTION_LOCATION);
  }
 


 // Overwrite default porosity
 if (CommandLineArgs::command_line_flag_has_been_set("--porosity"))
  {
   oomph_info << "Overwriting default porosity with: " << porosity 
              << " for all tissues\n";
   Global_Physical_Variables::Porosity_cord=porosity;
   Global_Physical_Variables::Porosity_pia=porosity;
   Global_Physical_Variables::Porosity_filum=porosity;
   Global_Physical_Variables::Porosity_block=porosity;
   Global_Physical_Variables::Porosity_dura=porosity;
  }


 // Set up doc info
 DocInfo doc_info;

 // Set output directory
 doc_info.set_directory(Global_Physical_Variables::Directory);

 // Create problem
 AxisymmetricSpineProblem<TAxisymmetricPoroelasticityElement<1>,
                          AxisymmetricTTaylorHoodElement> problem;
 //                        MyFluidElement> problem;

 // Update dependent problem parameters
 Global_Physical_Variables::update_dependent_parameters();

 // Doc dependent parameters
 Global_Physical_Variables::doc_dependent_parameters();


 if (CommandLineArgs::command_line_flag_has_been_set
     ("--pin_radial_veloc_at_in_and_outlet"))
  {
   oomph_info << "Pinning radially velocity at in/outlet.\n";
  }
 else
  {
   oomph_info << "NOT pinning radially velocity at in/outlet.\n";
  }

 // Check that setting for history is legal
 if (CommandLineArgs::command_line_flag_has_been_set
     ("--history_level_for_fluid_mesh_node_update"))
  {
   if (Global_Physical_Variables::History_level_for_fluid_mesh_node_update==-1)
    {
     oomph_info << "Skipping fluid node update\n";
    }
   else if 
    (Global_Physical_Variables::History_level_for_fluid_mesh_node_update==0)
    {
     oomph_info << "Fully implicit fluid node update\n";
    }
   else if 
    (Global_Physical_Variables::History_level_for_fluid_mesh_node_update==1)
    {
     oomph_info << "Lagged fluid node update\n";
    }
   else
    {
     oomph_info 
      << "Wrong value for fluid node update: "
      << Global_Physical_Variables::History_level_for_fluid_mesh_node_update
      << std::endl
      << "Should be -1 (none); 0 (fully implicit) 1 (lagged)\n";
     abort();
    }
  }
 
 
 // Check setup of timesteps
 if (CommandLineArgs::command_line_flag_has_been_set
     ("--nsteps_per_unit_wave_travel"))
  {
   if (CommandLineArgs::command_line_flag_has_been_set("--dt")||
       CommandLineArgs::command_line_flag_has_been_set("--dt_in_seconds"))
    {
     throw OomphLibError(
      "Cannot set both --dt or --dt_in_seconds and --nsteps_per_unit_wave_travel",
      OOMPH_CURRENT_FUNCTION,
      OOMPH_EXCEPTION_LOCATION);
    }
   else
    {
     dt=1.0/Global_Physical_Variables::Dura_wavespeed/
      double(nstep_per_unit_wave_travel);
     oomph_info << "Setting dt so that we perform "
                << nstep_per_unit_wave_travel 
                << " timesteps in the in the time it takes \n"
                << "a pulswave travelling with the dura-based wavespeed\n"
                << "one diameter: dt="
                << dt << std::endl;
    }
  }
 else
  {
   if (CommandLineArgs::command_line_flag_has_been_set("--dt")&&
       CommandLineArgs::command_line_flag_has_been_set("--dt_in_seconds"))
    {
     throw OomphLibError(
      "Cannot set both --dt and --dt_in_seconds",
      OOMPH_CURRENT_FUNCTION,
      OOMPH_EXCEPTION_LOCATION);
    }
   dt=dt_in_seconds*Global_Physical_Variables::Re* 
    Global_Physical_Variables::T_factor_in_inverse_seconds;
   oomph_info << "Setting non-dim dt = " << dt 
              << " from Re and T factor. " << std::endl;
  }


 // Sanity check
 if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy")&&
     CommandLineArgs::command_line_flag_has_been_set("--everything_is_porous"))
  {
   throw OomphLibError(
    "Cannot set both --pin_darcy and --everything_is_porous",
    OOMPH_CURRENT_FUNCTION,
    OOMPH_EXCEPTION_LOCATION);
  }


 // Sanity check
 if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy")&&
     CommandLineArgs::command_line_flag_has_been_set("--cord_pia_and_filum_are_porous"))
  {
   throw OomphLibError(
    "Cannot set both --pin_darcy and --cord_pia_and_filum_are_porous",
    OOMPH_CURRENT_FUNCTION,
    OOMPH_EXCEPTION_LOCATION);
  }


 // Sanity check
 if (CommandLineArgs::command_line_flag_has_been_set("--cord_pia_and_filum_are_porous")&&
     CommandLineArgs::command_line_flag_has_been_set("--everything_is_porous"))
  {
   throw OomphLibError(
    "Cannot set both --cord_pia_and_filum_are_porous and --everything_is_porous",
    OOMPH_CURRENT_FUNCTION,
    OOMPH_EXCEPTION_LOCATION);
  }

 // Do heart-beat-like excitation with specified period?
 if (CommandLineArgs::command_line_flag_has_been_set(
      "--heart_beat_like_forcing"))
  {
   oomph_info 
    << "Doing heart-beat-like forcing with Excitation_period_in_seconds = "
    << Global_Physical_Variables::Excitation_period_in_seconds << std::endl
    << "and fraction of pressure peak = " 
    << Global_Physical_Variables::Fraction_of_pressure_peak
    << std::endl;
  }




 // Check computation of moving mesh fluid volumes, then stop
 // problem.check_computation_of_fluid_volumes();

 // Restart?
 if (CommandLineArgs::command_line_flag_has_been_set("--restart_file"))
  {
   problem.restart(doc_info);
   oomph_info << "Done restart\n";

   // Doc the read-in initial conditions 
   problem.doc_solution(doc_info);
  }
 else
  {
   oomph_info << "Not doing restart\n";

   // Set the initial time to t=0
   problem.time_pt()->time()=0.0;
   
   // Set up impulsive start from rest (which also initialises the timestep)
   problem.assign_initial_values_impulsive(dt);

   // Doc the initial conditions
   problem.doc_solution(doc_info);
  }


 // // Check mesh motion fix (pre-warping)
 // //------------------------------------
 // {
 //  unsigned n=20;
 //  for (unsigned j=0;j<n;j++)
 //   {
 //    problem.impose_fake_cord_motion(j,n);
 //    problem.move_fluid_nodes
 //     (Global_Physical_Variables::History_level_for_fluid_mesh_node_update);
 //    problem.doc_solution(doc_info);
 //   }
 // }
 // exit(0);

 // Do only 3 steps if validating
 if(CommandLineArgs::command_line_flag_has_been_set("--validation"))
  {
   nstep=3;
  }

 // Check if we are doing a steady solve only
 if(CommandLineArgs::command_line_flag_has_been_set(
     "--steady_solve"))
  {
   if (CommandLineArgs::command_line_flag_has_been_set(
        "--syrinx_cycle_average_pressure_filename"))
    {
     oomph_info << "Doing single steady solve with read-in pressures only\n";
     problem.steady_inner_poro_only_run(doc_info);
    }
   else
    {
     // do poro-only solve
     oomph_info << "Doing steady solve for zero flux \n";
     problem.steady_inner_poro_only_for_zero_flux_run(doc_info);
    }
   exit(0);
  }
 
 // Timestep
 oomph_info << "About to do " << nstep << " timesteps with dt = "
            << dt << std::endl;
 
 double desired_fixed_timestep=dt;
 double next_full_time=problem.time_pt()->time()+desired_fixed_timestep;
 
 
 // Doc applied pressure
 {
  ofstream some_file;
  char filename[1000];
  sprintf(filename,"cranial_pressure_forcing.dat");
  some_file.open(filename);
  
  unsigned nperiod=3;
  unsigned nstep_per_period=80;
  double dt=problem.time_pt()->dt();
  double time=0.0;
  for (unsigned i=0;i<nperiod;i++)
   {
    for (unsigned j=0;j<nstep_per_period;j++)
     {
      some_file 
       << time << " " 
       << Global_Physical_Variables::time_dependent_magnitude_of_driving_pressure(time)
       << std::endl;
      
      // Bump
      time+=dt;
     }
   }
  
  some_file.close();
 }


 // Do the timestepping
 unsigned istep=0;
 while (istep<nstep)
  {
   oomph_info << "Solving at time " << problem.time_pt()->time()+dt
              << std::endl;
   
   // Fake tolerance -- make sure next timestep gets accepted no
   // matter what
   double epsilon=DBL_MAX;
   problem.adaptive_unsteady_newton_solve(dt,epsilon);

   double distance_to_target_time=
    std::fabs(problem.time_pt()->time()-next_full_time);
   oomph_info << "Distance to target time: " << distance_to_target_time
              << std::endl;

   // Are we there yet?
   // Made it to the next (constant) timestep increment
   if (distance_to_target_time<1.0e-10)
    {
     // Do next step with fixed increment again
     dt=desired_fixed_timestep;
     
     oomph_info << "We're there... Taking next step with dt="
                << dt << std::endl;
     
     // Update next target time
     next_full_time=problem.time_pt()->time()+desired_fixed_timestep;
     oomph_info << "Next target time = " << next_full_time << std::endl;


     // We're about to write the next full step
     ofstream some_file;
     char filename[1000];
     sprintf(filename,"%s/full_step%i.dat",
             doc_info.directory().c_str(),
             istep);
     some_file.open(filename);
     some_file << doc_info.number() << std::endl;
     some_file.close();

     // Bump counter
     istep++;
    }
   else
    {
     // No -- don't doc but use the current (reduced) timestep
     // until we do
     dt=problem.time_pt()->dt();
     oomph_info << "Not there yet... Taking another step with dt="
                << dt << std::endl;
    }

   // Doc the solution
   problem.doc_solution(doc_info);

  }

#ifdef OOMPH_HAS_MPI 

 // Cleanup
 MPI_Helpers::finalize();

#endif

} // end_of_main



//  // Timestep
//  oomph_info << "About to do " << nstep << " timesteps with dt = "
//             << dt << std::endl;
//  // Do the timestepping
//  for(unsigned istep=0;istep<nstep;istep++)
//   {
//    oomph_info << "Solving at time " << problem.time_pt()->time()+dt
//               << std::endl;

//    // Fake tolerance -- make sure next timestep gets accepted no
//    // matter what
//    double epsilon=DBL_MAX;
//    problem.adaptive_unsteady_newton_solve(dt,epsilon);

//    // Solve for this timestep
//    //problem.unsteady_newton_solve(dt);

//    // Doc the solution
//    problem.doc_solution(doc_info);
//   }

// #ifdef OOMPH_HAS_MPI 

//  // Cleanup
//  MPI_Helpers::finalize();

// #endif

// } // end_of_main

