#------------------------------------
# Extract full steps over one period
#------------------------------------


#============================================
# Stem for files  to be linked
#============================================
link_vtu_file_stem_list="soln-syrinx-fluid soln-sss-fluid soln-inner-poro soln-outer-poro "
link_dat_file_stem_list="soln-inner-poro soln-syrinx-fluid sss_inner_fsi_boundary_veloc_line syrinx_centreline_veloc_line poro_output_on_fsi_inner_sss_boundary  poro_output_on_fsi_syrinx_boundary upper_radial_line_through_syrinx_cover lower_radial_line_through_syrinx_cover poro_fsi_syrinx poro_fsi_inner_sss "



#============================================
# Range
#============================================
first_full_step=1600 # 880 # 720 # 9920 # 720 # 820 # 240 # 750 # 660 # 550 # 600 #650 # 800 # 400
nsteps_per_period=200 # 80
let last_full_step=$first_full_step+$nsteps_per_period



#====================
# Full step directory
#====================
chris_dir="FULL_STEPS_STARTING_FROM_STEP$first_full_step"
#chris_dir="FOR_CHRIS_FULL_STEPS_STARTING_FROM_STEP$first_full_step"
if [ -e "$chris_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $chris_dir and try again"
    echo " "
    exit
fi
mkdir $chris_dir


#============================================
# Only deal with full steps
#============================================
full_step_gz_file_list=`ls full_step*.dat.gz`
for full_step_gz_file in `echo $full_step_gz_file_list`; do
    gunzip $full_step_gz_file
done

full_step_file_list=`ls full_step*.dat`
count=0
full_step_count=0
for full_step_file in `echo $full_step_file_list`; do
    full_step=`cat full_step$count.dat`
    if [ $full_step -ge $first_full_step ]; then
        if [ $full_step -lt $last_full_step  ]; then
            for link_vtu_file_stem in `echo $link_vtu_file_stem_list`; do
                old_vtu_file=`echo $link_vtu_file_stem``printf "%05d" $full_step`".vtu"
                if [ -e $old_vtu_file.gz ]; then
                    gunzip $old_vtu_file.gz
                fi
                new_vtu_file=`echo $link_vtu_file_stem``printf "%05d" $full_step_count`".vtu"
                ln -s `pwd`/$old_vtu_file $chris_dir/$new_vtu_file
            done
            for link_dat_file_stem in `echo $link_dat_file_stem_list`; do
                old_dat_file=`echo $link_dat_file_stem`$full_step".dat"
                if [ -e $old_dat_file.gz ]; then
                    gunzip $old_dat_file.gz
                fi
                new_dat_file=`echo $link_dat_file_stem`$full_step_count".dat"
                ln -s `pwd`/$old_dat_file $chris_dir/$new_dat_file
            done


            let full_step_count=$full_step_count+1
        fi
    fi

    let count=$count+1
done

#=========================================
# Create pvd files
#=========================================
cd $chris_dir
makePvd soln-syrinx-fluid soln-syrinx-fluid.pvd
makePvd soln-sss-fluid soln-sss-fluid.pvd
makePvd soln-inner-poro soln-inner-poro.pvd
makePvd soln-outer-poro soln-outer-poro.pvd


echo " "
echo "==============================================================="
echo " "
echo "Re-numbered/linked files are in: "
echo " " 
echo "    "`pwd`
echo " " 
echo "See the final_scripts/README file for what to do next..." 
echo "==============================================================="
echo " "


exit




