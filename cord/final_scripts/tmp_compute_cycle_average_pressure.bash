#! /bin/bash


first_step=0
nsteps_per_period=80


data_dir=`pwd`
echo "Data directory: "$data_dir

# Master run directory; make and move into it
#--------------------------------------------
run_dir=AGAIN_CYCLE_AVERAGE_PORE_PRESSURE_FIRST_`echo $first_step`_N_`echo $nsteps_per_period`
if [ -e "$run_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $run_dir and try again"
    echo " "
    exit
fi
mkdir $run_dir
cd $run_dir


min_p=1000000
max_p=-1000000
min_z=1000000
max_z=-1000000

stem_list="sss_inner_fsi_boundary_veloc_line syrinx_centreline_veloc_line"
for stem in `echo $stem_list`; do

    step=$first_step
    count=0
    while [ $count -lt $nsteps_per_period ]; do
        file=$stem`echo $step`".dat"
        echo $file
        awk '{if ($1!="ZONE"){print $2 " " $5}}' $data_dir/$file > extracted_z_p_`echo $file`  
        if [ $count -eq 0 ]; then
            cp extracted_z_p_`echo $file` .tmp_sum
        else
            paste extracted_z_p_`echo $file` .tmp_sum > .both.dat
            awk '{{print $1 " " $2+$4}}' .both.dat > .tmp_sum
            awk 'BEGIN{max_z=-100000; min_z=100000;max_p=-100000; min_p=100000} {if ($1>max_z){max_z=$1}if ($2>max_p){max_p=$2}if ($1<min_z){min_z=$1}if ($2<min_p){min_p=$2}}END{print min_z " " max_z " " min_p " " max_p}' .both.dat > .min_max
            min_z=`awk -v current_min=$min_z '{if ($1<current_min){print$1}else{print current_min}}' .min_max` 
            max_z=`awk -v current_max=$max_z '{if ($2>current_max){print$2}else{print current_max}}' .min_max` 
            min_p=`awk -v current_min=$min_p '{if ($3<current_min){print$3}else{print current_min}}' .min_max` 
            max_p=`awk -v current_max=$max_p '{if ($4>current_max){print$4}else{print current_max}}' .min_max` 
        fi
        awk '{print "0.0 "$1" "$2}' extracted_z_p_`echo $file` | sort -g -k 2 > .junk
        mv .junk extracted_z_p_`echo $file`
        oomph-convert -o -p2 -z extracted_z_p_`echo $file`
        let step=$step+1
        let count=$count+1
    done
    
    awk -v n=$nsteps_per_period '{print "0.0 " $1 " " $2/n}' .tmp_sum | sort -g -k 2 > cycle_average_`echo $stem`.dat

    makePvd extracted_z_p_$stem extracted_z_p_`echo $stem`.pvd
    oomph-convert -o -p2 cycle_average_`echo $stem`.dat
done


echo "0.0" $min_z $min_p > cycle_average_bounding_box.dat
echo "0.0" $max_z $min_p >> cycle_average_bounding_box.dat
echo "0.0" $max_z $max_p >> cycle_average_bounding_box.dat
echo "0.0" $min_z $max_p >> cycle_average_bounding_box.dat
echo "0.0" $min_z $min_p >> cycle_average_bounding_box.dat
oomph-convert -p2 -o cycle_average_bounding_box.dat


x_min_syrinx=`awk 'BEGIN{x_left = 100000000}{if ($2<x_left){x_left=$2}}END{print x_left}' cycle_average_syrinx_centreline_veloc_line.dat `
x_max_syrinx=`awk 'BEGIN{x_right=-100000000}{if ($2>x_right){x_right=$2}}END{print x_right}' cycle_average_syrinx_centreline_veloc_line.dat `

echo "xmin/max syrinx: "$x_min_syrinx " " $x_max_syrinx

av_pressure_sss=`awk -v x_left=$x_min_syrinx -v x_right=$x_max_syrinx 'BEGIN{integral=0; count=0}{if (($2>x_left)&&($2<x_right)){if (count==0){prev_x=$2;prev_p=$3}else{integral+=0.5*($3+prev_p)*($2-prev_x); prev_p=$3;prev_x=$2};count++}}END{print integral/(x_right-x_left)}' cycle_average_sss_inner_fsi_boundary_veloc_line.dat `

av_pressure_syrinx=`awk -v x_left=$x_min_syrinx -v x_right=$x_max_syrinx 'BEGIN{integral=0; count=0}{if (($2>x_left)&&($2<x_right)){if (count==0){prev_x=$2;prev_p=$3}else{integral+=0.5*($3+prev_p)*($2-prev_x); prev_p=$3;prev_x=$2};count++}}END{print integral/(x_right-x_left)}' cycle_average_syrinx_centreline_veloc_line.dat `


echo "Average pressure in SSS over length of syrinx   : "$av_pressure_sss
echo "Average pressure in syrinx over length of syrinx: "$av_pressure_syrinx
ptm=`awk -v syr=$av_pressure_syrinx -v sss=$av_pressure_sss 'BEGIN{print sss-syr}' `
echo "Net pressure difference                         : " $ptm



