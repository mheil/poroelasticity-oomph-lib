#! /bin/bash


first_step=0
nsteps_per_period=80


data_dir=`pwd`
echo "Data directory: "$data_dir

# Master run directory; make and move into it
#--------------------------------------------

run_dir=CYCLE_AVERAGE_QUANTITIES_THROUGH_RADIAL_SLICE_FIRST_`echo $first_step`_N_`echo $nsteps_per_period`
if [ -e "$run_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $run_dir and try again"
    echo " "
    exit
fi
mkdir $run_dir
cd $run_dir


stem_list="upper_radial_line_through_syrinx_cover"
for stem in `echo $stem_list`; do

    step=$first_step
    count=0
    while [ $count -lt $nsteps_per_period ]; do
        file=$data_dir/$stem`echo $step`".dat"
        echo $file
        if [ $count -eq 0 ]; then
            cp `echo $file` .tmp_sum
        else
            paste `echo $file` .tmp_sum > .both.dat
            awk '{for (i=1;i<=(NF/2);i++){printf("%s ",$i+$(NF/2+i))}; print " "}' .both.dat > .tmp_sum
        fi
        let step=$step+1
        let count=$count+1
    done
    
    awk -v n=$nsteps_per_period '{for (i=1;i<=NF;i++){printf("%s ",$i/n)}; print " "}' .tmp_sum > cycle_average_`echo $stem`.dat
#
#    makePvd extracted_z_p_$stem extracted_z_pore_pressure_`echo $stem`.pvd
#    oomph-convert -o -p2 cycle_average_`echo $stem`.dat
#    awk '{print $1 " " $2}' cycle_average_`echo $stem`.dat > cycle_average_radial_traction_`echo $stem`.dat
#    awk '{print $1 " " $3}' cycle_average_`echo $stem`.dat > cycle_average_axial_traction_`echo $stem`.dat
#    awk '{print $1 " " $4}' cycle_average_`echo $stem`.dat > cycle_average_pressure_`echo $stem`.dat

done

