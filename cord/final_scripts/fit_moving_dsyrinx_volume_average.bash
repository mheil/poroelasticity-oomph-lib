#! /bin/bash

# Make fitting code
#------------------
my_dir=`pwd`
levenberg_marquart_dir=/home/mheil/version_chris/private/cord
levenberg_marquart_code=levenberg_marquardt_for_damped_exponential
cd $levenberg_marquart_dir
make $levenberg_marquart_code
cd $my_dir


# Specify start time for fit
#---------------------------
t_start=70.0 


#dir=/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/FINAL_RUNS/NEW_cover_only_porous_alpha_0_and_1/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio1.0_non_stress_div_form_implicit_fluid_mesh_update

#dir=/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/FINAL_RUNS/NEW_RUN_all_porous_k1em13/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio1.0_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous

dir=/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/FINAL_RUNS/NEW_RUN_all_porous_k1em12/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio10.0_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous

#dir=/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/FINAL_RUNS/NEW_RUN_all_porous_k1em14/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio0.1_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous

#dir=/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/FINAL_RUNS/NEW_cover_only_porous_alpha_0_and_1/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio1.0_non_stress_div_form_implicit_fluid_mesh_update

#dir=/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/FINAL_RUNS/NEW_RUN_all_porous_k1em13_elliptical_boundaries/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio1.0_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous

file=`echo $dir`/dsyrinx_volume_cycle_average.dat
`echo $levenberg_marquart_dir"/"$levenberg_marquart_code` --trace_file $file --fit_test_file test_fit.dat --asymptote_file asymptote.dat --final_value_file final_value.dat --t_start_fit $t_start

normalised_file=`echo $dir`/normalised_dsyrinx_volume_cycle_average.dat
`echo $levenberg_marquart_dir"/"$levenberg_marquart_code` --trace_file $normalised_file --fit_test_file test_fit_normalised.dat --asymptote_file normalised_asymptote.dat --final_value_file normalised_final_value.dat --t_start_fit $t_start

awk '{print $1 " " $11}' `echo $dir`/trace.dat > orig_trace.dat
awk '{print $1 " " $11/$10}' `echo $dir`/trace.dat > normalised_orig_trace.dat



#################################################################################


exit

ellipt_dir=/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/FINAL_RUNS/NEW_RUN_all_porous_k1em13_elliptical_boundaries/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio1.0_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous


ellipt_file=`echo $ellipt_dir`/dsyrinx_volume_cycle_average.dat
`echo $levenberg_marquart_dir"/"$levenberg_marquart_code` --trace_file $ellipt_file --fit_test_file test_fit_ellipt.dat --asymptote_file ellipt_asymptote.dat --final_value_file ellipt_final_value.dat --t_start_fit $t_start

awk '{print $1 " " $11}' `echo $ellipt_dir`/trace.dat > ellipt_orig_trace.dat



echo "more below here..."

exit


k12_dir=/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/FINAL_RUNS/NEW_RUN_all_porous_k1em12/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio10.0_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous

k12_file=`echo $k12_dir`/dsyrinx_volume_cycle_average.dat
`echo $levenberg_marquart_dir"/"$levenberg_marquart_code` --trace_file $k12_file --fit_test_file test_fit_k1em12.dat --asymptote_file k1em12_asymptote.dat --final_value_file k1em12_final_value.dat --t_start_fit $t_start

awk '{print $1 " " $11}' `echo $k12_dir`/trace.dat > k1em12_orig_trace.dat



k13_dir=/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/FINAL_RUNS/NEW_RUN_all_porous_k1em13/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio1.0_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous

k13_file=`echo $k13_dir`/dsyrinx_volume_cycle_average.dat
`echo $levenberg_marquart_dir"/"$levenberg_marquart_code` --trace_file $k13_file --fit_test_file test_fit_k1em13.dat --asymptote_file k1em13_asymptote.dat  --final_value_file k1em13_final_value.dat --t_start_fit $t_start

awk '{print $1 " " $11}' `echo $k13_dir`/trace.dat > k1em13_orig_trace.dat



k14_dir=/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/FINAL_RUNS/NEW_RUN_all_porous_k1em14/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio0.1_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous
k14_file=`echo $k14_dir`/dsyrinx_volume_cycle_average.dat
`echo $levenberg_marquart_dir"/"$levenberg_marquart_code` --trace_file $k14_file --fit_test_file test_fit_k1em14.dat --asymptote_file k1em14_asymptote.dat  --final_value_file k1em14_final_value.dat --t_start_fit $t_start

awk '{print $1 " " $11}' `echo $k14_dir`/trace.dat > k1em14_orig_trace.dat


echo " "
echo "tecplot it with: "
echo " "
echo " tecplot k1em12_orig_trace.dat   k1em13_orig_trace.dat   k1em14_orig_trace.dat $k12_file $k13_file $k14_file test_fit_k1em12.dat test_fit_k1em13.dat test_fit_k1em14.dat k1em12_asymptote.dat k1em13_asymptote.dat k1em14_asymptote.dat k1em12_final_value.dat k1em13_final_value.dat k1em14_final_value.dat"
echo " " 

