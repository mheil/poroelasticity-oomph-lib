#! /bin/bash

file_list1=`ls poro_traction_syrinx?.dat`
file_list2=`ls poro_traction_syrinx??.dat`
file_list3=`ls poro_traction_syrinx???.dat`
file_list=`echo $file_list1 " " $file_list2 " " $file_list3 " " `

count=0
rm -f radial_displ_trace_from_traction_files.dat
for file in `echo $file_list`; do
    awk -v count=$count '{if (NR==2){print count " " $3}}' $file >> radial_displ_trace_from_traction_files.dat
    let count=$count+1
done
