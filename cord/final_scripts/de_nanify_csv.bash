#! /bin/bash


#--------------------------------------------------------------
# Get rid of lines containing nans from all files of the form
# *.*.csv (up to three digits) and write them to line_data*.dat
#--------------------------------------------------------------
#prefix=sss_slice_z_minus7pt5
prefix=syrinx_slice_z_minus7pt5
list0=`ls $prefix*.?.csv`
list1=`ls $prefix*.??.csv`
list2=`ls $prefix*.???.csv`
list=`echo $list0 $list1 $list2`
count=0
for file in `echo $list`; do
    echo "Doing: "$file
    awk 'BEGIN{FS = ","; first_line=1}{if ($1!="nan"){if (first_line==1){print "VARIABLES="$0; first_line=0}else{print $0}}}' $file > `echo $prefix`_line_data`echo $count`.dat
    let count=$count+1
done


