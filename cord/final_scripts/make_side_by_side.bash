#! /bin/bash

########################################

stem1="radial_seepage_maple_vs_oomph_for_radial_line_through_syrinx_cover_oomph_only_"
stem2="pressure_maple_vs_oomph_for_radial_line_through_syrinx_cover_oomph_only_"
combined_stem="combined_oomph_only_"


stem1="radial_seepage_maple_vs_oomph_for_radial_line_through_syrinx_cover_"
stem2="pressure_maple_vs_oomph_for_radial_line_through_syrinx_cover_"
combined_stem="combined_"



count=1
file_list=`ls $stem1*.png`
for file in `echo $file_list`; do
    file1=`echo $stem1`$count".png"
    file2=`echo $stem2`$count".png"

    #Crop
    convert -crop  2000x1750+20+100 $file1 junk1.png
    convert -crop  2000x1750+20+100 $file2 junk2.png

    #Append sideways
    combined_file=`echo $combined_stem$count".png"`
    echo "COMBINING: " $file1 $file2 $combined_file
    convert +append junk1.png junk2.png $combined_file
    
    let count=$count+1
done

exit

########################################


ln -s ../NEW_RUN_all_porous_k1em12/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio10.0_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous/FULL_STEPS_STARTING_FROM_STEP750/CYCLE_AVERAGE_NORMAL_SEEPAGE_FLOW_FIRST_0_N_80/ k12_flux_raw_data

ln -s ../NEW_RUN_all_porous_k1em13/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio1.0_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous/FULL_STEPS_STARTING_FROM_STEP750/CYCLE_AVERAGE_NORMAL_SEEPAGE_FLOW_FIRST_0_N_80/ k13_flux_raw_data

ln -s ../NEW_RUN_all_porous_k1em14/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio0.1_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous/FULL_STEPS_STARTING_FROM_STEP750/CYCLE_AVERAGE_NORMAL_SEEPAGE_FLOW_FIRST_0_N_80/ k14_flux_raw_data



dir_stem_list="k12 k13 k14"
for dir_stem in `echo $dir_stem_list`; do
    new_dir=`echo $dir_stem`_post_processed
    mkdir $new_dir
    old_dir=`echo $dir_stem`_flux_raw_data
    file_list=`ls $old_dir/*.png`
    for file in `echo $file_list`; do
        new_file=`echo $new_dir`/cropped_`basename $file`
        #echo $file " " $new_file
        convert -crop 1569x1600+211+64 $file $new_file
    done
done

exit
