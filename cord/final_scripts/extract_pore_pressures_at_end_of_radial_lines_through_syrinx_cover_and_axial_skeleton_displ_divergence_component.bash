#! /bin/bash

#========================================================
# Extract divergence and pore pressure at inside and 
# outside of radial line across syrinx cover from
# one period's worth of files.
#
# Note: Output is:
#
#           t, quantity, t+T
#
#       to facilitate assessment of time-periodicity.
#========================================================
period=14.14

#------------------------------------------
# Stem of filename containing relevant data
#------------------------------------------
stem="upper_radial_line_through_syrinx_cover"
file_list=`ls $stem[0-9]*.dat`
nstep=`echo $file_list | wc -w`

# Output fiiles
inner_trace_file=`echo $stem`"_inner_pressure_trace.dat"
outer_trace_file=`echo $stem`"_outer_pressure_trace.dat"
inner_flux_trace_file=`echo $stem`"_inner_radial_flux_trace.dat"
outer_flux_trace_file=`echo $stem`"_outer_radial_flux_trace.dat"
div_trace_file=`echo $stem`"_average_axial_divergence_component_of_skeleton_displ_trace.dat"
rm -f $inner_trace_file
rm -f $outer_trace_file
rm -f $div_trace_file

# Process 'em
count=0
for dummy_file in `echo $file_list`; do
    file=`echo $stem`$count".dat"

    #--------------------------------------------
    # Pore pressure is in column 8 of line output
    #--------------------------------------------
    awk -v period=$period -v count=$count -v nstep=$nstep '{if (NR==1){print period/(nstep-1)*count " " $8 " " period+period/(nstep-1)*count}}' $file >> $inner_trace_file
    awk -v period=$period -v count=$count -v nstep=$nstep 'END{print period/(nstep-1)*count " " $8" " period+period/(nstep-1)*count}'           $file >> $outer_trace_file

    #--------------------------------------------
    # Radial flux is in column 5 of line output
    #--------------------------------------------
    awk -v period=$period -v count=$count -v nstep=$nstep '{if (NR==1){print period/(nstep-1)*count " " $5 " " period+period/(nstep-1)*count}}' $file >> $inner_flux_trace_file
    awk -v period=$period -v count=$count -v nstep=$nstep 'END{print period/(nstep-1)*count " " $5" " period+period/(nstep-1)*count}'           $file >> $outer_flux_trace_file


    #--------------------------------------------------------------------------------------------
    # Axial component of divergence of solid skeleton displacement is in column 16 of line output
    #--------------------------------------------------------------------------------------------
    awk -v period=$period -v count=$count -v nstep=$nstep 'BEGIN{sum=0; n_item=0}{sum+=$16;n_item++}END{print period/(nstep-1)*count " " sum/n_item" " period+period/(nstep-1)*count}'           $file >> $div_trace_file
    let count=$count+1
done
