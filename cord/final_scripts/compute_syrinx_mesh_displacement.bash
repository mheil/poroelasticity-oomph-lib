#! /bin/bash


syrinx_file0=soln-syrinx-fluid0.dat
syrinx_file1=soln-syrinx-fluid1.dat

awk '{if (($1=="ZONE")||(NF==3)||(NF==0)){}else{print $1 " " $2}}' $syrinx_file0 > .tmp0
awk '{if (($1=="ZONE")||(NF==3)||(NF==0)){}else{print $1 " " $2}}' $syrinx_file1 > .tmp1

paste .tmp0 .tmp1 > .both.dat
awk '{print $1 " " $2 " " $3-$1 " " $4-$2}' .both.dat > .tmp_diff


# fill in triangle annotation
awk 'BEGIN{count=0}{
        if (NF!=0){
        if (count==0){print "ZONE N=15, E=16, F=FEPOINT, ET=TRIANGLE"; print $0; count++}
        else if (count<14){print $0;count++}
        else if (count==14)
         {print$0;
          print "1 2 6";
          print "2 7 6";
          print "2 3 7";
          print "3 8 7";
          print "3 4 8";
          print "4 9 8";
          print "4 5 9";
          print "6 7 10";
          print "7 11 10";
          print "7 8 11";
          print "8 12 11";
          print "8 9 12";
          print "10 11 13";
          print "11 14 13";
          print "11 12 14";
          print "13 14 15"; count=0}}}' .tmp_diff > syrinx_mesh_displacement.dat

#makePvd extracted_cord_$stem extracted_displ_`echo $stem`.pvd
#oomph-convert -o cycle_average_`echo $stem`.dat


