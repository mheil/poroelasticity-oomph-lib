-- Decide on first full step (for fully period solution) and run

      final_scripts/extract_one_period_full_step_data.bash

   in the RESLT directory. This links actual data/vtu files
   to re-enumerated files in FULL_STEPS_STARTING_FROM_* directory
   and prepares pvd files for fluid velocity plots.


-- Create fluid velocity plots with 

         final_scripts/sss_contours_cool_to_warm.pvsm

         final_scripts/sss_carpet.pvsm

   in FULL_* directory 


-- Create solid deformation with

         final_scripts/solid_deformation.pvsm

   in FULL_* directory 


-- Compute cycle average pressure and seepage flow by running 

      final_scripts/compute_cycle_average_normal_seepage_flow_and_pore_pressure.bash

   in FULL_* directory. Creates relavant data in 
  
    CYCLE_AVERAGE_NORMAL_SEEPAGE_FLOW_FIRST_*_N* directory.

   Can be animated with 

         final_scripts/cycle_average_pressure.mcr

-- Compute moving average of seepage flux and change in syrinx volume
   by running this script in actual result directory (specify
   name of trace file (if combined from restarted runs, say) first
   if necessary):

         final_scripts/compute_moving_average_seepage_flux_and_change_in_syrinx_volume.bash

   Creates four files which can be visualised with

       tecplot dsyrinx_volume_cycle_average.dat \
               dsyrinx_volume_trace.dat \
               syrinx_seepage_flux_cycle_average.dat \
               syrinx_seepage_flux_trace.dat &


    Get all of them from top level directory with


       tecplot */*/dsyrinx_volume_cycle_average.dat  \
               */*/dsyrinx_volume_trace.dat \
               */*/syrinx_seepage_flux_cycle_average.dat \
               */*/syrinx_seepage_flux_trace.dat &

    Fit decaying exponential to dsyrinx_volume_cycle_average.dat files
    using

       final_scripts/fit_moving_dsyrinx_volume_average.bash

    Read through this file first to see if all variables have been
    set correctly.

    Data and layout files currently live in 
    
       FINAL_RUNS/AnalyseCycleAverageSyrinxVolumeEtc/

    and have also been copied across to latex figures directory.

    Ditto for analysis of net volume flux; see 

       FINAL_RUNS/AnalyseCycleAverageSyrinxVolumeEtc/net_volume_flux_into_syrinx_and_fded_syrinx_volume_all_k_README.txt

    for details (file couldn't be journaled).




-- Animation of pore pressure and seepage flux at fsi boundaries:

    Go to FULL_* directory and run

      final_scripts/compute_cycle_average_normal_seepage_flow_and_pore_pressure.bash

    Go into resulting directory and create png files with

      final_scripts/animate_seepage_flux_and_pressure_at_fsi_interfaces.mcr

    (Enter scaling factor to adjust for larger seepage flow at
    larger permeabilities so for k=1e-13 use 1.0; for k=1e-14 use 10.0 etc.


     Post-process (crop, append, etc) in separate directory, currently

        AnalyseSeepageFlowAcrossFSIBoundaries

     where original data is linked to and the post-processed with

        final_scripts/post_process_seepage_flow_over_fsi_boundaries.bash

     Resulting directories can be copied into latex directory.



-- Animation of radial seepage flow:

   In FULL* directory:

      oomph-convert -z -p2 upper_radial_line_through_syrinx_cover*.dat

   (if needed)

   Then run animation with 

       final_scripts/radial_seepage_flow.pvsm

   and create pngs with width of 2000.


-- To compare variation of stuff through syrinx cover against
   1D maple model: Run

      final_scripts/extract_pore_pressures_at_end_of_radial_lines_through_syrinx_cover_and_axial_skeleton_displ_divergence_component.bash

   in the FULL* directory. Creates files:

      upper_radial_line_through_syrinx_cover_average_axial_divergence_component_of_skeleton_displ_trace.dat
      upper_radial_line_through_syrinx_cover_inner_pressure_trace.dat
      upper_radial_line_through_syrinx_cover_outer_pressure_trace.dat

   which contain t, quantity, t+T to allow assessment of time periodicity.


   FOR COMPARISON AGAINST ACTUAL FSI RUN:

     Fit these to time harmonic oscillation using

        final_scripts/fit_data_for_one_d_model.bash


     NOTE: This needs specification of the location of the levenberg marquart
     executable built from 

        private/cord/levenberg_marquardt_for_simple_harmonic.cc


     Output from this fitting run is in

       OUTPUT_fit_for_comparison_with_one_d_model

     and contains the fitting coefficients. 

     Add these to the maple script in 
   
          private/cord_poro_only/analyse_one_d_two_layer.map

     and 

     END ONLY FSI RUN

   Subsequently choose with case you want to run via the
   k_identifier variable (search for hierher).

   Also update the permeability ratio and the directory to point
   back to the FULL* directory.

   Then run maple script (in xmaple because of graphics).

   The *_from_maple.dat files are then created in the original
   FULL* directory, where they can be compared against the FE
   solution using

      final_scripts/animate_maple_vs_oomph_for_radial_line_through_syrinx_cover.mcr

    
