#! /bin/bash


# Data file must contain r,z,u_r,u_z
#-----------------------------------
file_list='cycle_average_displ_poro_output_on_fsi_syrinx_boundary_NORMAL_GEOMETRY.dat cycle_average_displ_poro_output_on_fsi_syrinx_boundary_ELLIPTICAL_GEOMETRY.dat'

for file in `echo $file_list`; do


echo "file: " $file
awk '
BEGIN{
      orig_vol=0;
      new_vol=0;
      orig_head_vol=0;
      orig_central_vol=0;
      orig_tail_vol=0;
      new_head_vol=0;
      new_central_vol=0;
      new_tail_vol=0;
      count=0;
      pi=3.141592654;
      z_upper=-4.5;
      z_lower=-10.5
     }
{
 if (count==0)
  {
   prev_z=$2;
   prev_r=$1;

   prev_uz=$4;
   prev_ur=$3;
  }
 else
  {
   current_z=$2;
   current_r=$1;
   current_uz=$4;
   current_ur=$3;
   
   orig_dvol=1/2*(current_z-prev_z)*pi*(current_r*current_r+prev_r*prev_r);
   orig_vol+=orig_dvol;

   new_dvol=1/2*((current_z+current_uz)-(prev_z+prev_uz))*pi*((current_r+current_ur)*(current_r+current_ur)+(prev_r+prev_ur)*(prev_r+prev_ur));
   new_vol +=new_dvol;

   in_central_part=1
   if (current_z>=z_upper)
    {
     orig_head_vol+=orig_dvol;
     new_head_vol+=new_dvol;
     in_central_part=0;
    }
   if (prev_z<=z_lower)
    {
     orig_tail_vol+=orig_dvol;
     new_tail_vol+=new_dvol;
     in_central_part=0;
    }
   if (in_central_part==1)
    {
     orig_central_vol+=orig_dvol;
     new_central_vol+=new_dvol;
    }

   prev_z=current_z;
   prev_r=current_r;
   prev_uz=current_uz;
   prev_ur=current_ur;
  }
  count++
}
END{print "Orig vol: " orig_vol
    print "New  vol: " new_vol
    print "Diff vol: " new_vol-orig_vol
    print "Rel diff: " (new_vol-orig_vol)/orig_vol 
    print "Old head: " orig_head_vol 
    print "Old cent: " orig_central_vol 
    print "Old tail: " orig_tail_vol 
    print "CHECK: " orig_vol-(orig_head_vol+orig_central_vol+orig_tail_vol)
    print "New head: " new_head_vol 
    print "New cent: " new_central_vol 
    print "New tail: " new_tail_vol 
    print "CHECK: " new_vol-(new_head_vol+new_central_vol+new_tail_vol)
    print "Change head: " new_head_vol-orig_head_vol
    print "Change cent: " new_central_vol-orig_central_vol
    print "Change tail: " new_tail_vol-orig_tail_vol 
    print "Rel change head: " (new_head_vol-orig_head_vol)/orig_head_vol
    print "Rel change cent: " (new_central_vol-orig_central_vol)/orig_central_vol
    print "Rel change tail: " (new_tail_vol-orig_tail_vol)/orig_tail_vol
}' $file

done
