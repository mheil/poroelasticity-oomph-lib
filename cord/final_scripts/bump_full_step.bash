#----------------------------------------------------------------------
# Rename name of full step files by bumping the number in the filename
#----------------------------------------------------------------------
bump=891


#====================
# Backup
#====================
backup_dir="orig_full_step_files"
if [ -e "$backup_dir" ];  then
    echo " "
    echo "ERROR: directory $backup_dir already exists"
    echo " "
    exit
fi
mkdir $backup_dir


full_step_file_list=`ls full_step*.dat`
for full_step_file in `echo $full_step_file_list`; do
    cp $full_step_file $backup_dir
    number=`basename $full_step_file .dat | awk '{print substr($0,10)}'`
    let new_number=$number+$bump
    #echo $full_step_file " " $number" " $new_number
    new_filename="full_step$new_number.dat"
    echo $new_filename
    #mv $full_step_file $new_filename
done
