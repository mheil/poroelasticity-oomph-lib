#! /bin/bash

#-----------------------------------------------------------
# Specify directory that contains Levenberg Marquart fitter
#-----------------------------------------------------------
levenberg_marquart_driver=/home/mheil/version_chris/private/cord/levenberg_marquardt_for_simple_harmonic

output="OUTPUT_fit_for_comparison_with_one_d_model"
if [ -e "$output" ];  then
    echo " "
    echo "ERROR: Please delete file $output and try again"
    echo " "
    exit
fi
echo "OUTPUTING DATA INTO $output"

echo " "  >> `echo $output`
echo "======================================="  >> `echo $output`
echo "INNER PRESSURE"  >> `echo $output`
echo "======================================="  >> `echo $output`
echo " "  >> `echo $output`
`echo $levenberg_marquart_driver` --trace_file  upper_radial_line_through_syrinx_cover_inner_pressure_trace.dat --fit_test_file test_upper_radial_line_through_syrinx_cover_inner_pressure_trace.dat >> `echo $output`

echo " " >> `echo $output`
echo "=======================================" >> `echo $output`
echo "OUTER PRESSURE"  >> `echo $output`
echo "=======================================" >> `echo $output`
echo " "  >> `echo $output`
`echo $levenberg_marquart_driver` --trace_file  upper_radial_line_through_syrinx_cover_outer_pressure_trace.dat --fit_test_file test_upper_radial_line_through_syrinx_cover_outer_pressure_trace.dat  >> `echo $output`

echo " " >> `echo $output`
echo "=======================================" >> `echo $output`
echo "AVERAGE DIVERGENCE" >> `echo $output`
echo "=======================================" >> `echo $output`
echo " "  >> `echo $output`
`echo $levenberg_marquart_driver` --trace_file  upper_radial_line_through_syrinx_cover_average_axial_divergence_component_of_skeleton_displ_trace.dat --fit_test_file test_upper_radial_line_through_syrinx_cover_average_axial_divergence_component_of_skeleton_displ_trace.dat >> `echo $output`

echo "CAN POSTPROCESS THIS WITH: "
echo " " 
echo "tecplot upper_radial_line_through_syrinx_cover_inner_pressure_trace.dat test_upper_radial_line_through_syrinx_cover_inner_pressure_trace.dat"
echo "tecplot upper_radial_line_through_syrinx_cover_outer_pressure_trace.dat test_upper_radial_line_through_syrinx_cover_outer_pressure_trace.dat"
echo "tecplot upper_radial_line_through_syrinx_cover_average_axial_divergence_component_of_skeleton_displ_trace.dat test_upper_radial_line_through_syrinx_cover_average_axial_divergence_component_of_skeleton_displ_trace.dat"
echo " " 
echo "or all in one go:"
echo " "
echo "tecplot upper_radial_line_through_syrinx_cover_inner_pressure_trace.dat test_upper_radial_line_through_syrinx_cover_inner_pressure_trace.dat upper_radial_line_through_syrinx_cover_outer_pressure_trace.dat test_upper_radial_line_through_syrinx_cover_outer_pressure_trace.dat upper_radial_line_through_syrinx_cover_average_axial_divergence_component_of_skeleton_displ_trace.dat test_upper_radial_line_through_syrinx_cover_average_axial_divergence_component_of_skeleton_displ_trace.dat"
echo " "


echo " " 
echo " " 
echo "MAPLE STUFF FOR variable_para_list:" 
echo " "
awk 'BEGIN{steady_count=0; steady_count=0}{if ($1=="Steady:"){steady_count+=1; if (steady_count==1){print "P_inner_steady="$2","}; if (steady_count==2){print "P_outer_steady="$2","}; if (steady_count==3){print "Dwdz_steady:="$2";"}}; if ($1=="Unsteady:"){unsteady_count+=1; if (unsteady_count==1){print "P_inner="$2","}; if (unsteady_count==2){print "P_outer="$2"\n"}; if (unsteady_count==3){print "Dwdz:="$2";"}}}' OUTPUT_fit_for_comparison_with_one_d_model 
