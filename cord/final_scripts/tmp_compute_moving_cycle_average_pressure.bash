#! /bin/bash





nsteps_per_period=80

last_available_step_for_moving_average=1720
first_step_to_be_used_for_moving_average=890 
step_increment_for_moving_average=$nsteps_per_period


first_step_list=""
first_step_for_moving_average=$first_step_to_be_used_for_moving_average
let last_step=$first_step_for_moving_average+$nsteps_per_period

while [ $last_step -lt $last_available_step_for_moving_average ]; do
    first_step_list=`echo $first_step_list`" "$first_step_for_moving_average
    let first_step_for_moving_average=$first_step_for_moving_average+$step_increment_for_moving_average
    let last_step=$first_step_for_moving_average+$nsteps_per_period
done

echo $first_step_list

data_dir=`pwd`
echo "Data directory: "$data_dir

# Master run directory; make and move into it
#--------------------------------------------
run_dir=MOVING_CYCLE_AVERAGE_PORE_PRESSURE
if [ -e "$run_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $run_dir and try again"
    echo " "
    exit
fi
mkdir $run_dir
cd $run_dir


min_p=1000000
max_p=-1000000
min_z=1000000
max_z=-1000000


for first_step in `echo $first_step_list`; do

    stem_list="sss_inner_fsi_boundary_veloc_line syrinx_centreline_veloc_line"
    for stem in `echo $stem_list`; do
        
        step=$first_step
        count=0
        while [ $count -lt $nsteps_per_period ]; do
            file=$stem`echo $step`".dat"
            echo $file
            awk '{if ($1!="ZONE"){print $2 " " $5}}' $data_dir/$file > extracted_z_p_`echo $file`  
            if [ $count -eq 0 ]; then
                cp extracted_z_p_`echo $file` .tmp_sum
            else
                paste extracted_z_p_`echo $file` .tmp_sum > .both.dat
                awk '{{print $1 " " $2+$4}}' .both.dat > .tmp_sum
                awk 'BEGIN{max_z=-100000; min_z=100000;max_p=-100000; min_p=100000} {if ($1>max_z){max_z=$1}if ($2>max_p){max_p=$2}if ($1<min_z){min_z=$1}if ($2<min_p){min_p=$2}}END{print min_z " " max_z " " min_p " " max_p}' .both.dat > .min_max
                min_z=`awk -v current_min=$min_z '{if ($1<current_min){print$1}else{print current_min}}' .min_max` 
                max_z=`awk -v current_max=$max_z '{if ($2>current_max){print$2}else{print current_max}}' .min_max` 
                min_p=`awk -v current_min=$min_p '{if ($3<current_min){print$3}else{print current_min}}' .min_max` 
                max_p=`awk -v current_max=$max_p '{if ($4>current_max){print$4}else{print current_max}}' .min_max` 
            fi
            awk '{print "0.0 "$1" "$2}' extracted_z_p_`echo $file` | sort -g -k 2 > .junk
            mv .junk extracted_z_p_`echo $file`
            #oomph-convert -o -p2 -z extracted_z_p_`echo $file`
            let step=$step+1
            let count=$count+1
        done
        
        cycle_av_file="cycle_average_"`echo $stem`"_first_step"`echo $first_step`".dat"
        echo "Average file: " $cycle_av_file
        awk -v n=$nsteps_per_period '{print "0.0 " $1 " " $2/n}' .tmp_sum | sort -g -k 2 > cycle_average_`echo $stem`_first_step`printf "%05d" $first_step`.dat
        
        #makePvd extracted_z_p_$stem extracted_z_p_`echo $stem`.pvd
        #oomph-convert -o -p2 cycle_average_`echo $stem`.dat
    done
done

