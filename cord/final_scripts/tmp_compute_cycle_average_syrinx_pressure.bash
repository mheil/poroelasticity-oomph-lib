#! /bin/bash


first_step=0
nsteps_per_period=80


data_dir=`pwd`
echo "Data directory: "$data_dir

# Master run directory; make and move into it
#--------------------------------------------
run_dir=CYCLE_AVERAGE_SYRINX_PRESSURE_FIRST_`echo $first_step`_N_`echo $nsteps_per_period`
if [ -e "$run_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $run_dir and try again"
    echo " "
    exit
fi
mkdir $run_dir
cd $run_dir


min_p=1000000
max_p=-1000000
min_z=1000000
max_z=-1000000

stem_list="soln-syrinx-fluid"
for stem in `echo $stem_list`; do

    step=$first_step
    count=0
    while [ $count -lt $nsteps_per_period ]; do
        file=$stem`echo $step`".dat"
        echo $file
        awk '{if (($1=="ZONE")||(NF==3)||(NF==0)){}else{print $1 " " $2 " " $6}}' $data_dir/$file > extracted_p_`echo $file`  
        if [ $count -eq 0 ]; then
            cp extracted_p_`echo $file` .tmp_sum
        else
            paste extracted_p_`echo $file` .tmp_sum > .both.dat
            awk '{print $1 " " $2 " " $3+$6}' .both.dat > .tmp_sum
        fi
        # fill in triangle annotation
        awk 'BEGIN{count=0}{
        if (NF!=0){
        if (count==0){print "ZONE N=15, E=16, F=FEPOINT, ET=TRIANGLE"; print $0; count++}
        else if (count<14){print $0;count++}
        else if (count==14)
         {print$0;
          print "1 2 6";
          print "2 7 6";
          print "2 3 7";
          print "3 8 7";
          print "3 4 8";
          print "4 9 8";
          print "4 5 9";
          print "6 7 10";
          print "7 11 10";
          print "7 8 11";
          print "8 12 11";
          print "8 9 12";
          print "10 11 13";
          print "11 14 13";
          print "11 12 14";
          print "13 14 15"; count=0}}}' extracted_p_`echo $file` > .junk
        mv .junk extracted_p_`echo $file`
        oomph-convert -o -z extracted_p_`echo $file`
        let step=$step+1
        let count=$count+1
    done
    
    awk -v n=$nsteps_per_period '{print $1 " " $2 " " $3/n}' .tmp_sum > cycle_average_`echo $stem`.dat
        # fill in triangle annotation
        awk 'BEGIN{count=0}{
        if (NF!=0){
        if (count==0){print "ZONE N=15, E=16, F=FEPOINT, ET=TRIANGLE"; print $0; count++}
        else if (count<14){print $0;count++}
        else if (count==14)
         {print$0;
          print "1 2 6";
          print "2 7 6";
          print "2 3 7";
          print "3 8 7";
          print "3 4 8";
          print "4 9 8";
          print "4 5 9";
          print "6 7 10";
          print "7 11 10";
          print "7 8 11";
          print "8 12 11";
          print "8 9 12";
          print "10 11 13";
          print "11 14 13";
          print "11 12 14";
          print "13 14 15"; count=0}}}' cycle_average_`echo $stem`.dat > .junk
        mv .junk cycle_average_`echo $stem`.dat

    makePvd extracted_p_$stem extracted_z_`echo $stem`.pvd
    oomph-convert -o cycle_average_`echo $stem`.dat
done

