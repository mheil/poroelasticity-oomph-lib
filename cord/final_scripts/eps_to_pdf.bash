#! /bin/bash


#--------------------------------------------------------------
# ps2pdf
#--------------------------------------------------------------
stem=cycle_average_pressure
list0=`ls $stem?.eps`
list1=`ls $stem??.eps`
list2=`ls $stem???.eps`
list=`echo $list0 $list1 $list2`
count=0
for file in `echo $list`; do
    echo "Doing: "$file
    epstopdf $file -o `echo $stem``echo $count`.pdf
    let count=$count+1
done


