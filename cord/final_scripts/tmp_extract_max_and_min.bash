#! /bin/bash



stem=`basename $1 .dat`

t_min=70;

awk -v t_min=$t_min 'BEGIN{count=0}{if ($1>t_min){ if(count>1){if ((prev_prev<prev)&&($2<prev)){print x_prev " " prev}; x_prev_prev=x_prev; x_prev=$1; prev_prev=prev; prev=$2}; if(count==1){x_prev=$1; prev=$2;count++}; if (count==0){x_prev_prev=$1; prev_prev=$2};count++}}' $1 > `echo $stem`_max.dat

awk -v t_min=$t_min 'BEGIN{count=0}{if ($1>t_min){ if(count>1){if ((prev_prev>prev)&&($2>prev)){print x_prev " " prev}; x_prev_prev=x_prev; x_prev=$1; prev_prev=prev; prev=$2}; if(count==1){x_prev=$1; prev=$2;count++}; if (count==0){x_prev_prev=$1; prev_prev=$2};count++}}' $1 > `echo $stem`_min.dat
