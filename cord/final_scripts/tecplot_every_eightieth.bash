#! /bin/bash

stem=upper_radial_line_through_syrinx_cover

label=80

tecplot_string="/home/mheil/local/tecplot360ex/bin/tec360 -h /home/mheil/local/tecplot360ex "
keep_going=1
while [ $keep_going == 1 ]; do
    
    filename=$stem$label".dat"
    echo "FILENAME " $filename

    if [ -e $filename ]; then
        echo "exists"
        tecplot_string=$tecplot_string" "$filename
    else
        echo "doesn't exist"
        keep_going=0
    fi
    
    echo "keep going: " $keep_going
    let label=$label+80

done


`echo $tecplot_string`

