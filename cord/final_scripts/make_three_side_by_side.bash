#! /bin/bash

########################################

stem=solid_deformation
sub_dir_with_slash_if_provided=""

count=0
file_list=`ls NoShift/$sub_dir_with_slash_if_provided$stem?.png NoShift/$sub_dir_with_slash_if_provided$stem??.png`
for file in `echo $file_list`; do

    files=`ls [A-Z]*/$sub_dir_with_slash_if_provided$stem$count.png`
    combined_file=`echo "all_three_side_by_side/"$stem$count".png"`
    echo "doing "$files " " $combined_file

    #Append sideways
    convert +append $files $combined_file
    
    let count=$count+1
done

exit

########################################


ln -s ../NEW_RUN_all_porous_k1em12/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio10.0_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous/FULL_STEPS_STARTING_FROM_STEP750/CYCLE_AVERAGE_NORMAL_SEEPAGE_FLOW_FIRST_0_N_80/ k12_flux_raw_data

ln -s ../NEW_RUN_all_porous_k1em13/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio1.0_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous/FULL_STEPS_STARTING_FROM_STEP750/CYCLE_AVERAGE_NORMAL_SEEPAGE_FLOW_FIRST_0_N_80/ k13_flux_raw_data

ln -s ../NEW_RUN_all_porous_k1em14/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio0.1_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous/FULL_STEPS_STARTING_FROM_STEP750/CYCLE_AVERAGE_NORMAL_SEEPAGE_FLOW_FIRST_0_N_80/ k14_flux_raw_data



dir_stem_list="k12 k13 k14"
for dir_stem in `echo $dir_stem_list`; do
    new_dir=`echo $dir_stem`_post_processed
    mkdir $new_dir
    old_dir=`echo $dir_stem`_flux_raw_data
    file_list=`ls $old_dir/*.png`
    for file in `echo $file_list`; do
        new_file=`echo $new_dir`/cropped_`basename $file`
        #echo $file " " $new_file
        convert -crop 1569x1600+211+64 $file $new_file
    done
done

exit
