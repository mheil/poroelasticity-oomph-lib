#!MC 1410

$!VARSET |reynolds|=14142.1
$!VARSET |q_fsi|=2.82843e-08
$!VARSET |k_perm|=0.00883884






#################################
# update these  
#################################
$!VARSET |do_pressure|=1
$!VARSET |with_maple|=1

##############################################
# 8e-5 is good for q under normal conditions
##############################################
#$!VARSET |Y_range_q|=8e-5 # normal
$!VARSET |Y_range_q|=2.5e-6 # k=1e-15 different properties
#$!VARSET |Y_range_q|=4.5e-5 # k=1e-15 pia = cord
#$!VARSET |Y_range_q|=5e-4 # k=1e-13 pia = cord
#$!VARSET |Y_range_q|=2e-4 # k=1e-14 pia = cord
#$!VARSET |Y_range_q|=1.5e-5 # k=1e-14 pia != cord

##############################################
# 1.2 is good for p under normal conditions
##############################################
#$!VARSET |Y_range_p|=1.2 # normal
$!VARSET |Y_range_p|=2.0 # k1e-15 pia = cord also for k1e-14
#$!VARSET |Y_range_p|=1.5 # for pia != cord k1e-14




$!IF |with_maple|==0
$!VARSET |oomph_only_string|="_oomph_only"
$!ELSE
$!VARSET |oomph_only_string|=""
$!ENDIF


$!VARSET |first_oomph|=0
$!VARSET |n_oomph|=80 # 320
$!VARSET |n_minus_one_oomph|=(|n_oomph|-1)
$!READDATASET  '"upper_radial_line_through_syrinx_cover|first_oomph|.dat" '
  READDATAOPTION = NEW
  RESETSTYLE = YES
  VARLOADMODE = BYNAME
  ASSIGNSTRANDIDS = YES

$!LOOP |n_minus_one_oomph|
$!VARSET |number|=(|first_oomph|+|LOOP|)

$!READDATASET  '"upper_radial_line_through_syrinx_cover|number|.dat" '
  READDATAOPTION = APPEND
  RESETSTYLE = NO
  VARLOADMODE = BYNAME
  ASSIGNSTRANDIDS = YES

$!ENDLOOP

$!IF |do_pressure| == 1
$!READDATASET  '"p_from_maple.dat" '
  READDATAOPTION = APPEND
  RESETSTYLE = NO
  VARLOADMODE = BYNAME
  ASSIGNSTRANDIDS = YES
$!ELSE
$!READDATASET  '"q_from_maple.dat" '
  READDATAOPTION = APPEND
  RESETSTYLE = NO
  VARLOADMODE = BYNAME
  ASSIGNSTRANDIDS = YES
$!ENDIF


$!XYLINEAXIS GRIDAREA{DRAWBORDER = YES}
$!XYLINEAXIS XDETAIL 1 {TITLE{TITLEMODE = USETEXT}}
$!XYLINEAXIS XDETAIL 1 {TITLE{TEXT = 'radial coordinate'}}
$!XYLINEAXIS YDETAIL 1 {TITLE{TITLEMODE = USETEXT}}
$!IF |do_pressure| == 1
$!XYLINEAXIS YDETAIL 1 {TITLE{TEXT = 'pore pressure'}}
$!ELSE
$!XYLINEAXIS YDETAIL 1 {TITLE{TEXT = 'radial seepage velocity'}}
$!ENDIF


$!ACTIVELINEMAPS -= [1-9]
$!DELETELINEMAPS  [1-9]

$!LOOP |NUMZONES|
$!CREATELINEMAP 
$!LINEMAP [|LOOP|]  NAME = '&ZN&'
$!ACTIVELINEMAPS += [|LOOP|]
$!LINEMAP [|LOOP|]  ASSIGN{ZONE = |LOOP|}
$!ENDLOOP

$!VIEW FIT
$!LINEMAP [1-|n_oomph|]  NAME = 'oomph'
$!IF |do_pressure| == 1
$!ALTERDATA  [1-|n_oomph|]
  EQUATION = 'v8=v8/|reynolds|/|q_fsi|'
$!LINEMAP [1-|n_oomph|]  ASSIGN{YAXISVAR = 8}
$!ELSE
$!ALTERDATA  [1-|n_oomph|]
  EQUATION = 'v5=v5*|k_perm|'
$!LINEMAP [1-|n_oomph|]  ASSIGN{YAXISVAR = 5}
$!ENDIF
$!LINEMAP [1-|n_oomph|]  LINES{COLOR = BLUE}
$!LINEMAP [1-|n_oomph|]  LINES{LINETHICKNESS = 0.400000000000000022}
$!VIEW FIT
$!VARSET |n_plus_one_oomph|=(|n_oomph|+1)
$!LINEMAP [|n_plus_one_oomph|]  LINES{SHOW = NO}
$!LINEMAP [|n_plus_one_oomph|]  LINES{SHOW = YES}
$!LINEMAP [|n_plus_one_oomph|-|NUMZONES|]  LINES{COLOR = BLACK}
$!LINEMAP [|n_plus_one_oomph|-|NUMZONES|]  LINES{LINEPATTERN = DASHED}
$!LINEMAP [|n_plus_one_oomph|-|NUMZONES|]  LINES{LINETHICKNESS = 0.800000000000000022}
$!VIEW FIT


$!IF |with_maple|==0
$!LINEMAP [|n_plus_one_oomph|-|NUMZONES|]  LINES{SHOW = NO}
$!ENDIF



$!IF |do_pressure| == 1
$!ALTERDATA  [|n_plus_one_oomph|-|NUMZONES|]
  EQUATION = 'v2=v2/|reynolds|/|q_fsi|'
$!ELSE
$!ALTERDATA  [|n_plus_one_oomph|-|NUMZONES|]
  EQUATION = 'v2=v2*|k_perm|'
$!ENDIF



$!VIEW FIT
$!FRAMELAYOUT SHOWBORDER = NO
$!XYLINEAXIS VIEWPORTPOSITION{X1 = 23}
$!XYLINEAXIS VIEWPORTPOSITION{X2 = 98}
$!XYLINEAXIS VIEWPORTPOSITION{Y2 = 91}
$!XYLINEAXIS VIEWPORTPOSITION{Y1 = 14}
$!XYLINEAXIS YDETAIL 1 {TITLE{TEXTSHAPE{HEIGHT = 4.59999999999999964}}}
$!XYLINEAXIS XDETAIL 1 {TITLE{TEXTSHAPE{HEIGHT = 4.59999999999999964}}}
$!XYLINEAXIS XDETAIL 1 {TICKLABEL{TEXTSHAPE{HEIGHT = 4}}}
$!XYLINEAXIS YDETAIL 1 {TICKLABEL{TEXTSHAPE{HEIGHT = 4}}}
$!XYLINEAXIS XDETAIL 1 {TITLE{OFFSET = 6}}

$!IF |do_pressure| == 1
$!XYLINEAXIS YDETAIL 1 {RANGEMIN = -|Y_range_p|}
$!XYLINEAXIS YDETAIL 1 {RANGEMAX =  |Y_range_p|}
$!XYLINEAXIS YDETAIL 1 {TITLE{OFFSET = 10}}
$!ELSE
$!XYLINEAXIS YDETAIL 1 {TICKLABEL{NUMFORMAT{FORMATTING = SUPERSCRIPT}}}
$!XYLINEAXIS YDETAIL 1 {TICKLABEL{NUMFORMAT{PRECISION = 1}}}
$!XYLINEAXIS YDETAIL 1 {RANGEMIN = -|Y_range_q|}
$!XYLINEAXIS YDETAIL 1 {RANGEMAX =  |Y_range_q|}
$!XYLINEAXIS YDETAIL 1 {TITLE{OFFSET = 18}}
$!ENDIF

$!VARSET |nstep|=|n_oomph|
$!LOOP |nstep|
$!ACTIVELINEMAPS -= [1-|NUMZONES|]
$!ACTIVELINEMAPS += [|LOOP|]
$!VARSET |maple|=(|n_oomph|+|LOOP|)
$!IF  |maple| > |NUMZONES|  
  $!VARSET |maple|=(|maple|-|n_oomph|)
$!ENDIF
$!ACTIVELINEMAPS += [|maple|]
$!REDRAWALL 
#$!DELAY 0.01
#$!PAUSE "BLA"



$!PRINTSETUP PALETTE = COLOR
$!EXPORTSETUP IMAGEWIDTH = 2000
$!EXPORTSETUP USESUPERSAMPLEANTIALIASING = YES
$!IF |do_pressure|==1
$!EXPORTSETUP EXPORTFNAME = 'pressure_maple_vs_oomph_for_radial_line_through_syrinx_cover|oomph_only_string|_|LOOP|.png'
$!ELSE
$!EXPORTSETUP EXPORTFNAME = 'radial_seepage_maple_vs_oomph_for_radial_line_through_syrinx_cover|oomph_only_string|_|LOOP|.png'
$!ENDIF
$!EXPORT 
  EXPORTREGION = ALLFRAMES

$!ENDLOOP


$!ACTIVELINEMAPS -= [1-|NUMZONES|]
$!VARSET |incr|=10
$!VARSET |offset|=5
$!VARSET |nstep|=8
$!LOOP |nstep|
$!VARSET |maple|=(|n_oomph|+|offset|+(|LOOP|-1)*|incr|)
$!ACTIVELINEMAPS += [|maple|]
$!VARSET |oomph|=(|n_oomph|-80+|offset|+(|LOOP|-1)*|incr|)
$!ACTIVELINEMAPS += [|oomph|]
$!ENDLOOP

$!IF |with_maple|==0
$!LINEMAP [|n_plus_one_oomph|-|NUMZONES|]  LINES{SHOW = NO}
$!ENDIF


$!REDRAWALL 

$!IF |do_pressure|==1
$!EXPORTSETUP EXPORTFNAME = 'pressure_maple_vs_oomph_for_radial_line_through_syrinx_cover|oomph_only_string|.png'
$!EXPORTSETUP EXPORTFNAME = 'pressure_maple_vs_oomph_for_radial_line_through_syrinx_cover|oomph_only_string|.eps'
$!ELSE
$!EXPORTSETUP EXPORTFNAME = 'radial_seepage_maple_vs_oomph_for_radial_line_through_syrinx_cover|oomph_only_string|.png'
$!EXPORTSETUP EXPORTFNAME = 'radial_seepage_maple_vs_oomph_for_radial_line_through_syrinx_cover|oomph_only_string|.eps'
$!ENDIF
$!EXPORT 
  EXPORTREGION = ALLFRAMES