#! /bin/bash


first_step=0
nsteps_per_period=80


data_dir=`pwd`
echo "Data directory: "$data_dir

# Master run directory; make and move into it
#--------------------------------------------

run_dir=CYCLE_AVERAGE_PORO_PRESSURE_AND_TRACTION_FIRST_`echo $first_step`_N_`echo $nsteps_per_period`
if [ -e "$run_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $run_dir and try again"
    echo " "
    exit
fi
mkdir $run_dir
cd $run_dir


stem_list="poro_fsi_syrinx poro_fsi_inner_sss"
for stem in `echo $stem_list`; do

    step=$first_step
    count=0
    while [ $count -lt $nsteps_per_period ]; do
        file=$stem`echo $step`".dat"
        echo $file
        awk '{if ($1!="ZONE"){print $2 " " $5 " " $6 " " $7}}' $data_dir/$file > .tmp.dat
        sort -g -k 1 .tmp.dat > extracted_z_traction_and_pressure_`echo $file` 
        if [ $count -eq 0 ]; then
            cp extracted_z_traction_and_pressure_`echo $file` .tmp_sum
        else
            paste extracted_z_traction_and_pressure_`echo $file` .tmp_sum > .both.dat
            awk '{{print $1 " " $2+$6 " " $3+$7 " " $4+$8 }}' .both.dat > .tmp_sum
        fi
        awk '{print $1" "$2" "$3" "$4}' extracted_z_traction_and_pressure_`echo $file` | sort -g -k 1 > .junk
        mv .junk extracted_z_traction_pressure_`echo $file`
        oomph-convert -o -p2 -z extracted_z_traction_pressure_`echo $file`
        let step=$step+1
        let count=$count+1
    done
    
    awk -v n=$nsteps_per_period '{print $1 " " $2/n" " $3/n" " $4/n}' .tmp_sum | sort -g -k 1 > cycle_average_`echo $stem`.dat

    makePvd extracted_z_p_$stem extracted_z_pore_pressure_`echo $stem`.pvd
    oomph-convert -o -p2 cycle_average_`echo $stem`.dat

    awk '{print $1 " " $2}' cycle_average_`echo $stem`.dat > cycle_average_radial_traction_`echo $stem`.dat
    awk '{print $1 " " $3}' cycle_average_`echo $stem`.dat > cycle_average_axial_traction_`echo $stem`.dat
    awk '{print $1 " " $4}' cycle_average_`echo $stem`.dat > cycle_average_pressure_`echo $stem`.dat

done

