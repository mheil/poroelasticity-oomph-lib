#! /bin/bash

if [ $# -ne 1 ]; then
 echo "Specify stem of png files (without any number or dots)"
 exit 1
fi

stem=$1

count=0
file_list=`ls $stem*.png`
for file in `echo $file_list`; do
    echo $file
    new_file=relabeled_$stem`echo $count`.png
    ln -s $file $new_file
    let count=$count+1
done

