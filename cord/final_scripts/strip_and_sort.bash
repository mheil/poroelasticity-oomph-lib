#! /bin/bash


# List of file stems to be processed
#-----------------------------------
stem_list="poro_output_on_fsi_syrinx_boundary poro_output_on_fsi_inner_sss_boundary poro_output_on_fsi_outer_sss_boundary"

# New directory for safety so we don't overwrite any data in current directory
#-----------------------------------------------------------------------------
stripped_and_sorted_dir="STRIPPED_AND_SORTED"

# Make sure we don't trample on already existing data (not that it matters here
# but it's good practice)
if [ -e "$stripped_and_sorted_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $stripped_and_sorted_dir and try again"
    echo " "
    exit
fi
mkdir $stripped_and_sorted_dir 


# Loop over all file stems
#-------------------------
for stem in `echo $stem_list`; do

    # Files are enumerated linearly from 0 to 79
    count=0
    while [  $count -lt 80 ]; do
        
        # This is the old file name
        old_file=`echo $stem``echo $count`.dat

        # This is the new filename
        new_file=stripped_and_sorted_`echo $stem``printf "%05d" $count`.dat

        # I don't trust myself so output them first (before doing any more; remnant
        # of writing this script; can now go...)
        echo "FILES: " $old_file " " $new_file

        # Here's where the heavy lifting happens: Awk writes the entire line
        # in old_file unless its first entry is the string "ZONE"; the result
        # of this is piped ("|", i.e. directly forwarded to another command
        # without creating any intermediate files) to the sort command which
        # sorts based on numerical value ("-n") of the second column ("-k 2").
        # Result gets written to the new file (which lives in its own directory.
        awk '{if ($1!="ZONE"){print $0}}' `echo $old_file` | sort -n -k 2 > `echo $stripped_and_sorted_dir"/"$new_file`
        
        # Bump counter
        let count=count+1
        
    done
done

exit
