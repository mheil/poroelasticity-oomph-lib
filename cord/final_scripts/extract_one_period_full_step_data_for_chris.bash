#------------------------------------
# Extract full steps over one period
#------------------------------------

#===================================================
# Do all of them or just radial line (for poro-only)
#===================================================
upper_radial_line_only=0


#============================================
# Stem for files  to be linked
#============================================
link_vtu_file_stem_list=" " #"soln-syrinx-fluid soln-sss-fluid soln-inner-poro soln-outer-poro "
link_dat_file_stem_list=" syrinx_centreline_veloc_line  upper_radial_line_through_syrinx_cover lower_radial_line_through_syrinx_cover poro_fsi_syrinx poro_fsi_inner_sss poro_fsi_outer_sss "

if [ $upper_radial_line_only -eq 1 ]; then
    link_dat_file_stem_list=" upper_radial_line_through_syrinx_cover "
fi


#============================================
# Range
#============================================
first_full_step=720 # 9920 # 720 # 720 # 820 # 240 # 750 # 660 # 550 # 600 #650 # 800 # 400
nsteps_per_period=80
let last_full_step=$first_full_step+$nsteps_per_period



#====================
# Full step directory
#====================
chris_dir="FOR_CHRIS_LINE_DATA_ONLY_FULL_STEPS_STARTING_FROM_STEP$first_full_step"
if [ -e "$chris_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $chris_dir and try again"
    echo " "
    exit
fi
mkdir $chris_dir

#=============================
# Unzip trace and output files
#=============================
if [ -e trace.dat.gz ]; then
    gunzip trace.dat.gz
fi
if [ -e trace_header.dat.gz ]; then
    gunzip trace_header.dat.gz
fi
if [ -e OUTPUT.gz ]; then
    gunzip OUTPUT.gz
fi


#===============================================================
# Check if timestep in trace file is constant
#===============================================================
junk=`awk 'BEGIN{failed=0; dt_prev=-2;t_prev=0}{if (dt_prev==-2){t_prev=$1;dt_prev=-1}else if(dt_prev==-1){dt_prev=$1-t_prev; t_prev=$1}else{dt=$1-t_prev; error=sqrt((dt-dt_prev)*(dt-dt_prev))/sqrt(dt_prev*dt_prev); if (error>0.01){failed=1; print "Error! Variable timestep detected at: t, dt, dt_prev "$1" "dt" "dt_prev}; t_prev=$1;dt_prev=dt}}END{if (failed==1){print "Error! Variable timestep detected!"}}' trace.dat`

if [ "$junk" == "" ]; then
    echo "timestep appears to be constant in trace file; good!"
else
    echo "timestep appears to be variable in trace file!"
    echo $junk
    echo " " 
    echo "ABORTING!" 
    echo " " 
    echo "[You want to relax the error tolerance for very long runs " 
    echo " because of finite precision in the trace file...]" 
    echo " " 
    exit
fi




#============================================
# Only deal with full steps
#============================================
full_step_gz_file_list=`ls full_step*.dat.gz`
for full_step_gz_file in `echo $full_step_gz_file_list`; do
    gunzip $full_step_gz_file
done

full_step_file_list=`ls full_step*.dat`
count=0
full_step_count=0
for full_step_file in `echo $full_step_file_list`; do
    full_step=`cat full_step$count.dat`
    if [ $full_step -ge $first_full_step ]; then
        if [ $full_step -lt $last_full_step  ]; then
            for link_vtu_file_stem in `echo $link_vtu_file_stem_list`; do
                old_vtu_file=`echo $link_vtu_file_stem``printf "%05d" $full_step`".vtu"
                if [ -e $old_vtu_file.gz ]; then
                    gunzip $old_vtu_file.gz
                fi
                new_vtu_file=`echo $link_vtu_file_stem``printf "%05d" $full_step_count`".vtu"
                ln -s `pwd`/$old_vtu_file $chris_dir/$new_vtu_file
            done
            for link_dat_file_stem in `echo $link_dat_file_stem_list`; do
                old_dat_file=`echo $link_dat_file_stem`$full_step".dat"
                if [ -e $old_dat_file.gz ]; then
                    gunzip $old_dat_file.gz
                fi
                new_dat_file=`echo $link_dat_file_stem`$full_step_count".dat"
                ln -s `pwd`/$old_dat_file $chris_dir/$new_dat_file
            done


            let full_step_count=$full_step_count+1
        fi
    fi

    let count=$count+1
done

#=========================================
# Link trace file and header
#=========================================
ln -s `pwd`/OUTPUT $chris_dir/OUTPUT
ln -s `pwd`/trace.dat $chris_dir/trace.dat
ln -s `pwd`/trace_header.dat $chris_dir/trace_header.dat




#=========================================
# What's what
#=========================================   
cd $chris_dir
ls -l > whats_what.txt



#=========================================
# Now move all files
#=========================================   
user_at_server=mheil@130.88.16.11
#leylandii
dir_on_server=/home/ds413/matthias/ForChrisFinalRunsLineDataOnlyFrom_ac5cc_Disk`pwd`
echo " "
echo "Moving files to: " 
echo " " 
echo "    "$dir_on_server
echo " " 

ssh $user_at_server "mkdir -p $dir_on_server"
file_list=`ls *.* OUTPUT`
scp $file_list $user_at_server:$dir_on_server

echo " "
echo "About to compress files. Please wait..."
ssh $user_at_server "gzip $dir_on_server/*.dat"
echo "...done compressing files."
echo " "

echo " "
echo "==============================================================="
echo " "
echo "Re-numbered/linked files are in: "
echo " " 
echo "    "`pwd`
echo " " 
echo "See the final_scripts/README file for what to do next..." 
echo "==============================================================="
echo " "



exit




