#! /bin/bash

#============================================================
# Compute moving time average of net volume flux into syrinx
# and change of syrinx volume from trace file.
#============================================================

#---------------------------------------
# Specify trace file
#---------------------------------------
trace_file="trace.dat"

# Timestep
#hierher dt=`date | awk 'END{print 14.1421/80}'`
dt=`date | awk 'END{print 35.3553/200}'`
echo "dt="$dt

# Initial record and increment for start of average (referring to lines in trace file)
lo_step=0
increment=1 # 20

# Final starting point:
n_record=`wc -l $trace_file | awk '{print $1}'`
#hierher let hi_step=$n_record-81
let hi_step=$n_record-201

echo "hi_step " $hi_step

# "Change in syrinx volume" is in column 11
awk '{if (NR!=1){print $1 " " $11}}' $trace_file > dsyrinx_volume_trace.dat

rm -f dsyrinx_volume_cycle_average.dat
for bla in `seq $lo_step $increment $hi_step`; do 
    echo "start: "$bla
    # hierher awk -v dt=$dt -v start=$bla 'BEGIN{count=0; sum=0}{if (NR>start){if (count<80){sum+=$2; count++}}}END{print start*dt " " sum/count}'  dsyrinx_volume_trace.dat >> dsyrinx_volume_cycle_average.dat
    awk -v dt=$dt -v start=$bla 'BEGIN{count=0; sum=0}{if (NR>start){if (count<200){sum+=$2; count++}}}END{print start*dt " " sum/count}'  dsyrinx_volume_trace.dat >> dsyrinx_volume_cycle_average.dat
done

# "Syrinx volume" and "Change in syrinx volume" are in columns 10 and 11
awk '{if (NR!=1){print $1 " " $11/$10}}' $trace_file > normalised_dsyrinx_volume_trace.dat

rm -f normalised_dsyrinx_volume_cycle_average.dat
for bla in `seq $lo_step $increment $hi_step`; do 
    echo "start: "$bla
    # hierher awk -v dt=$dt -v start=$bla 'BEGIN{count=0; sum=0}{if (NR>start){if (count<80){sum+=$2; count++}}}END{print start*dt " " sum/count}'  normalised_dsyrinx_volume_trace.dat >> normalised_dsyrinx_volume_cycle_average.dat
    awk -v dt=$dt -v start=$bla 'BEGIN{count=0; sum=0}{if (NR>start){if (count<200){sum+=$2; count++}}}END{print start*dt " " sum/count}'  normalised_dsyrinx_volume_trace.dat >> normalised_dsyrinx_volume_cycle_average.dat
done


# Total seepage flux through syrinx fsi surface is in column 25
awk '{if (NR!=1){print $1 " " $25}}' $trace_file > syrinx_seepage_flux_trace.dat

rm -f syrinx_seepage_flux_cycle_average.dat
for bla in `seq $lo_step $increment $hi_step`; do 
    echo "start: "$bla
    # hierher awk -v dt=$dt -v start=$bla 'BEGIN{count=0; sum=0}{if (NR>start){if (count<80){sum+=$2; count++}}}END{print start*dt " " sum/count}'  syrinx_seepage_flux_trace.dat >> syrinx_seepage_flux_cycle_average.dat
    awk -v dt=$dt -v start=$bla 'BEGIN{count=0; sum=0}{if (NR>start){if (count<200){sum+=$2; count++}}}END{print start*dt " " sum/count}'  syrinx_seepage_flux_trace.dat >> syrinx_seepage_flux_cycle_average.dat
done
