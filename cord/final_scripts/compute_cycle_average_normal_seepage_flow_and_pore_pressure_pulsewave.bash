#! /bin/bash


first_step=0
nsteps_per_period=200 # 80


data_dir=`pwd`
echo "Data directory: "$data_dir

# Master run directory; make and move into it
#--------------------------------------------
run_dir=CYCLE_AVERAGE_NORMAL_SEEPAGE_FLOW_FIRST_`echo $first_step`_N_`echo $nsteps_per_period`
if [ -e "$run_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $run_dir and try again"
    echo " "
    exit
fi
mkdir $run_dir
cd $run_dir


stem_list="poro_output_on_fsi_syrinx_boundary poro_output_on_fsi_inner_sss_boundary "
for stem in `echo $stem_list`; do
    
    step=$first_step
    count=0
    while [ $count -lt $nsteps_per_period ]; do
        file=$stem`echo $step`".dat"
        echo $file
        
        #------------------------------------------------------------------------------------------------------------------
        # displ in columns 3 and 4; pressure is in column 7, qr and qz are columns 8 and 9; nr and nz are columns 14 and 15
        #------------------------------------------------------------------------------------------------------------------

        # SEEPAGE FLUX:
        #==============
        awk '{if ($1!="ZONE"){print $1 " " $2 " " ($8*$14+$9*$15)*$14 " " ($8*$14+$9*$15)*$15 }}' $data_dir/$file | sort -g -k 2 > extracted_r_z_qn_r_qn_z_`echo $file`
        if [ $count -eq 0 ]; then
            cp extracted_r_z_qn_r_qn_z_`echo $file` .tmp_qsum
        else
            paste extracted_r_z_qn_r_qn_z_`echo $file` .tmp_qsum > .both.dat
            awk '{{print $1 " " $2 " " $3+$7 " "$4+$8 }}' .both.dat > .tmp_qsum
        fi
        oomph-convert -o -p2 -z extracted_r_z_qn_r_qn_z_`echo $file`

        # DISPLACEMENTS:
        #==============
        awk '{if ($1!="ZONE"){print $1 " " $2 " " $3 " " $4 }}' $data_dir/$file | sort -g -k 2 > extracted_r_z_u_r_u_z_`echo $file`
        if [ $count -eq 0 ]; then
            cp extracted_r_z_u_r_u_z_`echo $file` .tmp_usum
        else
            paste extracted_r_z_u_r_u_z_`echo $file` .tmp_usum > .both.dat
            awk '{{print $1 " " $2 " " $3+$7 " "$4+$8 }}' .both.dat > .tmp_usum
        fi
        oomph-convert -o -p2 -z extracted_r_z_u_r_u_z_`echo $file`

        # hierher
        #let step=$step+1
        #let count=$count+1
        

        # PRESSURE:
        #==========
        awk '{if ($1!="ZONE"){print $1 " " $2 " " (-$7*$14) " " (-$7*$15) }}' $data_dir/$file | sort -g -k 2 > extracted_r_z_pn_r_pn_z_`echo $file`
        if [ $count -eq 0 ]; then
            cp extracted_r_z_pn_r_pn_z_`echo $file` .tmp_psum
        else
            paste extracted_r_z_pn_r_pn_z_`echo $file` .tmp_psum > .both.dat
            awk '{{print $1 " " $2 " " $3+$7 " "$4+$8 }}' .both.dat > .tmp_psum
        fi
        oomph-convert -o -p2 -z extracted_r_z_pn_r_pn_z_`echo $file`
        let step=$step+1
        let count=$count+1
        

    done


        
    #awk 'BEGIN{count=0}{if (count==0){prev_q=sqrt($3*$3+$4*$4); prev_r=$1; prev_z=$2}else{if ($2!=prev_z){q=sqrt($3*$3+$4*$4); print 0.5*($2+prev_z) " " $3/sqrt($3*$3)*0.5*(q+prev_q)*sqrt(1+(($1-prev_r)/($2-prev_z))^2)*0.5*($1+prev_r)}}; count++; prev_r=$1; prev_z=$2; prev_q=q}' cycle_average_`echo $stem`.dat > cycle_average_normal_seepage_flow_times_jacobian_`echo $stem`.dat

    awk -v n=$nsteps_per_period '{print $1 " " $2 " " $3/n " " $4/n}' .tmp_qsum | sort -g -k 2 > cycle_average_normal_seepage_flow_`echo $stem`.dat
    makePvd extracted_r_z_qn_r_qn_z_`echo $stem` extracted_r_z_qn_r_qn_z_`echo $stem`.pvd
    oomph-convert -o -p2 cycle_average_normal_seepage_flow_`echo $stem`.dat
    
    awk -v n=$nsteps_per_period '{print $1 " " $2 " " $3/n " " $4/n}' .tmp_psum | sort -g -k 2 > cycle_average_pressure_`echo $stem`.dat
    makePvd extracted_r_z_pn_r_pn_z_`echo $stem` extracted_r_z_pn_r_pn_z_`echo $stem`.pvd
    oomph-convert -o -p2 cycle_average_pressure_`echo $stem`.dat

    awk -v n=$nsteps_per_period '{print $1 " " $2 " " $3/n " " $4/n}' .tmp_usum | sort -g -k 2 > cycle_average_displ_`echo $stem`.dat
    makePvd extracted_r_z_u_r_u_z_`echo $stem` extracted_r_z_u_r_u_z_`echo $stem`.pvd
    oomph-convert -o -p2 cycle_average_displ_`echo $stem`.dat
    
done


