#! /bin/bash


########################################

mkdir k13_with_and_without_cover_side_by_side
file_list=`ls k13_post_processed_for_comparison_with__with_and_without_cover/*.png`
for file in `echo $file_list`; do
    actual_file=`basename $file`
    files=`ls k13_post_processed_for_comparison_with__with_and_without_cover/$actual_file k13_cover_only*/$actual_file`
    stemm=`basename $file`

    #Append sideways
    combined_file=`echo "k13_with_and_without_cover_side_by_side/next_to_each_other_"$stemm`
    echo "FILES: "$files "   TO  "$combined_file
    convert +append `echo $files` `echo $combined_file`
    
done

exit
########################################


new_dir="."
#file_list=`ls $old_dir/*.png`
file_list=`ls seepage_flux_at_fsi_surfaces?.png seepage_flux_at_fsi_surfaces??.png`
for file in `echo $file_list`; do
    new_file=`echo $new_dir`/cropped_`basename $file`
    echo $file " " $new_file
    convert -crop 1569x1600+211+64 $file $new_file
done
convert  -crop 1569x1600+211+64 cycle_average_seepage_flux_at_fsi_surfaces.png cropped_cycle_average_seepage_flux_at_fsi_surfaces.png
convert  -crop 1569x1600+211+64 cycle_average_seepage_flux_at_fsi_surfaces_rescaled.png cropped_cycle_average_seepage_flux_at_fsi_surfaces_rescaled.png

exit



########################################

mkdir side_by_side
file_list=`ls k12_post_processed/*.png`
for file in `echo $file_list`; do
    actual_file=`basename $file`
    files=`ls k*post_processed/$actual_file`
    stemm=`basename $file`

    #Append sideways
    combined_file=`echo "side_by_side/next_to_each_other_"$stemm`
    convert +append `echo $files` `echo $combined_file`
    
done

exit

########################################


ln -s ../NEW_RUN_all_porous_k1em12/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio10.0_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous/FULL_STEPS_STARTING_FROM_STEP750/CYCLE_AVERAGE_NORMAL_SEEPAGE_FLOW_FIRST_0_N_80/ k12_flux_raw_data

ln -s ../NEW_RUN_all_porous_k1em13/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio1.0_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous/FULL_STEPS_STARTING_FROM_STEP750/CYCLE_AVERAGE_NORMAL_SEEPAGE_FLOW_FIRST_0_N_80/ k13_flux_raw_data

ln -s ../NEW_RUN_all_porous_k1em14/RESLT_dt0.005_el_syrinx_cover0.0001_el_area_fluid0.01_el_area_solid0.01_alpha1.0_drained_nu0.35_k_ratio0.1_non_stress_div_form_implicit_fluid_mesh_update_all_inner_solid_porous/FULL_STEPS_STARTING_FROM_STEP750/CYCLE_AVERAGE_NORMAL_SEEPAGE_FLOW_FIRST_0_N_80/ k14_flux_raw_data



dir_stem_list="k12 k13 k14"
for dir_stem in `echo $dir_stem_list`; do
    new_dir=`echo $dir_stem`_post_processed
    mkdir $new_dir
    old_dir=`echo $dir_stem`_flux_raw_data
    file_list=`ls $old_dir/*.png`
    for file in `echo $file_list`; do
        new_file=`echo $new_dir`/cropped_`basename $file`
        #echo $file " " $new_file
        convert -crop 1569x1600+211+64 $file $new_file
    done
done

exit
