// Generic oomph-lib sources
#include "generic.h"

using namespace std;

using namespace oomph;


/// Some function that we're going to sample and fit to. 
double function(const double& x)
{ 
 return sin(2.0*x+3.0);
}




//============================================================
/// Spline class
//============================================================
class Spline
{
 
public:
 
 /// \short Constructor: Pass x and y coordinates of function to be
 /// fitted (natural spline -- extension to prescribed slope is easy...)
 Spline(const Vector<double> &x_sample, const Vector<double> &y_sample) :
  X_sample(x_sample), Y_sample(y_sample)
  {

   int i=0;
   int k=0;
   double p=0.0;
   double qn=0.0;
   double sig=0.0;
   double un=0.0;;
 
   int n=x_sample.size();
   Spline_coeff.resize(n);
   Vector<double> u(n-1);
 
   // Natural spline
   // if (yp1 > 0.99e30)
   Spline_coeff[0]=u[0]=0.0;
   // else {
   //Spline_coeff[0] = -0.5;
   //u[0]=(3.0/(X_sample[1]-X_sample[0]))*
   //((Y_sample[1]-Y_sample[0])/(X_sample[1]-X_sample[0])-yp1);
   //}
   for (i=1;i<n-1;i++) 
    {
     sig=(X_sample[i]-X_sample[i-1])/(X_sample[i+1]-X_sample[i-1]);
     p=sig*Spline_coeff[i-1]+2.0;
     Spline_coeff[i]=(sig-1.0)/p;
     u[i]=(Y_sample[i+1]-Y_sample[i])/(X_sample[i+1]-X_sample[i]) - 
      (Y_sample[i]-Y_sample[i-1])/(X_sample[i]-X_sample[i-1]);
     u[i]=(6.0*u[i]/(X_sample[i+1]-X_sample[i-1])-sig*u[i-1])/p;
    }
   // Natural spline
   //if (ypn > 0.99e30)
   qn=un=0.0;
   //else {
   //qn=0.5;
   //un=(3.0/(X_sample[n-1]-X_sample[n-2]))*
   //(ypn-(Y_sample[n-1]-Y_sample[n-2])/(X_sample[n-1]-X_sample[n-2]));
   // }

   Spline_coeff[n-1]=(un-qn*u[n-2])/(qn*Spline_coeff[n-2]+1.0);
   for (k=n-2;k>=0;k--)
    {
     Spline_coeff[k]=Spline_coeff[k]*Spline_coeff[k+1]+u[k];
    }
  }


/// Spline evalution
 double spline(const double& x)
  {
   double y=0.0;
   int k=0;
   double h=0.0;
   double b=0.0;
   double a=0.0;
   
   int n=X_sample.size();
   int klo=0;
   int khi=n-1;
   while (khi-klo > 1)
    {
     k=(khi+klo) >> 1;
     if (X_sample[k] > x) 
      {
       khi=k;
      }
     else
      {
       klo=k;
      }
    }
   h=X_sample[khi]-X_sample[klo];
   if (h == 0.0)
    {
     oomph_info << "Bad X_sample input to routine splint\n";
     abort();
    }
   a=(X_sample[khi]-x)/h;
   b=(x-X_sample[klo])/h;
   y=a*Y_sample[klo]+b*Y_sample[khi]+((a*a*a-a)*Spline_coeff[klo]
                                      +(b*b*b-b)*Spline_coeff[khi])*(h*h)/6.0;
   
   return y;
  }
 
 
private:
 
 /// x samples
 Vector<double> X_sample;
 
 /// y samples
 Vector<double> Y_sample;
 
 /// Spline coefficients
 Vector<double> Spline_coeff;
 
};


//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////



//====================================================================
/// Demo driver code for spline fitter
//====================================================================
int main(int argc, char **argv)
{


 // // Number of fitting points
 // unsigned n_fit=fitting_data.size();

 // // Sample
 // unsigned n=100;
 // double x_min=0.0;
 // double x_max=10.0;
 // Vector<double> x_sample(n);
 // Vector<double> y_sample(n);
 // for (unsigned i=0;i<n;i++)
 //  {
 //   x_sample[i]=x_min+(x_max-x_min)*double(i)/double(n-1);
 //   y_sample[i]=function(x_sample[i]);
 //  }
 
 // Store command line arguments
 CommandLineArgs::setup(argc,argv);
 
 // Define possible command line arguments and parse the ones that
 // were actually specified
 
 std::string sample_file_name;
 CommandLineArgs::specify_command_line_flag("--sample_file",
                                            &sample_file_name);
 
 std::string fit_test_file_name;
 CommandLineArgs::specify_command_line_flag("--fit_test_file",
                                            &fit_test_file_name);
 
 // Parse command line
 CommandLineArgs::parse_and_assign(); 
 
 // Doc what has actually been specified on the command line
 CommandLineArgs::doc_specified_flags();

 // Fitting range:
 double x_min=DBL_MAX;
 double x_max=-DBL_MAX;
 Vector<double> x_sample;
 Vector<double> y_sample;

 ifstream sample_file;
 sample_file.open(sample_file_name.c_str());
 double x=0.0;
 double y=0.0;
 std::string line;
 while (std::getline(trace_file, line))
  {
   if (line.empty()) continue;     // skips empty lines
   std::istringstream is(line);    // construct temporary istringstream
   is >> x >> y;
   x_sample.push_back(x);
   y_sample.push_back(y);
   if (x>x_max) x_max=x;
   if (x<x_min) x_min=x;
  }

  
 // Build spline
 Spline spline(x_sample,y_sample);

 // Evaluate
 unsigned n=x_sample.size();
 double y_spline=0.0;
 for (unsigned i=0;i<10*n;i++)
  {
   double x=x_min+(x_max-x_min)*double(i)/double(10*n-1);
   if (CommandLineArgs::command_line_flag_has_been_set("--fit_test_file"))
    {
     outfile << x << " " << spline.spline(x) << std::endl;
    }
   else
    {
     oomph_info << x << " " << spline.spline(x) << std::endl;
    }
  }
 if (CommandLineArgs::command_line_flag_has_been_set("--fit_test_file"))
  {
   outfile.close();
  }
       
}




