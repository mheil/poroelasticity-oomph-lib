//Header file for num rec inspired code

#ifndef OOMPH_NR_HEADER
#define OOMPH_NR_HEADER

									       
// Config header generated by autoconfig
#ifdef HAVE_CONFIG_H
 #include <oomph-lib-config.h>
#endif


//OOMPH-LIB headers
#include "../../src/generic/nodes.h"
#include "../../src/generic/Qelements.h"
#include "../../src/generic/oomph_utilities.h"



namespace oomph
{


//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////



//=====================================================================
/// Base class for functions whose parameters can be fitted
/// by Levenberg Marquardt technique.
//=====================================================================
class LevenbergMarquardtFittingFunctionObject
{
 
public:


 /// Constructor: Specify number of fitting parameters
 LevenbergMarquardtFittingFunctionObject(const unsigned& n_param)
  {
   Parameter.resize(n_param);
   for (unsigned i=0;i<n_param;i++)
    {
     Parameter[i]=0.0;
    }
  }

 /// Empty destructor
 virtual ~LevenbergMarquardtFittingFunctionObject() {}

 /// \short Evaluate the fitting function for the current set
 /// of parameters: Pure virtual, must be implemented.
 virtual double fitting_function(const double& x)=0;

 /// \short Evaluate the fitting function and its derivatives
 /// w.r.t. fitting parameters (done by FD by default; can be
 /// overloaded)
 virtual double fitting_function(const double& x,
                                 Vector<double>& dfit_dparam);
                   
 /// \short Number of parameters in fitting function. Pure virtual, must be
 /// implemented.
 virtual unsigned nparameter()=0;

 /// Access to i-th fitting parameter
 double& parameter(const unsigned& i)
  {
   return Parameter[i];
  }

 /// Access to vector of fitting parameters
 Vector<double>& parameter()
  {
   return Parameter;
  }


protected:

 /// Vector of fitting parameters
 Vector<double> Parameter;

};





//=====================================================================
/// Damped oscillatory function whose parameters can be
/// fitted with Levenberg Marquardt.
//=====================================================================
class DampedOscillatoryFittingFunctionObject : 
 virtual public LevenbergMarquardtFittingFunctionObject
{
 
public:
 
 /// Constructor: Number of fitting parameters is five. 
 DampedOscillatoryFittingFunctionObject() :  
  LevenbergMarquardtFittingFunctionObject(5)
  {}


 /// \short Evaluate the fitting function for the current set
 /// of parameters
 double fitting_function(const double& x)
  {
   return Parameter[0]+
    exp(Parameter[1]*x)*Parameter[2]*sin(Parameter[3]*x+Parameter[4]);
  }
                                 
 /// \short Overload all interfaces of the fitting function, call the default
 /// finite difference version
 double fitting_function(const double &x,
                         Vector<double> &dfit_dparam)
  {
   return 
    LevenbergMarquardtFittingFunctionObject::fitting_function(x,dfit_dparam);
  }

 /// Number of parameters in fitting function
 virtual unsigned nparameter()
  {
   return 5;
  }


};


//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////


//=====================================================================
/// Class that allows fitting of free parameters in function
/// (represented by a LevenbergMarquardtFittingFunctionObject)
/// to given (x,y) data.
//=====================================================================
class LevenbergMarquardtFitter
{
 
public:

 /// Empty constructor 
 LevenbergMarquardtFitter(): Fitting_function_object_pt(0)
  {}
 
 /// \short Access to pointer to LevenbergMarquardtFittingFunctionObject
 /// whose parameters we want to determine by fit to data.
 LevenbergMarquardtFittingFunctionObject*& fitting_function_object_pt()
  {
   return Fitting_function_object_pt;
  }

 /// \short Fit the parameters to the pairs of (x,y) data specified, 
 /// using max_iter Levenberg Marquardt iterations
 void fit_it(const Vector<std::pair<double,double> >& fitting_data,
             const unsigned& max_iter,
             const bool& quiet=true);

private:

 /// Pointer to LevenbergMarquardtFittingFunctionObject
 LevenbergMarquardtFittingFunctionObject* Fitting_function_object_pt;


 /// Private helper function -- don't look into it...
 void mrqcof(Vector<double>& x, 
             Vector<double>& y, 
             Vector<double>& sig, 
             Vector<double>& a,
             std::vector<bool>& ia, 
             DenseDoubleMatrix& alpha, 
             Vector<double>& beta, 
             double& chisq);

 
};




//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////



//=========================================================================
/// Namespaces for (Numerical-Recipes-based) functions for
/// eigensolver, based on Jacobi rotations.
//=========================================================================
namespace JacobiEigenSolver
{

 /// Perform one Jacobi rotation on matrix a
 extern inline void rot(DenseDoubleMatrix&a, const double s, const double tau, 
                 const unsigned long i, const unsigned long j, 
                        const unsigned long k, const unsigned long l);

/// \short Use Jacobi rotations to determine eigenvalues and eigenvectors of 
/// matrix a. d[i]=i-th eigenvalue; v(i,j)=i-th component of j-th eigenvector
/// (note that this is the transpose of what we'd like to have...);
/// nrot=number of rotations used. 
 extern void jacobi(DenseDoubleMatrix& a, Vector<double>& d, 
             DenseDoubleMatrix& v, unsigned long& nrot);

}

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////



}


#endif
