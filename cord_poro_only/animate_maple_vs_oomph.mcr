#!MC 1410



$!VARSET |do_pressure|=0

$!VARSET |first_oomph|=0
$!VARSET |n_oomph|=320
$!VARSET |n_minus_one_oomph|=(|n_oomph|-1)
$!READDATASET  '"upper_radial_line_through_syrinx_cover|first_oomph|.dat" '
  READDATAOPTION = NEW
  RESETSTYLE = YES
  VARLOADMODE = BYNAME
  ASSIGNSTRANDIDS = YES

$!LOOP |n_minus_one_oomph|
$!VARSET |number|=(|first_oomph|+|LOOP|)

$!READDATASET  '"upper_radial_line_through_syrinx_cover|number|.dat" '
  READDATAOPTION = APPEND
  RESETSTYLE = NO
  VARLOADMODE = BYNAME
  ASSIGNSTRANDIDS = YES

$!ENDLOOP

$!IF |do_pressure| == 1
$!READDATASET  '"/home/mheil/version730/poroelasticity-oomph-lib/cord_poro_only/p_from_maple.dat" '
  READDATAOPTION = APPEND
  RESETSTYLE = NO
  VARLOADMODE = BYNAME
  ASSIGNSTRANDIDS = YES
$!ELSE
$!READDATASET  '"/home/mheil/version730/poroelasticity-oomph-lib/cord_poro_only/q_from_maple.dat" '
  READDATAOPTION = APPEND
  RESETSTYLE = NO
  VARLOADMODE = BYNAME
  ASSIGNSTRANDIDS = YES
$!ENDIF


$!XYLINEAXIS GRIDAREA{DRAWBORDER = YES}
$!XYLINEAXIS XDETAIL 1 {TITLE{TITLEMODE = USETEXT}}
$!XYLINEAXIS XDETAIL 1 {TITLE{TEXT = 'radial coordinate'}}
$!XYLINEAXIS YDETAIL 1 {TITLE{TITLEMODE = USETEXT}}
$!IF |do_pressure| == 1
$!XYLINEAXIS YDETAIL 1 {TITLE{TEXT = 'pore pressure'}}
$!ELSE
$!XYLINEAXIS YDETAIL 1 {TITLE{TEXT = 'radial seepage velocity'}}
$!ENDIF


$!ACTIVELINEMAPS -= [1-9]
$!DELETELINEMAPS  [1-9]

$!LOOP |NUMZONES|
$!CREATELINEMAP 
$!LINEMAP [|LOOP|]  NAME = '&ZN&'
$!ACTIVELINEMAPS += [|LOOP|]
$!LINEMAP [|LOOP|]  ASSIGN{ZONE = |LOOP|}
$!ENDLOOP

$!VIEW FIT
$!LINEMAP [1-|n_oomph|]  NAME = 'oomph'
$!IF |do_pressure| == 1
$!LINEMAP [1-|n_oomph|]  ASSIGN{YAXISVAR = 8}
$!ELSE
$!LINEMAP [1-|n_oomph|]  ASSIGN{YAXISVAR = 5}
$!ENDIF
$!LINEMAP [1-|n_oomph|]  LINES{COLOR = RED}
$!VIEW FIT
$!VARSET |n_plus_one_oomph|=(|n_oomph|+1)
$!LINEMAP [|n_plus_one_oomph|]  LINES{SHOW = NO}
$!LINEMAP [|n_plus_one_oomph|]  LINES{SHOW = YES}
$!LINEMAP [|n_plus_one_oomph|-|NUMZONES|]  LINES{COLOR = BLACK}
$!LINEMAP [|n_plus_one_oomph|-|NUMZONES|]  LINES{LINEPATTERN = DASHED}
$!LINEMAP [|n_plus_one_oomph|-|NUMZONES|]  LINES{LINETHICKNESS = 0.400000000000000022}
$!VIEW FIT


$!VARSET |nstep|=|n_oomph|
$!LOOP |nstep|
$!ACTIVELINEMAPS -= [1-|NUMZONES|]
$!ACTIVELINEMAPS += [|LOOP|]
$!VARSET |maple|=(|n_oomph|+|LOOP|)
$!IF  |maple| > |NUMZONES|  
  $!VARSET |maple|=(|maple|-|n_oomph|)
$!ENDIF
$!ACTIVELINEMAPS += [|maple|]
$!REDRAWALL 
#$!DELAY 0.01
#$!PAUSE "BLA"
$!ENDLOOP


$!ACTIVELINEMAPS -= [1-|NUMZONES|]
$!VARSET |incr|=20
$!VARSET |offset|=10
$!VARSET |nstep|=4
$!LOOP |nstep|
$!VARSET |maple|=(|n_oomph|+|offset|+(|LOOP|-1)*|incr|)
$!ACTIVELINEMAPS += [|maple|]
$!VARSET |oomph|=(|n_oomph|-80+|offset|+(|LOOP|-1)*|incr|)
$!ACTIVELINEMAPS += [|oomph|]
$!ENDLOOP

$!REDRAWALL 

