
master_dir_list="NEW_RUNS_short_parallel_pia_is_cord NEW_RUNS_short_parallel"
master_dir_list="NEW_RUNS_short_parallel_different_ptm"
master_dir_list="NEW_RUNS_short_parallel_full"
master_dir_list="NEW_RUNS_short_parallel_ptm"
master_dir_list="NEW_RUNS_short_parallel_no_inertia"

dir_list=`find $master_dir_list -name "RESLT*" `

stem_list="radial_line_through_syrinx_cover_veloc_line"

if [ 1 == 1 ]; then
    current_dir=`pwd`
    for dir in `echo $dir_list`; do
        cp radial_line.mcr $dir
        cd $dir
        
        for stem in `echo $stem_list`; do
            file_list=`ls $stem*.dat`
            count=0
            for file in `echo $file_list`; do
                old_file=`echo $stem``echo $count`.dat
                new_file=stripped_`echo $stem``printf "%05d" $count`.dat
                echo $old_file " " $new_file
                awk '{if ($1!="ZONE"){print $0}}' `echo $old_file` | sort -n -k 2 > `echo $new_file`
                let count=$count+1
            done
        done
        cd $current_dir
    done
fi



current_dir=`pwd`
for master_dir in `echo $master_dir_list`; do
    cp compare_radial_line160.mcr $master_dir
    cd $master_dir
    echo "I'm in "`pwd`


    echo "\documentclass{beamer}" > compare.tex 
    echo "\usepackage{beamerthemesplit}" >> compare.tex 
    echo "\usepackage{ifthen}" >> compare.tex 
    echo "\usepackage{multimedia}" >> compare.tex 
    echo "\usepackage{xmpmulti}" >> compare.tex 
    echo "\usepackage{ulem}" >> compare.tex 
    echo "\usepackage{graphicx}" >> compare.tex 
    echo "\usepackage{amsbsy}" >> compare.tex 
    echo "\usepackage{amssymb}" >> compare.tex 
    echo "\usepackage{rotating}" >> compare.tex 
    echo "\usepackage{color}" >> compare.tex 
    echo "\usepackage{animate}" >> compare.tex 
    echo "\providecommand\thispdfpagelabel[1]{}" >> compare.tex 
    echo "\begin{document}" >> compare.tex 

    alpha1_dir_list=`ls -d RE*alpha1*`
    for alpha1_dir in `echo $alpha1_dir_list`; do
        echo "Doing "$alpha1_dir
        rm -f dir1
        ln -s $alpha1_dir dir1
        alpha2_dir=`echo $alpha1_dir | sed 's/_alpha1.0_drained_nu0.35_/_alpha0.0_drained_nu0.49_/g' `
        echo "..... "`ls -d $alpha2_dir`

        if [ 1 == 1 ]; then
            rm -f dir2
            ln -s $alpha2_dir dir2
            rm -rf compare_`echo $alpha1_dir` 
            mkdir compare_`echo $alpha1_dir` 
            rm -f compare
            ln -s compare_`echo $alpha1_dir` compare
            /home/mheil/local/tecplot/bin/tec360 -h /home/mheil/local/tecplot -b compare_radial_line160.mcr
        fi

        escaped_dir=`echo "compare_$alpha1_dir" | sed 's/_/\\\_/g'`
        echo "ESCAPED DIR $escaped_dir"
        echo "\frame[plain]{" >> compare.tex 
        echo "\begin{overlayarea}{\textwidth}{\textheight}" >> compare.tex 
        echo "\vspace{-0.3cm}" >> compare.tex 
        echo "\begin{minipage}[h!]{0.8\textwidth}" >> compare.tex
        echo "\begin{center} " >> compare.tex
        echo "\scalebox{0.7}{\tiny $escaped_dir } " >> compare.tex 
        echo "\end{center}" >> compare.tex 
        echo "\animategraphics[autoplay,loop,width=\textwidth,controls,every=1]{100}{compare_"`echo $alpha1_dir`"/compare}{1}{160}" >> compare.tex
        echo "\end{minipage}" >> compare.tex
        echo "\end{overlayarea}" >> compare.tex  
        echo "}" >> compare.tex

    done
done

echo "\end{document}" >> compare.tex 
pdflatex compare.tex
exit

rm -f dir1
rm -f dir2
rm -f compare



exit

png_file_stem="export"
ratio_list="ratio0.1 ratio1.0 ratio10.0"
for ratio in `echo $ratio_list`; do
   dir1=`ls -d NEW_RUNS_short_parallel*/*$ratio* | awk -v row=1 '{if (NR==row){ print $1}}'`
   dir2=`ls -d NEW_RUNS_short_parallel*/*$ratio* | awk -v row=2 '{if (NR==row){ print $1}}'`
   dir3=`ls -d NEW_RUNS_short_parallel*/*$ratio* | awk -v row=3 '{if (NR==row){ print $1}}'`
   dir4=`ls -d NEW_RUNS_short_parallel*/*$ratio* | awk -v row=4 '{if (NR==row){ print $1}}'`
   mkdir merged_ratio$ratio
   current_dir=`pwd`
   cd $dir1
   file_list=`ls $png_file_stem*.png`
   cd $current_dir
   for file in `echo $file_list`; do
       montage -mode concatenate -tile 2x2 $dir1/$file $dir2/$file $dir3/$file $dir4/$file merged_ratio$ratio/$file
   done
done





exit
