#! /bin/bash


# mpi or not?
run_command="mpirun -np 2  "
run_command=" "

# Important files to be moved
important_files_list="poro_elastic_annulus poro_elastic_annulus.cc $0"

# Master run directory; make and move into it
#--------------------------------------------
run_dir="/media/mheil/9e93acef-2d9b-499c-ae86-480fff25e3b1/scratch/FINAL_RUNS/NEW_RUNS_poro_elastic_extra_fine_k1em14_no_inertia_only_line_output"

if [ -e "$run_dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $run_dir and try again"
    echo " "
    exit
fi
mkdir $run_dir
cp $important_files_list "$run_dir"
cd $run_dir


#Pin everything apart from syrinx cover?
#---------------------------------------
pin_everything_bla=1

pin_everything_dir_label=""
pin_everything_flag=" "
if [ $pin_everything_bla == 1 ]; then
    pin_everything_dir_label="_everything_apart_from_syrinx_cover_pinned"
    pin_everything_flag=" --pin_everything_outside_syrinx_cover "
fi

#Element area for solid
#----------------------
el_area_solid=0.0000001 # hierher 0.00001 # 0.01

# Biot parameter alpha
#---------------------
alpha=1.0

# Drained Poisson's ratio
#------------------------
drained_nu=0.35

# Do proper poro-elasticity or darcy only
#----------------------------------------
proper_poro_elasticity_flag_list="1" # "0 1" # hierher
for proper_poro_elasticity_flag in `echo $proper_poro_elasticity_flag_list`; do

if [ $proper_poro_elasticity_flag == 0 ]; then
    alpha=0.0
    drained_nu=0.49
else
    if [ $proper_poro_elasticity_flag == 1 ]; then
        alpha=1.0
        drained_nu=0.35
    else
        echo "Wrong proper_poro_elasticity_flag"
        exit
    fi
fi 


echo "hello1"

#Pia is cord?
#------------
pia_is_cord_command_line_flag=" "
pia_is_cord_dir_flag=""

pia_is_cord_flag_list="0 1" # 1" # "0 1"
for pia_is_cord_flag in `echo $pia_is_cord_flag_list`; do
if [ $pia_is_cord_flag == 1 ]; then
    pia_is_cord_command_line_flag=" --pia_properties_are_cord_properties "
    pia_is_cord_dir_flag="_pia_is_cord"
fi

echo "hello2"

# Timestep in seconds
#--------------------
dt_in_seconds_list="0.005" # 0.001 0.0005"
for dt_in_seconds in `echo $dt_in_seconds_list`; do


echo "hello3"

# Application of transmural pressure
#-----------------------------------
#ptm_fraction_list="0.0" # " 1.0" # "0.0 0.5 1.0"  # hierher
#for ptm_fraction in `echo $ptm_fraction_list`; do
#echo "hello4"

# Permeability ratio
#-------------------
perm_ratio_list="0.1" # 0.001 0.01 0.1 1.0 10.0" # "0.01 0.001 0.0001" #""1.0" # "0.01" # 0.1 1.0 10.0" # 1.0 10.0" # "1.0" # 1.0" # "0.1 1.0 10.0"
for perm_ratio in `echo $perm_ratio_list`; do

echo "hello5"


# Element area in syrinx cover
#-----------------------------
el_area_syrinx_cover_list="0.0000001" # "0.000000001" # "0.00000001" # "0.000001" # 0.0001 0.00001" # hierher "0.00001" 
for el_area_syrinx_cover in `echo $el_area_syrinx_cover_list`; do

echo "hello6"


# Restart file
#-------------
restart_file=""
#restart_file="--restart_file RESLT_el_syrinx_cover0.01_alpha1.0_dt0.001_restarted/restart313.dat"


# Create result directory and copy stuff across
#----------------------------------------------
dir=RESLT_dt`echo $dt_in_seconds`_el_syrinx_cover`echo $el_area_syrinx_cover`_el_area_solid`echo $el_area_solid`_alpha`echo $alpha`_drained_nu`echo $drained_nu`_k_ratio`echo $perm_ratio``echo $pin_everything_dir_label``echo $pia_is_cord_dir_flag`

#_ptm_fraction_`echo $ptm_fraction`

if [ -e "$dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $dir and try again"
    echo " "
    exit
fi
mkdir $dir
cp $important_files_list "$dir"

# Prepare restart flag
#---------------------
restart_flag=" "
if [ "$restart_file" != "" ]; then
    restart_flag=" --restart_file $restart_file "
fi

# Switch off darcy altogether?
#-----------------------------
no_darcy_flag=" "
if [ $perm_ratio == 0.0 ]; then
    no_darcy_flag=" --pin_darcy "
fi


# Assemble command line flag
#---------------------------
command_line_flag=" --dt_in_seconds $dt_in_seconds "
command_line_flag=`echo $command_line_flag --el_area_poro $el_area_solid `
command_line_flag=`echo $command_line_flag --el_area_syrinx_cover $el_area_syrinx_cover `
command_line_flag=`echo $command_line_flag --alpha $alpha `
command_line_flag=`echo $command_line_flag --drained_nu $drained_nu `
command_line_flag=`echo $command_line_flag --permeability_ratio $perm_ratio `
command_line_flag=`echo $command_line_flag $no_darcy_flag `
command_line_flag=`echo $command_line_flag --re 14140 `
command_line_flag=`echo $command_line_flag --z_shrink_factor 100 ` 
command_line_flag=`echo $command_line_flag --nstep 10000 ` #hierher 
command_line_flag=`echo $command_line_flag --suppress_all_output_apart_from_trace_and_restart ` # hierher
command_line_flag=`echo $command_line_flag --suppress_restart_files ` 
command_line_flag=`echo $command_line_flag $restart_flag `
command_line_flag=`echo $command_line_flag --dir $dir `
command_line_flag=`echo $command_line_flag --suppress_regularly_spaced_output `
command_line_flag=`echo $command_line_flag $pin_everything_flag `
command_line_flag=`echo $command_line_flag --parallel_syrinx_cover `
command_line_flag=`echo $command_line_flag --radially_sliding_syrinx_cover ` 
command_line_flag=`echo $command_line_flag --only_outside_uniform_pressure ` 
#command_line_flag=`echo $command_line_flag --outside_fraction_of_transmural_pressure $ptm_fraction `
command_line_flag=`echo $command_line_flag $pia_is_cord_command_line_flag `
command_line_flag=`echo $command_line_flag --no_inertia ` # hierher
echo $command_line_flag > `echo $dir`/command_line_flag.txt

# Run it
#-------


# hierher add "&" to run simultaneously
$run_command ./poro_elastic_annulus `echo $command_line_flag` > $dir/OUTPUT &


done
done
done
done
done
#done

exit


##################################################################################

