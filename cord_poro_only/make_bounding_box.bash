#! /bin/bash

stem="soln-inner-poro-syrinx-cover"
rm `echo $stem`_bounding_box.dat

# Column that contains the z coordinate
z_col=5

x_max=-1000000000
y_max=-1000000000
z_max=-1000000000 
x_min=1000000000
y_min=1000000000
z_min=1000000000

file_list=`ls $stem*.dat`
for file in `echo $file_list`; do
    x_max_file=`awk                 'BEGIN{max=-10000000000}{if (($1!="ZONE")&&(NF!=3)){if ($1>max){max=$1}}}END{print max}' $file`
    y_max_file=`awk                 'BEGIN{max=-10000000000}{if (($1!="ZONE")&&(NF!=3)){if ($2>max){max=$2}}}END{print max}' $file`
    z_max_file=`awk -v z_col=$z_col 'BEGIN{max=-10000000000}{if (($1!="ZONE")&&(NF!=3)){if ($z_col>max){max=$z_col}}}END{print max}' $file`
    echo "$file xmax: $x_max_file"
    echo "$file ymax: $y_max_file"
    echo "$file zmax: $z_max_file"
    x_max=`date | awk -v value1=$x_max -v value2=$x_max_file '{if (value1>value2){print value1}else{print value2}}'`
    y_max=`date | awk -v value1=$y_max -v value2=$y_max_file '{if (value1>value2){print value1}else{print value2}}'`
    z_max=`date | awk -v value1=$z_max -v value2=$z_max_file '{if (value1>value2){print value1}else{print value2}}'`
    echo "NEW X MAX:   $x_max"
    echo "NEW Y MAX:   $y_max"
    echo "NEW Z MAX:   $z_max"

    x_min_file=`awk                 'BEGIN{min=10000000000}{if (($1!="ZONE")&&(NF!=3)){if ($1<min){min=$1}}}END{print min}' $file`
    y_min_file=`awk                 'BEGIN{min=10000000000}{if (($1!="ZONE")&&(NF!=3)){if ($2<min){min=$2}}}END{print min}' $file`
    z_min_file=`awk -v z_col=$z_col 'BEGIN{min=10000000000}{if (($1!="ZONE")&&(NF!=3)){if ($z_col<min){min=$z_col}}}END{print min}' $file`
    echo "$file xmin: $x_min_file"
    echo "$file ymin: $y_min_file"
    echo "$file zmin: $z_min_file"
    x_min=`date | awk -v value1=$x_min -v value2=$x_min_file '{if (value1<value2){print value1}else{print value2}}'`
    y_min=`date | awk -v value1=$y_min -v value2=$y_min_file '{if (value1<value2){print value1}else{print value2}}'`
    z_min=`date | awk -v value1=$z_min -v value2=$z_min_file '{if (value1<value2){print value1}else{print value2}}'`
    echo "NEW X MIN:   $x_min"
    echo "NEW Y MIN:   $y_min"
    echo "NEW Z MIN:   $z_min"
done

echo "$x_min $y_min 0.0 $z_min" >  `echo $stem`_bounding_box.dat
echo "$x_max $y_min 0.0 $z_min" >> `echo $stem`_bounding_box.dat
echo "$x_max $y_min 0.0 $z_max" >> `echo $stem`_bounding_box.dat
echo "$x_min $y_min 0.0 $z_max" >> `echo $stem`_bounding_box.dat
echo "$x_min $y_max 0.0 $z_min" >> `echo $stem`_bounding_box.dat
echo "$x_max $y_max 0.0 $z_min" >> `echo $stem`_bounding_box.dat
echo "$x_max $y_max 0.0 $z_max" >> `echo $stem`_bounding_box.dat
echo "$x_min $y_max 0.0 $z_max" >> `echo $stem`_bounding_box.dat

oomph-convert -p3 -o `echo $stem`_bounding_box.dat


exit
