#! /bin/bash

trace_file_list=`find . -name 'trace.dat'`

for trace_file in `echo $trace_file_list`; do
    echo $trace_file
    dir=`dirname $trace_file`
    orig_dir=`pwd`
    cd $dir
    cat trace_header.dat trace.dat > trace_with_header.dat
    cd $orig_dir
done