#! /bin/bash



full_step_reslt_dir=full_step_reslt
#hierher
mkdir $full_step_reslt_dir


dat_stem_list="soln-inner-poro-syrinx-cover regular_inner_poro"
vtu_stem_list="soln-inner-poro soln-inner-poro_mesh"

n_full_step=`ls full_step*.dat | wc -w `


count=0
while [ $count -lt $n_full_step ]; do
    full_step_filename="full_step"`echo $count`".dat"
    actual_number=`awk '{print $1}' $full_step_filename`
    
    for dat_file_stem in `echo $dat_stem_list`; do
        new_file=$full_step_reslt_dir/$dat_file_stem`echo $count`.dat
        old_file=$dat_file_stem`echo $actual_number`.dat
        echo "Old file: $old_file"
        echo "New file: $new_file"
        ln -s $old_file $new_file
    done   

    let count=$count+1
done