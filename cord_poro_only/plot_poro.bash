#! /bin/bash

oomph-convert -z soln-inner-poro-syrinx-cover*.dat
makePvd soln-inner-poro-syrinx-cover soln-inner-poro-syrinx-cover.pvd

oomph-convert -z -p2 poro_traction_sss*.dat
makePvd poro_traction_sss poro_traction_sss.pvd

oomph-convert -z -p2 poro_traction_syrinx*.dat
makePvd poro_traction_syrinx poro_traction_syrinx.pvd
