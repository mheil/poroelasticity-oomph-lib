//LIC// ====================================================================
//LIC// This file forms part of oomph-lib, the object-oriented,
//LIC// multi-physics finite-element library, available
//LIC// at http://www.oomph-lib.org.
//LIC//
//LIC//           Version 0.90. August 3, 2009.
//LIC//
//LIC// Copyright (C) 2006-2009 Matthias Heil and Andrew Hazel
//LIC//
//LIC// This library is free software; you can redistribute it and/or
//LIC// modify it under the terms of the GNU Lesser General Public
//LIC// License as published by the Free Software Foundation; either
//LIC// version 2.1 of the License, or (at your option) any later version.
//LIC//
//LIC// This library is distributed in the hope that it will be useful,
//LIC// but WITHOUT ANY WARRANTY; without even the implied warranty of
//LIC// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//LIC// Lesser General Public License for more details.
//LIC//
//LIC// You should have received a copy of the GNU Lesser General Public
//LIC// License along with this library; if not, write to the Free Software
//LIC// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//LIC// 02110-1301  USA.
//LIC//
//LIC// The authors may be contacted at oomph-lib@maths.man.ac.uk.
//LIC//
//LIC//====================================================================
// Driver
#include <fenv.h>

#include "generic.h"
#include "axisym_poroelasticity.h"
#include "meshes/triangle_mesh.h"

using namespace oomph;
using namespace std;



///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

//======================================================================
/// Face element that allows "locate zeta" operations based on 
/// matching coordinate (here the axial one, i.e. x(1).
//======================================================================
template <class ELEMENT>
class EulerianBasedFaceElement : public virtual FaceGeometry<ELEMENT>, 
public virtual FaceElement
{

public:
 
 /// \short Constructor, which takes a "bulk" element and the 
 /// face index
 EulerianBasedFaceElement(FiniteElement* const &element_pt, 
                  const int &face_index) : 
  FaceGeometry<ELEMENT>(), FaceElement()
  {  
   //Attach the geometrical information to the element. N.B. This function
   //also assigns nbulk_value from the required_nvalue of the bulk element
   element_pt->build_face_element(face_index,this);
  }


 /// \short Constructor 
 EulerianBasedFaceElement() : 
  FaceGeometry<ELEMENT>(), FaceElement()
  {  
  }

 
 /// \short Intrinsic coordinate is the second coordinate (for now -- hierher;
 /// generalise) 
 double zeta_nodal(const unsigned &n, const unsigned &k,           
                          const unsigned &i) const 
  {
   return node_pt(n)->x(1); 
  } 

 /// Output nodal coordinates
 void output(std::ostream &outfile)
  {
   outfile << "ZONE" << std::endl;
   unsigned nnod=nnode();
   for (unsigned j=0;j<nnod;j++)
    {
     Node* nod_pt=node_pt(j);
     unsigned dim=nod_pt->ndim();
     for (unsigned i=0;i<dim;i++)
      {
       outfile << nod_pt->x(i) << " ";
      }
     outfile << std::endl;
    }     
  }

 /// Output at n_plot points
 void output(std::ostream &outfile, const unsigned &n_plot)
  {FiniteElement::output(outfile,n_plot);}

 /// C-style output 
 void output(FILE* file_pt)
  {FiniteElement::output(file_pt);}

 /// C_style output at n_plot points
 void output(FILE* file_pt, const unsigned &n_plot)
  {FiniteElement::output(file_pt,n_plot);}

}; 

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

//=======================================================================
/// FaceElement to compute volume enclosed by collection of elements
/// (attached to bulk elements of type ELEMENT).
//=======================================================================
template<class ELEMENT>
class AxisymmetricPoroelasticityLinePostProcessingElement : 
 public virtual FaceGeometry<ELEMENT>,
 public virtual FaceElement
 {

 public:

  /// \short Contructor: Specify bulk element and index of face to which
  /// this face element is to be attached 
  AxisymmetricPoroelasticityLinePostProcessingElement(
   FiniteElement* const &element_pt, const int &face_index) : 
   FaceGeometry<ELEMENT>(), FaceElement()
   {
    //Attach the geometrical information to the element, by
    //making the face element from the bulk element
    element_pt->build_face_element(face_index,this);


    /// Sign of normal
    Normal_sign=1.0;
   }

  
  /// \short Change direction of normal compared to outer unit normal
  /// of bulk element that this face element is attached to
  void set_negative_normal()
   {
    Normal_sign=-1.0;
   }

  /// \short (Re-)set direction of normal to be the same as  outer unit normal
  /// of bulk element that this face element is attached to
  void set_positive_normal()
   {
    Normal_sign=1.0;
   }

  
  /// \short Return this element's contribution to the total volume enclosed
  /// and (via arg list) increment
  double contribution_to_enclosed_volume(double& 
                                         contribution_to_volume_increment)
   {
    bool bulk_is_solid_element=true;
    if (dynamic_cast<ELEMENT*>(bulk_element_pt())==0)
     {
      bulk_is_solid_element=false;
     }

    // Initialise
    double vol=0.0;
    double dvol=0.0;

    //Find out how many nodes there are
    const unsigned n_node = this->nnode();
    
   //Set up memeory for the shape functions
    Shape psi(n_node);
    DShape dpsids(n_node,1);
    
    //Set the value of n_intpt
    const unsigned n_intpt = this->integral_pt()->nweight();
    
    //Storage for the local coordinate
    Vector<double> s(1);
    
    //Loop over the integration points
    for(unsigned ipt=0;ipt<n_intpt;ipt++)
     {
      //Get the local coordinate at the integration point
      s[0] = this->integral_pt()->knot(ipt,0);
      
      //Get the integral weight
      double W = this->integral_pt()->weight(ipt);
      
      //Call the derivatives of the shape function at the knot point
      this->dshape_local_at_knot(ipt,psi,dpsids);
      
      // Get position and tangent vector
      Vector<double> interpolated_t1(2,0.0);
      Vector<double> interpolated_x(2,0.0);
      Vector<double> interpolated_u(2,0.0);
      for(unsigned l=0;l<n_node;l++)
       {
        //Loop over directional components
        for(unsigned i=0;i<2;i++)
         {
          interpolated_x[i]  += this->nodal_position(l,i)*psi(l);
          if (bulk_is_solid_element)
           {
            interpolated_u[i]  += this->nodal_value(l,i)*psi(l);
           }
          interpolated_t1[i] += this->nodal_position(l,i)*dpsids(l,0);
         }
       }
      
      //Calculate the length of the tangent Vector
      double tlength = interpolated_t1[0]*interpolated_t1[0] + 
       interpolated_t1[1]*interpolated_t1[1];
      
      //Set the Jacobian of the line element
      double J = sqrt(tlength)*interpolated_x[0];
      
      //Now calculate the normal Vector
      Vector<double> interpolated_n(2);
      this->outer_unit_normal(ipt,interpolated_n);
      
      // Assemble dot product
      double dot = 0.0;
      double ddot = 0.0;
      for(unsigned k=0;k<2;k++) 
       {
        dot += Normal_sign*interpolated_x[k]*interpolated_n[k];
        ddot += Normal_sign*interpolated_u[k]*interpolated_n[k];
       }
      
      // Add to volume with sign chosen so that the volume is
      // positive when the elements bound the solid

      // Factor of 1/3 comes from div trick
      vol  += 2.0*MathematicalConstants::Pi*dot*W*J/3.0;

      // No factor three here because this is actually the
      // genuine expression for the increase in volume (equivalent of
      // div trick would require incorporation of u and its derivs into
      // outer unit normal, Jacobian etc.)
      dvol += 2.0*MathematicalConstants::Pi*ddot*W*J;
     }
    
    contribution_to_volume_increment=dvol;
    return vol;
   }
  

  /// \short Output function
  void output(std::ostream &outfile)
   {
    unsigned n_plot=5;
    output(outfile,n_plot);
   }
  
  
 /// \short Output function
  void output(std::ostream &outfile, const unsigned &n_plot)
   {
    unsigned n_dim = this->nodal_dimension();
    Vector<double> x(n_dim);
    Vector<double> s(n_dim-1);

    // Tecplot header info
    outfile << this->tecplot_zone_string(n_plot);
    
    // Loop over plot points
    unsigned num_plot_points=this->nplot_points(n_plot);
    for (unsigned iplot=0;iplot<num_plot_points;iplot++)
     {
      // Get local coordinates of plot point
      this->get_s_plot(iplot,n_plot,s);
      
      // Get Eulerian and Lagrangian coordinates
      this->interpolated_x(s,x);
      
      // Outer unit normal
      Vector<double> unit_normal(n_dim);
      outer_unit_normal(s,unit_normal);
      
      //Output the x,y,..
      for(unsigned i=0;i<n_dim;i++) 
       {outfile << x[i] << " ";} // column 1,2
      
      // Output outer unit normal
      for(unsigned i=0;i<n_dim;i++) 
       { 
        outfile << Normal_sign*unit_normal[i] << " "; // column 3,4
       } 
      
      outfile << std::endl;
     }
    
    // Write tecplot footer (e.g. FE connectivity lists)
    this->write_tecplot_zone_footer(outfile,n_plot);
   }

 private:

  /// Sign of normal
  double Normal_sign;

};


///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

//===Start_of_Global_Physical_Variables_namespace======================
/// Namespace for global parameters
//======================================================================
namespace Global_Physical_Variables
{
 
 /// Output directory
 std::string Directory = "RESLT";

 /// Name of restart file
 std::string Restart_file="";

 /// The Reynolds number
 double Re = 10.0;

 /// The Strouhal number
 double St = 1.0;

 /// Period of forcing in seconds
 double Excitation_period_in_seconds=0.4;

 /// Period of forcing -- dependent parameter; compute
 double Excitation_period=0.0;

 /// Conversion factor for time
 double T_factor_in_inverse_seconds=2.5e-3; 
 
 /// Factor for wall inertia parameter (based on dura properties)
 double Lambda_factor=1.414213562e-6; 
 
 /// Factor for non-dim permeability (based on dura properties)
 double K_factor=125.0; 

 /// Poisson ratio for drained poro-elastic media (0.35 results in
 /// desired effective nu when used with porosity of n=0.3)
 double Nu_drained=0.35;

 // Dura and pia properties
 //------------------------

 /// \short Stiffness ratio for dura and pia: ratio of actual Young's modulus
 /// to Young's modulus used in the non-dim of the equations (unity because 
 /// we've scaled things on their properties)
 double Stiffness_ratio_dura_and_pia=1.0;

 /// \short Permeability ratio; ratio of actual permeability
 /// to permeability used in the non-dim of the equations (unity because 
 /// we've scaled things on their properties)
 double Permeability_ratio_dura_and_pia=1.0;

 /// Dura and Pia Poisson's ratio for drained poro-elastic medium
 double Nu_dura_and_pia = 0.35;

 /// Porosity for dura and pia
 double Porosity_dura_and_pia = 0.3;


 // Cord properties
 //----------------

 /// \short Stiffness ratio for cord: ratio of actual Young's modulus
 /// to Young's modulus used in the non-dim of the equations
 double Stiffness_ratio_cord=0.004;

 /// \short Permeability ratio for cord; ratio of actual permeability
 /// to permeability used in the non-dim of the equations
 double Permeability_ratio_cord=1.0;

 /// Cord Poisson's ratio for drained poro-elastic medium
 double Nu_cord = 0.35;

 /// Porosity for cord
 double Porosity_cord = 0.3;

 // Filum and block properties
 //---------------------------

 /// \short Stiffness ratio for filum and block: ratio of actual Young's modulus
 /// to Young's modulus used in the non-dim of the equations
 double Stiffness_ratio_filum_and_block=0.050;

 /// \short Permeability ratio for filum and block; ratio of actual 
 /// permeability to permeability used in the non-dim of the equations
 double Permeability_ratio_filum_and_block=1.0;

 /// Filum and block Poisson's ratio for drained poro-elastic medium
 double Nu_filum_and_block = 0.35;

 /// Porosity for filum and block
 double Porosity_filum_and_block= 0.3;


 // Generic poroelasticity parameters
 // ---------------------------------

 /// \short The poroelasticity inertia parameter -- dependent parameter;
 /// compute from Re lambda factor
 double Lambda_sq = 0;

 /// \short Non-dim permeability -- ratio of typical porous flux to fluid veloc
 /// scale -- dependent parameter; compute from Re and k factor
 double Permeability = 0.0;

 // Alpha, the Biot parameter (same for all; incompressible fluid and solid)
 double Alpha = 1.0;

 /// \short Ratio of the densities of the fluid and solid phases of
 /// the poroelastic material (same for all)
 double Density_ratio = 1.0;

 /// \short Permeability ratio (actual permeability to reference value);
 ///  currently assumed to be the same for all; can fine grain this if needed by
 /// making direct assignments to corresponding properties of the
 /// various (poro-)elastic regions
 double Permeability_ratio=1.0;

 /// Inverse slip rate coefficient
 double Inverse_slip_rate_coefficient = 0.0;


 
 // // Fraction of cranial pressure applied as pressure (rest applied as
 // // suction from syrinx) if pressure is spatially constant
 // double Outside_fraction_of_transmural_pressure=1.0;

 // // Syrinx extent hierher
 // double Z_syrinx_cranial=-4.0;
 // double P_cranial_t_max= 0.000274;
 // double P_cranial_t_min=-0.000256;
 
 // double Z_syrinx_distal=-11.0;
 // double P_distal_t_max= 0.000295;
 // double P_distal_t_min=-0.000284;
 
 // /// hierher Phase difference
 // double Phi_syrinx_degrees=-45.0;
 

 // /// SSS extent hierher
 // double Z_SSS_cranial=0.0;
 // double Z_SSS_distal=-30.0;
 // double Z_block_cranial=-6.86;
 // double Z_block_distal=-7.93;
 // double P_SSS_cranial=0.00039;
 // double P_SSS_distal_t_max=0.000303;
 // double P_SSS_distal_t_min=-0.000221;
 
 // // Phase difference hierher
 // double Phi_SSS_cranial_degrees=-80.0;


 // Ramp up load?
 bool Do_ramp=true;

 /// axial component of divergence of skeleton displacement
 double Dwdz=2.0e-3;

 /// Update axial displacements
 void update_axial_displacements(Node* nod_pt, const double& time)
 {
  double scale_factor=1.0;
  if ((Do_ramp)&&(time<0.5*Excitation_period))
   {
    scale_factor=
     0.5*(1.0-cos(2.0*MathematicalConstants::Pi*time/Excitation_period));
   }
  double z=nod_pt->x(1);
  double w=Dwdz*z*cos(2.0*MathematicalConstants::Pi*time/Excitation_period)*scale_factor;
  nod_pt->set_value(1,w);
 }


 /// Steady pressures
 double P_inner_steady=8.856505e-06;
 double P_outer_steady=-1.995155e-06;

 // Amplitudes of unsteady pressures
 double P_inner=2.928154e-04;
 double P_outer=4.323867e-04;

 // Phase differences
 double Phi_inner=5.832084e+00; 
 double Phi_outer=6.168015e+00;

 // Syrinx pressure
 //================

 /// Fake syrinx pressure
 double fake_syrinx_pressure(const double& time, const double &z)
 {  

  double scale_factor=1.0;
  if ((Do_ramp)&&(time<0.5*Excitation_period))
   {
    scale_factor=
     0.5*(1.0-cos(2.0*MathematicalConstants::Pi*time/Excitation_period));
   }

  return scale_factor*(
   P_inner_steady+
   P_inner*cos(2.0*MathematicalConstants::Pi*time/Excitation_period+Phi_inner));

  // double p_mean_syrinx_cranial=0.5*(P_cranial_t_max+P_cranial_t_min);
  // double P_cranial=(p_mean_syrinx_cranial
  //                   +0.5*(P_cranial_t_max-P_cranial_t_min)*sin(t_arg+phi))*
  //  scale_factor;
  
  
  // double p_mean_Z_syrinx_distal=0.5*(P_distal_t_max+P_distal_t_min);
  // double P_distal=(p_mean_Z_syrinx_distal
  //                  +0.5*(P_distal_t_max-P_distal_t_min)*sin(t_arg+phi))*
  //  scale_factor;
  
  // double p=P_cranial+(P_distal-P_cranial)*
  //  (z-Z_syrinx_cranial)/(Z_syrinx_distal-Z_syrinx_cranial);
  
  // if (CommandLineArgs::command_line_flag_has_been_set
  //     ("--only_outside_uniform_pressure"))
  //  {
  //   // Apply fraction of cranial pressure as suction
  //   double p_cranial=P_SSS_cranial*sin(t_arg)*scale_factor;
  //   p=-(1.0-Outside_fraction_of_transmural_pressure)*p_cranial;
  //  }
  // return p;
 }

 /// Pressure in syrinx
 void syrinx_pressure(const double &time,
                        const Vector<double> &x,
                        const Vector<double> &n,
                        double &result)
 {
  result=fake_syrinx_pressure(time,x[1]);
 }
 
 
 /// Traction exerted by syrinx
 void syrinx_traction(const double &time,
                      const Vector<double> &x,
                      const Vector<double> &n,
                      Vector<double> &traction)
 {
  double p=fake_syrinx_pressure(time,x[1]);
  traction[0]=-p*n[0];
  traction[1]=-p*n[1];
 }


 // SSS pressure
 //=============

 /// Fake sss pressure
 double fake_sss_pressure(const double &time, const double &z)
 {

  double scale_factor=1.0;
  if ((Do_ramp)&&(time<0.5*Excitation_period))
   {
    scale_factor=
     0.5*(1.0-cos(2.0*MathematicalConstants::Pi*time/Excitation_period));
   }

  return scale_factor*(
   P_outer_steady+
   P_outer*cos(2.0*MathematicalConstants::Pi*time/Excitation_period+Phi_outer));

  // double t_arg=2.0*MathematicalConstants::Pi*time/Excitation_period;
  // double phi=Phi_syrinx_degrees*2.0*MathematicalConstants::Pi/360.0;

  // double scale_factor=1.0;
  // if (time<0.5*Excitation_period)
  //  {
  //   scale_factor=0.5*(1.0-cos(t_arg));
  //  }

  // double p=0.0;
  
  // double p_cranial=P_SSS_cranial*sin(t_arg)*scale_factor;
  
  // double p_mean=0.5*(P_SSS_distal_t_max+P_SSS_distal_t_min);
  // double p_distal=
  //  (p_mean+0.5*(P_SSS_distal_t_max-P_SSS_distal_t_min)*sin(t_arg+phi))*
  //  scale_factor;
  
  // if (z>Z_block_cranial)
  //  {
  //   p=p_cranial;
  //  }
  // else if (z<Z_block_distal)
  //  {
  //   p=p_distal;
  //  }
  // else
  //  {
  //   p=p_cranial+(p_distal-p_cranial)*(z-Z_block_cranial)/
  //    (Z_block_distal-Z_block_cranial);
  //  }

  // if (CommandLineArgs::command_line_flag_has_been_set
  //     ("--only_outside_uniform_pressure"))
  //  {
  //   // Apply fraction of cranial pressure as pressure
  //   double p_cranial=P_SSS_cranial*sin(t_arg)*scale_factor;
  //   p=Outside_fraction_of_transmural_pressure*p_cranial;
  //  }
 }


 /// Pressure in sss
 void sss_pressure(const double &time,
                   const Vector<double> &x,
                   const Vector<double> &n,
                   double &result)
 {
  result=fake_sss_pressure(time,x[1]);
 }
 
 
 /// Traction exerted by sss
 void sss_traction(const double &time,
                   const Vector<double> &x,
                   const Vector<double> &n,
                   Vector<double> &traction)
 {
  double p=fake_sss_pressure(time,x[1]);
  traction[0]=-p*n[0];
  traction[1]=-p*n[1];
 }

 
 // Mesh/problem parameters
 // -----------------------

 /// Target element area for poro-elastic meshes
 double Element_area_poro = 0.01;

 /// Target element area for syrinx cover
 double Element_area_syrinx_cover = 0.01;

 /// \short Overall scaling factor make inner diameter of outer porous
 /// region at inlet equal to one so Re etc. mean what they're
 /// supposed to mean
 double Length_scale = 1.0/20.0;

 /// Radial scaling -- vanilla length scale
 double R_scale = Length_scale;
 
 /// Aspect ratio factor to shrink domain in z direction
 double Z_shrink_factor=1.0;

 /// Axial scaling -- dependent parameter; update
 double Z_scale = Length_scale/Z_shrink_factor;

 /// Suppress regularly spaced output (can change via command line)
 bool Suppress_regularly_spaced_output=false;
 
 /// Regular output with approx 3 per thickness of dura
 unsigned Nplot_r_regular=33; 
 
 /// Regular output with equivalent axial spacing
 unsigned Nplot_z_regular=
  unsigned(double(Global_Physical_Variables::Nplot_r_regular)*600.0/11.0
           /Global_Physical_Variables::Z_shrink_factor);
 

 //-----------------------------------------------------------------------------

 /// \short Helper function to update dependent parameters (based on the
 /// linearised_poroelastic_fsi_pulsewave version)
 void update_dependent_parameters()
  {

   // changed permeability ratio? 
   if (CommandLineArgs::command_line_flag_has_been_set("--permeability_ratio"))
    {
     Global_Physical_Variables::Permeability_ratio_dura_and_pia=
      Global_Physical_Variables::Permeability_ratio;
     Global_Physical_Variables::Permeability_ratio_cord=
      Global_Physical_Variables::Permeability_ratio;
     Global_Physical_Variables::Permeability_ratio_filum_and_block=
      Global_Physical_Variables::Permeability_ratio;
     oomph_info << "\n\nNOTE: Set all permeability ratios to to: "
                << Global_Physical_Variables::Permeability_ratio 
                << "\n\n";
    }
   
   if (CommandLineArgs::command_line_flag_has_been_set
       ("--suppress_wall_inertia"))
    {
     Lambda_factor=0.0;
     oomph_info <<"\n\nNOTE: Set wall inertial to zero!\n\n";
    }

   if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
    {
     oomph_info << "Darcy is switched off everywhere. Setting Poisson's \n"
                << "ratio to 0.49\n";
     Nu_dura_and_pia = 0.49;
     Nu_cord = 0.49;
     Nu_filum_and_block = 0.49;
    }
   else
    {
     if (CommandLineArgs::command_line_flag_has_been_set
         ("--everything_is_porous"))
      {
       oomph_info << "Everything is porous. Setting Poisson's \n"
                  << "ratio everywhere to " << Nu_drained << "\n";
       Nu_dura_and_pia = Nu_drained;
       Nu_cord = Nu_drained;
       Nu_filum_and_block = Nu_drained;
      }
     else
      {
       
       oomph_info << "Only 'syrinx cover' is porous. Setting Poisson's \n"
                  << "ratio elsewhere to 0.49\n";
       Nu_dura_and_pia = 0.49;
       Nu_cord = 0.49;
       Nu_filum_and_block = 0.49;
      }
    }

   // (Square of) inertia parameter
   Lambda_sq = pow(Lambda_factor*Re,2);

   if (CommandLineArgs::command_line_flag_has_been_set
       ("--no_inertia"))
    {
     oomph_info << "Switching off inertia\n";
     Lambda_sq=0.0;
    }
   
   // Non-dim permeability
   Permeability=K_factor/Re;

   // Non-dimensional period of excitation
   Excitation_period=Excitation_period_in_seconds*
    T_factor_in_inverse_seconds*Re;



   if (CommandLineArgs::command_line_flag_has_been_set
       ("--pia_properties_are_cord_properties"))
    {
     oomph_info << "Setting pia properties to be the same as those of cord\n";
     Stiffness_ratio_dura_and_pia=Stiffness_ratio_cord;
     Permeability_ratio_dura_and_pia=Permeability_ratio_cord;
     Nu_dura_and_pia=Nu_cord;
     Porosity_dura_and_pia=Porosity_cord;
    }
  }

 /// Doc dependent parameters
 void doc_dependent_parameters()
  {
   oomph_info << std::endl;
   oomph_info << "Global problem parameters" << std::endl;
   oomph_info << "=========================" << std::endl;
   oomph_info << "Reynolds number (Re)                           : "
              << Re << std::endl;
   oomph_info << "Strouhal number (St)                           : "
              << St << std::endl;
   oomph_info << "Factor for wall inertia parameter              : "
              << Lambda_factor << std::endl;
   oomph_info << "Factor for non-dim permeability                : " 
              << K_factor << std::endl << std::endl;


   oomph_info << "Factor for time conversion (in sec^-1)         : " 
              << T_factor_in_inverse_seconds 
              << std::endl;
   oomph_info << "Timescale (in sec)                             : " 
              << 1.0/(T_factor_in_inverse_seconds*Re) 
              << std::endl << std::endl;
  
   
   oomph_info << "Time-harmonic pressure with period: "
              << Excitation_period << "\n\n";

   oomph_info << "\n\nCommon poro-elastic properties:\n";
   oomph_info <<     "===============================\n\n";

   oomph_info << "(Square of) wall inertia parameter (Lambda^2)  : "
              << Lambda_sq << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability << std::endl;
   oomph_info << "Biot parameter (alpha) in all porous media     : "
              << Alpha << std::endl;
   oomph_info << "Density ratio (rho_f/rho_s) in all porous media: "
              << Density_ratio << std::endl;
   oomph_info << "Inverse slip rate coefficient                  : "
              << Inverse_slip_rate_coefficient << std::endl;

   oomph_info << "\n\nDura and pia properties:\n";
   oomph_info <<     "========================\n\n";
   oomph_info << "Stiffness ratio (non-dim Young's modulus)      : "
              << Stiffness_ratio_dura_and_pia << std::endl;
   oomph_info << "Permeability ratio                             : "
              << Permeability_ratio_dura_and_pia << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability*Permeability_ratio_dura_and_pia << std::endl;
   if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
    {
     oomph_info << "Poisson's ratio                              : "
                << Nu_dura_and_pia << std::endl;
    }
   else
    {
     oomph_info << "Poisson's ratio (non-porous bits)              : "
                << Nu_dura_and_pia << std::endl;
     oomph_info << "Poisson's ratio for drained 'syrinx cover'     : "
                << Nu_drained << std::endl;
    }
   oomph_info << "Porosity                                       : "
              << Porosity_dura_and_pia <<std::endl;

   oomph_info << "\n\nCord properties:\n";
   oomph_info <<     "================\n\n";
   oomph_info << "Stiffness ratio (non-dim Young's modulus)      : "
              << Stiffness_ratio_cord << std::endl;
   oomph_info << "Permeability ratio                             : "
              << Permeability_ratio_cord << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability*Permeability_ratio_cord << std::endl;
   if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
    {
     oomph_info << "Poisson's ratio                              : "
                << Nu_dura_and_pia << std::endl;
    }
   else
    {
     oomph_info << "Poisson's ratio (non-porous bits)              : "
                << Nu_dura_and_pia << std::endl;
     oomph_info << "Poisson's ratio for drained 'syrinx cover'     : "
                << Nu_drained << std::endl;
    }
   oomph_info << "Porosity                                       : "
              << Porosity_cord <<std::endl;

   oomph_info << "\n\nFilum and block properties:\n";
   oomph_info <<     "===========================\n\n";
   oomph_info << "Stiffness ratio (non-dim Young's modulus)      : "
              << Stiffness_ratio_filum_and_block << std::endl;
   oomph_info << "Permeability ratio                             : "
              << Permeability_ratio_filum_and_block << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability*Permeability_ratio_filum_and_block << std::endl;
   oomph_info << "Poisson's ratio for drained material           : "
              << Nu_filum_and_block << std::endl;
   oomph_info << "Porosity                                       : "
              << Porosity_filum_and_block <<std::endl;
   oomph_info << std::endl << std::endl;
  }

 //-----------------------------------------------------------------------------
 
 /// Helper function to create equally spaced vertices between
 /// between final existing vertex and specified end point. 
 /// Vertices are pushed back into Vector of vertices.
 void push_back_vertices(const Vector<double>& end,
                         const double& edge_length,
                         Vector<Vector<double> >& vertex)
 {
  Vector<double> start(2);
  unsigned n_existing=vertex.size();
  start[0]=vertex[n_existing-1][0];
  start[1]=vertex[n_existing-1][1];
  double total_length=sqrt(pow(end[0]-start[0],2)+
                           pow(end[1]-start[1],2));
  unsigned n_seg=unsigned(total_length/edge_length);
  Vector<double> x(2);
  // Skip first one!
  for (unsigned j=1;j<n_seg;j++)
   {
    x[0]=start[0]+(end[0]-start[0])*double(j)/double(n_seg-1);
    x[1]=start[1]+(end[1]-start[1])*double(j)/double(n_seg-1);
    vertex.push_back(x);
   }
 }

 /// Helper function to create equally spaced vertices between
 /// between start and end points. Vertices are pushed back
 /// into Vector of vertices.
 void push_back_vertices(const Vector<double>& start,
                         const Vector<double>& end,
                         const double& edge_length,
                         Vector<Vector<double> >& vertex)
 {
  double total_length=sqrt(pow(end[0]-start[0],2)+
                           pow(end[1]-start[1],2));
  unsigned n_seg=unsigned(total_length/edge_length);
  Vector<double> x(2);
  for (unsigned j=0;j<n_seg;j++)
   {
    x[0]=start[0]+(end[0]-start[0])*double(j)/double(n_seg-1);
    x[1]=start[1]+(end[1]-start[1])*double(j)/double(n_seg-1);
    vertex.push_back(x);
   }
 }

 //-----------------------------------------------------------------------------
 
 /// Helper function to get intersection between two lines
 Vector<double> intersection(const Vector<Vector<double> >& line1,
                             const Vector<Vector<double> >& line2)
 {
  Vector<double> intersect(2);
  double a2 =  line2[1][1] - line2[0][1];
  double b2 = -line2[1][0] + line2[0][0];
  double c2 = a2*line2[0][0]+b2*line2[0][1];
  
  double a1 =  line1[1][1] - line1[0][1];
  double b1 = -line1[1][0] + line1[0][0];
  double c1 = a1*line1[0][0]+b1*line1[0][1];
  
  double det = a1*b2 - a2*b1;
  if (abs(det)<1.0e-16)
   {
    oomph_info << "Trouble";
    abort();
   }
  intersect[0]= (b2*c1-b1*c2)/det;
  intersect[1]= (a1*c2-a2*c1)/det;
  return intersect;
  
 }

 /// Helper function to get intersection between boundary layer
 /// to the right of the two lines
 Vector<double> bl_intersection(const Vector<Vector<double> >& line1,
                                const Vector<Vector<double> >& line2,
                                const double& bl_thick)
 {
  // Get normal to right for first line
  Vector<double> normal(2);
  normal[0]=line1[1][1]-line1[0][1];
  normal[1]=-(line1[1][0]-line1[0][0]);
  double norm=normal[0]*normal[0]+normal[1]*normal[1];
  normal[0]/=sqrt(norm);
  normal[1]/=sqrt(norm);

  // Get boundary layer line
  Vector<Vector<double> >bl_line1(2,Vector<double>(2));
  bl_line1[0][0]=line1[0][0]+bl_thick*normal[0];
  bl_line1[0][1]=line1[0][1]+bl_thick*normal[1]*
   Global_Physical_Variables::Z_shrink_factor;
  bl_line1[1][0]=line1[1][0]+bl_thick*normal[0];
  bl_line1[1][1]=line1[1][1]+bl_thick*normal[1]*
   Global_Physical_Variables::Z_shrink_factor;

  // Get normal to right for second line
  normal[0]=line2[1][1]-line2[0][1];
  normal[1]=-(line2[1][0]-line2[0][0]);
  norm=normal[0]*normal[0]+normal[1]*normal[1];
  normal[0]/=sqrt(norm);
  normal[1]/=sqrt(norm);

  // Get boundary layer line
  Vector<Vector<double> >bl_line2(2,Vector<double>(2));
  bl_line2[0][0]=line2[0][0]+bl_thick*normal[0];
  bl_line2[0][1]=line2[0][1]+bl_thick*normal[1]*
   Global_Physical_Variables::Z_shrink_factor;
  bl_line2[1][0]=line2[1][0]+bl_thick*normal[0];
  bl_line2[1][1]=line2[1][1]+bl_thick*normal[1]*
   Global_Physical_Variables::Z_shrink_factor;

  Vector<double> intersect=intersection(bl_line1,bl_line2);

  return intersect;
 }



//============================================================================
 /// \short A class to do comparison of vectors based on their z coordinate
 //============================================================================
 class CompareVector
 {
 public:
  
  ///The actual comparison operator
  int operator() (const Vector<double>& a, const Vector<double> b)
   {
    return (a[1]<=b[1]);
   }

 };
 

 //-----------------------------------------------------------------------------

 /// \short Global function that completes the edge sign setup
 template<class ELEMENT>
 void edge_sign_setup(Mesh* mesh_pt)
  {
   // The dictionary keeping track of edge signs
   std::map<Edge,unsigned> assignments;

   // Loop over all elements
   unsigned n_element = mesh_pt->nelement();
   for(unsigned e=0;e<n_element;e++)
    {
     ELEMENT* el_pt = dynamic_cast<ELEMENT*>(mesh_pt->element_pt(e));

     // Assign edge signs: Loop over the vertex nodes (always
     // first 3 nodes for triangles)
     for(unsigned i=0;i<3;i++)
      {
       Node *nod_pt1, *nod_pt2;
       nod_pt1 = el_pt->node_pt(i);
       nod_pt2 = el_pt->node_pt((i+1)%3);
       Edge edge(nod_pt1,nod_pt2);
       unsigned status = assignments[edge];

       // This relies on the default value for an int being 0 in a map
       switch(status)
        {
         // If not assigned on either side, give it a + on current side
        case 0:
         assignments[edge]=1;
         break;
         // If assigned + on other side, give it a - on current side
        case 1:
         assignments[edge]=2;
         el_pt->sign_edge(i)=-1;
         break;
         // If assigned - on other side, give it a + on current side
        case 2:
         assignments[edge]=1;
         break;
        }
      } // end of loop over vertex nodes

    } // end of loop over elements
  }

} // end_of_Global_Physical_Variables_namespace

//===start_of_problem_class=============================================
/// Problem class
//======================================================================
template<class PORO_ELEMENT>
class SyrinxCoverProblem : public Problem
{
public:

 /// Constructor
 SyrinxCoverProblem();
 
 /// Complete problem setup 
 void complete_problem_setup();
 
 /// Update before solve is empty
 void actions_before_newton_solve() {}
 
 /// Update after solve is empty
 void actions_after_newton_solve() {}


 /// Empty
 void actions_before_newton_convergence_check()
  {
  }

 /// \short Actions before read: Mesh contains face elements on unstructured
 /// meshes. Need to strip them out because the unstructured meshes are 
 /// completely re-built (with new elements) on restart.
 void actions_before_read_unstructured_meshes()
  {
   delete_face_elements();
   rebuild_global_mesh();
  }     
 
 /// Re-attach face elements after reading in (bulk) unstructured meshes
 void actions_after_read_unstructured_meshes()
  {
   create_poro_face_elements();
   complete_problem_setup();
   rebuild_global_mesh();
  }

 /// Actions before implicit timestep
 void actions_before_implicit_timestep()
  {
   double t=time_pt()->time();
   unsigned nnod=Inner_poro_mesh_pt->nnode();
   for (unsigned j=0;j<nnod;j++)
    {
     Node* nod_pt=Inner_poro_mesh_pt->node_pt(j);
     Global_Physical_Variables::update_axial_displacements(nod_pt,t);
    }
  }

 /// Restart
 void restart(DocInfo& doc_info)
  {
   // Pointer to restart file
   ifstream* restart_file_pt=0;
   
   // Open restart file from stem
   restart_file_pt=new ifstream(Global_Physical_Variables::Restart_file.c_str(),
                                ios_base::in);
   if (restart_file_pt!=0)
    {
     oomph_info << "Have opened "
                << Global_Physical_Variables::Restart_file.c_str() 
                << " for restart. " << std::endl;
    }
   else
    {
     std::ostringstream error_stream;
     error_stream
      << "ERROR while trying to open " 
      << Global_Physical_Variables::Restart_file.c_str()
      << " for restart." << std::endl;
     
     throw OomphLibError(
      error_stream.str(),
      "restart()",
      OOMPH_EXCEPTION_LOCATION);
    }
   
   
   // Read restart data:
   //-------------------
   if (restart_file_pt!=0)
    { 

     // Doc number
     //----------
     // Read line up to termination sign
     string input_string;
     getline(*restart_file_pt,input_string,'#');
     
     // Ignore rest of line
     restart_file_pt->ignore(80,'\n');
     
     // Doc number
     doc_info.number()=unsigned(atoi(input_string.c_str()));
     
     // Refine the mesh and read in the generic problem data
     Problem::read(*restart_file_pt);
    }
   
   // Clean up
   delete restart_file_pt;
  }

 /// Dump problem data to allow for later restart
 void dump_it(const DocInfo& doc_info, ofstream& dump_file)
  {
   // Write doc number
   dump_file << doc_info.number() << " # current doc number" << std::endl;

   // Dump the refinement pattern and the generic problem data
   Problem::dump(dump_file);
  }

 /// Doc the solution
 void doc_solution(DocInfo& doc_info,unsigned npts=5);
   
 /// Dummy Global error norm for adaptive time-stepping
 double global_temporal_error_norm(){return 0.0;}
 
private:

 /// Create the poroelasticity traction/pressure elements
 void create_poro_face_elements();

 /// Delete all face elements
 void delete_face_elements();

 /// Setup FSI
 void setup_fsi();

 /// Pointers to the inner poro mesh
 Mesh* Inner_poro_mesh_pt;

 /// \short Mesh of line elements to work out flux through radial line through
 /// syrinx cover
 Mesh* Inner_poro_radial_line_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to inner poro mesh from syrinx
 /// fluid. Attaches to inner poro mesh.
 Mesh* Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to inner poro mesh from syrinx
 /// fluid. Attaches to inner poro mesh.
 Mesh* Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to inner poro mesh from syrinx
 /// fluid. Attaches to inner poro mesh.
 Mesh* Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to inner poro mesh from SSS
 /// fluid. Attaches to inner poro mesh.
 Mesh* Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to inner poro mesh from SSS
 /// fluid. Attaches to inner poro mesh.
 Mesh* Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt;

 /// \short FSI mesh to apply traction/pressure to inner poro mesh from SSS
 /// fluid. Attaches to inner poro mesh.
 Mesh* Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt;

 /// \short Mesh as geom object representation of inner poro mesh
 MeshAsGeomObject* Inner_poro_mesh_geom_obj_pt;
 
 /// \short Vector of pairs containing pointers to elements and
 /// local coordinates within them for regularly spaced plot points
 Vector<std::pair<PORO_ELEMENT*,Vector<double> > > 
 Inner_poro_regularly_spaced_plot_point;

 /// Line visualiser for radial line through syrinx cover (upper)
 LineVisualiser* Upper_radial_line_through_syrinx_cover_visualiser_pt;

 /// Pointer to the poroelasticity timestepper
 TimeStepper* Poro_time_stepper_pt;

 /// Trace file
 std::ofstream Trace_file;


 /// Enumeration of the inner poro boundaries
 enum
  {
   Inner_poro_inlet_boundary_id,
   Inner_poro_upper_SSS_interface_boundary_id,
   Inner_poro_central_SSS_interface_boundary_id,
   Inner_poro_lower_SSS_interface_boundary_id,
   Inner_poro_outlet_boundary_id,
   Inner_poro_symmetry_near_outlet_boundary_id,
   Inner_poro_lower_syrinx_interface_boundary_id,
   Inner_poro_central_syrinx_interface_boundary_id,
   Inner_poro_upper_syrinx_interface_boundary_id,
   Inner_poro_symmetry_near_inlet_boundary_id,
   Inner_poro_pia_internal_boundary_id,
   Inner_poro_filum_internal_boundary_id,
   Inner_poro_lower_internal_boundary_of_poroelastic_region_boundary_id,
   Inner_poro_upper_internal_boundary_of_poroelastic_region_boundary_id,
   Inner_poro_radial_line_boundary_id
  };


 /// Enumeration of regions
 enum
  {
   Upper_cord_region_id, // Note: not specified therefore implied!
   Central_upper_cord_region_id, 
   Central_lower_cord_region_id, 
   Lower_cord_region_id, 
   Dura_region_id,
   Block_region_id,
   Upper_pia_region_id,
   Central_upper_pia_region_id,
   Central_lower_pia_region_id,
   Lower_pia_region_id,
   Filum_region_id,
   SSS_upstream_near_block_region_id,
   SSS_central_and_bl_upper_near_block_region_id,
   SSS_central_and_bl_lower_near_block_region_id,
   SSS_downstream_near_block_region_id
  };


 /// \short UNSCALED Z-coordinate of radial line along which we compute fluxes
 /// that go through gap, through porous syrinx cover and through syrinx
 double Z_radial_cut_through_syrinx;

}; // end_of_problem_class

//===start_of_constructor=============================================
/// Problem constructor:
//====================================================================
template<class PORO_ELEMENT>
SyrinxCoverProblem<PORO_ELEMENT>::SyrinxCoverProblem()
{

 // Avoid rough start
 Always_take_one_newton_step=true;


 if (MPI_Helpers::communicator_pt()->my_rank()==0)
  {
   // Open trace file
   string name=Global_Physical_Variables::Directory+"/trace.dat";
   Trace_file.open(name.c_str());
  }
 
 // Create timesteppers
 Poro_time_stepper_pt = new Newmark<2>;
 add_time_stepper_pt(Poro_time_stepper_pt);

 // Z-coordinate of radial line along which we compute fluxes
 // that go through gap, through porous syrinx cover and through syrinx
 Z_radial_cut_through_syrinx=-150.0;


 // Boundaries of syrinx cover etc.
 double r_cover_outer_up=5.64;
 double r_cover_inner_up=4.512;
 double r_cover_outer_down=5.16;
 double r_cover_inner_down=4.128;
 double r_pia_inner_boundary_inlet=5.8;
 double h_pia=0.2;
 double r_pia_inner_boundary_second_radius=4.04;
 double r_in_central_upper_central_pia=5.5;
 double r_in_central_lower_central_pia=5.1;
 double r_in_lower_central_cord=4.5;
 double r_in_upper_central_cord=4.5;
 double r_outer_kink=4.24;
 if (CommandLineArgs::command_line_flag_has_been_set("--parallel_syrinx_cover"))
  {
   r_cover_outer_down=r_cover_outer_up;
   r_cover_inner_down=r_cover_inner_up;
   r_pia_inner_boundary_inlet=r_cover_outer_up-h_pia;
   r_pia_inner_boundary_second_radius=r_cover_outer_up-h_pia;
   r_in_central_upper_central_pia=r_cover_outer_up-0.5*h_pia;
   r_in_central_lower_central_pia=r_cover_outer_up-0.5*h_pia;
   r_in_lower_central_cord=r_cover_inner_up+0.001;
   r_in_upper_central_cord=r_cover_inner_up+0.001;
   r_outer_kink=r_cover_outer_up;
  }




 // Create the problem geometry

 // Update axial scale factor
 Global_Physical_Variables::Z_scale = 
  Global_Physical_Variables::Length_scale/
  Global_Physical_Variables::Z_shrink_factor;
 
 // Update regular output with equivalent axial spacing
 Global_Physical_Variables::Nplot_z_regular=
  unsigned(double(Global_Physical_Variables::Nplot_r_regular)*600.0/11.0
           /Global_Physical_Variables::Z_shrink_factor);
 if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
 {
  oomph_info << "Regular output with: Nplot_r_regular Nplot_z_regular " 
             << Global_Physical_Variables::Nplot_r_regular << " " 
             << Global_Physical_Variables::Nplot_z_regular << std::endl;
 }


 // Poro mesh (inner part)
 // ---------------------
 Vector<TriangleMeshCurveSection*> inner_poro_outer_polyline_boundary_pt(10);

 // Inlet
 {
  Vector<Vector<double> > inner_coords(3, Vector<double>(2));
  inner_coords[0][0]=0.0;
  inner_coords[0][1]=0.0;
  
  inner_coords[1][0]=r_pia_inner_boundary_inlet;
  inner_coords[1][1]=0.0;
  
  inner_coords[2][0]=6.0;
  inner_coords[2][1]=0.0;

  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }

  // Build
  inner_poro_outer_polyline_boundary_pt[Inner_poro_inlet_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,Inner_poro_inlet_boundary_id);
 }


 // FSI interface with SSS (upper)
 {
  Vector<Vector<double> > inner_coords(2, Vector<double>(2));

  inner_coords[0][0]=6.0;
  inner_coords[0][1]=0.0;
  
  inner_coords[1][0]=r_cover_outer_up;
  inner_coords[1][1]=-90.0;
  
  
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt[Inner_poro_upper_SSS_interface_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,Inner_poro_upper_SSS_interface_boundary_id);
 }


 double r_outer_radial_line_in_syrinx_cover=0.0;

 // FSI interface with SSS (central)
 {
  Vector<Vector<double> > inner_coords(3, Vector<double>(2));

  inner_coords[0][0]=r_cover_outer_up;
  inner_coords[0][1]=-90.0;

  r_outer_radial_line_in_syrinx_cover=r_cover_outer_up+
   (r_cover_outer_down-r_cover_outer_up)*
   (Z_radial_cut_through_syrinx-(-90.0))/((-210.0)-(-90.0));
   
  inner_coords[1][0]=r_outer_radial_line_in_syrinx_cover;
  inner_coords[1][1]=Z_radial_cut_through_syrinx;

  inner_coords[2][0]=r_cover_outer_down;
  inner_coords[2][1]=-210.0;
  
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_central_SSS_interface_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_central_SSS_interface_boundary_id);
 }


 // FSI interface with SSS (lower)
 {
  Vector<Vector<double> > inner_coords(4, Vector<double>(2));

  inner_coords[0][0]=r_cover_outer_down;
  inner_coords[0][1]=-210.0;
    
  inner_coords[1][0]=r_outer_kink;
  inner_coords[1][1]=-440.0;
  
  inner_coords[2][0]=1.25;
  inner_coords[2][1]=-480.0;
  
  inner_coords[3][0]=1.25; 
  inner_coords[3][1]=-600.0;
  
  
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_lower_SSS_interface_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_lower_SSS_interface_boundary_id);
 }

 // Outlet
 {
  Vector<Vector<double> > inner_coords(3, Vector<double>(2));

  inner_coords[0][0]=1.25;
  inner_coords[0][1]=-600.0;
  
  inner_coords[1][0]=1.05;
  inner_coords[1][1]=-600.0;

  inner_coords[2][0]=0.0;
  inner_coords[2][1]=-600.0;
  
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt[Inner_poro_outlet_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,Inner_poro_outlet_boundary_id);
 }



 // Symm line near outlet
 {
  Vector<Vector<double> > inner_coords(3, Vector<double>(2));

  inner_coords[0][0]=0.0;
  inner_coords[0][1]=-600.0;
  
  // Need this one to define boundary with filum!
  inner_coords[1][0]=0.0;
  inner_coords[1][1]=-480.0;

  inner_coords[2][0]=0.0;
  inner_coords[2][1]=-220.0;

  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_symmetry_near_outlet_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_symmetry_near_outlet_boundary_id);
 }


 // FSI interface in syrinx (lower)
 {
  Vector<Vector<double> > inner_coords(2, Vector<double>(2));
  
  inner_coords[0][0]=0.0;
  inner_coords[0][1]=-220.0;
  
  inner_coords[1][0]=r_cover_inner_down;
  inner_coords[1][1]=-210.0;
  
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_lower_syrinx_interface_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_lower_syrinx_interface_boundary_id);
 }

 double r_inner_radial_line_in_syrinx_cover=0.0;

 // FSI interface in syrinx (central)
 {
  Vector<Vector<double> > inner_coords(3, Vector<double>(2));
    
  inner_coords[0][0]=r_cover_inner_down;
  inner_coords[0][1]=-210.0;
  
  r_inner_radial_line_in_syrinx_cover=r_cover_inner_down+
   (r_cover_inner_up-r_cover_inner_down)*
   (Z_radial_cut_through_syrinx-(-210.0))/((-90.0)-(-210.0));
   
  inner_coords[1][0]=r_inner_radial_line_in_syrinx_cover;
  inner_coords[1][1]=Z_radial_cut_through_syrinx;


  inner_coords[2][0]=r_cover_inner_up;
  inner_coords[2][1]=-90.0;
    
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_central_syrinx_interface_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_central_syrinx_interface_boundary_id);
 }

 // FSI interface in syrinx (upper)
 {
  Vector<Vector<double> > inner_coords(2, Vector<double>(2));
    
  inner_coords[0][0]=r_cover_inner_up;
  inner_coords[0][1]=-90.0;
  
  inner_coords[1][0]=0.0;
  inner_coords[1][1]=-80.0;
  
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_upper_syrinx_interface_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_upper_syrinx_interface_boundary_id);
 }
 
 
 // Symm line near inlet
 {
  Vector<Vector<double> > inner_coords(2, Vector<double>(2));
  
  inner_coords[0][0]=0.0;
  inner_coords[0][1]=-80.0;
  
  inner_coords[1][0]=0.0;
  inner_coords[1][1]=0.0;
  
  // Rescale
  unsigned n=inner_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    inner_coords[j][0]*=Global_Physical_Variables::R_scale;
    inner_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_poro_outer_polyline_boundary_pt
   [Inner_poro_symmetry_near_inlet_boundary_id] =
   new TriangleMeshPolyLine(inner_coords,
                            Inner_poro_symmetry_near_inlet_boundary_id);

 }
 TriangleMeshClosedCurve* inner_poro_outer_boundary_pt =
  new TriangleMeshClosedCurve(inner_poro_outer_polyline_boundary_pt);

 // We have five internal boundaries 
 Vector<TriangleMeshOpenCurve*> inner_inner_open_boundary_pt(9);
 TriangleMeshPolyLine *inner_inner_polyline1_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline2_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline3_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline4_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline5_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline6_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline7_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline8_pt = 0;
 TriangleMeshPolyLine *inner_inner_polyline9_pt = 0;

   
 // First internal boundary -- boundary between "pia" and genuine interior
 {
  Vector<Vector<double> > inner_inner_coords(4, Vector<double>(2,0.0));
  
  inner_inner_coords[0][0]=r_pia_inner_boundary_inlet;
  inner_inner_coords[0][1]=0.0;
  
  inner_inner_coords[1][0]=r_pia_inner_boundary_second_radius;
  inner_inner_coords[1][1]=-440.0;
  
  inner_inner_coords[2][0]=1.05;
  inner_inner_coords[2][1]=-480.0;
  
  inner_inner_coords[3][0]=1.05;
  inner_inner_coords[3][1]=-600.0;
  
  // Rescale
  for(unsigned i=0;i<4;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_inner_polyline1_pt =
   new TriangleMeshPolyLine(inner_inner_coords,
                            Inner_poro_pia_internal_boundary_id);
  
  // Connect initial vertex to middle vertex on inlet boundary
  inner_inner_polyline1_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>
   (inner_poro_outer_polyline_boundary_pt[Inner_poro_inlet_boundary_id]),1);

  // Connect final vertex to middle vertex on outlet boundary
  inner_inner_polyline1_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>
   (inner_poro_outer_polyline_boundary_pt[Inner_poro_outlet_boundary_id]),1);
  

  // Store in vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section1_pt(1);
  inner_inner_curve_section1_pt[0] = inner_inner_polyline1_pt;
  
  // Create first internal open curve
  inner_inner_open_boundary_pt[0] =
   new TriangleMeshOpenCurve(inner_inner_curve_section1_pt);
 }

 // Second internal boundary -- boundary between "filum" and genuine interior
 {
  Vector<Vector<double> > inner_inner_coords(2, Vector<double>(2,0.0));

  inner_inner_coords[0][0]=0.0;
  inner_inner_coords[0][1]=-480.0;
  
  inner_inner_coords[1][0]=1.05;
  inner_inner_coords[1][1]=-480.0;
  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_inner_polyline2_pt =
   new TriangleMeshPolyLine(inner_inner_coords,
                            Inner_poro_filum_internal_boundary_id);
  
  // Connect initial vertex of separating line with first vertex on 
  // symmetry line
  inner_inner_polyline2_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_symmetry_near_outlet_boundary_id]),1);

  // Conect final vertex to second vertex on pia internal boundary
  inner_inner_polyline2_pt->connect_final_vertex_to_polyline(
   inner_inner_polyline1_pt,2);
  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section2_pt(1);
  inner_inner_curve_section2_pt[0] = inner_inner_polyline2_pt;
  
  // Build
  inner_inner_open_boundary_pt[1] =
   new TriangleMeshOpenCurve(inner_inner_curve_section2_pt);
  
 }


 // Third internal boundary -- lower boundary of region that Chris
 // usually wants to be the only poro-elastic one
 {
  Vector<Vector<double> > inner_inner_coords(2, Vector<double>(2,0.0));

  inner_inner_coords[0][0]=r_cover_inner_down;
  inner_inner_coords[0][1]=-210.0;
  
  inner_inner_coords[1][0]=r_cover_outer_down;
  inner_inner_coords[1][1]=-210.0;
  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_inner_polyline3_pt =
   new TriangleMeshPolyLine(
    inner_inner_coords,
    Inner_poro_lower_internal_boundary_of_poroelastic_region_boundary_id);
  
  // Connect initial vertex of separating line with final vertex of
  // lower syrinx region boundary
  inner_inner_polyline3_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_lower_syrinx_interface_boundary_id]),1);

  // Connect final vertex to first vertex of
  // lower poro-sss boundary
  inner_inner_polyline3_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_lower_SSS_interface_boundary_id]),0);
  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section3_pt(1);
  inner_inner_curve_section3_pt[0] = inner_inner_polyline3_pt;
  
  // Build
  inner_inner_open_boundary_pt[2] =
   new TriangleMeshOpenCurve(inner_inner_curve_section3_pt);
 }


 // Fourth internal boundary -- upper boundary of region that Chris
 // usually wants to be the only poro-elastic one
 {
  Vector<Vector<double> > inner_inner_coords(2, Vector<double>(2,0.0));

  inner_inner_coords[0][0]=r_cover_inner_up;
  inner_inner_coords[0][1]=-90.0;
  
  inner_inner_coords[1][0]=r_cover_outer_up;
  inner_inner_coords[1][1]=-90.0;
  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_inner_polyline4_pt =
   new TriangleMeshPolyLine(
    inner_inner_coords,
    Inner_poro_upper_internal_boundary_of_poroelastic_region_boundary_id);
  
  // Connect initial vertex of separating line with first vertex of
  // upper syrinx region boundary
  inner_inner_polyline4_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_upper_syrinx_interface_boundary_id]),0);

  // Conect final vertex to first vertex on central part of 
  // poro-sss boundary
  inner_inner_polyline4_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_central_SSS_interface_boundary_id]),0);

  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section4_pt(1);
  inner_inner_curve_section4_pt[0] = inner_inner_polyline4_pt;
  
  // Build
  inner_inner_open_boundary_pt[3] =
   new TriangleMeshOpenCurve(inner_inner_curve_section4_pt);
 }


 // Fifth internal boundary -- radial line through syrinx cover
 {
  Vector<Vector<double> > inner_inner_coords(2, Vector<double>(2,0.0));

  inner_inner_coords[0][0]=r_inner_radial_line_in_syrinx_cover;
  inner_inner_coords[0][1]=Z_radial_cut_through_syrinx;
  
  inner_inner_coords[1][0]=r_outer_radial_line_in_syrinx_cover;
  inner_inner_coords[1][1]=Z_radial_cut_through_syrinx;
  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_inner_polyline5_pt =
   new TriangleMeshPolyLine(inner_inner_coords,
                            Inner_poro_radial_line_boundary_id);

  
  // Connect initial vertex of radial line with point on syrinx fsi boundary
  inner_inner_polyline5_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_central_syrinx_interface_boundary_id]),1);

  // Conect final vertex to vertex on central part of 
  // poro-sss boundary
  inner_inner_polyline5_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_central_SSS_interface_boundary_id]),1);
  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section5_pt(1);
  inner_inner_curve_section5_pt[0] = inner_inner_polyline5_pt;
  
  // Build
  inner_inner_open_boundary_pt[4] =
   new TriangleMeshOpenCurve(inner_inner_curve_section5_pt);
 }








//#############################################################




 // Cut line through vertices at lower end of syrinx cover
 {
  Vector<Vector<double> > inner_inner_coords(2, Vector<double>(2,0.0));

  inner_inner_coords[0][0]=r_cover_inner_down;
  inner_inner_coords[0][1]=-210.0;
  
  inner_inner_coords[1][0]=0.5*(r_cover_outer_down+r_cover_inner_down)-
   0.1*(r_cover_outer_down-r_cover_inner_down);
  inner_inner_coords[1][1]=-210.0+0.5*(r_cover_outer_down-r_cover_inner_down)*Global_Physical_Variables::Z_shrink_factor;

  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  unsigned id=1000;
  inner_inner_polyline6_pt =
   new TriangleMeshPolyLine(
    inner_inner_coords,id);
  
  // Connect initial vertex of line with final vertex of
  // lower syrinx region boundary
  inner_inner_polyline6_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_lower_syrinx_interface_boundary_id]),1);
  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section6_pt(1);
  inner_inner_curve_section6_pt[0] = inner_inner_polyline6_pt;
  
  // Build
  inner_inner_open_boundary_pt[5] =
   new TriangleMeshOpenCurve(inner_inner_curve_section6_pt);
 }








 // Cut line through vertices at lower end of syrinx cover
 {
  Vector<Vector<double> > inner_inner_coords(2, Vector<double>(2,0.0));
  
  inner_inner_coords[0][0]=r_cover_outer_down-0.5*h_pia;
  inner_inner_coords[0][1]=-210.0+0.5*h_pia*Global_Physical_Variables::Z_shrink_factor;

  inner_inner_coords[1][0]=r_cover_outer_down;
  inner_inner_coords[1][1]=-210.0;
  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  unsigned id=1001;
  inner_inner_polyline7_pt =
   new TriangleMeshPolyLine(
    inner_inner_coords,id);
  
  // Connect final vertex to first vertex of
  // lower poro-sss boundary
  inner_inner_polyline7_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_lower_SSS_interface_boundary_id]),0);
  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section7_pt(1);
  inner_inner_curve_section7_pt[0] = inner_inner_polyline7_pt;
  
  // Build
  inner_inner_open_boundary_pt[6] =
   new TriangleMeshOpenCurve(inner_inner_curve_section7_pt);
 }

















 // Cut line through vertices at upper end of syrinx cover
 {
  Vector<Vector<double> > inner_inner_coords(2, Vector<double>(2,0.0));

  inner_inner_coords[0][0]=r_cover_inner_up;
  inner_inner_coords[0][1]=-90.0;
  
  inner_inner_coords[1][0]=0.5*(r_cover_outer_up+r_cover_inner_up)-
   0.1*(r_cover_outer_up-r_cover_inner_up);
  inner_inner_coords[1][1]=-90.0-0.5*(r_cover_outer_up-r_cover_inner_up)*Global_Physical_Variables::Z_shrink_factor;
  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  unsigned id=1002;
  inner_inner_polyline8_pt =
   new TriangleMeshPolyLine(
    inner_inner_coords,id);
  
  // Connect initial vertex of separating line with first vertex of
  // upper syrinx region boundary
  inner_inner_polyline8_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_upper_syrinx_interface_boundary_id]),0);
  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section8_pt(1);
  inner_inner_curve_section8_pt[0] = inner_inner_polyline8_pt;
  
  // Build
  inner_inner_open_boundary_pt[7] =
   new TriangleMeshOpenCurve(inner_inner_curve_section8_pt);
 }





 // Cut line through vertices at upper end of syrinx cover
 {
  Vector<Vector<double> > inner_inner_coords(2, Vector<double>(2,0.0));
    
  inner_inner_coords[0][0]=r_cover_outer_up-0.5*h_pia;
  inner_inner_coords[0][1]=-90.0-0.5*h_pia*Global_Physical_Variables::Z_shrink_factor;

  inner_inner_coords[1][0]=r_cover_outer_up;
  inner_inner_coords[1][1]=-90.0;
  
  // Rescale
  for(unsigned i=0;i<2;i++)
   {
    inner_inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  unsigned id=1003;
  inner_inner_polyline9_pt =
   new TriangleMeshPolyLine(
    inner_inner_coords,id);
  

  // Conect final vertex to first vertex on central part of 
  // poro-sss boundary
  inner_inner_polyline9_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>(
    inner_poro_outer_polyline_boundary_pt
    [Inner_poro_central_SSS_interface_boundary_id]),0);

  
  // Stick into vector
  Vector<TriangleMeshCurveSection*> inner_inner_curve_section9_pt(1);
  inner_inner_curve_section9_pt[0] = inner_inner_polyline9_pt;
  
  // Build
  inner_inner_open_boundary_pt[8] =
   new TriangleMeshOpenCurve(inner_inner_curve_section9_pt);
 }






//#############################################################





 // Mesh parameters
 TriangleMeshParameters
  inner_poro_mesh_parameters(inner_poro_outer_boundary_pt);
 inner_poro_mesh_parameters.element_area() =
  Global_Physical_Variables::Element_area_poro;


 oomph_info << "Setting area of syrinx cover to " 
            << Global_Physical_Variables::Element_area_syrinx_cover 
            << std::endl;

 // Specify different target area for syrinx cover (four central regions)
 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_upper_cord_region_id,
   Global_Physical_Variables::Element_area_syrinx_cover);

 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_lower_cord_region_id,
   Global_Physical_Variables::Element_area_syrinx_cover);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_upper_pia_region_id,
   Global_Physical_Variables::Element_area_syrinx_cover);
 
 inner_poro_mesh_parameters.set_target_area_for_region
  (Central_lower_pia_region_id,
   Global_Physical_Variables::Element_area_syrinx_cover);
 
 // Define inner boundaries
 inner_poro_mesh_parameters.internal_open_curves_pt() =
  inner_inner_open_boundary_pt;

 // Define region coordinates
 Vector<double> region_coords(2);

 // Upper cord not specified -- defaults to zero

 // Point inside the upper central cord
 region_coords[0] = Global_Physical_Variables::R_scale*
  r_in_upper_central_cord;
 region_coords[1] = Global_Physical_Variables::Z_scale*(-100.0);
 inner_poro_mesh_parameters.add_region_coordinates(
  Central_upper_cord_region_id, 
  region_coords);

 // Point inside the lower central cord
 region_coords[0] = Global_Physical_Variables::R_scale*
  r_in_lower_central_cord;
 region_coords[1] = Global_Physical_Variables::Z_scale*(-209.0);
 inner_poro_mesh_parameters.add_region_coordinates(
  Central_lower_cord_region_id, 
  region_coords);

 // Point inside the lower cord
 region_coords[0] = Global_Physical_Variables::R_scale*2.0;
 region_coords[1] = Global_Physical_Variables::Z_scale*(-350.0);
 inner_poro_mesh_parameters.add_region_coordinates(Lower_cord_region_id, 
                                                   region_coords);

 // Point inside the upper pia
 region_coords[0] = Global_Physical_Variables::R_scale*5.6;
 region_coords[1] = Global_Physical_Variables::Z_scale*(-60.0);
 inner_poro_mesh_parameters.add_region_coordinates(Upper_pia_region_id, 
                                                   region_coords);

 // Point inside the upper central pia
 region_coords[0] = Global_Physical_Variables::R_scale*
  r_in_central_upper_central_pia;
 region_coords[1] = Global_Physical_Variables::Z_scale*(-100.0);
 inner_poro_mesh_parameters.add_region_coordinates(Central_upper_pia_region_id, 
                                                   region_coords);

 // Point inside the lower central pia
 region_coords[0] = Global_Physical_Variables::R_scale*
  r_in_central_lower_central_pia;
 region_coords[1] = Global_Physical_Variables::Z_scale*(-203.0);
 inner_poro_mesh_parameters.add_region_coordinates(Central_lower_pia_region_id, 
                                                   region_coords);

 // Point inside the lower pia
 region_coords[0] = Global_Physical_Variables::R_scale*1.06; 
 region_coords[1] = Global_Physical_Variables::Z_scale*(-599.0);
 inner_poro_mesh_parameters.add_region_coordinates(Lower_pia_region_id, 
                                                   region_coords);

 // Point inside the filum
 region_coords[0] = Global_Physical_Variables::R_scale*0.5;
 region_coords[1] = Global_Physical_Variables::Z_scale*(-500.0);
 inner_poro_mesh_parameters.add_region_coordinates(Filum_region_id, 
                                                   region_coords);

 // Build the mesh
 Inner_poro_mesh_pt = new TriangleMesh<PORO_ELEMENT>(
   inner_poro_mesh_parameters, Poro_time_stepper_pt);

 // Properly reorder to ensure correct sequence when restarting (spine
 // node update!)
 Inner_poro_mesh_pt->reorder_nodes();

 // Initialise Mesh as geom object representation of inner poro mesh
 Inner_poro_mesh_geom_obj_pt=0;




 // Output mesh and boundaries
 Inner_poro_mesh_pt->output
  (Global_Physical_Variables::Directory+"/inner_poro_mesh.dat",2);
 Inner_poro_mesh_pt->output_boundaries
  (Global_Physical_Variables::Directory+"/inner_poro_mesh_boundaries.dat");
 
 // ---------------------------------------------------------------------------

 Inner_poro_radial_line_mesh_pt =new Mesh;

 // Create the FSI meshes between inner poro and syrinx fluid
 Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt=new Mesh;
 Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt=new Mesh;
 Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt=new Mesh;

 // Create the FSI meshes between inner poro and SSS fluid
 Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt=new Mesh;
 Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt=new Mesh;
 Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt=new Mesh;
 
 
 // ---------------------------------------------------------------------------

 // Make surface meshes
 create_poro_face_elements();

 // ---------------------------------------------------------------------------

 // Undo z-scaling?
 if (CommandLineArgs::command_line_flag_has_been_set
     ("--undo_z_scaling_by_element_stretching"))
  {
   Vector<Mesh*> all_bulk_mesh_pt;
   all_bulk_mesh_pt.push_back(Inner_poro_mesh_pt);
   unsigned n=all_bulk_mesh_pt.size();
   for (unsigned m=0;m<n;m++)
    {
     Mesh* my_mesh_pt=all_bulk_mesh_pt[m];
     unsigned nnod=my_mesh_pt->nnode();
     for (unsigned j=0;j<nnod;j++)
      {
       my_mesh_pt->node_pt(j)->x(1)*=Global_Physical_Variables::Z_shrink_factor;
      }
    }
  }
 
 
 // ----------------------------------------------------------------------------

 // Complete problem setup (boundary conditions, pointers etc.)
 complete_problem_setup();

 // Add the poro submeshes to the problem
 add_sub_mesh(Inner_poro_mesh_pt);
 

 // // Add the FSI meshes to the problem
 add_sub_mesh(Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt);
 add_sub_mesh(Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt);
 add_sub_mesh(Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt);

 add_sub_mesh(Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt);
 add_sub_mesh(Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt);
 add_sub_mesh(Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt);
 
 // Now build the global mesh
 build_global_mesh();


//  #ifdef OOMPH_HAS_MUMPS

//  // Set solver
//  oomph_info << "Using mumps\n";
//  MumpsSolver* mumps_solver_pt= new MumpsSolver;
//  linear_solver_pt() = mumps_solver_pt;
//  mumps_solver_pt->enable_suppress_warning_about_MPI_COMM_WORLD();

// #endif

 // Assign equation numbers
 oomph_info << assign_eqn_numbers() << " equations assigned" << std::endl;
 
} // end_of_constructor




//===start_of_complete_problem_setup=================================
/// Complete problem setup 
//===================================================================
template<class PORO_ELEMENT>
void SyrinxCoverProblem<PORO_ELEMENT>::complete_problem_setup()
{
 // Mesh as geom object representation of inner poro mesh
 Inner_poro_mesh_geom_obj_pt=new MeshAsGeomObject(Inner_poro_mesh_pt);
 
 // Extract regularly spaced points
 if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
  {
   double t_start = TimingHelpers::timer();
   
   // Speed up search (somewhat hand-tuned; adjust if this takes too long
   // or misses too many points)
   Inner_poro_mesh_geom_obj_pt->max_spiral_level()=4;
   unsigned nx_back= Multi_domain_functions::Nx_bin;
   Multi_domain_functions::Nx_bin=10;
   unsigned ny_back= Multi_domain_functions::Ny_bin;
   Multi_domain_functions::Ny_bin=10;
   
   // Populate it...
   Vector<double> x(2);
   Vector<double> s(2);
   Inner_poro_regularly_spaced_plot_point.clear();
   for (unsigned ir=0;ir<Global_Physical_Variables::Nplot_r_regular;ir++)
    {
     // outermost radius of dura
     x[0]=double(ir)/double(Global_Physical_Variables::Nplot_r_regular-1)*0.55;
     for (unsigned iz=0;iz<Global_Physical_Variables::Nplot_z_regular;iz++)
      {
       x[1]=double(iz)/double(Global_Physical_Variables::Nplot_z_regular-1)*
        (-600.0)*Global_Physical_Variables::Z_scale;
       
       // Pointer to GeomObject that contains this point
       GeomObject* geom_obj_pt=0;
       
       // Get it
       Inner_poro_mesh_geom_obj_pt->locate_zeta(x,geom_obj_pt,s);
       
       // Store it
       if (geom_obj_pt!=0)
        {
         std::pair<PORO_ELEMENT*,Vector<double> > tmp;
         tmp = std::make_pair(dynamic_cast<PORO_ELEMENT*>(geom_obj_pt),s);
         Inner_poro_regularly_spaced_plot_point.push_back(tmp);
        }
      }
    }

   // Reset number of bins in binning method 
   Multi_domain_functions::Nx_bin=nx_back;
   Multi_domain_functions::Ny_bin=ny_back;
   oomph_info << "Took: " << TimingHelpers::timer()-t_start
              << " sec to setup regularly spaced points for inner poro mesh\n";
  }
 // ----------------------------------------------------------------------------

 // Complete the problem setup to make the elements fully functional
 
 // Do edge sign setup for inner poro mesh
 Global_Physical_Variables::edge_sign_setup<PORO_ELEMENT>(Inner_poro_mesh_pt);
 
 // Loop over the elements in cord region of inner poro mesh
 //---------------------------------------------------------
 {
  Vector<unsigned> cord_region_id;
  cord_region_id.push_back(Upper_cord_region_id);
  cord_region_id.push_back(Central_upper_cord_region_id);
  cord_region_id.push_back(Central_lower_cord_region_id);
  cord_region_id.push_back(Lower_cord_region_id);
  unsigned nr=cord_region_id.size();
  for (unsigned rr=0;rr<nr;rr++)
   {
    unsigned r=cord_region_id[rr];
    unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)->
     nregion_element(r);
    
    ofstream some_file;
    std::string name;
    switch(r)
     {
     case Upper_cord_region_id:
      name=Global_Physical_Variables::Directory+"/upper_cord_region.dat";
              
      break;
      
     case Central_upper_cord_region_id:
      name=Global_Physical_Variables::Directory+"/central_upper_cord_region.dat";
      break;

     case Central_lower_cord_region_id:
      name=Global_Physical_Variables::Directory+"/central_lower_cord_region.dat";
      break;
      
     case Lower_cord_region_id:
      name=Global_Physical_Variables::Directory+"/lower_cord_region.dat";
      break;
      
     default:
      throw OomphLibError(
       "Wrong region id",
       OOMPH_CURRENT_FUNCTION,
       OOMPH_EXCEPTION_LOCATION);
     }
    

    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      some_file.open(name.c_str());
     }
    for(unsigned e=0;e<nel;e++)
     {
      // Cast to a bulk element
      PORO_ELEMENT *el_pt=dynamic_cast<PORO_ELEMENT*>(
       dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)
       ->region_element_pt(r,e));

      if (MPI_Helpers::communicator_pt()->my_rank()==0)
       {
        // Output region just using vertices
        el_pt->output(some_file,2);
       }

      // Set the pointer to the Lambda^2 parameter (universal)
      el_pt->lambda_sq_pt() = &Global_Physical_Variables::Lambda_sq;
      
      // Set the pointer to non-dim permeability (universal)
      el_pt->permeability_pt()= &Global_Physical_Variables::Permeability;
      
      // Set the pointer to the Biot parameter (same for all; incompressible
      // fluid and solid)
      el_pt->alpha_pt()=&Global_Physical_Variables::Alpha;
      
      // Set the pointer to the density ratio (pore fluid to solid) (same 
      // for all)
      el_pt->density_ratio_pt()= &Global_Physical_Variables::Density_ratio;

      // Set the pointer to Poisson's ratio      
      if (((r==Central_upper_cord_region_id)||
           (r==Central_lower_cord_region_id))&&
          (!CommandLineArgs::command_line_flag_has_been_set("--pin_darcy")))
       {
        el_pt->nu_pt() = &Global_Physical_Variables::Nu_drained;
       }
      else
       {
        el_pt->nu_pt() = &Global_Physical_Variables::Nu_cord;
       }

      // Set the pointer to non-dim Young's modulus (ratio of Young's modulus
      // to Young's modulus used in the non-dimensionalisation of the
      // equations
      el_pt->youngs_modulus_pt() = 
       &Global_Physical_Variables::Stiffness_ratio_cord;
      
      // Set the pointer to permeability ratio (ratio of actual permeability
      // to the one used in the non-dim of the equations)
      el_pt->permeability_ratio_pt()=
       &Global_Physical_Variables::Permeability_ratio_cord;
      
      // Set the pointer to the porosity
      el_pt->porosity_pt()=&Global_Physical_Variables::Porosity_cord;
      
      // Set the internal q dofs' timestepper to the problem timestepper
      el_pt->set_q_internal_timestepper(Poro_time_stepper_pt);
      
      // Switch off Darcy flow everywhere?
      if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
       {
        el_pt->switch_off_darcy();
       }
      // If porous everywhere, don't switch off darcy, otherwise
      // switch it off everywhere apart from the central cord region
      else if ((!CommandLineArgs::command_line_flag_has_been_set
                ("--everything_is_porous"))&&
               ((r!=Central_upper_cord_region_id)&&
                (r!=Central_lower_cord_region_id)))
       {
        el_pt->switch_off_darcy();
       }

     }// end loop over cord elements
    
    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      some_file.close();
     }
   }
 }

 // Loop over the elements in pia region of inner poro mesh
 //---------------------------------------------------------
 {
  Vector<unsigned> pia_region_id;
  pia_region_id.push_back(Upper_pia_region_id);
  pia_region_id.push_back(Central_upper_pia_region_id);
  pia_region_id.push_back(Central_lower_pia_region_id);
  pia_region_id.push_back(Lower_pia_region_id);
  unsigned nr=pia_region_id.size();
  for (unsigned rr=0;rr<nr;rr++)
   {
    unsigned r=pia_region_id[rr];
    unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)->
     nregion_element(r);

    ofstream some_file;
    std::string name;
    switch(r)
     {
     case Upper_pia_region_id:
      name=Global_Physical_Variables::Directory+"/upper_pia_region.dat";
      break;

     case Central_upper_pia_region_id:
      name=Global_Physical_Variables::Directory+"/central_upper_pia_region.dat";
      break;

     case Central_lower_pia_region_id:
      name=Global_Physical_Variables::Directory+"/central_lower_pia_region.dat";
      break;

     case Lower_pia_region_id:
      name=Global_Physical_Variables::Directory+"/lower_pia_region.dat";
      break;

     default:
      throw OomphLibError(
       "Wrong region id",
       OOMPH_CURRENT_FUNCTION,
       OOMPH_EXCEPTION_LOCATION);
     }
    
    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      some_file.open(name.c_str());
     }
    for(unsigned e=0;e<nel;e++)
     {
      // Cast to a bulk element
      PORO_ELEMENT *el_pt=dynamic_cast<PORO_ELEMENT*>(
       dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)
       ->region_element_pt(r,e));
      
      if (MPI_Helpers::communicator_pt()->my_rank()==0)
       {
        // Output region just using vertices
        el_pt->output(some_file,2);
       }

      // Set the pointer to the Lambda^2 parameter (universal)
      el_pt->lambda_sq_pt() = &Global_Physical_Variables::Lambda_sq;
      
      // Set the pointer to non-dim permeability (universal)
      el_pt->permeability_pt()= &Global_Physical_Variables::Permeability;
      
      // Set the pointer to the Biot parameter (same for all; incompressible
      // fluid and solid)
      el_pt->alpha_pt()=&Global_Physical_Variables::Alpha;
      
      // Set the pointer to the density ratio (pore fluid to solid) (same 
      // for all)
      el_pt->density_ratio_pt()= &Global_Physical_Variables::Density_ratio;
      
      // Set the pointer to Poisson's ratio      
      if (((r==Central_upper_pia_region_id)||
           (r==Central_lower_pia_region_id))&&
          (!CommandLineArgs::command_line_flag_has_been_set("--pin_darcy")))
       {
        el_pt->nu_pt() = &Global_Physical_Variables::Nu_drained;
       }
      else
       {
        el_pt->nu_pt() = &Global_Physical_Variables::Nu_dura_and_pia;
       }

      // Set the pointer to non-dim Young's modulus (ratio of Young's modulus
      // to Young's modulus used in the non-dimensionalisation of the
      // equations
      el_pt->youngs_modulus_pt() = 
       &Global_Physical_Variables::Stiffness_ratio_dura_and_pia;
      
      // Set the pointer to permeability ratio (ratio of actual permeability
      // to the one used in the non-dim of the equations)
      el_pt->permeability_ratio_pt()=
       &Global_Physical_Variables::Permeability_ratio_dura_and_pia;
      
      // Set the pointer to the porosity
      el_pt->porosity_pt()=&Global_Physical_Variables::Porosity_dura_and_pia;
      
      // Set the internal q dofs' timestepper to the problem timestepper
      el_pt->set_q_internal_timestepper(Poro_time_stepper_pt);
      

      // Switch off Darcy flow everywhere?
      if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
       {
        el_pt->switch_off_darcy();
       }
      // If porous everywhere, don't switch off darcy, otherwise
      // switch it off everywhere apart from the central pia region
      else if ((!CommandLineArgs::command_line_flag_has_been_set
                ("--everything_is_porous"))&&
               ((r!=Central_upper_pia_region_id)&&
                (r!=Central_lower_pia_region_id)))
       {
        el_pt->switch_off_darcy();
       }
     }// end loop over pia elements
    
    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      some_file.close();
     }
   }
 }

 // Loop over the elements in filum region of inner poro mesh
 //----------------------------------------------------------
 {
  ofstream some_file;
  std::string name;
  name=Global_Physical_Variables::Directory+"/filum_region.dat";
  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.open(name.c_str());
   }
  unsigned r=Filum_region_id;
  unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)->
   nregion_element(r);
  for(unsigned e=0;e<nel;e++)
   {
    // Cast to a bulk element
    PORO_ELEMENT *el_pt=dynamic_cast<PORO_ELEMENT*>(
     dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)
     ->region_element_pt(r,e));
    
    if (MPI_Helpers::communicator_pt()->my_rank()==0)
     {
      // Output region just using vertices
      el_pt->output(some_file,2);
     }

    // Set the pointer to the Lambda^2 parameter (universal)
    el_pt->lambda_sq_pt() = &Global_Physical_Variables::Lambda_sq;
    
    // Set the pointer to non-dim permeability (universal)
    el_pt->permeability_pt()= &Global_Physical_Variables::Permeability;
    
    // Set the pointer to the Biot parameter (same for all; incompressible
    // fluid and solid)
    el_pt->alpha_pt()=&Global_Physical_Variables::Alpha;
    
    // Set the pointer to the density ratio (pore fluid to solid) (same for all)
    el_pt->density_ratio_pt()= &Global_Physical_Variables::Density_ratio;
    
    // Set the pointer to Poisson's ratio
    el_pt->nu_pt() = &Global_Physical_Variables::Nu_filum_and_block;
    
    // Set the pointer to non-dim Young's modulus (ratio of Young's modulus
    // to Young's modulus used in the non-dimensionalisation of the
    // equations
    el_pt->youngs_modulus_pt() = 
     &Global_Physical_Variables::Stiffness_ratio_filum_and_block;
    
    // Set the pointer to permeability ratio (ratio of actual permeability
    // to the one used in the non-dim of the equations)
    el_pt->permeability_ratio_pt()=
     &Global_Physical_Variables::Permeability_ratio_filum_and_block;
    
    // Set the pointer to the porosity
    el_pt->porosity_pt()=&Global_Physical_Variables::Porosity_filum_and_block;
    
    // Set the internal q dofs' timestepper to the problem timestepper
    el_pt->set_q_internal_timestepper(Poro_time_stepper_pt);
    
    // Switch off Darcy flow?
    if ((CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))||
        (!CommandLineArgs::command_line_flag_has_been_set
         ("--everything_is_porous")))
     {
      el_pt->switch_off_darcy();
     }

   }// end loop over filum elements

  if (MPI_Helpers::communicator_pt()->my_rank()==0)
   {
    some_file.close();
   }
 }



 // ------------------------------------------------------------------------

 // Boundary conditions for the inner poroelasticity mesh
 // ----------------------------------------------------

 // Axisymmetric conditions on these boundaries
 {

  // Vector of the boundaries we will pin on the inner poro mesh
  Vector<unsigned> inner_poro_pinned_boundaries;
  inner_poro_pinned_boundaries.push_back(
   Inner_poro_symmetry_near_inlet_boundary_id);
  inner_poro_pinned_boundaries.push_back(
   Inner_poro_symmetry_near_outlet_boundary_id);

  // Get the number of boundaries from the vector
  unsigned n_b=inner_poro_pinned_boundaries.size();
  
  // Set boundary conditions for the inner poro mesh
  for(unsigned ibound=0;ibound<n_b;ibound++)
   {
    unsigned num_nod=Inner_poro_mesh_pt->nboundary_node(
     inner_poro_pinned_boundaries[ibound]);
    for(unsigned inod=0;inod<num_nod;inod++)
     {
      // Get pointer to node
      Node* nod_pt=Inner_poro_mesh_pt->boundary_node_pt(
       inner_poro_pinned_boundaries[ibound],inod);
      
      // Axisymmetric conditions and u_z = 0 on r = 0
      
      // Pin u_r
      nod_pt->pin(0);
      
      // If this node stores edge flux dofs, pin these
      if(nod_pt->nvalue()==4)
       {
        nod_pt->pin(2); 
        nod_pt->pin(3); 
       }
      
     } // end_of_loop_over_nodes
   } // end_of_loop_over_inner_poro_boundaries
 }
 
 // No porous flux or solid displacement on these
 {
  Vector<unsigned> inner_poro_pinned_boundaries;
  inner_poro_pinned_boundaries.push_back(Inner_poro_inlet_boundary_id);
  inner_poro_pinned_boundaries.push_back(Inner_poro_outlet_boundary_id);
  
  // Get the number of boundaries from the vector
  unsigned n_b=inner_poro_pinned_boundaries.size();
  
  // Set boundary conditions for the inner poro mesh
  for(unsigned ibound=0;ibound<n_b;ibound++)
   {
    unsigned num_nod=Inner_poro_mesh_pt->nboundary_node(
     inner_poro_pinned_boundaries[ibound]);
    for(unsigned inod=0;inod<num_nod;inod++)
     {
      // Get pointer to node
      Node* nod_pt=Inner_poro_mesh_pt->boundary_node_pt(
       inner_poro_pinned_boundaries[ibound],inod);
      
      // No porous flux or solid displacement on the inlet and outlet
      
      // Pin u_r, u_z
      if (CommandLineArgs::command_line_flag_has_been_set
          ("--radially_fixed_poro"))
       {
        nod_pt->pin(0);
       }
      nod_pt->pin(1);
           
      // If this node stores edge flux dofs, pin these
      if(nod_pt->nvalue()==4)
       {
        nod_pt->pin(2);
        nod_pt->pin(3);
       }
      
     } // end_of_loop_over_nodes
   } // end_of_loop_over_inner_poro_boundaries
 }


 
 // Pin everything outside syrinx cover
 //------------------------------------
 if (CommandLineArgs::command_line_flag_has_been_set
     ("--pin_everything_outside_syrinx_cover"))
  {
   oomph_info 
    << "Pinning everything outside syrinx cover -- so problem is linear!\n";
   //Problem::Problem_is_nonlinear=false;
   enable_jacobian_reuse();

   Vector<unsigned> pinned_region_id;
   pinned_region_id.push_back(Filum_region_id);
   pinned_region_id.push_back(Upper_cord_region_id);
   pinned_region_id.push_back(Lower_cord_region_id);
   pinned_region_id.push_back(Upper_pia_region_id);
   pinned_region_id.push_back(Lower_pia_region_id);
   PORO_ELEMENT* first_el_pt=0;

   unsigned np=pinned_region_id.size();
   for (unsigned rr=0;rr<np;rr++)
    {
     unsigned r=pinned_region_id[rr];
     unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(
      Inner_poro_mesh_pt)->nregion_element(r);
     for(unsigned e=0;e<nel;e++)
      {
       // Cast to a bulk element
       PORO_ELEMENT* el_pt=dynamic_cast<PORO_ELEMENT*>(
        dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)
        ->region_element_pt(r,e));

       unsigned nint=el_pt->ninternal_data();
       for (unsigned j=0;j<nint;j++)
        {
         Data* data_pt=el_pt->internal_data_pt(j);      
         unsigned nval=data_pt->nvalue();
         for (unsigned i=0;i<nval;i++)
          {
           data_pt->pin(i);
          }
        }

       unsigned nnod=el_pt->nnode();
       for (unsigned j=0;j<nnod;j++)
        {
         Node* nod_pt=el_pt->node_pt(j);

         // Pin u_r and uz
         nod_pt->pin(0);
         nod_pt->pin(1);
         
         // If this node stores edge flux dofs, pin these
         if(nod_pt->nvalue()==4)
          {
           nod_pt->pin(2); 
           nod_pt->pin(3); 
          }
         if (nod_pt->nvalue()>4) abort();

        }
      }
    }
   
   // for (unsigned rr=0;rr<np;rr++)
   //  {
   //   unsigned r=pinned_region_id[rr];
   //   unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(
   //    Inner_poro_mesh_pt)->nregion_element(r);
   //   for(unsigned e=0;e<nel;e++)
   //    {
   //     // Cast to a bulk element
   //     PORO_ELEMENT* el_pt=dynamic_cast<PORO_ELEMENT*>(
   //      dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)
   //      ->region_element_pt(r,e));
       
   //     // Now move element out of the way so we can visualise
   //     // the syrinx cover more easily
   //     if (first_el_pt==0)
   //      {
   //       first_el_pt=el_pt;
   //      }
   //     else
   //      {
   //       unsigned nnod=el_pt->nnode();
   //       for (unsigned j=0;j<nnod;j++)
   //        {
   //         el_pt->node_pt(j)=first_el_pt->node_pt(j);
   //        }
   //      }
   //    }
   //  }


  }



 
 // Allow radial sliding at ends of syrinx cover
 //---------------------------------------------
 if (CommandLineArgs::command_line_flag_has_been_set
     ("--radially_sliding_syrinx_cover"))
  {
   oomph_info << "Allowing radial sliding of syrinx cover\n";

   // Pin all axial displacements
   unsigned nnod=Inner_poro_mesh_pt->nnode();
   for (unsigned j=0;j<nnod;j++)
    {
     Node* nod_pt=Inner_poro_mesh_pt->node_pt(j);
     nod_pt->pin(1);
    }

   Vector<unsigned> boundary_id;
   boundary_id.push_back
    (Inner_poro_upper_internal_boundary_of_poroelastic_region_boundary_id);
   boundary_id.push_back
    (Inner_poro_lower_internal_boundary_of_poroelastic_region_boundary_id);

   unsigned nb=boundary_id.size();
   for (unsigned bb=0;bb<nb;bb++)
    {
     unsigned b=boundary_id[bb];
     unsigned nnod=Inner_poro_mesh_pt->nboundary_node(b);
     for(unsigned j=0;j<nnod;j++)
      {
       Node* nod_pt=Inner_poro_mesh_pt->boundary_node_pt(b,j);

       // Unpin u_r [seepage flux remains unchanged -- we still
       // don't want any normal flux out of element
       nod_pt->unpin(0);

       oomph_info << "unpinning at : " 
                  << nod_pt->x(0) << " " 
                  << nod_pt->x(1) << " " 
                  << std::endl;
      }
    }
  }



 // Line visualiser for radial line through syrinx cover
 {
  double t_start = TimingHelpers::timer();

  // Where do you want it? Halfway between upper vertex of syrinx
  // and upper vertex of gap under block
  double z=-115.0;
  
  // Max. range of syrinx cover
  double r_min=4.128+(4.512-4.128)*(z-(-210.0))/((-90.0)-(-210.0));
  double r_max=5.16 +(5.64 -5.16 )*(z-(-210.0))/((-90.0)-(-210.0));
  if (CommandLineArgs::command_line_flag_has_been_set("--parallel_syrinx_cover"))
   {
    r_min=4.512;
    r_max=5.64;
    z=0.5*(-210.0-90.0);
   }

  r_min*=Global_Physical_Variables::R_scale;
  r_max*=Global_Physical_Variables::R_scale;
  z*=Global_Physical_Variables::Z_scale;

  // How many points to you want?
  unsigned npt=10000;
  Vector<Vector<double> > coord_vec(npt);
  for (unsigned j=0;j<npt;j++)
   {
    coord_vec[j].resize(2);
    coord_vec[j][0]=r_min+(r_max-r_min)*double(j)/unsigned(npt-1);
    coord_vec[j][1]=z;
   }
  
  // Setup line visualiser 
  double max_search_radius=2.0*(r_max-r_min);
  Upper_radial_line_through_syrinx_cover_visualiser_pt=
   new LineVisualiser(Inner_poro_mesh_pt,
                      coord_vec,
                      max_search_radius);
  
  oomph_info 
   << "Took: " << TimingHelpers::timer()-t_start
   << " sec to set up upper line visualiser for points \n"
   << " between " << r_min << " and " << r_max 
   << " at z = " << z << std::endl;
 }

 



}



//===start_of_delete_face_elements==============================
/// Delete face elements
//===================================================================
template<class PORO_ELEMENT>
void SyrinxCoverProblem<PORO_ELEMENT>::delete_face_elements()
{
 Vector<Mesh*> all_face_mesh_pt;
 all_face_mesh_pt.push_back(Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt);
 all_face_mesh_pt.push_back(Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt);

 unsigned n=all_face_mesh_pt.size();
 for (unsigned m=0;m<n;m++)
  {
   Mesh* my_mesh_pt=all_face_mesh_pt[m];
   unsigned nel=my_mesh_pt->nelement();
   for (unsigned e=0;e<nel;e++)
    {
     delete my_mesh_pt->element_pt(e);
    }
   my_mesh_pt->flush_element_and_node_storage();
  }
}



//===start_of_create_poro_face_elements==============================
/// Make poro face elements
//===================================================================
template<class PORO_ELEMENT>
void SyrinxCoverProblem<PORO_ELEMENT>::
create_poro_face_elements()
{

 // ----------------------------------
 // Create the poro FSI surface meshes
 // ----------------------------------

 // Inner poro mesh (from syrinx fluid mesh)
 // ---------------------------------------
 Vector<unsigned> fsi_interface;
 fsi_interface.push_back(Inner_poro_lower_syrinx_interface_boundary_id);
 fsi_interface.push_back(Inner_poro_central_syrinx_interface_boundary_id);
 fsi_interface.push_back(Inner_poro_upper_syrinx_interface_boundary_id);
 unsigned nb=fsi_interface.size();
 for (unsigned bb=0;bb<nb;bb++)
  {
   // Now loop over the bulk elements and create the face elements
   unsigned b=fsi_interface[bb];
   unsigned nel = Inner_poro_mesh_pt->nboundary_element(b);
   for(unsigned e=0;e<nel;e++)
    {
     // Create the face element that applies fluid traction to poroelastic
     // solid
     AxisymmetricPoroelasticityTractionElement<PORO_ELEMENT>* face_element_pt=
      new AxisymmetricPoroelasticityTractionElement<PORO_ELEMENT>
      (Inner_poro_mesh_pt->boundary_element_pt(b,e),
       Inner_poro_mesh_pt->face_index_at_boundary(b,e));
     

     // Add to the appropriate surface mesh
     switch (b)
      {
      case Inner_poro_upper_syrinx_interface_boundary_id:
       Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt->
        add_element_pt(face_element_pt);
       break;

      case Inner_poro_central_syrinx_interface_boundary_id:
       Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt->
        add_element_pt(face_element_pt);
       break;

      case Inner_poro_lower_syrinx_interface_boundary_id:
       Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt->
        add_element_pt(face_element_pt);
       break;
       
      default: 
       throw OomphLibError(
        "Wrong boundary id",
        OOMPH_CURRENT_FUNCTION,
        OOMPH_EXCEPTION_LOCATION);
      }

     // Set function pointers
     face_element_pt->pressure_fct_pt()=
      &Global_Physical_Variables::syrinx_pressure;
     face_element_pt->traction_fct_pt()=
      &Global_Physical_Variables::syrinx_traction;
    }
  }

 
 // Inner poro mesh (from SSS fluid mesh)
 // -------------------------------------
 {
  Vector<unsigned> fsi_interface;
  fsi_interface.push_back(Inner_poro_upper_SSS_interface_boundary_id);
  fsi_interface.push_back(Inner_poro_central_SSS_interface_boundary_id);
  fsi_interface.push_back(Inner_poro_lower_SSS_interface_boundary_id);
  unsigned nb=fsi_interface.size();
  for (unsigned bb=0;bb<nb;bb++)
   {
    // Now loop over the bulk elements and create the face elements
    unsigned b=fsi_interface[bb];
    unsigned nel = Inner_poro_mesh_pt->nboundary_element(b);
    for(unsigned e=0;e<nel;e++)
     {

     // Create the face element that applies fluid traction to poroelastic
     // solid
     AxisymmetricPoroelasticityTractionElement<PORO_ELEMENT>* face_element_pt=
      new AxisymmetricPoroelasticityTractionElement<PORO_ELEMENT>
      (Inner_poro_mesh_pt->boundary_element_pt(b,e),
       Inner_poro_mesh_pt->face_index_at_boundary(b,e));
     
      // Add to the appropriate surface mesh
      switch (b)
       {
       case Inner_poro_upper_SSS_interface_boundary_id:
        Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt->
         add_element_pt(face_element_pt);
        break;
        
       case Inner_poro_central_SSS_interface_boundary_id:
        Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt->
         add_element_pt(face_element_pt);
        break;
        
       case Inner_poro_lower_SSS_interface_boundary_id:
        Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt->
         add_element_pt(face_element_pt);
        break;
        
       default: 
        throw OomphLibError(
         "Wrong boundary id",
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
       }
      
      // Set function pointers
      face_element_pt->pressure_fct_pt()=
       &Global_Physical_Variables::sss_pressure;
      face_element_pt->traction_fct_pt()=
       &Global_Physical_Variables::sss_traction;
     }
   }
 }
 


 // Post-processing elements along radial line through syrinx cover
 // ---------------------------------------------------------------
 {
  unsigned b=Inner_poro_radial_line_boundary_id;
  unsigned nel = Inner_poro_mesh_pt->nboundary_element(b);
  for(unsigned e=0;e<nel;e++)
   {
    // Create the face element that allows postprocessing
    AxisymmetricPoroelasticityTractionElement
     <PORO_ELEMENT>* analysis_element_pt=
     new AxisymmetricPoroelasticityTractionElement
     <PORO_ELEMENT>(Inner_poro_mesh_pt->boundary_element_pt(b,e),
                    Inner_poro_mesh_pt->face_index_at_boundary(b,e));
    
    // We're attaching FaceElements on both sides of this internal
    // boundary! Only keep the one whose outer unit normal points
    // in the negative z direction
    
    // Get the outer unit normal
    Vector<double> s(1,0.5);
    Vector<double> interpolated_normal(2);
    analysis_element_pt->outer_unit_normal(s,interpolated_normal);
    
    // Add element?
    if (interpolated_normal[1]<0.0)
     {
      Inner_poro_radial_line_mesh_pt->add_element_pt(analysis_element_pt);
     }
    else
     {
      delete analysis_element_pt;
     }
   }
 }

} // end_of_create_poro_face_elements



//==start_of_doc_solution=================================================
/// Doc the solution
//========================================================================
template<class PORO_ELEMENT>
void SyrinxCoverProblem<PORO_ELEMENT>::
doc_solution(DocInfo& doc_info, unsigned npts)
{

 if (MPI_Helpers::communicator_pt()->my_rank()==0)
  {
   double t_start=TimingHelpers::timer();

   ofstream some_file;
   char filename[1000];

   // Write restart file
   if (!CommandLineArgs::command_line_flag_has_been_set
       ("--suppress_restart_files"))
    {
     sprintf(filename,"%s/restart%i.dat",doc_info.directory().c_str(),
             doc_info.number());
     ofstream dump_file;
     dump_file.open(filename);
     dump_file.precision(20); 
     dump_it(doc_info,dump_file);
     dump_file.close();
    }



     // unsigned nnod=Inner_poro_mesh_pt->nnode();
     // for(unsigned j=0;j<nnod;j++)
     //  {
     //   Node* nod_pt=Inner_poro_mesh_pt->node_pt(j);


     //   oomph_info << "SHITE" << doc_info.number() << ": " 
     //              << nod_pt->x(0) << " " 
     //              << nod_pt->x(1) << " ";
     //   if (nod_pt->is_pinned(0))
     //    {
     //     oomph_info << 2 << std::endl;
     //    }
     //   else
     //    {
     //     oomph_info << 1 << std::endl;
     //    }
     //  }








   if (!CommandLineArgs::command_line_flag_has_been_set
       ("--suppress_all_output_apart_from_trace_and_restart"))
    {

     // Write tecplot files for bulk?
     if (!CommandLineArgs::command_line_flag_has_been_set
         ("--suppress_tecplot_bulk_output"))
      {
       
       // if (!CommandLineArgs::command_line_flag_has_been_set
       //     ("--pin_everything_outside_syrinx_cover"))
        {
         // Output solution on inner poro mesh
         sprintf(filename,"%s/soln-inner-poro%i.dat",
                 doc_info.directory().c_str(),
                 doc_info.number());
         some_file.open(filename);
         Inner_poro_mesh_pt->output(some_file,npts);
         some_file.close();
        }
      } // end tecplot bulk output


     //Output Poisson's ratio and non-dim permeability for all solid elements
     if (doc_info.number()==0)
      {
       sprintf(filename,"%s/nu_and_perm%i.dat",doc_info.directory().c_str(),
               doc_info.number());
       some_file.open(filename);
       Vector<Mesh*> tmp_solid_mesh_pt;
       tmp_solid_mesh_pt.push_back(Inner_poro_mesh_pt);
       unsigned nm=tmp_solid_mesh_pt.size();
       for (unsigned m=0;m<nm;m++)
        {
         Mesh* m_pt=tmp_solid_mesh_pt[m];
         unsigned nel = m_pt->nelement();
         for(unsigned e=0;e<nel;e++)
          {
           PORO_ELEMENT* el_pt=dynamic_cast<PORO_ELEMENT*>(m_pt->element_pt(e));
           double nu=*(el_pt->nu_pt());
           double perm=*(el_pt->permeability_pt());
           double perm_ratio=*(el_pt->permeability_ratio_pt());
           if (el_pt->darcy_is_switched_off())
            {
             perm=0.0;
             perm_ratio=0.0;
            }
           unsigned nnod=el_pt->nnode();
           for (unsigned j=0;j<nnod;j++)
            {
             Node* nod_pt=el_pt->node_pt(j);
             some_file << nod_pt->x(0) << " " 
                       << nod_pt->x(1) << " " 
                       << nu << " " 
                       << perm << " " 
                       << perm_ratio << " " 
                       << std::endl;
            }
          }
        }
       some_file.close();
      }

     // Doc FSI traction acting on poro-elastic materials:
     //---------------------------------------------------
     sprintf(filename,"%s/poro_traction_syrinx%i.dat",
             doc_info.directory().c_str(),
             doc_info.number());
     some_file.open(filename);
     Inner_poro_upper_syrinx_fluid_FSI_surface_mesh_pt->output(some_file);
     Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt->output(some_file);
     Inner_poro_lower_syrinx_fluid_FSI_surface_mesh_pt->output(some_file);
     some_file.close();

     sprintf(filename,"%s/poro_traction_sss%i.dat",
             doc_info.directory().c_str(),
             doc_info.number());
     some_file.open(filename);
     Inner_poro_upper_SSS_fluid_FSI_surface_mesh_pt->output(some_file);
     Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt->output(some_file);
     Inner_poro_lower_SSS_fluid_FSI_surface_mesh_pt->output(some_file);
     some_file.close();

     // Output solution on syrinx cover
     //--------------------------------
     sprintf(filename,"%s/soln-inner-poro-syrinx-cover%i.dat",
             doc_info.directory().c_str(),
             doc_info.number());
     some_file.open(filename);
     Vector<unsigned> region_id;
     region_id.push_back(Central_lower_cord_region_id);
     region_id.push_back(Central_upper_cord_region_id);
     region_id.push_back(Central_lower_pia_region_id);
     region_id.push_back(Central_upper_pia_region_id);
     unsigned nr=region_id.size();
     for (unsigned rr=0;rr<nr;rr++)
      {
       unsigned r=region_id[rr];
       unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>
        (Inner_poro_mesh_pt)->nregion_element(r);
       oomph_info << "Number of elements in syrinx cover region "
                  << r << " : " << nel << std::endl;
       for(unsigned e=0;e<nel;e++)
        {
         // Cast to a bulk element
         PORO_ELEMENT *el_pt=dynamic_cast<PORO_ELEMENT*>(
          dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Inner_poro_mesh_pt)
          ->region_element_pt(r,e));
     
         // Output
         el_pt->output(some_file,npts);
        }
      }
     some_file.close();
 

     // Inner poro at regularly spaced points
     {
      sprintf(filename,"%s/regular_inner_poro%i.dat",
              doc_info.directory().c_str(),
              doc_info.number());
      some_file.open(filename);
      Vector<double> x(2);
      Vector<double> s(2);
      unsigned n=Inner_poro_regularly_spaced_plot_point.size();
      for (unsigned i=0;i<n;i++)
       {
        // Pointer to element
        PORO_ELEMENT* el_pt=Inner_poro_regularly_spaced_plot_point[i].first;
    
        // Coordinates in it
        s=Inner_poro_regularly_spaced_plot_point[i].second;
    
        // Get coords
        el_pt->interpolated_x(s,x);
    
        some_file << x[0] << " "
                  << x[1] << " "
                  << el_pt->interpolated_u(s,0) << " "
                  << el_pt->interpolated_u(s,1) << " "
                  << el_pt->interpolated_q(s,0) << " "
                  << el_pt->interpolated_q(s,1) << " "
                  << el_pt->interpolated_p(s) << " "
                  << std::endl;
       }
      some_file.close();
     }


     // Output bulk meshes in Paraview format
     bool output_in_paraview_format=true;
     if(output_in_paraview_format)
      {

       // if (!CommandLineArgs::command_line_flag_has_been_set
       //     ("--pin_everything_outside_syrinx_cover"))
          {
         // Output solution on inner poro mesh in Paraview format 
         // (with padded 0s)
         sprintf(filename,"%s/soln-inner-poro%05i.vtu",
                 doc_info.directory().c_str(),
                 doc_info.number());
         some_file.open(filename);
         Inner_poro_mesh_pt->output_paraview(some_file,npts);
         some_file.close();
         
         // Coarse (to visualise mesh)
         unsigned npts_coarse=2;
         
         // // Output solution on inner poro mesh in Paraview format 
         // // (with padded 0s)
         // sprintf(filename,"%s/soln-inner-poro_mesh%05i.vtu",
         //         doc_info.directory().c_str(),
         //         doc_info.number());
         // some_file.open(filename);
         // Inner_poro_mesh_pt->output_paraview(some_file,npts_coarse);
         // some_file.close();
        }  

      } // end paraview bulk output
    }
   

   // Plot porous stuff in radial line across syrinx cover
   //-----------------------------------------------------
   sprintf(filename,"%s/radial_line_through_syrinx_cover_veloc_line%i.dat",
           doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   Inner_poro_radial_line_mesh_pt->output(some_file,npts);
   some_file.close();
 
   // Compute volume flux across radial line in syrinx cover
   double syrinx_cover_radial_line_seepage_flux=0.0;
   double syrinx_cover_radial_line_skeleton_flux=0.0;
   {
    unsigned nel=Inner_poro_radial_line_mesh_pt->nelement();
    for (unsigned e=0;e<nel;e++)
     {
      double syrinx_cover_radial_line_seepage_flux_contrib=0.0;
      double syrinx_cover_radial_line_skeleton_flux_contrib=0.0;
      dynamic_cast<AxisymmetricPoroelasticityTractionElement
       <PORO_ELEMENT>*>(Inner_poro_radial_line_mesh_pt->element_pt(e))->
       contribution_to_total_porous_flux(
        syrinx_cover_radial_line_skeleton_flux_contrib,
        syrinx_cover_radial_line_seepage_flux_contrib);
      syrinx_cover_radial_line_seepage_flux+=
       Global_Physical_Variables::St*
       syrinx_cover_radial_line_seepage_flux_contrib;
      syrinx_cover_radial_line_skeleton_flux+=
       Global_Physical_Variables::St*
       syrinx_cover_radial_line_skeleton_flux_contrib;
     }
   }



     // Output poroelastic stuff along radial line through syrinx cover
     {
      oomph_info << " about to do output from upper line visualiser"
                 << std::endl;

      double t_start=TimingHelpers::timer();
      sprintf(filename,"%s/upper_radial_line_through_syrinx_cover%i.dat",
              doc_info.directory().c_str(),
              doc_info.number());
      some_file.open(filename);
      Upper_radial_line_through_syrinx_cover_visualiser_pt->output(some_file);
      some_file.close();
      oomph_info << "Took: " << TimingHelpers::timer()-t_start
                 << " sec to do output from upper line visualiser \n";
     }


   // Compute porous flux over inner syrinx cover boundary
   //------------------------------------------------------
   double inner_syrinx_cover_seepage_flux=0.0;
   unsigned nel=Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt->nelement();
   for (unsigned e=0;e<nel;e++)
    {
     double inner_syrinx_cover_seepage_flux_contrib=0.0;
     double inner_syrinx_cover_skeleton_flux_contrib=0.0;
     AxisymmetricPoroelasticityTractionElement<PORO_ELEMENT>* el_pt=
      dynamic_cast<AxisymmetricPoroelasticityTractionElement<PORO_ELEMENT>*>(
       Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt->element_pt(e));
     el_pt->contribution_to_total_porous_flux(
      inner_syrinx_cover_skeleton_flux_contrib,
      inner_syrinx_cover_seepage_flux_contrib);
   
     inner_syrinx_cover_seepage_flux+=
      Global_Physical_Variables::St*
      inner_syrinx_cover_seepage_flux_contrib;
    }



   // Compute porous flux over outer syrinx cover boundary
   //-----------------------------------------------------
   double outer_upstream_syrinx_cover_seepage_flux=0.0;
   double outer_downstream_syrinx_cover_seepage_flux=0.0;
   nel=Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt->nelement();
   for (unsigned e=0;e<nel;e++)
    {
     double syrinx_cover_seepage_flux_contrib=0.0;
     double syrinx_cover_skeleton_flux_contrib=0.0;
     AxisymmetricPoroelasticityTractionElement<PORO_ELEMENT>* el_pt=
      dynamic_cast<AxisymmetricPoroelasticityTractionElement<PORO_ELEMENT>*>(
       Inner_poro_central_SSS_fluid_FSI_surface_mesh_pt->element_pt(e));
     el_pt->contribution_to_total_porous_flux(
      syrinx_cover_skeleton_flux_contrib,
      syrinx_cover_seepage_flux_contrib);
   
     // Are all the element's nodes upstream or downstream of 
     // radial control line?
     unsigned upstream_count=0;
     unsigned downstream_count=0;
     double nnod=el_pt->nnode();
     for (unsigned j=0;j<nnod;j++)
      {
       double z=el_pt->node_pt(j)->x(1);
       if (z>=Z_radial_cut_through_syrinx*Global_Physical_Variables::Z_scale)
        {
         upstream_count++;
        }
       if (z<=Z_radial_cut_through_syrinx*Global_Physical_Variables::Z_scale)
        {
         downstream_count++;
        }
      }
   
     // Add to the appropriate flux
     if (upstream_count==nnod)
      {
       outer_upstream_syrinx_cover_seepage_flux+=
        Global_Physical_Variables::St*
        syrinx_cover_seepage_flux_contrib;
       if (downstream_count==nnod)
        {
         throw OomphLibError(
          "Element is both upstream and downstream! Something's wrong.",
          OOMPH_CURRENT_FUNCTION,
          OOMPH_EXCEPTION_LOCATION);
        }
      }
     else if (downstream_count==nnod)
      {
       outer_downstream_syrinx_cover_seepage_flux+=
        Global_Physical_Variables::St*
        syrinx_cover_seepage_flux_contrib;
      }
     else
      {
       std::ostringstream error_stream;
       error_stream
        << "Element is neither upstream nor downstream! Something's wrong.",
        throw OomphLibError(
         error_stream.str(),
         OOMPH_CURRENT_FUNCTION,
         OOMPH_EXCEPTION_LOCATION);
      }
    }
 

   // Keeps getting over-written but it's easier to maintain if
   // if info is generated here
   sprintf(filename,"%s/trace_header.dat",doc_info.directory().c_str());
   some_file.open(filename);
   some_file << "VARIABLES = ";
   double t=time_pt()->time();
   Trace_file << t << " "; // column 1
   some_file << "\"time\", ";
   
   double z=0.0; // Global_Physical_Variables::Z_SSS_cranial;
   Trace_file << Global_Physical_Variables::fake_sss_pressure(t,z) << " " ;  // column 2
   some_file << "\"entry pressure\", ";

   // Net porous seepage flux through radial control line in syrinx cover
   Trace_file << syrinx_cover_radial_line_seepage_flux << " "; // column 3
   some_file << "\"seepage vol flux across radial line across syrinx cover\", ";

   // Net skeleton flux through radial control line in syrinx cover
   Trace_file << syrinx_cover_radial_line_skeleton_flux << " "; // column 4
   some_file <<"\"skeleton vol flux across radial line across syrinx cover\", ";

   // Net skeleton+seepage flux through radial control line in syrinx cover
   Trace_file <<  syrinx_cover_radial_line_seepage_flux
    +syrinx_cover_radial_line_skeleton_flux  << " "; // column 5
    some_file <<"\"vol flux across radial line across syrinx cover\", ";

   // Net porous seepage flux through syrinx cover
   Trace_file << inner_syrinx_cover_seepage_flux << " "; // column 6
   some_file << "\"seepage flux over syrinx cover (through syrinx fsi surface)\", ";

   // Net porous seepage flux through outer upstream syrinx cover
   Trace_file << outer_upstream_syrinx_cover_seepage_flux << " "; // column 7
    some_file << "\"seepage flux over syrinx cover (through upstream sss fsi surface)\", ";

   // Net porous seepage flux through outer downstream syrinx cover
   Trace_file << outer_downstream_syrinx_cover_seepage_flux << " "; // column 8
   some_file << "\"seepage flux over syrinx cover (through downstream sss fsi surface)\", ";

   // Net porous seepage flux through complete outer syrinx cover
   Trace_file << outer_upstream_syrinx_cover_seepage_flux+
    outer_downstream_syrinx_cover_seepage_flux<< " "; // column 9
    some_file << "\"seepage flux over syrinx cover (through complete sss fsi surface)\", ";

   // Step number
   Trace_file << doc_info.number() // column 10
              << " " << std::endl;
   some_file << "\"doc step\" "<< std::endl;
   some_file.close();
   Trace_file.flush();

   // Finally create file that indicates that all the data for
   // this timestep has been written so an external shell script
   // can start moving them off the disk...
   sprintf(filename,"%s/written_step%i.dat",
           doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   some_file << "Step has been written. Time t = " 
             << time_pt()->time()  << " "
             << "Timestep dt = " << time_pt()->dt() << "\n";
   some_file.close();


   oomph_info << "Time for doc_solution: "
              << TimingHelpers::timer()-t_start << std::endl;
  }
 
 // Bump counter
 doc_info.number()++;
 
} // end_of_doc_solution




///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


//===start_of_main======================================================
/// Driver code
//======================================================================
int main(int argc, char* argv[])
{
 //feenableexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW);

#ifdef OOMPH_HAS_MPI

 // Initialise MPI
 MPI_Helpers::init(argc,argv);
 
  // Swtich off output modifier
 oomph_info.output_modifier_pt() = &default_output_modifier;
 
 // switch off oomph_info output for all processors but rank 0
 if (MPI_Helpers::communicator_pt()->my_rank()!=0)
  {
   oomph_info.stream_pt() = &oomph_nullstream;
   OomphLibWarning::set_stream_pt(&oomph_nullstream);
   OomphLibError::set_stream_pt(&oomph_nullstream);
  }
 else
  {
   oomph_info << "\n\n=====================================================\n";
   oomph_info << "Number of processors: " 
              << MPI_Helpers::communicator_pt()->nproc() << "\n";
  }
#endif

  // Store command line arguments
 CommandLineArgs::setup(argc,argv);

 // Define possible command line arguments and parse the ones that
 // were actually specified

 // Biot alpha parameter
 CommandLineArgs::specify_command_line_flag(
  "--alpha",
  &Global_Physical_Variables::Alpha);

 // Drained Poisson's ratio of porous media
 CommandLineArgs::specify_command_line_flag(
  "--drained_nu",
  &Global_Physical_Variables::Nu_drained);

 // Permeability of all materials (currently) to reference value
 CommandLineArgs::specify_command_line_flag(
  "--permeability_ratio",&Global_Physical_Variables::Permeability_ratio);
   
 // Name of restart file
 CommandLineArgs::specify_command_line_flag
  ("--restart_file",
   &Global_Physical_Variables::Restart_file);
  
 // Reynolds number
 CommandLineArgs::specify_command_line_flag(
   "--re",
   &Global_Physical_Variables::Re);

 // Pin darcy?
 CommandLineArgs::specify_command_line_flag(
   "--pin_darcy");

 // Leave everything porous (rather than the default which
 // is to only make the region above the syrinx poro-elastic
  CommandLineArgs::specify_command_line_flag
   ("--everything_is_porous");

 // No wall inertia
 CommandLineArgs::specify_command_line_flag(
   "--suppress_wall_inertia");

 // Start from rest?
 CommandLineArgs::specify_command_line_flag(
   "--steady_solve");

 // Target element area for poro meshes
 CommandLineArgs::specify_command_line_flag(
   "--el_area_poro",
   &Global_Physical_Variables::Element_area_poro);

 // Target element area for poro meshes
 CommandLineArgs::specify_command_line_flag(
   "--el_area_syrinx_cover",
   &Global_Physical_Variables::Element_area_syrinx_cover);

 // Axial scaling parameter -- shrinks aspect ratio of
 // domain by this factor
 CommandLineArgs::specify_command_line_flag(
   "--z_shrink_factor",
   &Global_Physical_Variables::Z_shrink_factor);

 // Axial scaling parameter -- shrinks aspect ratio of
 // domain by this factor
 CommandLineArgs::specify_command_line_flag(
  "--undo_z_scaling_by_element_stretching");

 // Output directory
 CommandLineArgs::specify_command_line_flag(
   "--dir",
   &Global_Physical_Variables::Directory);

 // Timestep
 double dt=0.00001;
 CommandLineArgs::specify_command_line_flag(
   "--dt",&dt);

 // Timestep
 double dt_in_seconds=0.0;
 CommandLineArgs::specify_command_line_flag(
   "--dt_in_seconds",&dt_in_seconds);

 // Number of timesteps to perform
 unsigned nstep=1000;
 CommandLineArgs::specify_command_line_flag(
   "--nstep",
   &nstep);

 // Pin radial displacement at "inlet/outlet"
 CommandLineArgs::specify_command_line_flag("--radially_fixed_poro");

 // Suppress output of tecplot files for bulk (already written in
 // paraview format)
 CommandLineArgs::specify_command_line_flag("--suppress_tecplot_bulk_output");

 // Suppress writing of restart files (they're big!)
 CommandLineArgs::specify_command_line_flag("--suppress_restart_files");

 // Suppress all output apart from trace and restart
 CommandLineArgs::specify_command_line_flag
  ("--suppress_all_output_apart_from_trace_and_restart");

 // Suppress output of data on regularly spaced points
 CommandLineArgs::specify_command_line_flag
  ("--suppress_regularly_spaced_output");
 
 // Pin everything outside syrinx cover?
 CommandLineArgs::specify_command_line_flag
  ("--pin_everything_outside_syrinx_cover");

 // Cord=pia
 CommandLineArgs::specify_command_line_flag
  ("--pia_properties_are_cord_properties");

 // Make syrinx cover parallel
 CommandLineArgs::specify_command_line_flag("--parallel_syrinx_cover");

 // Allow free radial sliding at syrinx cover boundaries
 CommandLineArgs::specify_command_line_flag("--radially_sliding_syrinx_cover");

 // Apply only outside, spatially uniform pressure
 CommandLineArgs::specify_command_line_flag("--only_outside_uniform_pressure");

 // // Fraction of transmural pressure assigned to outside so that
 // // if set to 1 all the pressure is applied by pressuration on the
 // // outside; if set to 0 all the pressure is applied by suction
 // // on the inside
 // CommandLineArgs::specify_command_line_flag(
 //   "--outside_fraction_of_transmural_pressure",
 //   &Global_Physical_Variables::Outside_fraction_of_transmural_pressure);

 // Switch off inertia?
 CommandLineArgs::specify_command_line_flag
  ("--no_inertia");

 // Parse command line
 CommandLineArgs::parse_and_assign();

 // Doc what has actually been specified on the command line
 CommandLineArgs::doc_specified_flags();

 // Sanity check
 if ( CommandLineArgs::command_line_flag_has_been_set
      ("--outside_fraction_of_transmural_pressure") && 
      (!CommandLineArgs::command_line_flag_has_been_set
       ("--only_outside_uniform_pressure")) )
  {
   oomph_info << "Specifying --outside_fraction_of_transmural_pressure \n"
              << "without --only_outside_uniform_pressure \n"
              << "doesn't make sense.\n";
   abort();
  }


 // Kill regularly spaced output?
 if (CommandLineArgs::command_line_flag_has_been_set
     ("--suppress_regularly_spaced_output"))
  {
   Global_Physical_Variables::Suppress_regularly_spaced_output=true;
  }

 // Set up doc info
 DocInfo doc_info;

 // Set output directory
 doc_info.set_directory(Global_Physical_Variables::Directory);

 // Create problem
 SyrinxCoverProblem<TAxisymmetricPoroelasticityElement<1> > problem;

 // Update dependent problem parameters
 Global_Physical_Variables::update_dependent_parameters();

 // Doc dependent parameters
 Global_Physical_Variables::doc_dependent_parameters();

 
 if (CommandLineArgs::command_line_flag_has_been_set("--dt")&&
     CommandLineArgs::command_line_flag_has_been_set("--dt_in_seconds"))
  {
   throw OomphLibError(
    "Cannot set both --dt and --dt_in_seconds",
    OOMPH_CURRENT_FUNCTION,
    OOMPH_EXCEPTION_LOCATION);
  }
 dt=dt_in_seconds*Global_Physical_Variables::Re* 
  Global_Physical_Variables::T_factor_in_inverse_seconds;
 oomph_info << "Setting non-dim dt = " << dt 
            << " from Re and T factor. " << std::endl;



 // Check if we are doing a steady solve only
 if(CommandLineArgs::command_line_flag_has_been_set(
     "--steady_solve"))
  {
   oomph_info << "STeady solve -- overwriting dt\n";
   dt=1.0;
  }

 // Sanity check
 if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy")&&
     CommandLineArgs::command_line_flag_has_been_set("--everything_is_porous"))
  {
   throw OomphLibError(
    "Cannot set both --pin_darcy and --everything_is_porous",
    OOMPH_CURRENT_FUNCTION,
    OOMPH_EXCEPTION_LOCATION);
  }


 // Restart?
 if (CommandLineArgs::command_line_flag_has_been_set("--restart_file"))
  {
   problem.restart(doc_info);
   oomph_info << "Done restart\n";

   // Doc the read-in initial conditions 
   problem.doc_solution(doc_info);
  }
 else
  {
   oomph_info << "Not doing restart\n";

   // Set the initial time to t=0
   problem.time_pt()->time()=0.0;
   
   // Set up impulsive start from rest (which also initialises the timestep)
   problem.assign_initial_values_impulsive(dt);

   // Doc the initial conditions
   problem.doc_solution(doc_info);
  }

 // Check if we are doing a steady solve only
 if(CommandLineArgs::command_line_flag_has_been_set(
     "--steady_solve"))
  {
   oomph_info << "Doing steady solve only: dt=" 
              << problem.time_pt()->dt() << std::endl;
   
   /// Steady pressures
   double p_inner_ref=Global_Physical_Variables::P_inner_steady;
   double p_outer_ref=Global_Physical_Variables::P_outer_steady;

   Global_Physical_Variables::Dwdz=0.0;
   Global_Physical_Variables::Do_ramp=false;

   // Amplitudes of unsteady pressures
   Global_Physical_Variables::P_inner=0.0;
   Global_Physical_Variables::P_outer=0.0;



   double dp=1.0e-3;
   double ptm=0.0;

   unsigned n_ptm=3;
   for (unsigned i_ptm=0;i_ptm<n_ptm;i_ptm++)
    {
     unsigned n_inner=3;
     Global_Physical_Variables::P_inner_steady=0.0;
     for (unsigned i_inner=0;i_inner<n_inner;i_inner++)
      {

       // Apply transmural pressure
       Global_Physical_Variables::P_outer_steady=
        Global_Physical_Variables::P_inner_steady-ptm;

       oomph_info << "Steady solve for p_inner/outer/ptm: "
                  << Global_Physical_Variables::P_inner_steady << " "
                  << Global_Physical_Variables::P_outer_steady << " "
                  << ptm << " "
                  << std::endl;
       
       // Do a steady solve
       problem.steady_newton_solve();
       
       // Output solution
       problem.doc_solution(doc_info);
       
       // Bump
       Global_Physical_Variables::P_inner_steady+=dp;
      }
     
     // Bump
     ptm+=dp;

    }
   exit(0);
  }

 // Timestep
 oomph_info << "About to do " << nstep << " timesteps with dt = "
            << dt << std::endl;
 
 double desired_fixed_timestep=dt;
 double next_full_time=problem.time_pt()->time()+desired_fixed_timestep;
 
 bool allow_output_at_fractional_timesteps=true;

 // Do the timestepping
 unsigned istep=0;
 while (istep<nstep)
  {
   oomph_info << "Solving at time " << problem.time_pt()->time()+dt
              << std::endl;
   
   // Fake tolerance -- make sure next timestep gets accepted no
   // matter what
   double epsilon=DBL_MAX;
   problem.adaptive_unsteady_newton_solve(dt,epsilon);

   double distance_to_target_time=
    std::fabs(problem.time_pt()->time()-next_full_time);
   oomph_info << "Distance to target time: " << distance_to_target_time
              << std::endl;

   // Are we there yet?
   // Made it to the next (constant) timestep increment
   if ((distance_to_target_time<1.0e-10)||allow_output_at_fractional_timesteps)
    {
     // Do next step with fixed increment again
     dt=desired_fixed_timestep;
     
     oomph_info << "We're there... Taking next step with dt="
                << dt << std::endl;
     
     // Update next target time
     next_full_time=problem.time_pt()->time()+desired_fixed_timestep;
     oomph_info << "Next target time = " << next_full_time << std::endl;

     // Bump counter
     istep++;

     // Doc the solution
     problem.doc_solution(doc_info);
    }
   else
    {
     // No -- don't doc but use the current (reduced) timestep
     // until we do
     dt=problem.time_pt()->dt();
     oomph_info << "Not there yet... Taking another step with dt="
                << dt << std::endl;
    }

   // Solve for this timestep
   //problem.unsteady_newton_solve(dt);
  }

#ifdef OOMPH_HAS_MPI 

 // Cleanup
 MPI_Helpers::finalize();

#endif

} // end_of_main


