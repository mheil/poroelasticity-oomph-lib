//LIC// ====================================================================
//LIC// This file forms part of oomph-lib, the object-oriented,
//LIC// multi-physics finite-element library, available
//LIC// at http://www.oomph-lib.org.
//LIC//
//LIC//           Version 0.90. August 3, 2009.
//LIC//
//LIC// Copyright (C) 2006-2009 Matthias Heil and Andrew Hazel
//LIC//
//LIC// This library is free software; you can redistribute it and/or
//LIC// modify it under the terms of the GNU Lesser General Public
//LIC// License as published by the Free Software Foundation; either
//LIC// version 2.1 of the License, or (at your option) any later version.
//LIC//
//LIC// This library is distributed in the hope that it will be useful,
//LIC// but WITHOUT ANY WARRANTY; without even the implied warranty of
//LIC// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//LIC// Lesser General Public License for more details.
//LIC//
//LIC// You should have received a copy of the GNU Lesser General Public
//LIC// License along with this library; if not, write to the Free Software
//LIC// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//LIC// 02110-1301  USA.
//LIC//
//LIC// The authors may be contacted at oomph-lib@maths.man.ac.uk.
//LIC//
//LIC//====================================================================
// Driver
#include <fenv.h>


#include "generic.h"
#include "axisym_poroelasticity.h"
#include "axisym_navier_stokes.h"
#include "meshes/triangle_mesh.h"

using namespace oomph;
using namespace std;

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

//=======================================================================
/// FaceElement to output Navier-Stokes velocities
//=======================================================================
template<class ELEMENT>
class AxisymmetricNavierStokesLinePostProcessingElement : 
 public virtual FaceGeometry<ELEMENT>,
 public virtual FaceElement
 {

 public:

  /// \short Contructor: Specify bulk element and index of face to which
  /// this face element is to be attached 
  AxisymmetricNavierStokesLinePostProcessingElement(
   FiniteElement* const &element_pt, const int &face_index) : 
   FaceGeometry<ELEMENT>(), FaceElement()
   {
    //Attach the geometrical information to the element, by
    //making the face element from the bulk element
    element_pt->build_face_element(face_index,this);
   }
  
  
  /// \short Output function: 
  /// r,z,u,v,w,p
  /// in tecplot format. Specified number of plot points in each
  /// coordinate direction.
  void output(std::ostream &outfile, const unsigned &nplot)
   {
    //Vector of local coordinates
    Vector<double> s(1);
    Vector<double> s_bulk(2);
    
    // Get bulk element
    ELEMENT* bulk_el_pt=dynamic_cast<ELEMENT*>(this->bulk_element_pt());
    
    // Tecplot header info
    outfile << tecplot_zone_string(nplot);
    
    // Loop over plot points
    unsigned num_plot_points=nplot_points(nplot);
    for (unsigned iplot=0;iplot<num_plot_points;iplot++)
     {
      
      // Get local coordinates of plot point
      get_s_plot(iplot,nplot,s);
      
      // Get coordinate in bulk
      s_bulk=local_coordinate_in_bulk(s);
      
      // Coordinates
      for(unsigned i=0;i<2;i++)
       {
        outfile << bulk_el_pt->interpolated_x(s_bulk,i) << " ";
       }
      
      // Velocities
      for(unsigned i=0;i<2;i++) 
       {
        outfile << bulk_el_pt->interpolated_u_axi_nst(s_bulk,i) << " ";
       }
      
      // Pressure
      outfile << bulk_el_pt->interpolated_p_axi_nst(s_bulk)  << " ";
      
      outfile << std::endl;   
     }
    
    // Write tecplot footer (e.g. FE connectivity lists)
    write_tecplot_zone_footer(outfile,nplot);
 
   }

  
};


///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

//=======================================================================
/// FaceElement to compute volume enclosed by collection of elements
/// (attached to bulk elements of type ELEMENT).
//=======================================================================
template<class ELEMENT>
class AxisymmetricPoroelasticityLinePostProcessingElement : 
 public virtual FaceGeometry<ELEMENT>,
 public virtual FaceElement
 {

 public:

  /// \short Contructor: Specify bulk element and index of face to which
  /// this face element is to be attached 
  AxisymmetricPoroelasticityLinePostProcessingElement(
   FiniteElement* const &element_pt, const int &face_index) : 
   FaceGeometry<ELEMENT>(), FaceElement()
   {
    //Attach the geometrical information to the element, by
    //making the face element from the bulk element
    element_pt->build_face_element(face_index,this);
   }
  
  /// \short Return this element's contribution to the total volume enclosed
  /// and (via arg list) increment
  double contribution_to_enclosed_volume(double& 
                                         contribution_to_volume_increment)
   {
    // Initialise
    double vol=0.0;
    double dvol=0.0;

    //Find out how many nodes there are
    const unsigned n_node = this->nnode();
    
   //Set up memeory for the shape functions
    Shape psi(n_node);
    DShape dpsids(n_node,1);
    
    //Set the value of n_intpt
    const unsigned n_intpt = this->integral_pt()->nweight();
    
    //Storage for the local coordinate
    Vector<double> s(1);
    
    //Loop over the integration points
    for(unsigned ipt=0;ipt<n_intpt;ipt++)
     {
      //Get the local coordinate at the integration point
      s[0] = this->integral_pt()->knot(ipt,0);
      
      //Get the integral weight
      double W = this->integral_pt()->weight(ipt);
      
      //Call the derivatives of the shape function at the knot point
      this->dshape_local_at_knot(ipt,psi,dpsids);
      
      // Get position and tangent vector
      Vector<double> interpolated_t1(2,0.0);
      Vector<double> interpolated_x(2,0.0);
      Vector<double> interpolated_u(2,0.0);
      for(unsigned l=0;l<n_node;l++)
       {
        //Loop over directional components
        for(unsigned i=0;i<2;i++)
         {
          interpolated_x[i]  += this->nodal_position(l,i)*psi(l);
          interpolated_u[i]  += this->nodal_value(l,i)*psi(l);
          interpolated_t1[i] += this->nodal_position(l,i)*dpsids(l,0);
         }
       }
      
      //Calculate the length of the tangent Vector
      double tlength = interpolated_t1[0]*interpolated_t1[0] + 
       interpolated_t1[1]*interpolated_t1[1];
      
      //Set the Jacobian of the line element
      double J = sqrt(tlength)*interpolated_x[0];
      
      //Now calculate the normal Vector
      Vector<double> interpolated_n(2);
      this->outer_unit_normal(ipt,interpolated_n);
      
      // Assemble dot product
      double dot = 0.0;
      double ddot = 0.0;
      for(unsigned k=0;k<2;k++) 
       {
        dot += interpolated_x[k]*interpolated_n[k];
        ddot += interpolated_u[k]*interpolated_n[k];
       }
      
      // Add to volume with sign chosen so that the volume is
      // positive when the elements bound the solid

      // Factor of 1/3 comes from div trick
      vol  += 2.0*MathematicalConstants::Pi*dot*W*J/3.0;

      // No factor three here because this is actually the
      // genuine expression for the increase in volume (equivalent of
      // div trick would require incorporation of u and its derivs into
      // outer unit normal, Jacobian etc.)
      dvol += 2.0*MathematicalConstants::Pi*ddot*W*J;
     }
    
    contribution_to_volume_increment=dvol;
    return vol;
   }
  

  
};


///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

//===Start_of_Global_Physical_Variables_namespace======================
/// Namespace for global parameters
//======================================================================
namespace Global_Physical_Variables
{
 /// Output directory
 std::string Directory = "RESLT";

 /// Pressure increases linearly to max in 30 seconds 
 double Load_increase_timescale_in_seconds=30.0;

 /// \short Non-dim time over which pressure increases to max
 /// (dependent parameter)
 double Load_increase_timescale=0.0;

 /// Global FSI parameter
 double Q = 1.0e-9;

 // Fluid parameters
 // ----------------

 /// The Reynolds number
 double Re = 10.0;

 /// The Strouhal number
 double St = 1.0;

 /// \short The Womersley number -- dependent parameter; compute
 /// from Re and St
 double Wo = 0.0;

 /// Conversion factor for time
 double T_factor_in_inverse_seconds=2.5e-3; 
 
 /// Factor for wall inertia parameter (based on dura properties)
 double Lambda_factor=1.414213562e-6; 
 
 /// Factor for FSI parameter (based on dura properties)
 double Q_factor=2.0e-12;
 
 /// Factor for non-dim permeability (based on dura properties)
 /// Note: Is multiplied by Permeability_multiplier before
 /// working out actual numbers)
 double K_factor=125.0; 

 /// Multiplier for permeability
 double Permeability_multiplier=1.0;

 /// Poisson ratio for drained poro-elastic media (0.35 results in
 /// desired effective nu when used with porosity of n=0.3)
 double Nu_drained=0.35;

 // Dura and pia properties
 //------------------------

 /// \short Stiffness ratio for dura and pia: ratio of actual Young's modulus
 /// to Young's modulus used in the non-dim of the equations (unity because 
 /// we've scaled things on their properties)
 double Stiffness_ratio_dura_and_pia=1.0;

 /// \short Permeability ratio; ratio of actual permeability
 /// to permeability used in the non-dim of the equations (unity because 
 /// we've scaled things on their properties)
 double Permeability_ratio_dura_and_pia=1.0;

 /// Dura and Pia Poisson's ratio for drained poro-elastic medium
 double Nu_dura_and_pia = 0.35;

 /// Porosity for dura and pia
 double Porosity_dura_and_pia = 0.3;


 // Filum and block properties
 //---------------------------

 /// \short Stiffness ratio for filum and block: ratio of actual Young's modulus
 /// to Young's modulus used in the non-dim of the equations
 double Stiffness_ratio_filum_and_block=1.0;

 /// \short Permeability ratio for filum and block; ratio of actual 
 /// permeability to permeability used in the non-dim of the equations
 double Permeability_ratio_filum_and_block=1.0;

 /// Filum and block Poisson's ratio for drained poro-elastic medium
 double Nu_filum_and_block = 0.35;

 /// Porosity for filum and block
 double Porosity_filum_and_block= 0.3;


 // Generic poroelasticity parameters
 // ---------------------------------

 /// \short The poroelasticity inertia parameter -- dependent parameter;
 /// compute from Re lambda factor
 double Lambda_sq = 0;

 /// \short Non-dim permeability -- ratio of typical porous flux to fluid veloc
 /// scale -- dependent parameter; compute from Re and k factor
 double Permeability = 0.0;

 // Alpha, the Biot parameter (same for all; incompressible fluid and solid)
 double Alpha = 1.0;

 /// \short Ratio of the densities of the fluid and solid phases of
 /// the poroelastic material (same for all)
 double Density_ratio = 1.0;

 /// Inverse slip rate coefficient
 double Inverse_slip_rate_coefficient = 0.0;


 // Mesh/problem parameters
 // -----------------------

 /// Target element area for fluid meshes
 double Element_area_fluid = 0.01; //0.001;

 /// Target element area for solid mesh
 double Element_area_solid = 0.01; // hierher  0.00005;

 /// Raw boundary layer thickness (in Chris' original coordinates)
 double BL_thick=0.02;

 /// \short Moens Korteweg wavespeed based on dura properties -- dependent
 /// parameter
 double Dura_wavespeed=0.0;

 /// \short Overall scaling factor make inner diameter of outer porous
 /// region at inlet equal to one so Re etc. mean what they're
 /// supposed to mean
 double Length_scale = 1.0/2.0;

 /// Radial scaling -- vanilla length scale
 double R_scale = Length_scale;

 /// Axial scaling -- vanilla length scale // hierher
 double Z_scale = Length_scale;
 
 /// Suppress regularly spaced output
 bool Suppress_regularly_spaced_output=false;
 
 /// Regular output with approx 3 per thickness of dura
 unsigned Nplot_r_regular_solid=33; 
 
 /// Regular output with equivalent axial spacing
 unsigned Nplot_z_regular_solid=
  unsigned(double(Global_Physical_Variables::Nplot_r_regular_solid)*8.2/1.1);
  
 /// Regular output with 10 pts across the entire domain
 unsigned Nplot_r_regular_fluid=6; 
 
 /// Regular output with equivalent axial spacing
 unsigned Nplot_z_regular_fluid=
  unsigned(double(Global_Physical_Variables::Nplot_r_regular_fluid)*8.2/1.1);
  

 //-----------------------------------------------------------------------------

 /// \short Helper function to update dependent parameters (based on the
 /// linearised_poroelastic_fsi_pulsewave version)
 void update_dependent_parameters()
  {
   if (CommandLineArgs::command_line_flag_has_been_set
       ("--suppress_wall_inertia"))
    {
     Lambda_factor=0.0;
     oomph_info <<"\n\nNOTE: Set wall inertial to zero!\n\n";
    }

   if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
    {
     oomph_info << "Darcy is switched off everywhere. Setting Poisson's \n"
                << "ratio to 0.49\n";
     Nu_dura_and_pia = 0.49;
     Nu_filum_and_block = 0.49;
    }
   else
    {
     if (CommandLineArgs::command_line_flag_has_been_set
         ("--everything_is_porous"))
      {
       oomph_info << "Everything is porous. Setting Poisson's \n"
                  << "ratio everywhere to " << Nu_drained << "\n";
       Nu_dura_and_pia = Nu_drained;
       Nu_filum_and_block = Nu_drained;
      }
     else
      {
       
       oomph_info << "Only 'syrinx cover' is porous. Setting Poisson's \n"
                  << "ratio elsewhere to 0.49\n";
       Nu_dura_and_pia = 0.49;
       Nu_filum_and_block = 0.49;
      }
    }
   // The Womersley number
   Wo = Re*St;

   // (Square of) inertia parameter
   Lambda_sq = pow(Lambda_factor*Re,2);
   
   // FSI parameter
   Q = Q_factor*Re;
   
   // Non-dim permeability
   Permeability=K_factor/Re*Permeability_multiplier;

   // Non-dimensional
   Load_increase_timescale=Load_increase_timescale_in_seconds*
    T_factor_in_inverse_seconds*Re;
  }

 /// Doc dependent parameters
 void doc_dependent_parameters()
  {
   oomph_info << std::endl;
   oomph_info << "Global problem parameters" << std::endl;
   oomph_info << "=========================" << std::endl;
   oomph_info << "Reynolds number (Re)                           : "
              << Re << std::endl;
   oomph_info << "Strouhal number (St)                           : "
              << St << std::endl;
   oomph_info << "Womersley number (ReSt)                        : "
              << Wo << std::endl;
   oomph_info << "FSI parameter (Q)                              : "
              << Q << std::endl << std::endl;

   oomph_info << "Factor for wall inertia parameter              : "
              << Lambda_factor << std::endl;
   oomph_info << "Factor for FSI parameter                       : "
              << Q_factor << std::endl;
   oomph_info << "Factor for non-dim permeability                : " 
              << K_factor << std::endl << std::endl;


   oomph_info << "Factor for time conversion (in sec^-1)         : " 
              << T_factor_in_inverse_seconds 
              << std::endl;
   oomph_info << "Timescale (in sec)                             : " 
              << 1.0/(T_factor_in_inverse_seconds*Re) 
              << std::endl << std::endl;
  

   oomph_info << "Extra multiplier for non-dim permeability      : " 
              << Permeability_multiplier << std::endl << std::endl;

   oomph_info << "Dura wavespeed                                 : " 
              << Dura_wavespeed << std::endl << std::endl;

   oomph_info << "Forcing: ";
   if (CommandLineArgs::command_line_flag_has_been_set("--impulsive_pressure"))
    {
     oomph_info << "Impulsively applied pressure.\n\n";
    }
   else
    {
     oomph_info 
      << "Linearly increasing pressure with timescale: "
      << Load_increase_timescale 
      << "\n(This is the time over which driving pressure increases to the\n"
      << "peak value that was used to non-dim the equations\n\n";
    }

   oomph_info << "\n\nCommon poro-elastic properties:\n";
   oomph_info <<     "===============================\n\n";

   oomph_info << "(Square of) wall inertia parameter (Lambda^2)  : "
              << Lambda_sq << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability << std::endl;
   oomph_info << "Biot parameter (alpha) in all porous media     : "
              << Alpha << std::endl;
   oomph_info << "Density ratio (rho_f/rho_s) in all porous media: "
              << Density_ratio << std::endl;
   oomph_info << "Inverse slip rate coefficient                  : "
              << Inverse_slip_rate_coefficient << std::endl;

   oomph_info << "\n\nDura and pia properties:\n";
   oomph_info <<     "========================\n\n";
   oomph_info << "Stiffness ratio (non-dim Young's modulus)      : "
              << Stiffness_ratio_dura_and_pia << std::endl;
   oomph_info << "Permeability ratio                             : "
              << Permeability_ratio_dura_and_pia << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability*Permeability_ratio_dura_and_pia << std::endl;
   if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
    {
     oomph_info << "Poisson's ratio                              : "
                << Nu_dura_and_pia << std::endl;
    }
   else
    {
     oomph_info << "Poisson's ratio (non-porous bits)              : "
                << Nu_dura_and_pia << std::endl;
     oomph_info << "Poisson's ratio for drained 'syrinx cover'     : "
                << Nu_drained << std::endl;
    }
   oomph_info << "Porosity                                       : "
              << Porosity_dura_and_pia <<std::endl;


   oomph_info << "\n\nFilum and block properties:\n";
   oomph_info <<     "===========================\n\n";
   oomph_info << "Stiffness ratio (non-dim Young's modulus)      : "
              << Stiffness_ratio_filum_and_block << std::endl;
   oomph_info << "Permeability ratio                             : "
              << Permeability_ratio_filum_and_block << std::endl;
   oomph_info << "Non-dim permeability                           : "
              << Permeability*Permeability_ratio_filum_and_block << std::endl;
   oomph_info << "Poisson's ratio for drained material           : "
              << Nu_filum_and_block << std::endl;
   oomph_info << "Porosity                                       : "
              << Porosity_filum_and_block <<std::endl;
   oomph_info << std::endl << std::endl;
  }

 //-----------------------------------------------------------------------------



 /// Time-dependent magnitude of driving pressure
 double time_dependent_magnitude_of_driving_pressure(const double &time)
 {
  double magnitude=1.0;
  if (!CommandLineArgs::command_line_flag_has_been_set("--impulsive_pressure"))
   {
    // Linear increase
    //magnitude=time/Load_increase_timescale;

    // Smooth increase
    if (time<Load_increase_timescale)
     {
      magnitude=0.5*(1.0-cos(MathematicalConstants::Pi*time/
                             Load_increase_timescale));
     }
    else
     {
      magnitude=1.0;
     }
   }
  return magnitude;
 }


 /// Inflow traction applied to the fluid mesh
 void fluid_inflow_boundary_traction(const double &time,
                                     const Vector<double> &x,
                                     const Vector<double> &n,
                                     Vector<double> &result)
  {
   double magnitude=time_dependent_magnitude_of_driving_pressure(time);
   result[0]=magnitude*Re*n[0];
   result[1]=magnitude*Re*n[1];
   result[2]=0.0;
  }

   
 //-----------------------------------------------------------------------------
 
 /// Helper function to create equally spaced vertices between
 /// between final existing vertex and specified end point. 
 /// Vertices are pushed back into Vector of vertices.
 void push_back_vertices(const Vector<double>& end,
                         const double& edge_length,
                         Vector<Vector<double> >& vertex)
 {
  Vector<double> start(2);
  double n_existing=vertex.size();
  start[0]=vertex[n_existing-1][0];
  start[1]=vertex[n_existing-1][1];
  double total_length=sqrt(pow(end[0]-start[0],2)+
                           pow(end[1]-start[1],2));
  unsigned n_seg=unsigned(total_length/edge_length);
  Vector<double> x(2);
  // Skip first one!
  for (unsigned j=1;j<n_seg;j++)
   {
    x[0]=start[0]+(end[0]-start[0])*double(j)/double(n_seg-1);
    x[1]=start[1]+(end[1]-start[1])*double(j)/double(n_seg-1);
    vertex.push_back(x);
   }
 }

 /// Helper function to create equally spaced vertices between
 /// between start and end points. Vertices are pushed back
 /// into Vector of vertices.
 void push_back_vertices(const Vector<double>& start,
                         const Vector<double>& end,
                         const double& edge_length,
                         Vector<Vector<double> >& vertex)
 {
  double total_length=sqrt(pow(end[0]-start[0],2)+
                           pow(end[1]-start[1],2));
  unsigned n_seg=unsigned(total_length/edge_length);
  Vector<double> x(2);
  for (unsigned j=0;j<n_seg;j++)
   {
    x[0]=start[0]+(end[0]-start[0])*double(j)/double(n_seg-1);
    x[1]=start[1]+(end[1]-start[1])*double(j)/double(n_seg-1);
    vertex.push_back(x);
   }
 }

 //-----------------------------------------------------------------------------
 
 /// Helper function to get intersection between two lines
 Vector<double> intersection(const Vector<Vector<double> >& line1,
                             const Vector<Vector<double> >& line2)
 {
  Vector<double> intersect(2);
  double a2 =  line2[1][1] - line2[0][1];
  double b2 = -line2[1][0] + line2[0][0];
  double c2 = a2*line2[0][0]+b2*line2[0][1];
  
  double a1 =  line1[1][1] - line1[0][1];
  double b1 = -line1[1][0] + line1[0][0];
  double c1 = a1*line1[0][0]+b1*line1[0][1];
  
  double det = a1*b2 - a2*b1;
  if (abs(det)<1.0e-16)
   {
    oomph_info << "Trouble";
    abort();
   }
  intersect[0]= (b2*c1-b1*c2)/det;
  intersect[1]= (a1*c2-a2*c1)/det;
  return intersect;
  
 }

 /// Helper function to get intersection between boundary layer
 /// to the right of the two lines
 Vector<double> bl_intersection(const Vector<Vector<double> >& line1,
                                const Vector<Vector<double> >& line2,
                                const double& bl_thick)
 {
  // Get normal to right for first line
  Vector<double> normal(2);
  normal[0]=line1[1][1]-line1[0][1];
  normal[1]=-(line1[1][0]-line1[0][0]);
  double norm=normal[0]*normal[0]+normal[1]*normal[1];
  normal[0]/=sqrt(norm);
  normal[1]/=sqrt(norm);

  // Get boundary layer line
  Vector<Vector<double> >bl_line1(2,Vector<double>(2));
  bl_line1[0][0]=line1[0][0]+bl_thick*normal[0];
  bl_line1[0][1]=line1[0][1]+bl_thick*normal[1];
  bl_line1[1][0]=line1[1][0]+bl_thick*normal[0];
  bl_line1[1][1]=line1[1][1]+bl_thick*normal[1];

  // Get normal to right for second line
  normal[0]=line2[1][1]-line2[0][1];
  normal[1]=-(line2[1][0]-line2[0][0]);
  norm=normal[0]*normal[0]+normal[1]*normal[1];
  normal[0]/=sqrt(norm);
  normal[1]/=sqrt(norm);

  // Get boundary layer line
  Vector<Vector<double> >bl_line2(2,Vector<double>(2));
  bl_line2[0][0]=line2[0][0]+bl_thick*normal[0];
  bl_line2[0][1]=line2[0][1]+bl_thick*normal[1];
  bl_line2[1][0]=line2[1][0]+bl_thick*normal[0];
  bl_line2[1][1]=line2[1][1]+bl_thick*normal[1];

  Vector<double> intersect=intersection(bl_line1,bl_line2);

  return intersect;
 }

 //-----------------------------------------------------------------------------

 /// \short Global function that completes the edge sign setup
 template<class ELEMENT>
 void edge_sign_setup(Mesh* mesh_pt)
  {
   // The dictionary keeping track of edge signs
   std::map<Edge,unsigned> assignments;

   // Loop over all elements
   unsigned n_element = mesh_pt->nelement();
   for(unsigned e=0;e<n_element;e++)
    {
     ELEMENT* el_pt = dynamic_cast<ELEMENT*>(mesh_pt->element_pt(e));

     // Assign edge signs: Loop over the vertex nodes (always
     // first 3 nodes for triangles)
     for(unsigned i=0;i<3;i++)
      {
       Node *nod_pt1, *nod_pt2;
       nod_pt1 = el_pt->node_pt(i);
       nod_pt2 = el_pt->node_pt((i+1)%3);
       Edge edge(nod_pt1,nod_pt2);
       unsigned status = assignments[edge];

       // This relies on the default value for an int being 0 in a map
       switch(status)
        {
         // If not assigned on either side, give it a + on current side
        case 0:
         assignments[edge]=1;
         break;
         // If assigned + on other side, give it a - on current side
        case 1:
         assignments[edge]=2;
         el_pt->sign_edge(i)=-1;
         break;
         // If assigned - on other side, give it a + on current side
        case 2:
         assignments[edge]=1;
         break;
        }
      } // end of loop over vertex nodes

    } // end of loop over elements
  }

} // end_of_Global_Physical_Variables_namespace

//===start_of_problem_class=============================================
/// Problem class
//======================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
class PorousBlockProblem : public Problem
{
public:

 /// Constructor
 PorousBlockProblem();

 /// Update before solve is empty
 void actions_before_newton_solve() {}

 /// Update after solve is empty
 void actions_after_newton_solve() {}

 /// Actions before implicit timestep
 void actions_before_implicit_timestep()
  {
   oomph_info 
    << "Time: " << time_pt()->time() 
    << " pressure magnitude: " 
    << Global_Physical_Variables::
    time_dependent_magnitude_of_driving_pressure(time_pt()->time())
    << std::endl;

  }

 /// Doc the solution
 void doc_solution(DocInfo& doc_info,unsigned npts=5);

private:

 /// Create the poroelasticity traction/pressure elements
 void create_poro_face_elements();

 /// Create the fluid traction elements
 void create_fluid_face_elements();

 /// Setup FSI
 void setup_fsi();

 /// Pointers to the outer poro mesh
 Mesh* Solid_mesh_pt;

 /// Pointer to the syrinx fluid mesh
 Mesh* Syrinx_fluid_mesh_pt;

 /// Pointer to the upper fluid mesh
 Mesh* Upper_fluid_mesh_pt;

 /// Pointer to the lower fluid mesh
 Mesh* Lower_fluid_mesh_pt;

 /// Inflow fluid surface mesh
 Mesh* Inflow_fluid_surface_mesh_pt;

 /// Mesh that applies fsi traction from upper fluid to solid (on dura)
 Mesh* Solid_upper_fluid_dura_FSI_surface_mesh_pt;

 /// Mesh that applies fsi traction from upper fluid to solid (on block)
 Mesh* Solid_upper_fluid_block_FSI_surface_mesh_pt;
 
 /// Mesh that applies BJS BCs onto upper fluid (on dura)
 Mesh* Upper_fluid_dura_FSI_surface_mesh_pt;

 /// Mesh that applies BJS BCs onto upper fluid (on block)
 Mesh* Upper_fluid_block_FSI_surface_mesh_pt;

 /// Mesh that applies fsi traction from lower fluid to solid (on dura)
 Mesh* Solid_lower_fluid_dura_FSI_surface_mesh_pt;

 /// Mesh that applies fsi traction from lower fluid to solid (on block)
 Mesh* Solid_lower_fluid_block_FSI_surface_mesh_pt;
 
 /// Mesh that applies BJS BCs onto lower fluid (on dura)
 Mesh* Lower_fluid_dura_FSI_surface_mesh_pt;

 /// Mesh that applies BJS BCs onto lower fluid (on block)
 Mesh* Lower_fluid_block_FSI_surface_mesh_pt;

 /// \short Mesh as geom object representation of Upper fluid mesh
 MeshAsGeomObject* Upper_fluid_mesh_geom_obj_pt;
 
 /// \short Vector of pairs containing pointers to elements and
 /// local coordinates within them for regularly spaced plot points
 Vector<std::pair<FLUID_ELEMENT*,Vector<double> > > 
 Upper_fluid_regularly_spaced_plot_point;

 /// \short Mesh as geom object representation of lower fluid mesh
 MeshAsGeomObject* Lower_fluid_mesh_geom_obj_pt;
 
 /// \short Vector of pairs containing pointers to elements and
 /// local coordinates within them for regularly spaced plot points
 Vector<std::pair<FLUID_ELEMENT*,Vector<double> > > 
 Lower_fluid_regularly_spaced_plot_point;

 /// \short Mesh as geom object representation of syrinx fluid mesh
 MeshAsGeomObject* Syrinx_fluid_mesh_geom_obj_pt;
 
 /// \short Vector of pairs containing pointers to elements and
 /// local coordinates within them for regularly spaced plot points
 Vector<std::pair<FLUID_ELEMENT*,Vector<double> > > 
 Syrinx_fluid_regularly_spaced_plot_point;

 /// \short Mesh as geom object representation of outer poro mesh
 MeshAsGeomObject* Solid_mesh_geom_obj_pt;
 
 /// \short Vector of pairs containing pointers to elements and
 /// local coordinates within them for regularly spaced plot points
 Vector<std::pair<PORO_ELEMENT*,Vector<double> > > 
 Solid_regularly_spaced_plot_point;

 /// \short Mesh as geom object representation of inner poro mesh
 MeshAsGeomObject* Inner_poro_mesh_geom_obj_pt;
 
 /// \short Vector of pairs containing pointers to elements and
 /// local coordinates within them for regularly spaced plot points
 Vector<std::pair<PORO_ELEMENT*,Vector<double> > > 
 Inner_poro_regularly_spaced_plot_point;

 /// Pointer to the poroelasticity timestepper
 TimeStepper* Poro_time_stepper_pt;

 /// Pointer to the fluid timestepper
 TimeStepper* Fluid_time_stepper_pt;

 /// Trace file
 std::ofstream Trace_file;

 /// \short Enumeration for Lagrange multiplier IDs for Lagrange multipliers 
 /// that enforce BJS boundary condition on fluid from poro meshes
 enum
 {
  Lagrange_id_upper_fluid_solid,
  Lagrange_id_lower_fluid_solid
 };



 /// Enumeration of the solid boundaries
 enum
  {
   Solid_inlet_boundary_id,
   Solid_outer_boundary_id,
   Solid_outlet_boundary_id,
   Solid_fluid_interface_near_outlet_boundary_id,
   Solid_fluid_interface_lower_porous_block_boundary_id,
   Solid_porous_block_sym_boundary_id,
   Solid_fluid_interface_upper_porous_block_boundary_id,
   Solid_fluid_interface_near_inlet_boundary_id,
   Solid_inner_block_boundary_id
  };

 /// Enumeration of the upper fluid boundaries
 enum
  {
   Upper_fluid_block_boundary_id,
   Upper_fluid_symmetry_boundary_id,
   Upper_fluid_inflow_boundary_id,
   Upper_fluid_dura_boundary_id,
   Upper_fluid_bl_boundary_id
  };


 /// Enumeration of the lower fluid boundaries
 enum
  {
   Lower_fluid_block_boundary_id,
   Lower_fluid_symmetry_boundary_id,
   Lower_fluid_outflow_boundary_id,
   Lower_fluid_dura_boundary_id,
   Lower_fluid_bl_boundary_id
  };


 //hierher strip out!

 /// Enumeration of regions
 enum
  {
   Upper_cord_region_id, // Note: not specified therefore implied!
   Central_cord_region_id, 
   Lower_cord_region_id, 
   Dura_region_id,
   Block_region_id,
   Upper_pia_region_id,
   Central_pia_region_id,
   Lower_pia_region_id,
   Filum_region_id
  };

 /// Line visualiser in solid mesh
 LineVisualiser* Solid_mesh_visualiser_pt;

 /// Line visualiser in fluid mesh
 LineVisualiser* Fluid_mesh_visualiser_pt;


}; // end_of_problem_class

//===start_of_constructor=============================================
/// Problem constructor:
//====================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
PorousBlockProblem<PORO_ELEMENT, FLUID_ELEMENT>::
PorousBlockProblem()
{
 
 // Open trace file
 string name=Global_Physical_Variables::Directory+"/trace.dat";
 Trace_file.open(name.c_str());
 
 // Create timesteppers
 Poro_time_stepper_pt = new Newmark<2>;
 add_time_stepper_pt(Poro_time_stepper_pt);
 Fluid_time_stepper_pt = new BDF<2>;
 add_time_stepper_pt(Fluid_time_stepper_pt);

 // Create the problem geometry 

 // Update axial scale factor
 Global_Physical_Variables::Z_scale = Global_Physical_Variables::Length_scale;
 
 // Update regular output with equivalent axial spacing
 Global_Physical_Variables::Nplot_z_regular_solid=
  unsigned(double(Global_Physical_Variables::Nplot_r_regular_solid)*8.2/1.1);
 if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
 {
  oomph_info << "Regular solid output with: Nplot_r_regular Nplot_z_regular " 
             << Global_Physical_Variables::Nplot_r_regular_solid << " " 
             << Global_Physical_Variables::Nplot_z_regular_solid << std::endl;
 }

 // Update regular output with equivalent axial spacing
 Global_Physical_Variables::Nplot_z_regular_fluid=
  unsigned(double(Global_Physical_Variables::Nplot_r_regular_fluid)*8.2/1.1);
 if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
 {
  oomph_info << "Regular fluid output with: Nplot_r_regular Nplot_z_regular " 
             << Global_Physical_Variables::Nplot_r_regular_fluid << " " 
             << Global_Physical_Variables::Nplot_z_regular_fluid << std::endl;
 }


 //-----------------------------------------------------------------------------

 // Poro mesh 
 // ---------
 Vector<TriangleMeshCurveSection*> solid_outer_polyline_boundary_pt(8);

 // Inlet
 {
  Vector<Vector<double> > outer_coords(2, Vector<double>(2));
  outer_coords[0][0]=1.0;
  outer_coords[0][1]=8.2;
  
  outer_coords[1][0]=1.1;
  outer_coords[1][1]=8.2;
  
  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }

  // Build
  solid_outer_polyline_boundary_pt[Solid_inlet_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,Solid_inlet_boundary_id);
 }

 // Outer boundary
 {

  Vector<Vector<double> > outer_coords(2, Vector<double>(2));

  outer_coords[0][0]=1.1;
  outer_coords[0][1]=8.2;
  
  outer_coords[1][0]=1.1;
  outer_coords[1][1]=0.0;

  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }

  // Build
  solid_outer_polyline_boundary_pt[Solid_outer_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,Solid_outer_boundary_id);
 }


 // Outlet
 {
  Vector<Vector<double> > outer_coords(2, Vector<double>(2));

  outer_coords[0][0]=1.1;
  outer_coords[0][1]=0.0;

  outer_coords[1][0]=1.0;
  outer_coords[1][1]=0.0;

  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  solid_outer_polyline_boundary_pt[Solid_outlet_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,Solid_outlet_boundary_id);
 }

 // Inner/FSI near outlet
 {
  Vector<Vector<double> > outer_coords(2, Vector<double>(2));

  outer_coords[0][0]=1.0;
  outer_coords[0][1]=0.0;
  
  outer_coords[1][0]=1.0;
  outer_coords[1][1]=3.0;

  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  solid_outer_polyline_boundary_pt[
   Solid_fluid_interface_near_outlet_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,
                            Solid_fluid_interface_near_outlet_boundary_id);
 }



 // Block FSI near outlet
 {
  Vector<Vector<double> > outer_coords(2, Vector<double>(2));

  outer_coords[0][0]=1.0;
  outer_coords[0][1]=3.0;

  outer_coords[1][0]=0.0;
  outer_coords[1][1]=3.0;
  
  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  solid_outer_polyline_boundary_pt[
   Solid_fluid_interface_lower_porous_block_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,
                            Solid_fluid_interface_lower_porous_block_boundary_id);
 }

 // Block sym bc
 {
  Vector<Vector<double> > outer_coords(2, Vector<double>(2));

  outer_coords[0][0]=0.0;
  outer_coords[0][1]=3.0;

  outer_coords[1][0]=0.0;
  outer_coords[1][1]=3.2;
  
  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  solid_outer_polyline_boundary_pt[Solid_porous_block_sym_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,Solid_porous_block_sym_boundary_id);
 }



 // Block FSI near inlet
 {
  Vector<Vector<double> > outer_coords(2, Vector<double>(2));

  outer_coords[0][0]=0.0;
  outer_coords[0][1]=3.2;

  outer_coords[1][0]=1.0;
  outer_coords[1][1]=3.2;
  
  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  solid_outer_polyline_boundary_pt[
   Solid_fluid_interface_upper_porous_block_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,
                            Solid_fluid_interface_upper_porous_block_boundary_id);
 }


 // Inner/FSI near inlet
 {
  Vector<Vector<double> > outer_coords(2, Vector<double>(2));
  
  outer_coords[0][0]=1.0;
  outer_coords[0][1]=3.2;
    
  outer_coords[1][0]=1.0;
  outer_coords[1][1]=8.2;
  
  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  solid_outer_polyline_boundary_pt[
   Solid_fluid_interface_near_inlet_boundary_id] =
   new TriangleMeshPolyLine(outer_coords,
                            Solid_fluid_interface_near_inlet_boundary_id);
 }

 TriangleMeshClosedCurve * solid_outer_boundary_pt =
  new TriangleMeshClosedCurve(solid_outer_polyline_boundary_pt);

 // Inner boundary -- separation of block from rest
 Vector<TriangleMeshOpenCurve*> outer_inner_open_boundary_pt(1);

 // Boundary of block
 {
  Vector<Vector<double> > outer_coords(2, Vector<double>(2,0.0));
  
  outer_coords[0][0]=1.0;
  outer_coords[0][1]=3.0;
  
  outer_coords[1][0]=1.0;
  outer_coords[1][1]=3.2;
  
  // Rescale
  unsigned n=outer_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    outer_coords[j][0]*=Global_Physical_Variables::R_scale;
    outer_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }

  // Build
  TriangleMeshPolyLine *outer_inner_polyline_pt =
   new TriangleMeshPolyLine(outer_coords,
                            Solid_inner_block_boundary_id);
  
  // Connect initial vertex to first (starting from zeroth) vertex 
  // on fsi boundary near outlet
  outer_inner_polyline_pt->connect_initial_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>
   (solid_outer_polyline_boundary_pt
    [Solid_fluid_interface_near_outlet_boundary_id]),1);
  
  // Connect final vertex to first (starting from zero) vertex of
  // the upper fsi boundary of block block
  outer_inner_polyline_pt->connect_final_vertex_to_polyline(
   dynamic_cast<TriangleMeshPolyLine*>
   (solid_outer_polyline_boundary_pt[
    Solid_fluid_interface_upper_porous_block_boundary_id]),1);

  // Store in vector
  Vector<TriangleMeshCurveSection*> outer_inner_curve_section_pt(1);
  outer_inner_curve_section_pt[0] = outer_inner_polyline_pt;
  
  // Create internal open curve
  outer_inner_open_boundary_pt[0] =
   new TriangleMeshOpenCurve(outer_inner_curve_section_pt);  
 }
 
 // Mesh parameters
 TriangleMeshParameters
  solid_mesh_parameters(solid_outer_boundary_pt);
 solid_mesh_parameters.element_area() =
  Global_Physical_Variables::Element_area_solid;
 
 // Define inner boundaries
 solid_mesh_parameters.internal_open_curves_pt() =
  outer_inner_open_boundary_pt;

 // Define region coordinates for outer region (dura)
 Vector<double> region_coords(2);
 region_coords[0] = Global_Physical_Variables::R_scale*1.05;
 region_coords[1] = Global_Physical_Variables::Z_scale*3.1;
 solid_mesh_parameters.add_region_coordinates(Dura_region_id
                                                   ,region_coords);

 // Define region coordinates for block
 region_coords[0] = Global_Physical_Variables::R_scale*0.5;
 region_coords[1] = Global_Physical_Variables::Z_scale*3.1;
 solid_mesh_parameters.add_region_coordinates(Block_region_id,
                                                   region_coords);

 // Build mesh
 Solid_mesh_pt = new TriangleMesh<PORO_ELEMENT>(
   solid_mesh_parameters, Poro_time_stepper_pt);

 // Mesh as geom object representation of outer poro mesh
 Solid_mesh_geom_obj_pt=new MeshAsGeomObject(Solid_mesh_pt);
 
 // Extract regularly spaced points
 if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
  {
   double t_start = TimingHelpers::timer();

   // Speed up search (somewhat hand-tuned; adjust if this takes too long
   // or misses too many points)
   Solid_mesh_geom_obj_pt->max_spiral_level()=4;
   unsigned nx_back= Multi_domain_functions::Nx_bin;
   Multi_domain_functions::Nx_bin=10;
   unsigned ny_back= Multi_domain_functions::Ny_bin;
   Multi_domain_functions::Ny_bin=10;
   
   // Populate it...
   Vector<double> x(2);
   Vector<double> s(2);
   for (unsigned ir=0;ir<Global_Physical_Variables::Nplot_r_regular_solid;ir++)
    {
     // outermost radius of dura
     x[0]=double(ir)/double(Global_Physical_Variables::Nplot_r_regular_solid-1)*0.55;
     for (unsigned iz=0;iz<Global_Physical_Variables::Nplot_z_regular_solid;iz++)
      {
       x[1]=double(iz)/double(Global_Physical_Variables::Nplot_z_regular_solid-1)*
        (8.2)*Global_Physical_Variables::Z_scale;
       
       // Pointer to GeomObject that contains this point
       GeomObject* geom_obj_pt=0;
       
       // Get it
       Solid_mesh_geom_obj_pt->locate_zeta(x,geom_obj_pt,s);
       
       // Store it
       if (geom_obj_pt!=0)
        {
         std::pair<PORO_ELEMENT*,Vector<double> > tmp;
         tmp = std::make_pair(dynamic_cast<PORO_ELEMENT*>(geom_obj_pt),s);
         Solid_regularly_spaced_plot_point.push_back(tmp);
        }
      }
    }

   // Reset number of bins in binning method 
   Multi_domain_functions::Nx_bin=nx_back;
   Multi_domain_functions::Ny_bin=ny_back;
   oomph_info << "Took: " << TimingHelpers::timer()-t_start
              << " sec to setup regularly spaced points for outer poro mesh\n";
  }
 

 // Output mesh and boundaries
 Solid_mesh_pt->output
  (Global_Physical_Variables::Directory+"/solid_mesh.dat",2);
 Solid_mesh_pt->output_boundaries
  (Global_Physical_Variables::Directory+"/solid_mesh_boundaries.dat");
 

 // Line visualiser
 //----------------
 {
  // Halfway through the plug
  double z=1.55;
  
  // From inside to outer boundary
  double r_min=0.0;
  double r_max=0.55;
  
  // How many points to you want?
  unsigned npt=100;
  Vector<Vector<double> > coord_vec(npt);
  for (unsigned j=0;j<npt;j++)
   {
    coord_vec[j].resize(2);
    coord_vec[j][0]=r_min+(r_max-r_min)*double(j)/unsigned(npt-1);
    coord_vec[j][1]=z;
   }
  
  // Setup line visualiser 
  Solid_mesh_visualiser_pt=new LineVisualiser(Solid_mesh_pt,
                                                       coord_vec);
 }


 // ----------------------------------------------------------------------------

 // Upper fluid mesh
 // ----------------
 Vector<TriangleMeshCurveSection*> upper_fluid_outer_polyline_boundary_pt(4);

 //  FSI interface with block
 {
  Vector<Vector<double> > upper_coords(2, Vector<double>(2));
  
  upper_coords[0][0]=1.0;
  upper_coords[0][1]=3.2;

  upper_coords[1][0]=0.0;
  upper_coords[1][1]=3.2;
  
  // Rescale
  unsigned n=upper_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    upper_coords[j][0]*=Global_Physical_Variables::R_scale;
    upper_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  upper_fluid_outer_polyline_boundary_pt[
   Upper_fluid_block_boundary_id]=
   new TriangleMeshPolyLine(
    upper_coords,
    Upper_fluid_block_boundary_id);
 }
 
 //  Sym bc
 {
  Vector<Vector<double> > upper_coords(2, Vector<double>(2));
  
  upper_coords[0][0]=0.0;
  upper_coords[0][1]=3.2;

  upper_coords[1][0]=0.0;
  upper_coords[1][1]=8.2;
  
  // Rescale
  unsigned n=upper_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    upper_coords[j][0]*=Global_Physical_Variables::R_scale;
    upper_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  upper_fluid_outer_polyline_boundary_pt[
   Upper_fluid_symmetry_boundary_id]=
   new TriangleMeshPolyLine(
    upper_coords,
    Upper_fluid_symmetry_boundary_id);
 }
 

 // Inlet
 {
  Vector<Vector<double> > upper_coords(4, Vector<double>(2));

  upper_coords[0][0]=0.0;
  upper_coords[0][1]=8.2;

  upper_coords[1][0]=0.0+Global_Physical_Variables::BL_thick;
  upper_coords[1][1]=8.2;

  upper_coords[2][0]=1.0-Global_Physical_Variables::BL_thick;
  upper_coords[2][1]=8.2;

  upper_coords[3][0]=1.0;
  upper_coords[3][1]=8.2;

  // Rescale
  unsigned n=upper_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    upper_coords[j][0]*=Global_Physical_Variables::R_scale;
    upper_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  upper_fluid_outer_polyline_boundary_pt[
   Upper_fluid_inflow_boundary_id]=
   new TriangleMeshPolyLine(upper_coords,Upper_fluid_inflow_boundary_id);
 }
 
 // FSI with dura
 {
  Vector<Vector<double> > upper_coords;
  Vector<double> vert(2);
  vert[0]=1.0;
  vert[1]=8.2;
  upper_coords.push_back(vert);

  vert[0]=1.0;
  vert[1]=3.2;
  upper_coords.push_back(vert);

  // Rescale
  unsigned n=upper_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    upper_coords[j][0]*=Global_Physical_Variables::R_scale;
    upper_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  upper_fluid_outer_polyline_boundary_pt[
   Upper_fluid_dura_boundary_id]=
   new TriangleMeshPolyLine(upper_coords,
                            Upper_fluid_dura_boundary_id);
 }


 TriangleMeshClosedCurve * upper_fluid_outer_boundary_pt =
  new TriangleMeshClosedCurve(upper_fluid_outer_polyline_boundary_pt);

 // Internal boundary for BL
 Vector<TriangleMeshOpenCurve*> inner_open_boundary_pt(1);
 TriangleMeshPolyLine* inner_polyline_pt = 0;

 {
  Vector<Vector<double> > inner_coords(4, Vector<double>(2,0.0));
  
  inner_coords[0][0]=1.0-Global_Physical_Variables::BL_thick;
  inner_coords[0][1]=8.2;

  inner_coords[1][0]=1.0-Global_Physical_Variables::BL_thick;
  inner_coords[1][1]=3.2+Global_Physical_Variables::BL_thick;

  inner_coords[2][0]=0.0+Global_Physical_Variables::BL_thick;
  inner_coords[2][1]=3.2+Global_Physical_Variables::BL_thick;

  inner_coords[3][0]=0.0+Global_Physical_Variables::BL_thick;
  inner_coords[3][1]=8.2;

  
  // Rescale
  for(unsigned i=0;i<4;i++)
   {
    inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_polyline_pt =
   new TriangleMeshPolyLine(inner_coords,
                            Upper_fluid_bl_boundary_id);
 }

 // Connect initial vertex to second (from zeroth) vertex on inlet boundary
 inner_polyline_pt->connect_initial_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (upper_fluid_outer_polyline_boundary_pt[Upper_fluid_inflow_boundary_id]),2);
 
 // Connect final vertex to first (from zeroth) vertex on inlet boundary
 inner_polyline_pt->connect_final_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (upper_fluid_outer_polyline_boundary_pt[Upper_fluid_inflow_boundary_id]),1);
 
 
 // Store in vector
 Vector<TriangleMeshCurveSection*> inner_curve_section_pt(1);
 inner_curve_section_pt[0] = inner_polyline_pt;
 
 // Create internal open curve
 inner_open_boundary_pt[0] = new TriangleMeshOpenCurve(inner_curve_section_pt);
 
 // Mesh parameters
 TriangleMeshParameters
  upper_fluid_mesh_parameters(upper_fluid_outer_boundary_pt);

 upper_fluid_mesh_parameters.element_area() =
  Global_Physical_Variables::Element_area_fluid;

 // Define inner boundaries
 upper_fluid_mesh_parameters.internal_open_curves_pt()=inner_open_boundary_pt;
 
 // Build mesh
 Upper_fluid_mesh_pt = new TriangleMesh<FLUID_ELEMENT>(
   upper_fluid_mesh_parameters, Fluid_time_stepper_pt);

 // Mesh as geom object representation of syrinx fluid mesh
 Upper_fluid_mesh_geom_obj_pt=new MeshAsGeomObject(Upper_fluid_mesh_pt);
 
 // Extract regularly spaced points
 if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
  {
   double t_start = TimingHelpers::timer();

   // Speed up search (somewhat hand-tuned; adjust if this takes too long
   // or misses too many points)
   Upper_fluid_mesh_geom_obj_pt->max_spiral_level()=4;
   unsigned nx_back= Multi_domain_functions::Nx_bin;
   Multi_domain_functions::Nx_bin=10;
   unsigned ny_back= Multi_domain_functions::Ny_bin;
   Multi_domain_functions::Ny_bin=10;

   // Populate it...
   Vector<double> x(2);
   Vector<double> s(2);
   for (unsigned ir=0;ir<Global_Physical_Variables::Nplot_r_regular_fluid;ir++)
    {
     // outermost radius of dura
     x[0]=double(ir)/double(Global_Physical_Variables::Nplot_r_regular_fluid-1)*0.55; 
     for (unsigned iz=0;iz<Global_Physical_Variables::Nplot_z_regular_fluid;iz++)
      {
       x[1]=double(iz)/double(Global_Physical_Variables::Nplot_z_regular_fluid-1)*
        (8.2)*Global_Physical_Variables::Z_scale;
       
       // Pointer to GeomObject that contains this point
       GeomObject* geom_obj_pt=0;
       
       // Get it
       Upper_fluid_mesh_geom_obj_pt->locate_zeta(x,geom_obj_pt,s);

       // Store it
       if (geom_obj_pt!=0)
        {
         std::pair<FLUID_ELEMENT*,Vector<double> > tmp;
         tmp = std::make_pair(dynamic_cast<FLUID_ELEMENT*>(geom_obj_pt),s);
         Upper_fluid_regularly_spaced_plot_point.push_back(tmp);
        }
      }
    }

   // Reset number of bins in binning method 
   Multi_domain_functions::Nx_bin=nx_back;
   Multi_domain_functions::Ny_bin=ny_back;
   oomph_info << "Took: " << TimingHelpers::timer()-t_start
              << " sec to setup regularly spaced points for upper mesh\n";
  }
 
  // Output mesh and boundaries
 Upper_fluid_mesh_pt->output
  (Global_Physical_Variables::Directory+"/upper_fluid_mesh.dat",2);
 Upper_fluid_mesh_pt->output_boundaries
  (Global_Physical_Variables::Directory+"/upper_fluid_mesh_boundaries.dat");
 

 // Line visualiser
 //----------------
 {
  // Somewhere in upper fluid
  double z=1.8;
  
  // From inside to outer boundary
  double r_min=0.0;
  double r_max=0.5;
  
  // How many points to you want?
  unsigned npt=100;
  Vector<Vector<double> > coord_vec(npt);
  for (unsigned j=0;j<npt;j++)
   {
    coord_vec[j].resize(2);
    coord_vec[j][0]=r_min+(r_max-r_min)*double(j)/unsigned(npt-1);
    coord_vec[j][1]=z;
   }
  
  // Setup line visualiser 
  Fluid_mesh_visualiser_pt=new LineVisualiser(Upper_fluid_mesh_pt,
                                                      coord_vec);
 }
  
  
 // ----------------------------------------------------------------------------

 // Lower fluid mesh
 // ----------------
 Vector<TriangleMeshCurveSection*> lower_fluid_outer_polyline_boundary_pt(4);

 //  FSI interface with block
 {
  Vector<Vector<double> > lower_coords(2, Vector<double>(2));
  
  lower_coords[0][0]=1.0;
  lower_coords[0][1]=3.0;

  lower_coords[1][0]=0.0;
  lower_coords[1][1]=3.0;
  
  // Rescale
  unsigned n=lower_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    lower_coords[j][0]*=Global_Physical_Variables::R_scale;
    lower_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  lower_fluid_outer_polyline_boundary_pt[
   Lower_fluid_block_boundary_id]=
   new TriangleMeshPolyLine(
    lower_coords,
    Lower_fluid_block_boundary_id);
 }
 
 //  Sym bc
 {
  Vector<Vector<double> > lower_coords(2, Vector<double>(2));
  
  lower_coords[0][0]=0.0;
  lower_coords[0][1]=3.0;

  lower_coords[1][0]=0.0;
  lower_coords[1][1]=0.0;
  
  // Rescale
  unsigned n=lower_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    lower_coords[j][0]*=Global_Physical_Variables::R_scale;
    lower_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  lower_fluid_outer_polyline_boundary_pt[
   Lower_fluid_symmetry_boundary_id]=
   new TriangleMeshPolyLine(
    lower_coords,
    Lower_fluid_symmetry_boundary_id);
 }
 

 // Outlet
 {
  Vector<Vector<double> > lower_coords(4, Vector<double>(2));

  lower_coords[0][0]=0.0;
  lower_coords[0][1]=0.0;

  lower_coords[1][0]=0.0+Global_Physical_Variables::BL_thick;
  lower_coords[1][1]=0.0;

  lower_coords[2][0]=1.0-Global_Physical_Variables::BL_thick;
  lower_coords[2][1]=0.0;

  lower_coords[3][0]=1.0;
  lower_coords[3][1]=0.0;

  // Rescale
  unsigned n=lower_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    lower_coords[j][0]*=Global_Physical_Variables::R_scale;
    lower_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  lower_fluid_outer_polyline_boundary_pt[
   Lower_fluid_outflow_boundary_id]=
   new TriangleMeshPolyLine(lower_coords,Lower_fluid_outflow_boundary_id);
 }
 
 // FSI with dura
 {
  Vector<Vector<double> > lower_coords;
  Vector<double> vert(2);
  vert[0]=1.0;
  vert[1]=0.0;
  lower_coords.push_back(vert);

  vert[0]=1.0;
  vert[1]=3.0;
  lower_coords.push_back(vert);

  // Rescale
  unsigned n=lower_coords.size();
  for (unsigned j=0;j<n;j++)
   {
    lower_coords[j][0]*=Global_Physical_Variables::R_scale;
    lower_coords[j][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  lower_fluid_outer_polyline_boundary_pt[
   Lower_fluid_dura_boundary_id]=
   new TriangleMeshPolyLine(lower_coords,
                            Lower_fluid_dura_boundary_id);
 }


 TriangleMeshClosedCurve * lower_fluid_outer_boundary_pt =
  new TriangleMeshClosedCurve(lower_fluid_outer_polyline_boundary_pt);

 // Internal boundary for BL
 inner_open_boundary_pt.resize(1);
 inner_polyline_pt = 0;

 {
  Vector<Vector<double> > inner_coords(4, Vector<double>(2,0.0));
  
  inner_coords[0][0]=1.0-Global_Physical_Variables::BL_thick;
  inner_coords[0][1]=0.0;

  inner_coords[1][0]=1.0-Global_Physical_Variables::BL_thick;
  inner_coords[1][1]=3.0-Global_Physical_Variables::BL_thick;

  inner_coords[2][0]=0.0+Global_Physical_Variables::BL_thick;
  inner_coords[2][1]=3.0-Global_Physical_Variables::BL_thick;

  inner_coords[3][0]=0.0+Global_Physical_Variables::BL_thick;
  inner_coords[3][1]=0.0;

  
  // Rescale
  for(unsigned i=0;i<4;i++)
   {
    inner_coords[i][0]*=Global_Physical_Variables::R_scale;
    inner_coords[i][1]*=Global_Physical_Variables::Z_scale;
   }
  
  // Build
  inner_polyline_pt =
   new TriangleMeshPolyLine(inner_coords,
                            Lower_fluid_bl_boundary_id);
 }

 // Connect initial vertex to second (from zeroth) vertex on outlet boundary
 inner_polyline_pt->connect_initial_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (lower_fluid_outer_polyline_boundary_pt[Lower_fluid_outflow_boundary_id]),2);
 
 // Connect final vertex to first (from zeroth) vertex on outlet boundary
 inner_polyline_pt->connect_final_vertex_to_polyline(
  dynamic_cast<TriangleMeshPolyLine*>
  (lower_fluid_outer_polyline_boundary_pt[Lower_fluid_outflow_boundary_id]),1);
 
 
 // Store in vector
 inner_curve_section_pt.resize(1);
 inner_curve_section_pt[0] = inner_polyline_pt;
 
 // Create internal open curve
 inner_open_boundary_pt[0] = new TriangleMeshOpenCurve(inner_curve_section_pt);
 
 // Mesh parameters
 TriangleMeshParameters
  lower_fluid_mesh_parameters(lower_fluid_outer_boundary_pt);

 lower_fluid_mesh_parameters.element_area() =
  Global_Physical_Variables::Element_area_fluid;

 // Define inner boundaries
 lower_fluid_mesh_parameters.internal_open_curves_pt()=inner_open_boundary_pt;
 
 // Build mesh
 Lower_fluid_mesh_pt = new TriangleMesh<FLUID_ELEMENT>(
   lower_fluid_mesh_parameters, Fluid_time_stepper_pt);

 // Mesh as geom object representation of syrinx fluid mesh
 Lower_fluid_mesh_geom_obj_pt=new MeshAsGeomObject(Lower_fluid_mesh_pt);
 
 // Extract regularly spaced points
 if (!Global_Physical_Variables::Suppress_regularly_spaced_output)
  {
   double t_start = TimingHelpers::timer();

   // Speed up search (somewhat hand-tuned; adjust if this takes too long
   // or misses too many points)
   Lower_fluid_mesh_geom_obj_pt->max_spiral_level()=4;
   unsigned nx_back= Multi_domain_functions::Nx_bin;
   Multi_domain_functions::Nx_bin=10;
   unsigned ny_back= Multi_domain_functions::Ny_bin;
   Multi_domain_functions::Ny_bin=10;

   // Populate it...
   Vector<double> x(2);
   Vector<double> s(2);
   for (unsigned ir=0;ir<Global_Physical_Variables::Nplot_r_regular_fluid;ir++)
    {
     // outermost radius of dura
     x[0]=double(ir)/double(Global_Physical_Variables::Nplot_r_regular_fluid-1)*0.55; 
     for (unsigned iz=0;iz<Global_Physical_Variables::Nplot_z_regular_fluid;iz++)
      {
       x[1]=double(iz)/double(Global_Physical_Variables::Nplot_z_regular_fluid-1)*
        (8.2)*Global_Physical_Variables::Z_scale;
       
       // Pointer to GeomObject that contains this point
       GeomObject* geom_obj_pt=0;
       
       // Get it
       Lower_fluid_mesh_geom_obj_pt->locate_zeta(x,geom_obj_pt,s);

       // Store it
       if (geom_obj_pt!=0)
        {
         std::pair<FLUID_ELEMENT*,Vector<double> > tmp;
         tmp = std::make_pair(dynamic_cast<FLUID_ELEMENT*>(geom_obj_pt),s);
         Lower_fluid_regularly_spaced_plot_point.push_back(tmp);
        }
      }
    }

   // Reset number of bins in binning method 
   Multi_domain_functions::Nx_bin=nx_back;
   Multi_domain_functions::Ny_bin=ny_back;
   oomph_info << "Took: " << TimingHelpers::timer()-t_start
              << " sec to setup regularly spaced points for lower mesh\n";
  }
 
  // Output mesh and boundaries
 Lower_fluid_mesh_pt->output
  (Global_Physical_Variables::Directory+"/lower_fluid_mesh.dat",2);
 Lower_fluid_mesh_pt->output_boundaries
  (Global_Physical_Variables::Directory+"/lower_fluid_mesh_boundaries.dat");
 
 // ---------------------------------------------------------------------------
 
 // Mesh that applies fsi traction from upper fluid to solid (on dura)
 Solid_upper_fluid_dura_FSI_surface_mesh_pt=new Mesh;

 // Mesh that applies fsi traction from upper fluid to solid (on block)
 Solid_upper_fluid_block_FSI_surface_mesh_pt=new Mesh;
 
 // Mesh that applies BJS BCs onto upper fluid (on dura)
 Upper_fluid_dura_FSI_surface_mesh_pt=new Mesh;

 // Mesh that applies BJS BCs onto upper fluid (on block)
 Upper_fluid_block_FSI_surface_mesh_pt=new Mesh;



 // Mesh that applies fsi traction from lower fluid to solid (on dura)
 Solid_lower_fluid_dura_FSI_surface_mesh_pt=new Mesh;

 // Mesh that applies fsi traction from lower fluid to solid (on block)
 Solid_lower_fluid_block_FSI_surface_mesh_pt=new Mesh;
 
 // Mesh that applies BJS BCs onto lower fluid (on dura)
 Lower_fluid_dura_FSI_surface_mesh_pt=new Mesh;

 // Mesh that applies BJS BCs onto lower fluid (on block)
 Lower_fluid_block_FSI_surface_mesh_pt=new Mesh;

 // Create the surface mesh for the inflow boundary
 Inflow_fluid_surface_mesh_pt = new Mesh;
 

 // ---------------------------------------------------------------------------

 // Make surface meshes
 create_fluid_face_elements();
 create_poro_face_elements();

 // ----------------------------------------------------------------------------

 // Complete the problem setup to make the elements fully functional
 
 // Do edge sign setup for outer poro mesh
 Global_Physical_Variables::edge_sign_setup<PORO_ELEMENT>(Solid_mesh_pt);


 // Loop over the elements in block region of solid mesh
 //-----------------------------------------------------
 {

  ofstream some_file;
  std::string name;
  name=Global_Physical_Variables::Directory+"/block_region.dat";
  some_file.open(name.c_str());

  unsigned r=Block_region_id;
  unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Solid_mesh_pt)->
   nregion_element(r);
  for(unsigned e=0;e<nel;e++)
   {
    // Cast to a bulk element
    PORO_ELEMENT *el_pt=dynamic_cast<PORO_ELEMENT*>(
     dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Solid_mesh_pt)
     ->region_element_pt(r,e));
        
    // Output region just using vertices
    el_pt->output(some_file,2);

    // Set the pointer to the Lambda^2 parameter (universal)
    el_pt->lambda_sq_pt() = &Global_Physical_Variables::Lambda_sq;
    
    // Set the pointer to non-dim permeability (universal)
    el_pt->permeability_pt()= &Global_Physical_Variables::Permeability;
    
    // Set the pointer to the Biot parameter (same for all; incompressible
    // fluid and solid)
    el_pt->alpha_pt()=&Global_Physical_Variables::Alpha;
    
    // Set the pointer to the density ratio (pore fluid to solid) (same for all)
    el_pt->density_ratio_pt()= &Global_Physical_Variables::Density_ratio;
    
    // Set the pointer to Poisson's ratio
    el_pt->nu_pt() = &Global_Physical_Variables::Nu_filum_and_block;
    
    // Set the pointer to non-dim Young's modulus (ratio of Young's modulus
    // to Young's modulus used in the non-dimensionalisation of the
    // equations
    el_pt->youngs_modulus_pt() = 
     &Global_Physical_Variables::Stiffness_ratio_filum_and_block;
    
    // Set the pointer to permeability ratio (ratio of actual permeability
    // to the one used in the non-dim of the equations)
    el_pt->permeability_ratio_pt()=
     &Global_Physical_Variables::Permeability_ratio_filum_and_block;
    
    // Set the pointer to the porosity
    el_pt->porosity_pt()=&Global_Physical_Variables::Porosity_filum_and_block;
    
    // Set the internal q dofs' timestepper to the problem timestepper
    el_pt->set_q_internal_timestepper(Poro_time_stepper_pt);
    
    // Switch off Darcy flow?
    if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))
     {
      oomph_info << "Switching off darcy in block\n";
      el_pt->switch_off_darcy();
     }

  }// end loop over block elements

  some_file.close();
 }

 // Loop over the elements in dura region of solid mesh
 //----------------------------------------------------
 {
  ofstream some_file;
  std::string name;
  name=Global_Physical_Variables::Directory+"/dura_region.dat";
  some_file.open(name.c_str());

  unsigned r=Dura_region_id;
  unsigned nel=dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Solid_mesh_pt)->
   nregion_element(r);
  for(unsigned e=0;e<nel;e++)
   {
    // Cast to a bulk element
    PORO_ELEMENT *el_pt=dynamic_cast<PORO_ELEMENT*>(
     dynamic_cast<TriangleMesh<PORO_ELEMENT>*>(Solid_mesh_pt)
     ->region_element_pt(r,e));

    // Output region just using vertices
    el_pt->output(some_file,2);

    // Set the pointer to the Lambda^2 parameter (universal)
    el_pt->lambda_sq_pt() = &Global_Physical_Variables::Lambda_sq;
    
    // Set the pointer to non-dim permeability (universal)
    el_pt->permeability_pt()= &Global_Physical_Variables::Permeability;
    
    // Set the pointer to the Biot parameter (same for all; incompressible
    // fluid and solid)
    el_pt->alpha_pt()=&Global_Physical_Variables::Alpha;
    
    // Set the pointer to the density ratio (pore fluid to solid) (same for all)
    el_pt->density_ratio_pt()= &Global_Physical_Variables::Density_ratio;
    
    // Set the pointer to Poisson's ratio
    el_pt->nu_pt() = &Global_Physical_Variables::Nu_dura_and_pia;
    
    // Set the pointer to non-dim Young's modulus (ratio of Young's modulus
    // to Young's modulus used in the non-dimensionalisation of the
    // equations
    el_pt->youngs_modulus_pt() = 
     &Global_Physical_Variables::Stiffness_ratio_dura_and_pia;
    
    // Set the pointer to permeability ratio (ratio of actual permeability
    // to the one used in the non-dim of the equations)
    el_pt->permeability_ratio_pt()=
     &Global_Physical_Variables::Permeability_ratio_dura_and_pia;
    
    // Set the pointer to the porosity
    el_pt->porosity_pt()=&Global_Physical_Variables::Porosity_dura_and_pia;
    
    // Set the internal q dofs' timestepper to the problem timestepper
    el_pt->set_q_internal_timestepper(Poro_time_stepper_pt);
    
    // Switch off Darcy flow?
    if ((CommandLineArgs::command_line_flag_has_been_set("--pin_darcy"))||
        (!CommandLineArgs::command_line_flag_has_been_set
         ("--everything_is_porous")))
     {
      oomph_info << "Switching off darcy in dura\n";
      el_pt->switch_off_darcy();
     }

  }// end loop over dura elements

  some_file.close();
 }


 // Loop over the elements in upper fluid mesh to set physical parameters etc
 //------------------------------------------------------------------------
 unsigned n_el_upper_fluid = Upper_fluid_mesh_pt->nelement();
 for(unsigned e=0;e<n_el_upper_fluid;e++)
  {
   // Cast to the particular element type
   FLUID_ELEMENT *el_pt = dynamic_cast<FLUID_ELEMENT*>
    (Upper_fluid_mesh_pt->element_pt(e));

   // There is no need for ALE
   el_pt->disable_ALE();

   // Set the Reynolds number for each element
   el_pt->re_pt() = &Global_Physical_Variables::Re;

   // Set the product of Reynolds and Strouhal numbers
   el_pt->re_st_pt() = &Global_Physical_Variables::Wo;

   // Pin u_theta at all nodes (no swirl velocity)
   unsigned n_node = el_pt->nnode();
   for(unsigned n=0;n<n_node;n++)
    {
     Node *nod_pt = el_pt->node_pt(n);
     nod_pt->pin(2);
    }
  }

  
 // Loop over the elements in lower fluid mesh to set physical parameters etc
 //------------------------------------------------------------------------
 unsigned n_el_lower_fluid = Lower_fluid_mesh_pt->nelement();
 for(unsigned e=0;e<n_el_lower_fluid;e++)
  {
   // Cast to the particular element type
   FLUID_ELEMENT *el_pt = dynamic_cast<FLUID_ELEMENT*>
    (Lower_fluid_mesh_pt->element_pt(e));

   // There is no need for ALE
   el_pt->disable_ALE();

   // Set the Reynolds number for each element
   el_pt->re_pt() = &Global_Physical_Variables::Re;

   // Set the product of Reynolds and Strouhal numbers
   el_pt->re_st_pt() = &Global_Physical_Variables::Wo;

   // Pin u_theta at all nodes (no swirl velocity)
   unsigned n_node = el_pt->nnode();
   for(unsigned n=0;n<n_node;n++)
    {
     Node *nod_pt = el_pt->node_pt(n);
     nod_pt->pin(2);
    }
  }


 // ------------------------------------------------------------------------

 // Boundary conditions for the solid mesh
 // --------------------------------------

 // Inlet: no porous flux
 {
  unsigned ibound=Solid_inlet_boundary_id;
  unsigned num_nod=Solid_mesh_pt->nboundary_node(ibound);
  for(unsigned inod=0;inod<num_nod;inod++)
   {
    // Get pointer to node
    Node* nod_pt=Solid_mesh_pt->boundary_node_pt(ibound,inod);
    
    // If this node stores edge flux dofs, pin these
    if(nod_pt->nvalue()==4)
     {
      nod_pt->pin(2);
      nod_pt->pin(3);
      oomph_info << "Pinning porous flux at "
                 << nod_pt->x(0) << " "
                 << nod_pt->x(1) << "\n";
     }
   }
 }

 // Outlet: pin axial displacements and no porous flux
 {
  unsigned ibound=Solid_outlet_boundary_id;
  unsigned num_nod=Solid_mesh_pt->nboundary_node(ibound);
  for(unsigned inod=0;inod<num_nod;inod++)
   {
    // Get pointer to node
    Node* nod_pt=Solid_mesh_pt->boundary_node_pt(ibound,inod);

    // Pin in z
    nod_pt->pin(1);
    oomph_info << "Pinning axial displacement ";

    // If this node stores edge flux dofs, pin these
    if(nod_pt->nvalue()==4)
     {
      nod_pt->pin(2);
      nod_pt->pin(3);
      oomph_info << "and porous flux "; 
     }
    oomph_info << " at " 
               << nod_pt->x(0) << " "
               << nod_pt->x(1) << "\n";
   }
 }


 // Symmetry: pin radial displacements and no flux
 {
  unsigned ibound=Solid_porous_block_sym_boundary_id;
  unsigned num_nod=Solid_mesh_pt->nboundary_node(ibound);
  for(unsigned inod=0;inod<num_nod;inod++)
   {
    // Get pointer to node
    Node* nod_pt=Solid_mesh_pt->boundary_node_pt(ibound,inod);

    // Pin in r
    nod_pt->pin(0);
    
    oomph_info << "Pinning radial displacement ";

    // If this node stores edge flux dofs, pin these
    if(nod_pt->nvalue()==4)
     {
      nod_pt->pin(2);
      nod_pt->pin(3);

      oomph_info << "and porous flux "; 
     }
    oomph_info << " at " 
               << nod_pt->x(0) << " "
               << nod_pt->x(1) << "\n";
   }
 }




 // Boundary conditions for the upper fluid mesh
 // --------------------------------------------

 // Inlet
 {
  unsigned ibound=Upper_fluid_inflow_boundary_id;
  unsigned num_nod=Upper_fluid_mesh_pt->nboundary_node(ibound);
  for(unsigned inod=0;inod<num_nod;inod++)
   {
    // Get pointer to node
    Node* nod_pt=Upper_fluid_mesh_pt->boundary_node_pt(ibound,inod);
    
    // Pin radial velocity (parallel in/outflow)
    if (CommandLineArgs::command_line_flag_has_been_set
        ("--pin_radial_veloc_at_in_and_outlet"))
     {
      nod_pt->pin(0);
     }
   }
 }
 
 // Symmetry
 {
  unsigned ibound=Upper_fluid_symmetry_boundary_id;
  unsigned num_nod=Upper_fluid_mesh_pt->nboundary_node(ibound);
  for(unsigned inod=0;inod<num_nod;inod++)
   {
    // Get pointer to node
    Node* nod_pt=Upper_fluid_mesh_pt->boundary_node_pt(ibound,inod);
    
    // Pin radial velocity 
    nod_pt->pin(0);
   }
 }


 // Boundary conditions for the lower fluid mesh
 // --------------------------------------------

 // Outlet
 {
  unsigned ibound=Lower_fluid_outflow_boundary_id;
  unsigned num_nod=Lower_fluid_mesh_pt->nboundary_node(ibound);
  for(unsigned inod=0;inod<num_nod;inod++)
   {
    // Get pointer to node
    Node* nod_pt=Lower_fluid_mesh_pt->boundary_node_pt(ibound,inod);
    
    // Pin radial velocity (parallel in/outflow)
    if (CommandLineArgs::command_line_flag_has_been_set
        ("--pin_radial_veloc_at_in_and_outlet"))
     {
      nod_pt->pin(0);
     }
   }
 }
 
 // Symmetry
 {
  unsigned ibound=Lower_fluid_symmetry_boundary_id;
  unsigned num_nod=Lower_fluid_mesh_pt->nboundary_node(ibound);
  for(unsigned inod=0;inod<num_nod;inod++)
   {
    // Get pointer to node
    Node* nod_pt=Lower_fluid_mesh_pt->boundary_node_pt(ibound,inod);
    
    // Pin radial velocity 
    nod_pt->pin(0);
   }
 }
 
 
 // ----------------------------------------------------------------------------

 // Pin relevant Lagrange multipliers in the
 // upper fluid <-> FSI surface meshes
  
 {
  Vector<Mesh*> fsi_mesh_pt;
  fsi_mesh_pt.push_back(Upper_fluid_dura_FSI_surface_mesh_pt);
  fsi_mesh_pt.push_back(Upper_fluid_block_FSI_surface_mesh_pt);
  unsigned nm=fsi_mesh_pt.size();
  for (unsigned m=0;m<nm;m++)
   {
    // Loop over the elements
    unsigned nel=fsi_mesh_pt[m]->nelement(); 
    for(unsigned e=0;e<nel;e++)
     {
      // Get a face element
      LinearisedAxisymPoroelasticBJS_FSIElement
       <FLUID_ELEMENT,PORO_ELEMENT>*
       traction_element_pt=
       dynamic_cast<LinearisedAxisymPoroelasticBJS_FSIElement
       <FLUID_ELEMENT,PORO_ELEMENT>*>
       (fsi_mesh_pt[m]->element_pt(e));
      
      // Get the number of nodes in element e
      unsigned n_node=traction_element_pt->nnode();
      
      // Loop over the nodes
      for(unsigned n=0;n<n_node;n++)
       {
        // Get a node
        Node *nod_pt=traction_element_pt->node_pt(n);
        
        // Cast to a boundary node
        BoundaryNodeBase *bnod_pt=dynamic_cast<BoundaryNodeBase*>(nod_pt);
        
        // Get the index of the first nodal value associated with this
        // FaceElement
        unsigned first_index=
         bnod_pt->index_of_first_value_assigned_by_face_element(
          Lagrange_id_upper_fluid_solid);
        
        // Pin swirl component of Lagrange multiplier
        nod_pt->pin(first_index+2);
        
        // Pin radial component if on the symmetry boundary
        if(bnod_pt->is_on_boundary(Upper_fluid_symmetry_boundary_id))
         {       
          nod_pt->pin(first_index+0);
         }

        // If radial velocity is pinned at inlet, pin associated 
        // Lagrange multiplier too
        if (CommandLineArgs::command_line_flag_has_been_set
            ("--pin_radial_veloc_at_in_and_outlet"))
         {
          if(bnod_pt->is_on_boundary(Upper_fluid_inflow_boundary_id))
           {       
            nod_pt->pin(first_index+0);
           }
         }

       } //end_of_loop_over_nodes
     } //end_of_loop_over_elements
   }
 }

 // Pin relevant Lagrange multipliers in the
 // lower fluid <-> FSI surface meshes
 
 {
  Vector<Mesh*> fsi_mesh_pt;
  fsi_mesh_pt.push_back(Lower_fluid_dura_FSI_surface_mesh_pt);
  fsi_mesh_pt.push_back(Lower_fluid_block_FSI_surface_mesh_pt);
  unsigned nm=fsi_mesh_pt.size();
  for (unsigned m=0;m<nm;m++)
   {
    // Loop over the elements
    unsigned nel=fsi_mesh_pt[m]->nelement(); 
    for(unsigned e=0;e<nel;e++)
     {
      // Get a face element
      LinearisedAxisymPoroelasticBJS_FSIElement
       <FLUID_ELEMENT,PORO_ELEMENT>*
       traction_element_pt=
       dynamic_cast<LinearisedAxisymPoroelasticBJS_FSIElement
       <FLUID_ELEMENT,PORO_ELEMENT>*>
       (fsi_mesh_pt[m]->element_pt(e));
      
      // Get the number of nodes in element e
      unsigned n_node=traction_element_pt->nnode();
      
      // Loop over the nodes
      for(unsigned n=0;n<n_node;n++)
       {
        // Get a node
        Node *nod_pt=traction_element_pt->node_pt(n);
        
        // Cast to a boundary node
        BoundaryNodeBase *bnod_pt=dynamic_cast<BoundaryNodeBase*>(nod_pt);
        
        // Get the index of the first nodal value associated with this
        // FaceElement
        unsigned first_index=
         bnod_pt->index_of_first_value_assigned_by_face_element(
          Lagrange_id_lower_fluid_solid);
        
        // Pin swirl component of Lagrange multiplier
        nod_pt->pin(first_index+2);
        
        // Pin radial component if on the symmetry boundary
        if(bnod_pt->is_on_boundary(Lower_fluid_symmetry_boundary_id))
         {       
          nod_pt->pin(first_index+0);
         }


        // If radial velocity is pinned at outlet, pin associated 
        // Lagrange multiplier too
        if (CommandLineArgs::command_line_flag_has_been_set
            ("--pin_radial_veloc_at_in_and_outlet"))
         {
          if(bnod_pt->is_on_boundary(Lower_fluid_outflow_boundary_id))
           {       
            nod_pt->pin(first_index+0);
           }
         }

       } //end_of_loop_over_nodes
     } //end_of_loop_over_elements
   }
 }


 //----------------------------------------------------------------------
  
 // Use non-stress-divergence form?
 if (CommandLineArgs::command_line_flag_has_been_set
     ("--use_non_stress_divergence_form_for_navier_stokes"))
  {
   oomph_info << "\nUsing non-stress divergence form of viscous terms\n";
   FLUID_ELEMENT::Gamma[0] = 0.0; //r-momentum
   FLUID_ELEMENT::Gamma[1] = 0.0; //z-momentum
  }
 else
  {
   oomph_info << "\nUsing stress divergence form of viscous terms\n";
  }

 // ----------------------------------------------------------------------------

 // Setup FSI
 setup_fsi();

 // Add the poro submeshes to the problem
 add_sub_mesh(Solid_mesh_pt);


 // // Add the fluid submeshes to the problem
 add_sub_mesh(Upper_fluid_mesh_pt);
 add_sub_mesh(Lower_fluid_mesh_pt);

 // Add the fluid inflow traction mesh to the problem
 add_sub_mesh(Inflow_fluid_surface_mesh_pt);


 // Add fsi sub-meshes
 add_sub_mesh(Solid_upper_fluid_dura_FSI_surface_mesh_pt);
 add_sub_mesh(Solid_upper_fluid_block_FSI_surface_mesh_pt);
 add_sub_mesh(Upper_fluid_dura_FSI_surface_mesh_pt);
 add_sub_mesh(Upper_fluid_block_FSI_surface_mesh_pt);

 add_sub_mesh(Solid_lower_fluid_dura_FSI_surface_mesh_pt);
 add_sub_mesh(Solid_lower_fluid_block_FSI_surface_mesh_pt);
 add_sub_mesh(Lower_fluid_dura_FSI_surface_mesh_pt);
 add_sub_mesh(Lower_fluid_block_FSI_surface_mesh_pt);

 // Now build the global mesh
 build_global_mesh();
 
 // Assign equation numbers
 oomph_info << assign_eqn_numbers() << " equations assigned" << std::endl;
 
} // end_of_constructor


//===start_of_create_poro_face_elements==============================
/// Make poro face elements
//===================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void PorousBlockProblem<PORO_ELEMENT, FLUID_ELEMENT>::
create_poro_face_elements()
{


 // Solid mesh near inlet exposed to upper fluid
 // --------------------------------------------
 unsigned b = Solid_fluid_interface_near_inlet_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 unsigned nel = Solid_mesh_pt->nboundary_element(b);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that applies fluid traction to poroelastic
   // solid
   FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>* face_element_pt=
    new FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>
    (Solid_mesh_pt->boundary_element_pt(b,e),
     Solid_mesh_pt->face_index_at_boundary(b,e));
   
   // Add to mesh
   Solid_upper_fluid_dura_FSI_surface_mesh_pt->add_element_pt(
    face_element_pt);
   
   // Set the FSI parameter
   face_element_pt->q_pt()=&Global_Physical_Variables::Q;
   
   // Associate element with bulk boundary (to allow it to access
   // the boundary coordinates in the bulk mesh)
   face_element_pt->set_boundary_number_in_bulk_mesh(b);
  }


 // Solid mesh -- block exposed to upper fluid
 // -------------------------------------------
 b = Solid_fluid_interface_upper_porous_block_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = Solid_mesh_pt->nboundary_element(b);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that applies fluid traction to poroelastic
   // solid
   FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>* face_element_pt=
    new FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>
    (Solid_mesh_pt->boundary_element_pt(b,e),
     Solid_mesh_pt->face_index_at_boundary(b,e));
   
   // Add to mesh
   Solid_upper_fluid_block_FSI_surface_mesh_pt->add_element_pt(
    face_element_pt);
   
   // Set the FSI parameter
   face_element_pt->q_pt()=&Global_Physical_Variables::Q;
   
   // Associate element with bulk boundary (to allow it to access
   // the boundary coordinates in the bulk mesh)
   face_element_pt->set_boundary_number_in_bulk_mesh(b);
  }
 


 // Solid mesh near outlet exposed to lower fluid
 // --------------------------------------------
 b = Solid_fluid_interface_near_outlet_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = Solid_mesh_pt->nboundary_element(b);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that applies fluid traction to poroelastic
   // solid
   FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>* face_element_pt=
    new FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>
    (Solid_mesh_pt->boundary_element_pt(b,e),
     Solid_mesh_pt->face_index_at_boundary(b,e));
   
   // Add to mesh
   Solid_lower_fluid_dura_FSI_surface_mesh_pt->add_element_pt(
    face_element_pt);
   
   // Set the FSI parameter
   face_element_pt->q_pt()=&Global_Physical_Variables::Q;
   
   // Associate element with bulk boundary (to allow it to access
   // the boundary coordinates in the bulk mesh)
   face_element_pt->set_boundary_number_in_bulk_mesh(b);
  }


 // Solid mesh -- block exposed to lower fluid
 // -------------------------------------------
 b = Solid_fluid_interface_lower_porous_block_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = Solid_mesh_pt->nboundary_element(b);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that applies fluid traction to poroelastic
   // solid
   FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>* face_element_pt=
    new FSILinearisedAxisymPoroelasticTractionElement
    <PORO_ELEMENT,FLUID_ELEMENT>
    (Solid_mesh_pt->boundary_element_pt(b,e),
     Solid_mesh_pt->face_index_at_boundary(b,e));
   
   // Add to mesh
   Solid_lower_fluid_block_FSI_surface_mesh_pt->add_element_pt(
    face_element_pt);
   
   // Set the FSI parameter
   face_element_pt->q_pt()=&Global_Physical_Variables::Q;
   
   // Associate element with bulk boundary (to allow it to access
   // the boundary coordinates in the bulk mesh)
   face_element_pt->set_boundary_number_in_bulk_mesh(b);
  }
 

} // end_of_create_poro_face_elements



//==start_of_create_fluid_face_elements===============================
/// Create the fluid traction elements
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void PorousBlockProblem<PORO_ELEMENT, FLUID_ELEMENT>::
create_fluid_face_elements()
{

 // Inflow:
 //--------
 unsigned bound=Upper_fluid_inflow_boundary_id;

 // Now loop over bulk elements and create the face elements
 unsigned nel=Upper_fluid_mesh_pt->nboundary_element(bound);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that applies pressure boundary condition
   // at inflow
   AxisymmetricNavierStokesTractionElement<FLUID_ELEMENT>* traction_element_pt
    = new AxisymmetricNavierStokesTractionElement<FLUID_ELEMENT>
    (Upper_fluid_mesh_pt->boundary_element_pt(bound,e),
     Upper_fluid_mesh_pt->face_index_at_boundary(bound,e));

   // Add to mesh
   Inflow_fluid_surface_mesh_pt->add_element_pt(traction_element_pt);

   // Set the applied traction
   traction_element_pt->traction_fct_pt() =
    &Global_Physical_Variables::fluid_inflow_boundary_traction;
  }


 // Upper fluid mesh: dura (from solid mesh)
 // ----------------------------------------
 bound=Upper_fluid_dura_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = Upper_fluid_mesh_pt->nboundary_element(bound);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that imposes Beavers Joseph Saffman condition at
   // FSI interface
   LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>*
    traction_element_pt=new
    LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>
    (Upper_fluid_mesh_pt->boundary_element_pt(bound,e),
     Upper_fluid_mesh_pt->face_index_at_boundary(bound,e),
     Lagrange_id_upper_fluid_solid);
   
   // Add to mesh
   Upper_fluid_dura_FSI_surface_mesh_pt->add_element_pt(
    traction_element_pt);
   
   // Associate element with bulk boundary (to allow it to access the boundary
   // coordinates in the bulk mesh)
   traction_element_pt->set_boundary_number_in_bulk_mesh(bound);
   
   // Set Strouhal number
   traction_element_pt->st_pt()=&Global_Physical_Variables::St;
   
   // Set inverse slip coefficient
   traction_element_pt->inverse_slip_rate_coefficient_pt()=
    &Global_Physical_Variables::Inverse_slip_rate_coefficient;
  }




 // Lower fluid mesh: dura (from solid mesh)
 // ----------------------------------------
 bound=Lower_fluid_dura_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = Lower_fluid_mesh_pt->nboundary_element(bound);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that imposes Beavers Joseph Saffman condition at
   // FSI interface
   LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>*
    traction_element_pt=new
    LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>
    (Lower_fluid_mesh_pt->boundary_element_pt(bound,e),
     Lower_fluid_mesh_pt->face_index_at_boundary(bound,e),
     Lagrange_id_lower_fluid_solid);
   
   // Add to mesh
   Lower_fluid_dura_FSI_surface_mesh_pt->add_element_pt(
    traction_element_pt);
   
   // Associate element with bulk boundary (to allow it to access the boundary
   // coordinates in the bulk mesh)
   traction_element_pt->set_boundary_number_in_bulk_mesh(bound);
   
   // Set Strouhal number
   traction_element_pt->st_pt()=&Global_Physical_Variables::St;
   
   // Set inverse slip coefficient
   traction_element_pt->inverse_slip_rate_coefficient_pt()=
    &Global_Physical_Variables::Inverse_slip_rate_coefficient;
  }


 // Upper fluid mesh: block (from solid mesh)
 // -----------------------------------------
 bound=Upper_fluid_block_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = Upper_fluid_mesh_pt->nboundary_element(bound);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that imposes Beavers Joseph Saffman condition at
   // FSI interface
   LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>*
    traction_element_pt=new
    LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>
    (Upper_fluid_mesh_pt->boundary_element_pt(bound,e),
     Upper_fluid_mesh_pt->face_index_at_boundary(bound,e),
     Lagrange_id_upper_fluid_solid);
   
   // Add to mesh
   Upper_fluid_block_FSI_surface_mesh_pt->add_element_pt(
    traction_element_pt);
   
   // Associate element with bulk boundary (to allow it to access the boundary
   // coordinates in the bulk mesh)
   traction_element_pt->set_boundary_number_in_bulk_mesh(bound);
   
   // Set Strouhal number
   traction_element_pt->st_pt()=&Global_Physical_Variables::St;
   
   // Set inverse slip coefficient
   traction_element_pt->inverse_slip_rate_coefficient_pt()=
    &Global_Physical_Variables::Inverse_slip_rate_coefficient;
  }




 // Lower fluid mesh: dura (from solid mesh)
 // ----------------------------------------
 bound=Lower_fluid_dura_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = Lower_fluid_mesh_pt->nboundary_element(bound);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that imposes Beavers Joseph Saffman condition at
   // FSI interface
   LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>*
    traction_element_pt=new
    LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>
    (Lower_fluid_mesh_pt->boundary_element_pt(bound,e),
     Lower_fluid_mesh_pt->face_index_at_boundary(bound,e),
     Lagrange_id_lower_fluid_solid);
   
   // Add to mesh
   Lower_fluid_dura_FSI_surface_mesh_pt->add_element_pt(
    traction_element_pt);
   
   // Associate element with bulk boundary (to allow it to access the boundary
   // coordinates in the bulk mesh)
   traction_element_pt->set_boundary_number_in_bulk_mesh(bound);
   
   // Set Strouhal number
   traction_element_pt->st_pt()=&Global_Physical_Variables::St;
   
   // Set inverse slip coefficient
   traction_element_pt->inverse_slip_rate_coefficient_pt()=
    &Global_Physical_Variables::Inverse_slip_rate_coefficient;
  }

 // Lower fluid mesh: block (from solid mesh)
 // -----------------------------------------
 bound=Lower_fluid_block_boundary_id;
 
 // Now loop over the bulk elements and create the face elements
 nel = Lower_fluid_mesh_pt->nboundary_element(bound);
 for(unsigned e=0;e<nel;e++)
  {
   // Create the face element that imposes Beavers Joseph Saffman condition at
   // FSI interface
   LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>*
    traction_element_pt=new
    LinearisedAxisymPoroelasticBJS_FSIElement
    <FLUID_ELEMENT,PORO_ELEMENT>
    (Lower_fluid_mesh_pt->boundary_element_pt(bound,e),
     Lower_fluid_mesh_pt->face_index_at_boundary(bound,e),
     Lagrange_id_lower_fluid_solid);
   
   // Add to mesh
   Lower_fluid_block_FSI_surface_mesh_pt->add_element_pt(
    traction_element_pt);
   
   // Associate element with bulk boundary (to allow it to access the boundary
   // coordinates in the bulk mesh)
   traction_element_pt->set_boundary_number_in_bulk_mesh(bound);
   
   // Set Strouhal number
   traction_element_pt->st_pt()=&Global_Physical_Variables::St;
   
   // Set inverse slip coefficient
   traction_element_pt->inverse_slip_rate_coefficient_pt()=
    &Global_Physical_Variables::Inverse_slip_rate_coefficient;
  }


} // end_of_create_fluid_face_elements

//==start_of_setup_fsi====================================================
/// Setup interaction between the poroelasticity and fluid meshes
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void PorousBlockProblem<PORO_ELEMENT, FLUID_ELEMENT>::
setup_fsi()
{

 // Solid <-> Upper fluid coupling
 // ------------------------------

 // Setup upper fluid traction on dura wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   Upper_fluid_dura_boundary_id,
   Upper_fluid_mesh_pt,
   Solid_upper_fluid_dura_FSI_surface_mesh_pt);

 // Setup BJS bc for upper fluid from dura wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Solid_fluid_interface_near_inlet_boundary_id,
   Solid_mesh_pt,
   Upper_fluid_dura_FSI_surface_mesh_pt);

 // Setup upper fluid traction on block wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   Upper_fluid_block_boundary_id,
   Upper_fluid_mesh_pt,
   Solid_upper_fluid_block_FSI_surface_mesh_pt);

 // Setup BJS bc for upper fluid from block wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Solid_fluid_interface_upper_porous_block_boundary_id,
   Solid_mesh_pt,
   Upper_fluid_block_FSI_surface_mesh_pt);




 // Solid <-> Lower fluid coupling
 // ------------------------------

 // Setup lower fluid traction on dura wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   Lower_fluid_dura_boundary_id,
   Lower_fluid_mesh_pt,
   Solid_lower_fluid_dura_FSI_surface_mesh_pt);

 // Setup BJS bc for lower fluid from dura wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Solid_fluid_interface_near_outlet_boundary_id,
   Solid_mesh_pt,
   Lower_fluid_dura_FSI_surface_mesh_pt);

 // Setup lower fluid traction on block wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <FLUID_ELEMENT,2>
  (this,
   Lower_fluid_block_boundary_id,
   Lower_fluid_mesh_pt,
   Solid_lower_fluid_block_FSI_surface_mesh_pt);

 // Setup BJS bc for lower fluid from block wall elements
 Multi_domain_functions::setup_bulk_elements_adjacent_to_face_mesh
  <PORO_ELEMENT,2>
  (this,
   Solid_fluid_interface_lower_porous_block_boundary_id,
   Solid_mesh_pt,
   Lower_fluid_block_FSI_surface_mesh_pt);



}

//==start_of_doc_solution=================================================
/// Doc the solution
//========================================================================
template<class PORO_ELEMENT, class FLUID_ELEMENT>
void PorousBlockProblem<PORO_ELEMENT, FLUID_ELEMENT>::
doc_solution(DocInfo& doc_info, unsigned npts)
{
 ofstream some_file;
 char filename[1000];

 // Output results in Tecplot format
 bool output_in_tecplot_format=true;
 bool output_in_paraview_format=true;

 if(output_in_tecplot_format)
  {
   
   // Output solution on solid mesh
   sprintf(filename,"%s/soln-solid%i.dat",doc_info.directory().c_str(),
     doc_info.number());
   some_file.open(filename);
   Solid_mesh_pt->output(some_file,npts);
   some_file.close();

   // Output solution on Upper fluid mesh
   sprintf(filename,"%s/soln-upper-fluid%i.dat",doc_info.directory().c_str(),
     doc_info.number());
   some_file.open(filename);
   Upper_fluid_mesh_pt->output(some_file,npts);
   some_file.close();

   // Output solution on lower fluid mesh
   sprintf(filename,"%s/soln-lower-fluid%i.dat",doc_info.directory().c_str(),
     doc_info.number());
   some_file.open(filename);
   Lower_fluid_mesh_pt->output(some_file,npts);
   some_file.close();

  }


 /// Output solid stuff along line
 sprintf(filename,"%s/solid_line%i.dat",
         doc_info.directory().c_str(),
         doc_info.number());
 some_file.open(filename);
 Solid_mesh_visualiser_pt->output(some_file);
 some_file.close();


 /// Output fluid stuff along line
 sprintf(filename,"%s/fluid_line%i.dat",
         doc_info.directory().c_str(),
         doc_info.number());
 some_file.open(filename);
 Fluid_mesh_visualiser_pt->output(some_file);
 some_file.close();
 
 
 // Outer solid at regularly spaced points
 {
  sprintf(filename,"%s/regular_solid%i.dat",
          doc_info.directory().c_str(),
          doc_info.number());
  some_file.open(filename);
  Vector<double> x(2);
  Vector<double> s(2);
  unsigned n=Solid_regularly_spaced_plot_point.size();
  for (unsigned i=0;i<n;i++)
   {
    // Pointer to element
    PORO_ELEMENT* el_pt=Solid_regularly_spaced_plot_point[i].first;
    
    // Coordinates in it
    s=Solid_regularly_spaced_plot_point[i].second;
    
    // Get coords
    el_pt->interpolated_x(s,x);
    
    some_file << x[0] << " "
              << x[1] << " "
              << el_pt->interpolated_u(s,0) << " "
              << el_pt->interpolated_u(s,1) << " "
              << el_pt->interpolated_q(s,0) << " "
              << el_pt->interpolated_q(s,1) << " "
              << el_pt->interpolated_p(s) << " "
              << std::endl;
   }
  some_file.close();
 }




 // Upper Fluid at regularly spaced points
 {
  sprintf(filename,"%s/regular_upper_fluid%i.dat",
          doc_info.directory().c_str(),
          doc_info.number());
  some_file.open(filename);
  unsigned npts=Upper_fluid_regularly_spaced_plot_point.size();
  Vector<double> x(2);
  Vector<double> s(2);
  Vector<double> veloc(3);
  for (unsigned i=0;i<npts;i++)
   {
    // Pointer to element
    FLUID_ELEMENT* el_pt=Upper_fluid_regularly_spaced_plot_point[i].first;
    
    // Coordinates in it
    s=Upper_fluid_regularly_spaced_plot_point[i].second;
    
    // Get coords
    el_pt->interpolated_x(s,x);
    
    // Get velocity
    el_pt->interpolated_u_axi_nst(s,veloc);
    
    some_file << x[0] << " "
              << x[1] << " "
              << veloc[0] << " "
              << veloc[1] << " "
              << veloc[2] << " "
              << el_pt->interpolated_p_axi_nst(s) << " "
              << std::endl;
   }
  some_file.close();
 }


 // Lower Fluid at regularly spaced points
 {
  sprintf(filename,"%s/regular_lower_fluid%i.dat",
          doc_info.directory().c_str(),
          doc_info.number());
  some_file.open(filename);
  unsigned npts=Lower_fluid_regularly_spaced_plot_point.size();
  Vector<double> x(2);
  Vector<double> s(2);
  Vector<double> veloc(3);
  for (unsigned i=0;i<npts;i++)
   {
    // Pointer to element
    FLUID_ELEMENT* el_pt=Lower_fluid_regularly_spaced_plot_point[i].first;
    
    // Coordinates in it
    s=Lower_fluid_regularly_spaced_plot_point[i].second;
    
    // Get coords
    el_pt->interpolated_x(s,x);
    
    // Get velocity
    el_pt->interpolated_u_axi_nst(s,veloc);
    
    some_file << x[0] << " "
              << x[1] << " "
              << veloc[0] << " "
              << veloc[1] << " "
              << veloc[2] << " "
              << el_pt->interpolated_p_axi_nst(s) << " "
              << std::endl;
   }
  some_file.close();
 }
 


 // Doc FSI traction acting on poro-elastic materials:
 //---------------------------------------------------
 sprintf(filename,"%s/fsi_upper_solid%i.dat",
         doc_info.directory().c_str(),
         doc_info.number());
 some_file.open(filename);
 Solid_upper_fluid_dura_FSI_surface_mesh_pt->output(some_file);
 Solid_upper_fluid_block_FSI_surface_mesh_pt->output(some_file);
 some_file.close();

 sprintf(filename,"%s/fsi_lower_solid%i.dat",
         doc_info.directory().c_str(),
         doc_info.number());
 some_file.open(filename);
 Solid_lower_fluid_dura_FSI_surface_mesh_pt->output(some_file);
 Solid_lower_fluid_block_FSI_surface_mesh_pt->output(some_file);
 some_file.close();


 // Doc BJS boundary condition for fluid
 //-------------------------------------

 sprintf(filename,"%s/bjs_fsi_upper_fluid%i.dat",
         doc_info.directory().c_str(),
         doc_info.number());
 some_file.open(filename);
 Upper_fluid_dura_FSI_surface_mesh_pt->output(some_file);
 Upper_fluid_block_FSI_surface_mesh_pt->output(some_file);
 some_file.close();

 sprintf(filename,"%s/bjs_fsi_lower_fluid%i.dat",
         doc_info.directory().c_str(),
         doc_info.number());
 some_file.open(filename);
 Lower_fluid_dura_FSI_surface_mesh_pt->output(some_file);
 Lower_fluid_block_FSI_surface_mesh_pt->output(some_file);
 some_file.close();
 

 // Output results in Paraview format
 if(output_in_paraview_format)
  {
   // Output solution on outer poro mesh in Paraview format (with padded 0s)
   sprintf(filename,"%s/soln-solid%05i.vtu",doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   Solid_mesh_pt->output_paraview(some_file,npts);
   some_file.close();

   // Output solution on upper fluid mesh in Paraview format (with padded 0s)
   sprintf(filename,"%s/soln-upper-fluid%05i.vtu",doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   Upper_fluid_mesh_pt->output_paraview(some_file,npts);
   some_file.close();

   // Output solution on lower fluid mesh in Paraview format (with padded 0s)
   sprintf(filename,"%s/soln-lower-fluid%05i.vtu",doc_info.directory().c_str(),
           doc_info.number());
   some_file.open(filename);
   Lower_fluid_mesh_pt->output_paraview(some_file,npts);
   some_file.close();
  }

 double t=time_pt()->time();
 Trace_file 
  << t << " "
  << Global_Physical_Variables::Re*
  Global_Physical_Variables::time_dependent_magnitude_of_driving_pressure(t) 
  << " "; 
 
//hierher
 // // Compute porous flux over syrinx cover boundary
 // {
 //  double flux=0.0;
 //  unsigned nel=Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt->nelement();
 //  for (unsigned e=0;e<nel;e++)
 //   {
 //    FSILinearisedAxisymPoroelasticTractionElement
 //      <PORO_ELEMENT,FLUID_ELEMENT>* el_pt=
 //     dynamic_cast<FSILinearisedAxisymPoroelasticTractionElement
 //      <PORO_ELEMENT,FLUID_ELEMENT>*>(
 //       Inner_poro_central_syrinx_fluid_FSI_surface_mesh_pt->element_pt(e));
    
 //    flux+=el_pt->contribution_to_total_porous_flux();
 //   }
 //  Trace_file  << flux << " "; 
 //  oomph_info << "Porous flux over syrinx cover       : " << flux << std::endl;
 // }

 Trace_file << doc_info.number() << " " << std::endl;

 // Bump counter
 doc_info.number()++;

} // end_of_doc_solution



//===start_of_main======================================================
/// Driver code
//======================================================================
int main(int argc, char* argv[])
{

 feenableexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW);	

 // Store command line arguments
 CommandLineArgs::setup(argc,argv);

 // Define possible command line arguments and parse the ones that
 // were actually specified

 // Reynolds number
 CommandLineArgs::specify_command_line_flag(
   "--re",
   &Global_Physical_Variables::Re);

 // Multiplier for permeability
 CommandLineArgs::specify_command_line_flag(
   "--permeability_multiplier",
   &Global_Physical_Variables::Permeability_multiplier);

 // Permeability of dura relative to reference value
 CommandLineArgs::specify_command_line_flag(
   "--permeability_ratio_dura",
   &Global_Physical_Variables::Permeability_ratio_dura_and_pia);

 // Permeability of block relative to reference value
 CommandLineArgs::specify_command_line_flag(
   "--permeability_ratio_block",
   &Global_Physical_Variables::Permeability_ratio_filum_and_block);

 // Stiffness of dura relative to reference value
 CommandLineArgs::specify_command_line_flag(
   "--stiffness_ratio_dura",
   &Global_Physical_Variables::Stiffness_ratio_dura_and_pia);

 // Stiffness of block relative to reference value
 CommandLineArgs::specify_command_line_flag(
   "--stiffness_ratio_block",
   &Global_Physical_Variables::Stiffness_ratio_filum_and_block);

 // Pin darcy?
 CommandLineArgs::specify_command_line_flag(
   "--pin_darcy");

 // Leave everything porous (rather than the default which
 // is to only make the region above the syrinx poro-elastic
  CommandLineArgs::specify_command_line_flag
   ("--everything_is_porous");

 // No wall inertia
 CommandLineArgs::specify_command_line_flag(
   "--suppress_wall_inertia");

 // Start from rest?
 CommandLineArgs::specify_command_line_flag(
   "--steady_solve");

 // Target element area for fluid meshes
 CommandLineArgs::specify_command_line_flag(
   "--el_area_fluid",
   &Global_Physical_Variables::Element_area_fluid);

 // Target element area for solid mesh
 CommandLineArgs::specify_command_line_flag(
   "--el_area_solid",
   &Global_Physical_Variables::Element_area_solid);

 // Output directory
 CommandLineArgs::specify_command_line_flag(
   "--dir",
   &Global_Physical_Variables::Directory);

 // Timestep
 double dt=0.00001;
 CommandLineArgs::specify_command_line_flag(
   "--dt",&dt);

 // Timestep
 double dt_in_seconds=0.0;
 CommandLineArgs::specify_command_line_flag(
   "--dt_in_seconds",&dt_in_seconds);

 // Number of timesteps to perform
 unsigned nstep=1000;
 CommandLineArgs::specify_command_line_flag(
   "--nstep",
   &nstep);

 // Validation?
 CommandLineArgs::specify_command_line_flag(
   "--validation");

 // Pin radial displacement at "inlet/outlet"
 CommandLineArgs::specify_command_line_flag("--radially_fixed_poro");

 // Pin radial velocity at inlet
 CommandLineArgs::specify_command_line_flag
  ("--pin_radial_veloc_at_in_and_outlet");

 // Use non-stress divergence form ?
 CommandLineArgs::specify_command_line_flag
  ("--use_non_stress_divergence_form_for_navier_stokes");

 // Parse command line
 CommandLineArgs::parse_and_assign();

 // Doc what has actually been specified on the command line
 CommandLineArgs::doc_specified_flags();

 // Set up doc info
 DocInfo doc_info;

 // Set output directory
 doc_info.set_directory(Global_Physical_Variables::Directory);

 // Create problem
 PorousBlockProblem<TAxisymmetricPoroelasticityElement<1>,
                          AxisymmetricTTaylorHoodElement> problem;

 // Update dependent problem parameters
 Global_Physical_Variables::update_dependent_parameters();

 // Doc dependent parameters
 Global_Physical_Variables::doc_dependent_parameters();


 if (CommandLineArgs::command_line_flag_has_been_set
     ("--pin_radial_veloc_at_in_and_outlet"))
  {
   oomph_info << "Pinning radially velocity at in/outlet.\n";
  }
 else
  {
   oomph_info << "NOT pinning radially velocity at in/outlet.\n";
  }

 if (CommandLineArgs::command_line_flag_has_been_set("--dt")&&
     CommandLineArgs::command_line_flag_has_been_set("--dt_in_seconds"))
  {
   throw OomphLibError(
    "Cannot set both --dt and --dt_in_seconds",
    OOMPH_CURRENT_FUNCTION,
    OOMPH_EXCEPTION_LOCATION);
  }


 if (CommandLineArgs::command_line_flag_has_been_set("--dt_in_seconds"))
  {
   dt=dt_in_seconds*Global_Physical_Variables::Re* 
    Global_Physical_Variables::T_factor_in_inverse_seconds;
   oomph_info << "Setting non-dim dt = " << dt 
              << " from Re and T factor. " << std::endl;
  }
 else
  {
   oomph_info << "Using dt=" << dt << std::endl;
  }


 // Sanity check
 if (CommandLineArgs::command_line_flag_has_been_set("--pin_darcy")&&
     CommandLineArgs::command_line_flag_has_been_set("--everything_is_porous"))
  {
   throw OomphLibError(
    "Cannot set both --pin_darcy and --everything_is_porous",
    OOMPH_CURRENT_FUNCTION,
    OOMPH_EXCEPTION_LOCATION);
  }



 // Set the initial time to t=0
 problem.time_pt()->time()=0.0;

 // Set up impulsive start from rest (which also initialises the timestep)
 problem.assign_initial_values_impulsive(dt);

 // Do only 3 steps if validating
 if(CommandLineArgs::command_line_flag_has_been_set("--validation"))
  {
   nstep=3;
  }

 // Check if we are doing a steady solve only
 if(CommandLineArgs::command_line_flag_has_been_set(
     "--steady_solve"))
  {
   oomph_info << "Doing steady solve only\n";
   
   // Do a steady solve
   problem.steady_newton_solve();
   
   // Output solution
   problem.doc_solution(doc_info);
   exit(0);
  }


 // Doc the initial conditions
 problem.doc_solution(doc_info);

 // Timestep
 oomph_info << "About to do " << nstep << " timesteps with dt = "
            << dt << std::endl;
 // Do the timestepping
 for(unsigned istep=0;istep<nstep;istep++)
  {
   oomph_info << "Solving at time " << problem.time_pt()->time()+dt
              << std::endl;

   // Solve for this timestep
   problem.unsteady_newton_solve(dt);

   // Doc the solution
   problem.doc_solution(doc_info);
  }

 return 0;
} // end_of_main

