#! /bin/bash

# Directory to store data
dir="NEW_RUNS_all_porous_ratios_half_and_two"

# Everything porous?
everything_is_porous_flag=" --everything_is_porous "

# Permeability of dura relative to reference value
#permeability_ratio_dura_flag=""
permeability_ratio_dura_flag=" --permeability_ratio_dura 0.5 "

# Permeability of block relative to reference value
#permeability_ratio_block_flag=""
permeability_ratio_block_flag=" --permeability_ratio_block 2.0"

#Stiffness of dura relative to reference value
#stiffness_ratio_dura_flag=""
stiffness_ratio_dura_flag=" --stiffness_ratio_dura 2.0 "

# Stiffness of block relative to reference value
#stiffness_ratio_block_flag=""
stiffness_ratio_block_flag=" --stiffness_ratio_block 0.5 "

# Flag for timestepping
dt_flag=" --dt_in_seconds 1.0 "

#Reynolds number (proxy for forcing pressure)
re=14140.0 # Corresponds to 500Pa

# Radially pinned (i.e. parallel) outflow
pin_radial_veloc_at_in_and_outlet_flag=" " 
#" --pin_radial_veloc_at_in_and_outlet " 


# Non-stress divergence form?
non_stress_divergence_form_flag=" --use_non_stress_divergence_form_for_navier_stokes " 


#Number of timesteps 
nstep=30

# Refinement
el_area_list="0.01 0.001 0.0001"

if [ -e "$dir" ];  then
    echo " "
    echo "ERROR: Please delete directory $dir and try again"
    echo " "
    exit
fi
mkdir "$dir"


# Rebuild executable
make porous_plug

# Move to working directory
cp porous_plug       "$dir"
cp porous_plug.cc    "$dir"
cp $0    "$dir"

cd "$dir"


for el_area in `echo $el_area_list`; do

    run_dir=RESLT_el_area`echo $el_area`
    time_flag=" $dt_flag --nstep $nstep "

    mkdir $run_dir
    ./porous_plug $time_flag --re $re --el_area_fluid $el_area  --el_area_solid $el_area  $non_stress_divergence_form_flag $pin_radial_veloc_at_in_and_outlet_flag $everything_is_porous_flag $permeability_ratio_dura_flag $permeability_ratio_block_flag $stiffness_ratio_dura_flag $stiffness_ratio_block_flag --dir $run_dir  > `echo $run_dir`/OUTPUT &

done

