#! /bin/bash

# Script to determine solid velocities in porous plug problem
# from solid displacements (in solid output files) and finite
# differencing.

# Timestep
dt=35.35

solid_file_list=`ls soln-solid?.dat`
solid_file_list=`echo "$solid_file_list "``ls soln-solid??.dat`

#nfile=`echo $solid_file_list | wc -w`
#echo $nfile


first_one=1
prev_file=""
count=1
for solid_file in `echo $solid_file_list`; do
    if [ $first_one -eq 1 ]; then
        first_one=0
        prev_file=$solid_file
    else
        current_file=$solid_file
        echo "fd-ing $current_file and $prev_file"

        awk '{if (NF==8){print $0}else{print "# "$0}}' $current_file > .current
        awk '{if (NF==8){print $0}else{print "@ "$0}}' $prev_file    > .previous
        paste .current .previous > .both
        awk -v dt=$dt '{if ($1!="#"){print $1 " " $2 " " ($3-$11)/dt " " ($4-$12)/dt }else{for (i=1;i<=NF;i++){if ($i!="#"){if($i=="@"){break} printf("%s ",$i)}}print " "}}' .both > solid_veloc`echo $count`.dat

        let count=$count+1

        prev_file=$solid_file
    fi
done

rm .current
rm .previous 
rm .both

oomph-convert -z solid_veloc*.dat
makePvd solid_veloc solid_veloc.pvd
