makePvd soln-solid soln-solid.pvd
makePvd soln-upper-fluid soln-upper-fluid.pvd
makePvd soln-lower-fluid soln-lower-fluid.pvd

oomph-convert -z *region.dat
oomph-convert *mesh.dat

oomph-convert -z -p2 regular_upper_fluid*.dat
makePvd regular_upper_fluid regular_upper_fluid.pvd

oomph-convert -z -p2 regular_lower_fluid*.dat
makePvd regular_lower_fluid regular_lower_fluid.pvd

oomph-convert -z -p2 bjs_fsi_upper_fluid*.dat
makePvd bjs_fsi_upper_fluid bjs_fsi_upper_fluid.pvd

oomph-convert -z -p2 bjs_fsi_lower_fluid*.dat
makePvd bjs_fsi_lower_fluid bjs_fsi_lower_fluid.pvd

oomph-convert -z -p2 fsi_lower_solid*.dat
makePvd fsi_lower_solid fsi_lower_solid.pvd

oomph-convert -z -p2 fsi_upper_solid*.dat
makePvd fsi_upper_solid fsi_upper_solid.pvd

#oomph-convert -z -p2 bjs_fsi_lower_fluid*.dat
#makePvd bjs_fsi_lower_fluid bjs_fsi_lower_fluid.pvd

#oomph-convert -z -p2 bjs_fsi_upper_fluid*.dat
#makePvd bjs_fsi_upper_fluid bjs_fsi_upper_fluid.pvd

exit
